# Track My Loved #

Android application developed for "Mobile Applications" course @ Polimi A.Y. 2013-2014

### Authors ###

* Luca Bondi
* Michael Lovisa

## Compiled APK file ##
The compiled file *track-my-loved/bin/track-my-loved.apk* can be copied to the device and installed.

## Importing project into Eclipse ##
### Android SDK Manager prerequisites ###
* Tools
    + Android SDK Tools
    + Android SDK Platform-tools
    + Android SDK Build-tools (Rev. 20)
* Android 4.4W (Api 20)
    + SDK Platform

### Import project and libraries ###
* File > Import... > General > Existing Projects into Workspace > Next
* Select archive file > Browse *Bondi-code.zip* > Finish
* *android-support-v7-appcompat* is the Android support library
* *google-play-services_lib* is the Google Play services library

### Set Google API key ###
* From Eclipse *Preferences > Android > Build* copy the *SHA1 fingerprint* of the debug key
* Goto https://code.google.com/apis/console
* Create a new project
* Enable *Google Maps Android API v2*
* Apis & auth > Credentials > Create e new key > Android Key
* Paste the *SHA1 fingerprint* followed by *;me.noip.unklb.trackmyloved*
* Copy the *API KEY* into *track-my-loved/AndroidManifest.xml* replacing android:value in

```
#!android
<meta-data
    android:name="com.google.android.maps.v2.API_KEY"
    android:value="Insert here the API KEY " />
```