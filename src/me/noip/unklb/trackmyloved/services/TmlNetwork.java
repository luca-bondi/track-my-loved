package me.noip.unklb.trackmyloved.services;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import me.noip.unklb.trackmyloved.R;
import me.noip.unklb.trackmyloved.controllers.TmlAccount;
import me.noip.unklb.trackmyloved.controllers.TmlChild;
import me.noip.unklb.trackmyloved.controllers.TmlChild.TmlChildrenListener;
import me.noip.unklb.trackmyloved.controllers.TmlDatabase;
import me.noip.unklb.trackmyloved.controllers.TmlNotification;
import me.noip.unklb.trackmyloved.controllers.TmlPosition;
import me.noip.unklb.trackmyloved.controllers.TmlSafeZone;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.util.Xml;

import com.google.android.gms.maps.model.LatLng;

/**
 * Network communications background service
 * 
 * IntentService class used for all network communications. To start a
 * communication thread, startService(Intent) is used. Intent contains an action
 * from the ones specified in this class constants and optional extras useful to
 * perform the specified action. If the calling activity expects a result it
 * must implement a LocalBroadcastReceiver and register it with
 * LocalBroadcastManager.
 */
public class TmlNetwork extends IntentService {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	private static final String TAG = "TmlNetwork";

	/*
	 * URLs used in network communications
	 */
	private static final String SERVER_CHECK_URL = "https://unklb.noip.me/tml/";
	private static final String ACCOUNT_LOGIN_URL = "https://unklb.noip.me/tml/accountLogin.php";
	private static final String ACCOUNT_SIGNUP_URL = "https://unklb.noip.me/tml/accountSignup.php";
	private static final String USER_CREATE_URL = "https://unklb.noip.me/tml/userCreate.php";
	private static final String POSITION_SYNC_URL_CHILD = "https://unklb.noip.me/tml/childSync.php";
	private static final String POSITION_SYNC_URL_PARENT = "https://unklb.noip.me/tml/parentSync.php";
	private static final String NEW_SAFE_ZONE_URL = "https://unklb.noip.me/tml/newSafeZone.php";
	private static final String REMOVE_SAFE_ZONE_URL = "https://unklb.noip.me/tml/removeSafeZone.php";
	private static final String SYNC_SAFE_ZONE_URL = "https://unklb.noip.me/tml/syncSafeZone.php";

	/*
	 * XML tags used to parse/create xml structure
	 */
	private static final String XML_DELETED_TAG = "deleted";
	private static final String XML_LAST_TIMESTAMP_TAG = "lastTs";
	private static final String XML_CHILD_TAG = "watched";
	private static final String XML_USER_TAG = "user";
	private static final String XML_ERROR_TAG = "error";
	private static final String XML_SUCCESS_TAG = "success";
	private static final String XML_CREATED_TAG = "created";
	private static final String XML_REMOVED_TAG = "removed";
	private static final String XML_TML_TAG = "tml";
	private static final String XML_ACCOUNT_TAG = "account";
	private static final String XML_MAIL_TAG = "mail";
	private static final String XML_PWD_TAG = "pwd";
	private static final String XML_DEVICE_TAG = "device";
	private static final String XML_SAFE_ZONE_TAG = "safezone";
	private static final String XML_POINT_TAG = "point";
	private static final String XML_LATITUDE_TAG = "lat";
	private static final String XML_LONGITUDE_TAG = "lng";
	private static final String XML_SAFE_ZONE_SYNC_NECESSARY_TAG = "safezone_sync_required";
	private static final String XML_POSITION_TAG = "position";
	private static final String XML_TIMESTAMP_TAG = "ts";
	private static final String XML_ACCURACY_TAG = "acc";
	private static final String XML_BATTERY_TAG = "batt";
	private static final String XML_ACTIVITY_TAG = "act";

	/*
	 * XML text used to parse xml content
	 */
	public static final String XML_MAIL_ALREADY_REGISTERED_TEXT = "mail_already_registered";
	public static final String XML_ACCOUNT_REGISTRATION_ERROR_TEXT = "account_not_created";
	public static final String XML_ACCOUNT_REGISTRATION_SUCCESSFUL_TEXT = "account_created";

	public static final String XML_USER_CREATION_ERROR_TEXT = "user_not_created";
	public static final String XML_USER_EXISTS_TEXT = "user_already_exists";
	public static final String XML_USER_CREATION_SUCCESSFUL_TEXT = "user_created";

	public static final String XML_USER_DELETE_SUCCESSFULL_TEXT = "user_deleted";
	/**
	 * File path to the CA certificate that signed the server SSL certificate
	 */
	private static final String CA_CERT = "unklb_ca_cert.crt";

	/*
	 * Intent actions used when calling startService(Intent) to start this
	 * service. These actions are also used when returning values to the calling
	 * activity (if needed), so these can be used to configure broadcast
	 * receiver intent filter
	 */
	public static final String INTENT_ACTION_CHECK_SERVER = "INTENT_ACTION_CHECK_SERVER";
	public static final String INTENT_ACTION_SYNC_POSITIONS = "INTENT_ACTION_SYNC_POSITIONS";
	public static final String INTENT_ACTION_ACCOUNT_LOGIN = "INTENT_ACTION_ACCOUNT_LOGIN";
	public static final String INTENT_ACTION_ACCOUNT_SIGNUP = "INTENT_ACTION_ACCOUNT_SIGNUP";
	public static final String INTENT_ACTION_USER_CREATE = "INTENT_ACTION_USER_CREATE";

	/*
	 * Intent extra keys used when calling startService(Intent) or when results
	 * are returned with sendBroadcast. Also used to return values in intent
	 * extra
	 */
	public static final String INTENT_EXTRA_KEY_ACCOUNT = "INTENT_EXTRA_KEY_ACCOUNT";
	public static final String INTENT_EXTRA_KEY_USER = "INTENT_EXTRA_KEY_USER";
	public static final String INTENT_EXTRA_KEY_RESULT = "INTENT_EXTRA_KEY_RESULT";
	public static final String INTENT_EXTRA_KEY_ERROR = "INTENT_EXTRA_KEY_ERROR";

	public static final String INTENT_EXTRA_ERROR_SERVER_ERROR = "INTENT_EXTRA_ERROR_SERVER_ERROR";
	public static final String INTENT_EXTRA_ERROR_SERVER_UNREACHABLE = "INTENT_EXTRA_ERROR_SERVER_UNREACHABLE";

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- CLASSES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Class used to handle connection url and xml data to be sent.
	 */
	private class CommunicationParameters {
		private String url;
		private String outputXml;

		public CommunicationParameters(String url, String outputXml) {
			setUrl(url);
			setOutputXml(outputXml);
		}

		public String getUrl() {
			return url;
		}

		private void setUrl(String url) {
			this.url = url;
		}

		public String getOutputXml() {
			return outputXml;
		}

		private void setOutputXml(String outputXml) {
			this.outputXml = outputXml;
		}

	}

	/*
	 * ------------------------------------------------------------------------
	 * ---------------------------- LISTENERS ---------------------------------
	 * ------------------------------------------------------------------------
	 */

	private static TmlChildrenListener childrenListener = new TmlChildrenListener() {

		@Override
		public void onAdd(TmlChild child) {
			childrenList.add(child);
		}

		@Override
		public void onRemove(TmlChild child) {
		}

		@Override
		public void onUpdate(TmlChild child) {
		}

	};

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- ATTRIBUTES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Children to sync when performing positions and preference synchronization
	 */
	private static ArrayList<TmlChild> childrenList;

	/*
	 * SSL related attributes
	 */
	private Certificate caCertificate;
	private SSLContext sslContext;

	/**
	 * Set when this service is running
	 */
	public static boolean running = false;

	/**
	 * Counter of intent received and not yet completed
	 */
	public static int intentCounter = 0;

	/**
	 * Notification manager instance
	 */
	private static TmlNotification notificationManager;

	/**
	 * Database instance. Needed in the account setup phase
	 */
	private static TmlDatabase db;

	/**
	 * Flag that indicates if the running position synchronization is complete.
	 * Don't declare as static!
	 */
	private boolean syncPositionsCompleted = false;

	/**
	 * Safe zone sync is necessary
	 */
	private boolean safeZoneSyncNecessary;

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- METHODS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Must call this when resetting the device
	 * 
	 * @return
	 */
	public static void resetDevice() {
		childrenList = null;
	}

	/**
	 * Internet connection check
	 * 
	 * @param context
	 * @return true if connection available, false otherwise
	 */
	public static boolean checkConnection(Context context) {
		ConnectivityManager connMgr = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		return networkInfo != null && networkInfo.isConnected();
	}

	/**
	 * Instantiate the children list
	 * 
	 * @param context
	 * @return
	 */
	private static boolean initChildrenList(Context context) {

		/*
		 * Instantiate the children list
		 */
		if (childrenList == null) {
			childrenList = TmlChild.getChildrenList(context, childrenListener);

			if (TmlChild.isParentDevice()) {
				notificationManager = TmlNotification.getInstance(context);
			}

			if (childrenList == null || childrenList.size() == 0) {
				return false;
			}
		}

		return true;

	}

	/**
	 * Check server availability
	 * 
	 * @return
	 * 
	 * @throws ConnectException
	 *             when server internal error
	 * @throws IOException
	 *             when server not reachable
	 */
	private boolean checkServer() throws ConnectException, IOException {
		if (!checkConnection(this)) {
			return false;
		}
		serverCommunication(new TmlNetwork.CommunicationParameters(
				SERVER_CHECK_URL, "<tml></tml>"));
		return true;
	}

	/*
	 * --------------------- POSITIONS SYNC METHODS ----------------------------
	 */

	/**
	 * Positions synchronization with remote server.
	 * 
	 * @throws ConnectException
	 *             when server internal error
	 * @throws IOException
	 *             when server not reachable
	 */
	private synchronized void syncPositions() throws ConnectException,
			IOException {

		/*
		 * Check Internet connection availability
		 */
		if (!checkConnection(this)) {
			throw new IOException("Network connection unavailable");
		}

		/*
		 * Check if initial synchronization is required
		 */
		if (TmlChild.getDb().isInitialSyncRequired()) {
			/*
			 * Rise notification when performing initial synchronization
			 */
			startForeground(TmlNotification.NOTIFICATION_ID_INITIAL_SYNC,
					TmlNotification.buildNotification(this,
							R.drawable.ic_stat_notify_sync,
							R.drawable.notif_large_icon,
							getString(R.string.notification_initSync_title),
							getString(R.string.notification_initSync_text)));
			/*
			 * Call sync method up to complete sync. syncPositionsCompleted is
			 * set to false when starting syncPositionsTask(). If at the end of
			 * the task the last local and remote timestamp are the same it is
			 * set to true
			 */
			while (!syncPositionsCompleted) {
				if (TmlServicesManager.areServicesStopping()) {
					return;
				}
				syncPositionsTask();
			}
			TmlChild.getDb().syncDone();
			/*
			 * Removing notification
			 */
			stopForeground(true);

			if (TmlChild.isChildDevice()) {
				/*
				 * Necessary to have UI thread notified of initial sync
				 * completed even if no positions inserted
				 */
				TmlChild.notifyUiThreadUpdate();
				/*
				 * Start tracking service at the end of initial synchronization
				 */
				Intent intent = new Intent(this, TmlTrack.class);
				startService(intent);
			}
		} else {
			/*
			 * Execute synchronization without rising notification. Usually this
			 * task takes a few seconds and notification is not necessary.
			 */
			while (!syncPositionsCompleted) {
				if (TmlServicesManager.areServicesStopping()) {
					return;
				}
				syncPositionsTask();
			}
		}

		if (syncPositionsCompleted) {
			/*
			 * Rise notifications if needed
			 */
			if (TmlChild.isParentDevice()) {
				notificationManager.riseNotifications();
			}

			/*
			 * Check for safe zones to be sent
			 */
			boolean sendNewSafeZone = false;
			boolean sendRemoveSafeZone = false;
			for (TmlChild child : childrenList) {
				for (TmlSafeZone safeZone : child.getSafeZoneList()) {
					if (safeZone.toBeCreated()) {
						sendNewSafeZone = true;
					}
					if (safeZone.toBeRemoved()) {
						sendRemoveSafeZone = true;
					}
				}
			}
			if (sendNewSafeZone) {
				newSafeZone();
			}
			if (sendRemoveSafeZone) {
				removeSafeZone();
			}
			if (safeZoneSyncNecessary) {
				syncSafeZone();
				safeZoneSyncNecessary = false;
			}
		}
	}

	/**
	 * Position sync task.
	 * 
	 * Performs all the communications, the parsing and the logic for positions
	 * synchronization
	 * 
	 * @throws ConnectException
	 *             when server internal error
	 * @throws IOException
	 *             when server not reachable
	 */
	private void syncPositionsTask() throws ConnectException, IOException {

		InputStream serverResponseStream;
		XmlPullParser xmlParser;

		/*
		 * Prepare last timestamp for each child and send it to the server
		 */
		String xmlString = xmlSendLastTimestamps();
		if (TmlChild.isParentDevice()) {
			serverResponseStream = serverCommunication(new TmlNetwork.CommunicationParameters(
					POSITION_SYNC_URL_PARENT, xmlString));
		} else {
			serverResponseStream = serverCommunication(new TmlNetwork.CommunicationParameters(
					POSITION_SYNC_URL_CHILD, xmlString));
		}

		/* Parse server response */
		xmlParser = getXmlPullParser(serverResponseStream);
		positionsXmlParser(xmlParser);
		closeInputStream(serverResponseStream);

		syncPositionsCompleted = isSyncPositionsCompleted();

		/* If child send new positions to server */
		if (TmlChild.isChildDevice() && !syncPositionsCompleted) {

			xmlString = xmlSendNewPositions();
			serverResponseStream = serverCommunication(new TmlNetwork.CommunicationParameters(
					POSITION_SYNC_URL_CHILD, xmlString));
			xmlParser = getXmlPullParser(serverResponseStream);
			positionsXmlParser(xmlParser);
			syncPositionsCompleted = isSyncPositionsCompleted();

			closeInputStream(serverResponseStream);

		}

	}

	/**
	 * Checks if all children positions are in sync with the server
	 * 
	 * @return true if sync completed, false otherwise
	 */
	private boolean isSyncPositionsCompleted() {
		boolean syncPositionsCompleted = true;

		for (TmlChild child : childrenList) {
			syncPositionsCompleted &= child.serverInSync();
		}

		return syncPositionsCompleted;
	}

	/**
	 * Creates the XML documents for the first sync phase: device tells server
	 * the last timestamp for each child. If a child has been deleted send the
	 * delete tag
	 * 
	 * @return Xml document ready to be sent
	 */
	private String xmlSendLastTimestamps() {

		StringWriter xmlWriter = new StringWriter();
		XmlSerializer xmlSerializer = Xml.newSerializer();
		initXml(xmlSerializer, xmlWriter, null);
		try {
			for (TmlChild child : childrenList) {
				xmlSerializer.startTag("", XML_CHILD_TAG);
				xmlSerializer.attribute("", "name", child.getName());
				if (child.getDeleted()) {
					xmlSerializer.startTag("", XML_DELETED_TAG);
					xmlSerializer.endTag("", XML_DELETED_TAG);
				} else {
					xmlSerializer.startTag("", XML_LAST_TIMESTAMP_TAG);
					xmlSerializer.text(Long.toString(child
							.getLastLocalTimestamp()));
					xmlSerializer.endTag("", XML_LAST_TIMESTAMP_TAG);
				}
				xmlSerializer.endTag("", XML_CHILD_TAG);
			}
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "xmlSendLastTimestamps: IllegalArgumentException");
		} catch (IllegalStateException e) {
			Log.e(TAG, "xmlSendLastTimestamps: IllegalStateException");
		} catch (IOException e) {
			Log.e(TAG, "xmlSendLastTimestamps: IOException");
		}
		return endXml(xmlSerializer, xmlWriter);

	}

	/**
	 * Creates the XML documents for the second sync phase: device sends the
	 * server the last positions, based on previously received server last
	 * timestamp for each child.
	 * 
	 * Used by child phone only
	 * 
	 * @return Xml document ready to be sent
	 */
	private String xmlSendNewPositions() {

		StringWriter xmlWriter = new StringWriter();
		XmlSerializer xmlSerializer = Xml.newSerializer();

		ArrayList<TmlPosition> newPositions = TmlChild.getDb()
				.getLastPositions(
						childrenList.get(0).getLastRemoteTimestamp() + 1);

		initXml(xmlSerializer, xmlWriter, null);
		try {
			xmlSerializer.startTag("", XML_CHILD_TAG);
			xmlSerializer.attribute("", "name", childrenList.get(0).getName());
			for (TmlPosition newPosition : newPositions) {
				positionXml(xmlSerializer, newPosition);
			}
			xmlSerializer.endTag("", XML_CHILD_TAG);
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "xmlSendPositions: IllegalArgumentException");
		} catch (IllegalStateException e) {
			Log.e(TAG, "xmlSendPositions: IllegalStateException");
		} catch (IOException e) {
			Log.e(TAG, "xmlSendPositions: IOException");
		}

		return endXml(xmlSerializer, xmlWriter);

	}

	/**
	 * Parse xml from server and retrieves new positions for childrenList,
	 * storing them in database through TmlChild class.
	 * 
	 * On a parent device if a new user is detected, it is added to the database
	 * and a broadcast intent is set to notify the fact.
	 * 
	 * 
	 * @param xmlParser
	 * @return true if new positions added to the database, false otherwise
	 */
	private void positionsXmlParser(XmlPullParser xmlParser) {

		ArrayList<TmlPosition> newPositionList = new ArrayList<TmlPosition>();

		TmlPosition receivedPosition = null;
		Long receivedTs = null;
		Double receivedLat = null;
		Double receivedLng = null;
		Float receivedAcc = null;
		Float receivedBatt = null;
		String receivedProvider = null;

		String actualUsername = null;
		Integer actualChildIdx = null;
		TmlChild actualChild = null;

		try {
			while (xmlParser.next() != XmlPullParser.END_DOCUMENT) {
				if (xmlParser.getEventType() == XmlPullParser.START_TAG) {
					if (xmlParser.getName().equals(XML_CHILD_TAG)) {
						actualUsername = xmlParser
								.getAttributeValue("", "name");
						newPositionList = new ArrayList<TmlPosition>();
						/* Search for a child with the same name */
						actualChildIdx = null;
						for (int idx = 0; idx < childrenList.size(); idx++) {
							if (childrenList.get(idx).getName()
									.equals(actualUsername)) {
								actualChildIdx = idx;
								break;
							}
						}
						if (actualChildIdx == null) {
							if (TmlChild.isParentDevice()) {
								/* Add new child into the application */
								actualChild = TmlChild.add(actualUsername);
							}
						} else {
							/* Actual child already in the database */
							actualChild = childrenList.get(actualChildIdx);
						}
					} else if (xmlParser.getName().equals(
							XML_LAST_TIMESTAMP_TAG)) {
						actualChild.setLastRemoteTimestamp(Long
								.parseLong(xmlParser.nextText()));
					} else if (xmlParser.getName().equals(XML_DELETED_TAG)) {
						if (!actualChild.getDeleted()) {
							/*
							 * Tell other application components that this user
							 * must be deleted.
							 */
							actualChild.delete();
						}
						childrenList.remove(actualChild);
					} else if (xmlParser.getName().equals(XML_POSITION_TAG)) {
						receivedTs = null;
						receivedLat = null;
						receivedLng = null;
						receivedAcc = null;
						receivedBatt = null;
						receivedProvider = null;
					} else if (xmlParser.getName().equals(XML_TIMESTAMP_TAG)) {
						receivedTs = Long.parseLong(xmlParser.nextText());
					} else if (xmlParser.getName().equals(XML_LATITUDE_TAG)) {
						receivedLat = Double.parseDouble(xmlParser.nextText());
					} else if (xmlParser.getName().equals(XML_LONGITUDE_TAG)) {
						receivedLng = Double.parseDouble(xmlParser.nextText());
					} else if (xmlParser.getName().equals(XML_ACCURACY_TAG)) {
						receivedAcc = Float.parseFloat(xmlParser.nextText());
					} else if (xmlParser.getName().equals(XML_BATTERY_TAG)) {
						receivedBatt = Float.parseFloat(xmlParser.nextText());
					} else if (xmlParser.getName().equals(XML_ACTIVITY_TAG)) {
						receivedProvider = xmlParser.nextText();
					} else if (xmlParser.getName().equals(
							XML_SAFE_ZONE_SYNC_NECESSARY_TAG)) {
						safeZoneSyncNecessary = true;
					}
				} else if (xmlParser.getEventType() == XmlPullParser.END_TAG) {
					if (xmlParser.getName().equals(XML_CHILD_TAG)) {
						if (newPositionList.size() > 0) {
							actualChild.insertPositionList(newPositionList);
						}
					} else if (xmlParser.getName().equals(XML_POSITION_TAG)) {
						receivedPosition = new TmlPosition(new LatLng(
								receivedLat, receivedLng), receivedTs,
								receivedAcc, receivedProvider, receivedBatt);
						newPositionList.add(receivedPosition);
					}
				}
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
			Log.e(TAG, "NullPointerException");
		} catch (NumberFormatException e) {
			Log.e(TAG, "NumberFormatException");
		} catch (XmlPullParserException e) {
			Log.e(TAG, "XmlPullParserException");
		} catch (IOException e) {
			Log.e(TAG, "IOException");
		}

		return;
	}

	/*
	 * ---------------------------- SAFEZONE METHODS ---------------------------
	 */

	/**
	 * Send new safe zones for all the children
	 * 
	 * @throws ConnectException
	 *             when server internal error
	 * @throws IOException
	 *             when server not reachable
	 */
	private void newSafeZone() throws ConnectException, IOException {

		InputStream serverResponseStream;
		XmlPullParser xmlParser;

		/*
		 * Prepare new safezones and send to the server
		 */
		String xmlString = xmlSendSafeZones(true, false);
		serverResponseStream = serverCommunication(new TmlNetwork.CommunicationParameters(
				NEW_SAFE_ZONE_URL, xmlString));

		/* Parse server response */
		xmlParser = getXmlPullParser(serverResponseStream);
		safeZoneXmlParser(xmlParser);
		closeInputStream(serverResponseStream);

	}

	/**
	 * Sends to server the cancel request of the safe zones for all the children
	 * 
	 * @throws ConnectException
	 *             when server internal error
	 * @throws IOException
	 *             when server not reachable
	 */
	private void removeSafeZone() throws ConnectException, IOException {

		InputStream serverResponseStream;
		XmlPullParser xmlParser;

		/*
		 * Prepare new safezones and send to the server
		 */
		String xmlString = xmlSendSafeZones(false, true);
		serverResponseStream = serverCommunication(new TmlNetwork.CommunicationParameters(
				REMOVE_SAFE_ZONE_URL, xmlString));

		/*
		 * If serverResponseStream == null something went wrong during
		 * serverCommunication
		 */
		if (serverResponseStream == null) {
			Log.w(TAG, "Communication error occurred");
			return;
		}

		/* Parse server response */
		xmlParser = getXmlPullParser(serverResponseStream);
		safeZoneXmlParser(xmlParser);
		closeInputStream(serverResponseStream);

	}

	/**
	 * Sync safe zones with the server
	 * 
	 * @throws ConnectException
	 *             when server internal error
	 * @throws IOException
	 *             when server not reachable
	 */
	private synchronized void syncSafeZone() throws ConnectException,
			IOException {

		InputStream serverResponseStream;
		XmlPullParser xmlParser;

		/*
		 * Send all safezones to the server
		 */
		String xmlString = xmlSendSafeZones(false, false);
		serverResponseStream = serverCommunication(new TmlNetwork.CommunicationParameters(
				SYNC_SAFE_ZONE_URL, xmlString));

		/*
		 * If serverResponseStream == null something went wrong during
		 * serverCommunication
		 */
		if (serverResponseStream == null) {
			Log.w(TAG, "Communication error occurred");
			return;
		}

		/* Parse server response */
		xmlParser = getXmlPullParser(serverResponseStream);
		safeZoneXmlParser(xmlParser);
		closeInputStream(serverResponseStream);

	}

	/**
	 * Creates the XML that contains safeZones for all children
	 * 
	 * @param sendOnlyNew
	 *            set to send only new safe zones
	 * @param sendOnlyCancel
	 *            set to send only canceled safe zones
	 * @return
	 */
	private String xmlSendSafeZones(boolean sendOnlyNew, boolean sendOnlyCancel) {

		if (sendOnlyNew && sendOnlyCancel) {
			Log.e(TAG, "Wrong usafe of xmlSendSafeZones");
			return null;
		}

		StringWriter xmlWriter = new StringWriter();
		XmlSerializer xmlSerializer = Xml.newSerializer();
		initXml(xmlSerializer, xmlWriter, null);
		try {
			for (TmlChild child : childrenList) {
				xmlSerializer.startTag("", XML_CHILD_TAG);
				xmlSerializer.attribute("", "name", child.getName());
				for (TmlSafeZone safeZone : child.getSafeZoneList()) {
					if (sendOnlyNew) {
						if (safeZone.toBeCreated()) {
							safeZoneXml(xmlSerializer, safeZone);
						}
					} else if (sendOnlyCancel) {
						if (safeZone.toBeRemoved()) {
							safeZoneXml(xmlSerializer, safeZone);
						}
					} else {
						safeZoneXml(xmlSerializer, safeZone);
					}
				}
				xmlSerializer.endTag("", XML_CHILD_TAG);
			}
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "xmlSendLastTimestamps: IllegalArgumentException");
		} catch (IllegalStateException e) {
			Log.e(TAG, "xmlSendLastTimestamps: IllegalStateException");
		} catch (IOException e) {
			Log.e(TAG, "xmlSendLastTimestamps: IOException");
		}
		return endXml(xmlSerializer, xmlWriter);

	}

	/**
	 * Add to the provided XmlSerializer the given safeZone
	 * 
	 * @param xmlSerializer
	 * @param safeZone
	 */
	private static void safeZoneXml(XmlSerializer xmlSerializer,
			TmlSafeZone safeZone) {

		try {
			xmlSerializer.startTag("", XML_SAFE_ZONE_TAG);
			xmlSerializer.attribute("", "serial",
					Integer.toString(safeZone.getSerial()));
			for (LatLng point : safeZone.getPointList()) {
				xmlSerializer.startTag("", XML_POINT_TAG);
				xmlSerializer.startTag("", XML_LATITUDE_TAG);
				xmlSerializer.text(Double.toString(point.latitude));
				xmlSerializer.endTag("", XML_LATITUDE_TAG);
				xmlSerializer.startTag("", XML_LONGITUDE_TAG);
				xmlSerializer.text(Double.toString(point.longitude));
				xmlSerializer.endTag("", XML_LONGITUDE_TAG);
				xmlSerializer.endTag("", XML_POINT_TAG);
			}
			xmlSerializer.endTag("", XML_SAFE_ZONE_TAG);
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "xmlSafeZone: IllegalArgumentException");
		} catch (IllegalStateException e) {
			Log.e(TAG, "xmlSafeZone: IllegalStateException");
		} catch (IOException e) {
			Log.e(TAG, "xmlSafeZone: IOException");
		}

	}

	/**
	 * Adds the XmlSerializer the Xml structure of a position
	 * 
	 * @param xmlSerializer
	 * @param position
	 */
	private static void positionXml(XmlSerializer xmlSerializer,
			TmlPosition position) {
		if (xmlSerializer == null) {
			return;
		}
		try {
			xmlSerializer.startTag("", XML_POSITION_TAG);
			xmlSerializer.startTag("", XML_TIMESTAMP_TAG);
			xmlSerializer.text(Long.toString(position.getTimestamp()));
			xmlSerializer.endTag("", XML_TIMESTAMP_TAG);
			xmlSerializer.startTag("", TmlNetwork.XML_LATITUDE_TAG);
			xmlSerializer
					.text(Double.toString(position.getCoordinates().latitude));
			xmlSerializer.endTag("", TmlNetwork.XML_LATITUDE_TAG);
			xmlSerializer.startTag("", TmlNetwork.XML_LONGITUDE_TAG);
			xmlSerializer
					.text(Double.toString(position.getCoordinates().longitude));
			xmlSerializer.endTag("", TmlNetwork.XML_LONGITUDE_TAG);
			xmlSerializer.startTag("", XML_ACCURACY_TAG);
			xmlSerializer.text(Float.toString(position.getAccuracy()));
			xmlSerializer.endTag("", XML_ACCURACY_TAG);
			xmlSerializer.startTag("", XML_BATTERY_TAG);
			xmlSerializer.text(Float.toString(position.getBattery()));
			xmlSerializer.endTag("", XML_BATTERY_TAG);
			xmlSerializer.startTag("", XML_ACTIVITY_TAG);
			xmlSerializer.text(position.getActivity());
			xmlSerializer.endTag("", XML_ACTIVITY_TAG);
			xmlSerializer.endTag("", XML_POSITION_TAG);
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "IllegalArgumentException");
		} catch (IllegalStateException e) {
			Log.e(TAG, "IllegalStateException");
		} catch (IOException e) {
			Log.e(TAG, "IOException");
		}

		return;
	}

	/**
	 * Parse newSafeZone server response. If a safezone has been added correctly
	 * the safe zone is tagged as sent
	 * 
	 * @param xmlParser
	 */
	private void safeZoneXmlParser(XmlPullParser xmlParser) {

		String actualUsername = null;
		TmlChild actualChild = null;

		Integer actualSafeZoneSerial = null;
		TmlSafeZone actualSafeZone = null;

		TmlSafeZone newSafeZone = null;

		double lat = 0;
		double lng = 0;

		try {
			while (xmlParser.next() != XmlPullParser.END_DOCUMENT) {
				if (xmlParser.getEventType() == XmlPullParser.START_TAG) {
					if (xmlParser.getName().equals(XML_CHILD_TAG)) {
						actualChild = null;
						actualUsername = xmlParser
								.getAttributeValue("", "name");
						/* Search for a child with the same name */
						for (TmlChild childTemp : childrenList) {
							if (childTemp.getName().equals(actualUsername)) {
								actualChild = childTemp;
								break;
							}
						}
					} else if (xmlParser.getName().equals(XML_SAFE_ZONE_TAG)) {
						String serial = xmlParser.getAttributeValue("",
								"serial");
						actualSafeZone = null;
						newSafeZone = null;
						if (actualChild != null) {
							if (serial == null) {
								/*
								 * New safe zone received
								 */
								newSafeZone = TmlSafeZone
										.getNewSafeZone(actualChild);
							} else {
								actualSafeZoneSerial = Integer.parseInt(serial);
								for (TmlSafeZone safeZoneTemp : actualChild
										.getSafeZoneList()) {
									if (safeZoneTemp.getSerial() == actualSafeZoneSerial
											.intValue()) {
										actualSafeZone = safeZoneTemp;
										break;
									}
								}
							}
						}
					} else if (xmlParser.getName().equals(XML_CREATED_TAG)) {
						if (actualSafeZone != null) {
							actualSafeZone.resetToBeCreated();
						}
					} else if (xmlParser.getName().equals(XML_REMOVED_TAG)) {
						if (actualSafeZone != null) {
							/*
							 * SafeZone deleted
							 */
							actualSafeZone.delete();
							notificationManager.riseNotifications();
						}
					} else if (xmlParser.getName().equals(XML_POINT_TAG)) {
						lat = 0;
						lng = 0;
					} else if (xmlParser.getName().equals(XML_LATITUDE_TAG)) {
						lat = Double.parseDouble(xmlParser.nextText());
					} else if (xmlParser.getName().equals(XML_LONGITUDE_TAG)) {
						lng = Double.parseDouble(xmlParser.nextText());
					}
				} else if (xmlParser.getEventType() == XmlPullParser.END_TAG) {
					if (xmlParser.getName().equals(XML_POINT_TAG)) {
						newSafeZone.addPoint(new LatLng(lat, lng));
					} else if (xmlParser.getName().equals(XML_SAFE_ZONE_TAG)) {
						if (newSafeZone != null) {
							/*
							 * SafeZone inserted
							 */
							newSafeZone.insertFromRemote();
							notificationManager.riseNotifications();
						}
					}
				}
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
			Log.e(TAG, "NullPointerException");
		} catch (NumberFormatException e) {
			Log.e(TAG, "NumberFormatException");
		} catch (XmlPullParserException e) {
			Log.e(TAG, "XmlPullParserException");
		} catch (IOException e) {
			Log.e(TAG, "IOException");
		}

	}

	/*
	 * ------------------------- ACCOUNT SETUP METHODS -------------------------
	 */

	/**
	 * Account login and users retrieval.
	 * 
	 * @param credentials
	 * @return If account exists ArrayList of String is returned (even empty if
	 *         no users available). If account doesn't exist null is returned.
	 * @throws ConnectException
	 *             when server internal error
	 * @throws IOException
	 *             when server not reachable
	 */
	private ArrayList<String> accountLogin(TmlAccount credentials)
			throws ConnectException, IOException {
		final String TAG = "accountLogin";

		if (credentials == null) {
			Log.e(TAG, "Null credentials");
			return null;
		}

		ArrayList<String> users = new ArrayList<String>();
		StringWriter xmlWriter = new StringWriter();
		XmlSerializer xmlSerializer = Xml.newSerializer();
		try {

			/* Prepare xml to be sent containing the account credentials */
			initXml(xmlSerializer, xmlWriter, credentials);
			String xmlString = endXml(xmlSerializer, xmlWriter);

			/* Send xml and retrieve the response from the server */
			InputStream serverResponseStream = serverCommunication(new TmlNetwork.CommunicationParameters(
					ACCOUNT_LOGIN_URL, xmlString));

			/* Parse server response */
			XmlPullParser xmlParser = getXmlPullParser(serverResponseStream);
			while (xmlParser.next() != XmlPullParser.END_DOCUMENT) {
				if (xmlParser.getEventType() == XmlPullParser.START_TAG) {
					if (xmlParser.getName().equals(XML_ERROR_TAG)) {
						/* Return null in case of errors */
						users = null;
						break;
					} else if (xmlParser.getName().equals(XML_USER_TAG)) {
						users.add(xmlParser.getAttributeValue("", "name"));
					}
				}
			}

			/* Users list returned for the given account or null */
			closeInputStream(serverResponseStream);
			return users;

		} catch (IllegalArgumentException e) {
			Log.w(TAG, "IllegalArgumentException");
		} catch (IllegalStateException e) {
			Log.w(TAG, "IllegalStateException");
		} catch (XmlPullParserException e) {
			Log.w(TAG, "XmlPullParserException");
		}

		/* Some exception occurred */
		return null;
	}

	/**
	 * Account signup.
	 * 
	 * @param credentials
	 * @return String describing the result of the operation, null if server
	 *         error occurred.
	 * @throws ConnectException
	 *             when server internal error
	 * @throws IOException
	 *             when server not reachable
	 */
	private String accountSignup(TmlAccount credentials)
			throws ConnectException, IOException {

		String returnValue = null;
		StringWriter xmlWriter = new StringWriter();
		XmlSerializer xmlSerializer = Xml.newSerializer();
		try {

			/* Prepare xml to be sent containing the account credentials */
			initXml(xmlSerializer, xmlWriter, credentials);
			String xmlString = endXml(xmlSerializer, xmlWriter);

			/* Send xml and retrieve the response from the server */
			InputStream serverResponseStream = serverCommunication(new TmlNetwork.CommunicationParameters(
					ACCOUNT_SIGNUP_URL, xmlString));

			/* Parse server response */
			XmlPullParser xmlParser = getXmlPullParser(serverResponseStream);
			while (xmlParser.next() != XmlPullParser.END_DOCUMENT) {
				if (xmlParser.getEventType() == XmlPullParser.START_TAG) {
					if (xmlParser.getName().equals(XML_ERROR_TAG)) {
						returnValue = xmlParser.nextText();
						break;
					} else if (xmlParser.getName().equals(XML_SUCCESS_TAG)) {
						returnValue = xmlParser.nextText();
						break;
					}
				}
			}

			/* Return response string or null */
			closeInputStream(serverResponseStream);
			return returnValue;

		} catch (IllegalArgumentException e) {
			Log.w(TAG, "IllegalArgumentException");
		} catch (IllegalStateException e) {
			Log.w(TAG, "IllegalStateException");
		} catch (XmlPullParserException e) {
			Log.w(TAG, "XmlPullParserException");
		}

		/* Some exception occurred */
		return null;
	}

	/**
	 * Create e new user for the given account
	 * 
	 * @param credentials
	 * @param user
	 * @return String describing the result of the operation, null if server
	 *         error occurred.
	 * @throws ConnectException
	 *             when server internal error
	 * @throws IOException
	 *             when server not reachable
	 */
	private String newUser(TmlAccount credentials, String user)
			throws ConnectException, IOException {
		final String TAG = "newUser";

		if (credentials == null || user == null) {
			Log.e(TAG, "Null credentials or user");
			return null;
		}

		String returnValue = null;
		StringWriter xmlWriter = new StringWriter();
		XmlSerializer xmlSerializer = Xml.newSerializer();
		try {

			/*
			 * Prepare xml to be sent containing the account credentials and the
			 * name and role of the new user
			 */
			initXml(xmlSerializer, xmlWriter, credentials);
			xmlSerializer.startTag("", XML_USER_TAG);
			xmlSerializer.attribute("", "name", user);
			xmlSerializer.endTag("", XML_USER_TAG);
			String xmlString = endXml(xmlSerializer, xmlWriter);

			/* Send xml and retrieve the response from the server */
			InputStream serverResponseStream = serverCommunication(new TmlNetwork.CommunicationParameters(
					USER_CREATE_URL, xmlString));

			/* Parse server response */
			XmlPullParser xmlParser = getXmlPullParser(serverResponseStream);
			while (xmlParser.next() != XmlPullParser.END_DOCUMENT) {
				if (xmlParser.getEventType() == XmlPullParser.START_TAG) {
					if (xmlParser.getName().equals(XML_ERROR_TAG)) {
						returnValue = xmlParser.nextText();
						break;
					} else if (xmlParser.getName().equals(XML_SUCCESS_TAG)) {
						returnValue = xmlParser.nextText();
						break;
					}
				}
			}

			/* Return response string or null */
			closeInputStream(serverResponseStream);
			return returnValue;

		} catch (IllegalArgumentException e) {
			Log.w(TAG, "IllegalArgumentException");
		} catch (IllegalStateException e) {
			Log.w(TAG, "IllegalStateException");

		} catch (XmlPullParserException e) {
			Log.w(TAG, "XmlPullParserException");
		}

		/* Some exception occurred */
		return null;
	}

	/*
	 * --------------------- GENERIC COMMUNICATION METHODS ---------------------
	 */

	/**
	 * Sends outputXmlString to url and returns the server response as
	 * InputStream
	 * 
	 * @param url
	 * @param outputXmlString
	 * @return an InputStream containing the server response
	 * @throws ConnectException
	 *             when server internal error
	 * @throws IOException
	 *             when server not reachable
	 */
	private InputStream serverCommunication(CommunicationParameters commParam)
			throws ConnectException, IOException {

		/* check if a connection exists */
		if (!checkConnection(this)) {
			throw new IOException("Network connection unavailable");
		}

		/* Create connection, send and receive data */
		HttpsURLConnection connection = this.getSSLConnection(commParam
				.getUrl());

		connection.setDoOutput(true);
		connection.setChunkedStreamingMode(0);

		OutputStream out;
		out = new BufferedOutputStream(connection.getOutputStream());
		OutputStreamWriter outStreamWriter = new OutputStreamWriter(out);
		outStreamWriter.write("xml=" + commParam.getOutputXml());
		outStreamWriter.flush();
		outStreamWriter.close();
		out.close();

		InputStream inputStream;
		inputStream = new BufferedInputStream(connection.getInputStream());
		return inputStream;

	}

	/**
	 * Close an input stream and manage exceptions
	 * 
	 * @param serverResponseStream
	 */
	private void closeInputStream(InputStream serverResponseStream) {
		try {
			serverResponseStream.close();
		} catch (NullPointerException e) {
			Log.e(TAG, "closeInputStream: NullPointerException");
		} catch (IOException e) {
			Log.e(TAG, "closeInputStream: IOException");
		}
	}

	/**
	 * CA certificate that signs webserver certificate
	 * 
	 * @return Certificate, null if errors
	 */
	private Certificate getCaCertificate() {

		if (this.caCertificate == null) {

			/* Load CA certificate from asset */
			CertificateFactory certificateFactory;
			try {
				certificateFactory = CertificateFactory.getInstance("X.509");
				AssetManager assetManager = getAssets();
				InputStream caInput;
				caInput = assetManager.open(CA_CERT);

				Certificate caCert;
				caCert = certificateFactory.generateCertificate(caInput);

				this.caCertificate = caCert;

				caInput.close();
			} catch (CertificateException e) {
				Log.e(TAG, "getCaCertificate: CertificateException");
			} catch (IOException e) {
				Log.e(TAG, "getCaCertificate: IOException");
			}

		}
		return this.caCertificate;
	}

	/**
	 * SSL context for HTTPS connection
	 * 
	 * @return SSLContext, null if errors
	 */
	private SSLContext getSSLContext() {

		if (this.sslContext == null) {

			Certificate caCert;
			try {
				caCert = this.getCaCertificate();
				if (caCert == null) {
					return null;
				}
				/* Create a KeyStore containing our trusted CAs */
				String keyStoreType = KeyStore.getDefaultType();
				KeyStore keyStore;
				keyStore = KeyStore.getInstance(keyStoreType);
				keyStore.load(null, null);

				keyStore.setCertificateEntry("ca", caCert);

				/* Create a TrustManager that trusts the CAs in our KeyStore */
				String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
				TrustManagerFactory tmf = TrustManagerFactory
						.getInstance(tmfAlgorithm);
				tmf.init(keyStore);

				/* Create an SSLContext that uses our TrustManager */
				SSLContext sslCont = SSLContext.getInstance("TLS");
				sslCont.init(null, tmf.getTrustManagers(), null);

				this.sslContext = sslCont;
			} catch (CertificateException e) {
				Log.e(TAG, "getSSLContext: CertificateException");
			} catch (IOException e) {
				Log.e(TAG, "getSSLContext: IOException");
			} catch (KeyStoreException e) {
				Log.e(TAG, "getSSLContext: KeyStoreException");
			} catch (NoSuchAlgorithmException e) {
				Log.e(TAG, "getSSLContext: NoSuchAlgorithmException");
			} catch (KeyManagementException e) {
				Log.e(TAG, "getSSLContext: KeyManagementException");
			}

		}

		return this.sslContext;
	}

	/**
	 * Https connection for the given url
	 * 
	 * @param urlString
	 *            starting with "https://"
	 * @return null if errors
	 */
	private HttpsURLConnection getSSLConnection(String urlString) {

		URL url;
		try {
			url = new URL(urlString);
			HttpsURLConnection urlConnection;
			urlConnection = (HttpsURLConnection) url.openConnection();
			urlConnection.setSSLSocketFactory(this.getSSLContext()
					.getSocketFactory());
			return urlConnection;
		} catch (MalformedURLException e) {
			Log.e(TAG, "getSSLConnection: MalformedURLException");
		} catch (IOException e) {
			Log.e(TAG, "getSSLConnection: IOException");
		}

		return null;

	}

	/**
	 * Adds provided account informations and device id to xmlSerializer
	 * 
	 * @param xmlSerializer
	 * @param account
	 *            if null db credentials are used
	 */
	private void accountXml(XmlSerializer xmlSerializer, TmlAccount account) {
		try {
			xmlSerializer.startTag("", XML_DEVICE_TAG);
			xmlSerializer.text(Settings.Secure.getString(getBaseContext()
					.getContentResolver(), Secure.ANDROID_ID));
			xmlSerializer.endTag("", XML_DEVICE_TAG);
			xmlSerializer.startTag("", XML_ACCOUNT_TAG);
			xmlSerializer.startTag("", XML_MAIL_TAG);
			if (account == null) {
				xmlSerializer.text(db.getEmail());
			} else {
				xmlSerializer.text(account.getEmail());
			}
			xmlSerializer.endTag("", XML_MAIL_TAG);
			xmlSerializer.startTag("", XML_PWD_TAG);
			if (account == null) {
				xmlSerializer.text(db.getPwdSHA1());
			} else {
				xmlSerializer.text(account.getPwdSHA1());
			}
			xmlSerializer.endTag("", XML_PWD_TAG);
			xmlSerializer.endTag("", XML_ACCOUNT_TAG);
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "accountXml: IllegalArgumentException");
		} catch (IllegalStateException e) {
			Log.e(TAG, "accountXml: accountXml");
		} catch (IOException e) {
			Log.e(TAG, "accountXml: IOException");
		}

		return;
	}

	/**
	 * Initialize xmlSerializer and stringWriter with tml tag and provided
	 * account informations
	 * 
	 * @param xmlSerializer
	 * @param stringWriter
	 * @param account
	 *            if null db credentials are used
	 */
	private void initXml(XmlSerializer xmlSerializer,
			StringWriter stringWriter, TmlAccount account) {

		try {
			xmlSerializer.setOutput(stringWriter);
			xmlSerializer.startDocument("UTF-8", true);
			xmlSerializer.startTag("", XML_TML_TAG);
			accountXml(xmlSerializer, account);
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "initXml: IllegalArgumentException");
		} catch (IllegalStateException e) {
			Log.e(TAG, "initXml: IllegalStateException");
		} catch (IOException e) {
			Log.e(TAG, "initXml: IOException");
		}

		return;
	}

	/**
	 * Completes an Xml document with tml tag.
	 * 
	 * @param xmlSerializer
	 * @param stringWriter
	 * @return String containing the xml ready to be sent
	 */
	private String endXml(XmlSerializer xmlSerializer, StringWriter stringWriter) {
		try {
			xmlSerializer.endTag("", XML_TML_TAG);
			xmlSerializer.endDocument();
			return stringWriter.toString();
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "endXml: IllegalArgumentException");
		} catch (IllegalStateException e) {
			Log.e(TAG, "endXml: IllegalStateException");
		} catch (IOException e) {
			Log.e(TAG, "endXml: IOException");
		}
		return null;

	}

	/**
	 * Create an XML Pull Parser and set its input to the given InputStream.
	 * Manage the exceptions
	 * 
	 * @param serverResponseStream
	 * @return
	 */
	private XmlPullParser getXmlPullParser(InputStream serverResponseStream) {
		XmlPullParser xmlParser = Xml.newPullParser();
		try {
			xmlParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES,
					false);
			xmlParser.setInput(serverResponseStream, "UTF-8");
			return xmlParser;
		} catch (IllegalArgumentException e) {
			Log.d(TAG, "getXmlPullParser: IllegalArgumentException");
		} catch (XmlPullParserException e) {
			Log.d(TAG, "getXmlPullParser: XmlPullParserException");
		}
		return null;
	}

	/*
	 * ------------------------------------------------------------------------
	 * ------------------- ACTIVITY LIFECYCLE METHODS -------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Default constructor. Needed for InstaceService subclasses.
	 */
	public TmlNetwork() {
		super("me.noip.unklb.trackmyloved.TmlNetwork");
	}

	@Override
	public void onCreate() {
		super.onCreate();

		Thread.currentThread().setName("TmlNetwork");
		running = true;
		if (db == null) {
			db = TmlDatabase.getInstance(this);
		}
		if (db.isRoleDefined()) {
			initChildrenList(this);
		}
	}

	/**
	 * Gateway to different synchronization methods. Intent used as argument in
	 * startService() are passed to this method one at time on a separate
	 * thread.
	 */
	@Override
	protected void onHandleIntent(Intent intent) {

		intentCounter++;

		if (intent != null) {

			if (intent.getAction().equals(INTENT_ACTION_ACCOUNT_LOGIN)) {
				/**
				 * Response intent
				 * 
				 * <pre>
				 * INTENT_EXTRA_KEY_ERROR
				 * - INTENT_EXTRA_RESULT_SERVER_ERROR: Server error
				 * - INTENT_EXTRA_RESULT_SERVER_UNREACHABLE: Server unreachable
				 * 
				 * INTENT_EXTRA_KEY_RESULT (if no INTENT_EXTRA_KEY_ERROR)
				 * - null: account doesn't exist
				 * - {@code ArrayList<String>}: account exists.
				 * 	 Child user name in the list (even empty if no children)
				 * 
				 * INTENT_EXTRA_KEY_ACCOUNT (if INTENT_EXTRA_KEY_RESULT != null)
				 * - login account
				 * </pre>
				 */

				/*
				 * Retrieve account data from intent extra
				 */
				TmlAccount account = intent
						.getParcelableExtra(INTENT_EXTRA_KEY_ACCOUNT);

				/*
				 * Prepare response intent
				 */
				Intent replyIntent = new Intent();
				replyIntent.setAction(intent.getAction());

				/*
				 * Perform login on the remote server
				 */
				ArrayList<String> result;
				try {
					result = accountLogin(account);
					replyIntent.putExtra(INTENT_EXTRA_KEY_RESULT, result);
					if (result != null) {
						replyIntent.putExtra(INTENT_EXTRA_KEY_ACCOUNT, account);
					}
				} catch (ConnectException e) {
					replyIntent.putExtra(INTENT_EXTRA_KEY_ERROR,
							INTENT_EXTRA_ERROR_SERVER_ERROR);
				} catch (IOException e) {
					replyIntent.putExtra(INTENT_EXTRA_KEY_ERROR,
							INTENT_EXTRA_ERROR_SERVER_UNREACHABLE);
				}

				/*
				 * Reply to the calling activity
				 */

				LocalBroadcastManager.getInstance(this).sendBroadcast(
						replyIntent);

			} else if (intent.getAction().equals(INTENT_ACTION_ACCOUNT_SIGNUP)) {
				/**
				 * Response intent
				 * 
				 * <pre>
				 * INTENT_EXTRA_KEY_ERROR
				 * - INTENT_EXTRA_RESULT_SERVER_ERROR: Server error
				 * - INTENT_EXTRA_RESULT_SERVER_UNREACHABLE: Server unreachable
				 * 
				 * INTENT_EXTRA_KEY_RESULT (if no INTENT_EXTRA_KEY_ERROR)
				 * - XML_ACCOUNT_REGISTRATION_ERROR_TEXT: Account registration server error
				 * - XML_MAIL_ALREADY_REGISTERED_TEXT: Mail already registered
				 * - XML_ACCOUNT_REGISTRATION_SUCCESSFUL_TEXT: User created successfully
				 * 
				 * INTENT_EXTRA_KEY_ACCOUNT (only if XML_ACCOUNT_REGISTRATION_SUCCESSFUL_TEXT)
				 * - created account
				 * </pre>
				 */

				/*
				 * Retrieve account data from intent extra
				 */
				TmlAccount account = intent
						.getParcelableExtra(INTENT_EXTRA_KEY_ACCOUNT);

				/*
				 * Prepare response intent
				 */
				Intent replyIntent = new Intent();
				replyIntent.setAction(intent.getAction());

				/*
				 * Perform signup on the remote server
				 */
				String result;
				try {
					result = accountSignup(account);
					replyIntent.putExtra(INTENT_EXTRA_KEY_RESULT, result);
					if (result.equals(XML_ACCOUNT_REGISTRATION_SUCCESSFUL_TEXT)) {
						replyIntent.putExtra(INTENT_EXTRA_KEY_ACCOUNT, account);
					}
				} catch (ConnectException e) {
					replyIntent.putExtra(INTENT_EXTRA_KEY_ERROR,
							INTENT_EXTRA_ERROR_SERVER_ERROR);
				} catch (IOException e) {
					replyIntent.putExtra(INTENT_EXTRA_KEY_ERROR,
							INTENT_EXTRA_ERROR_SERVER_UNREACHABLE);
				}

				/*
				 * Reply to the calling activity
				 */
				LocalBroadcastManager.getInstance(this).sendBroadcast(
						replyIntent);

			} else if (intent.getAction().equals(INTENT_ACTION_USER_CREATE)) {
				/**
				 * Response intent
				 * 
				 * <pre>
				 * INTENT_EXTRA_KEY_ERROR
				 * - INTENT_EXTRA_RESULT_SERVER_ERROR: Server error
				 * - INTENT_EXTRA_RESULT_SERVER_UNREACHABLE: Server unreachable
				 * 
				 * INTENT_EXTRA_KEY_RESULT (if no INTENT_EXTRA_KEY_ERROR)
				 * - XML_USER_CREATION_ERROR_TEXT: User creation server error
				 * - XML_USER_EXISTS_TEXT: User already exists for the account
				 * - XML_USER_CREATION_SUCCESSFUL_TEXT: User created successfully
				 * 
				 * INTENT_EXTRA_KEY_USER (only if XML_USER_CREATION_SUCCESSFUL_TEXT)
				 * - username of the new user
				 * </pre>
				 */

				/*
				 * Retrieve account and user data from intent extra
				 */
				TmlAccount account = intent
						.getParcelableExtra(INTENT_EXTRA_KEY_ACCOUNT);
				String user = intent.getStringExtra(INTENT_EXTRA_KEY_USER);

				/*
				 * Prepare response intent
				 */
				Intent replyIntent = new Intent();
				replyIntent.setAction(intent.getAction());

				/*
				 * Perform user creation on the remote server
				 */
				String result;
				try {
					result = newUser(account, user);
					replyIntent.putExtra(INTENT_EXTRA_KEY_RESULT, result);
					if (result.equals(XML_USER_CREATION_SUCCESSFUL_TEXT)) {
						replyIntent.putExtra(INTENT_EXTRA_KEY_USER, user);
					}
				} catch (ConnectException e) {
					replyIntent.putExtra(INTENT_EXTRA_KEY_ERROR,
							INTENT_EXTRA_ERROR_SERVER_ERROR);
				} catch (IOException e) {
					replyIntent.putExtra(INTENT_EXTRA_KEY_ERROR,
							INTENT_EXTRA_ERROR_SERVER_UNREACHABLE);
				}

				/*
				 * Reply to the calling activity
				 */
				LocalBroadcastManager.getInstance(this).sendBroadcast(
						replyIntent);

			} else if (intent.getAction().equals(INTENT_ACTION_CHECK_SERVER)) {
				/**
				 * Response intent
				 * 
				 * <pre>
				 * INTENT_EXTRA_KEY_ERROR
				 * - INTENT_EXTRA_RESULT_SERVER_ERROR: Server error
				 * - INTENT_EXTRA_RESULT_SERVER_UNREACHABLE: Server unreachable
				 * 
				 * INTENT_EXTRA_KEY_RESULT (if no INTENT_EXTRA_KEY_ERROR)
				 * - false: No network connection
				 * - true: Connected and server ok
				 * </pre>
				 */

				/*
				 * Prepare response intent
				 */
				Intent replyIntent = new Intent();
				replyIntent.setAction(intent.getAction());

				/*
				 * Server check
				 */
				boolean result;
				try {
					result = checkServer();
					replyIntent.putExtra(INTENT_EXTRA_KEY_RESULT, result);
				} catch (ConnectException e) {
					replyIntent.putExtra(INTENT_EXTRA_KEY_ERROR,
							INTENT_EXTRA_ERROR_SERVER_ERROR);
				} catch (IOException e) {
					replyIntent.putExtra(INTENT_EXTRA_KEY_ERROR,
							INTENT_EXTRA_ERROR_SERVER_UNREACHABLE);
				}

				/*
				 * Reply to the calling activity
				 */
				LocalBroadcastManager.getInstance(this).sendBroadcast(
						replyIntent);
			} else if (intent.getAction().equals(INTENT_ACTION_SYNC_POSITIONS)) {
				/*
				 * Synchronize positions with remote server
				 */
				try {
					syncPositions();
				} catch (ConnectException e) {
					/*
					 * Server internal error
					 */
					Log.w(TAG, "Server error");
				} catch (IOException e) {
					/*
					 * Server unreachable
					 */
					Log.w(TAG, "Server unreachable");
				}

			}
		}

		intentCounter--;

		if (intentCounter == 0) {
			TmlNetwork.running = false;
		}

	}

}
