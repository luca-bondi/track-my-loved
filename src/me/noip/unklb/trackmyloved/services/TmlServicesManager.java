package me.noip.unklb.trackmyloved.services;

import me.noip.unklb.trackmyloved.R;
import me.noip.unklb.trackmyloved.activities.TmlMainActivity;
import me.noip.unklb.trackmyloved.controllers.TmlChild;
import me.noip.unklb.trackmyloved.controllers.TmlColor;
import me.noip.unklb.trackmyloved.controllers.TmlDatabase;
import me.noip.unklb.trackmyloved.controllers.TmlNotification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

/**
 * Boot service and services manager
 */
public class TmlServicesManager extends BroadcastReceiver {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	public static final String TAG = "TmlServicesManager";

	/**
	 * Broadcast action used to notify that a new position has been inserted in
	 * the database
	 */
	public static final String BROADCAST_ACTION_CHILDREN_UPDATE = "BROADCAST_ACTION_CHILDREN_UPDATE";

	/**
	 * Broadcast action used to notify that the user interface is visible
	 */
	public static final String BROADCAST_ACTION_GUI_VISIBLE = "BROADCAST_ACTION_GUI_VISIBLE";

	/**
	 * Broadcast action used to notify that the user interface is hidden
	 */
	public static final String BROADCAST_ACTION_GUI_HIDDEN = "BROADCAST_ACTION_GUI_HIDDEN";

	/**
	 * Intent action used on child device when the user has been deleted in the
	 * server
	 */
	public static final String INTENT_ACTION_CHILD_DEVICE_RESET = "INTENT_ACTION_CHILD_DEVICE_RESET";

	/*
	 * ------------------------------------------------------------------------
	 * ------------------------- LISTENERS ------------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Called at boot to start services (see Manifest)
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent != null) {
			if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
				if (GooglePlayServicesUtil
						.isGooglePlayServicesAvailable(context) == ConnectionResult.SUCCESS) {
					startServices(context);
				} else {
					TmlNotification
							.showNotification(
									context,
									null,
									TmlNotification.NOTIFICATION_ID_GPS_MISSING,
									R.drawable.ic_stat_notify_error,
									R.drawable.notif_large_icon,
									context.getString(R.string.notification_google_play_services_missing_title),
									context.getString(R.string.notification_google_play_services_missing_text));
				}

			}
		}
	}

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- ATTRIBUTES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Set to indicate that services should stop. Used in TmlNetwork to stop
	 * long running synchronizations
	 */
	private static boolean areServicesStopping = false;

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- METHODS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Static method called to start services. If role is not defined no service
	 * is started. On parent device only TmlSync service is started. On child
	 * device both TmlSync and TmlTrack services are started.
	 * 
	 * @param context
	 */
	public static void startServices(Context context) {
		Log.i(TAG, "Starting services");

		areServicesStopping = false;

		TmlDatabase db = TmlDatabase.getInstance(context);

		if (db.isRoleDefined()) {

			Intent intent = new Intent(context, TmlSync.class);
			context.startService(intent);

			if (db.isChildDevice() && !db.isInitialSyncRequired()) {
				intent = new Intent(context, TmlTrack.class);
				context.startService(intent);
			}
		}
	}

	/**
	 * Static method called to stop services.
	 * 
	 * @param context
	 */
	public static void stopServices(Context context) {
		Log.i(TAG, "Stopping Services");

		areServicesStopping = true;

		TmlDatabase db = TmlDatabase.getInstance(context);

		Intent intent = new Intent(context, TmlSync.class);
		context.stopService(intent);

		if (db.isChildDevice()) {
			intent = new Intent(context, TmlTrack.class);
			context.stopService(intent);
		}

	}

	/**
	 * Services should stop
	 * 
	 * @return
	 */
	public static boolean areServicesStopping() {
		return areServicesStopping;
	}

	/**
	 * Stop services and clear database. If preserveAccount is set, email and
	 * password SHA1 are not deleted. Then restart application
	 * 
	 * @param context
	 * @param preserveAccount
	 * @param showMainActivity
	 *            true if MainActivity should be started after the reset, false
	 *            otherwise
	 */
	public static void resetDevice(final Context context,
			final boolean preserveAccount, final boolean showMainActivity) {

		stopServices(context);

		TmlNotification.cancelAll(context);

		final TmlDatabase db = TmlDatabase.getInstance(context);

		new Runnable() {

			@Override
			public void run() {

				if (!TmlNetwork.running && !TmlTrack.running
						&& !TmlSync.running) {
					TmlNotification.cancelAll(context);

					String email = null;
					String pwdSHA1 = null;
					if (preserveAccount) {
						email = db.getEmail();
						pwdSHA1 = db.getPwdSHA1();
					}

					db.clearDatabase();

					if (preserveAccount) {
						db.setEmail(email);
						db.setPwdSHA1(pwdSHA1);
					}

					TmlChild.resetDevice();
					TmlNetwork.resetDevice();
					TmlNotification.resetDevice();
					TmlColor.resetDevice();

					if (preserveAccount) {
						Toast.makeText(
								context,
								context.getString(R.string.reset_user_completed_toast_text),
								Toast.LENGTH_LONG).show();
					} else {
						Toast.makeText(
								context,
								context.getString(R.string.reset_device_completed_toast_text),
								Toast.LENGTH_LONG).show();
					}

					if (showMainActivity) {
						context.startActivity(new Intent(context,
								TmlMainActivity.class));
					}
				} else {
					Log.d(TAG, "Waiting for services to terminate");
					new Handler().postDelayed(this, 1000);
				}

			}

		}.run();

	}

}