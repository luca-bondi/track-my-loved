package me.noip.unklb.trackmyloved.services;

import java.util.ArrayList;

import me.noip.unklb.trackmyloved.controllers.TmlChild;
import me.noip.unklb.trackmyloved.controllers.TmlChild.TmlChildrenListener;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * Synchronization background service
 * 
 * Determine when to perform synchronization operations.
 * 
 * If a working Internet connection is available TmlNetwork service is called
 * and network operations are performed. Internet connection changes are
 * monitored through ConnectivityManager broadcast intent.
 */
public class TmlSync extends Service {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	private static final String TAG = "TmlSync";

	/**
	 * Position sync period when no user interface is shown. Only used in parent
	 * phone. [milliseconds]
	 */
	private static final int POSITION_SYNC_PERIOD_BACKGROUND = 180 * 1000;
	/**
	 * Position sync period when user interface is show. Only used in parent
	 * phone. [milliseconds]
	 */
	private static final int POSITION_SYNC_PERIOD_GUI = 15 * 1000;

	/**
	 * Initial sync connection and child loading complete interval
	 * [milliseconds]
	 */
	private static final long INITIAL_SYNC_CHECK_INTERVAL = 500;

	/*
	 * ------------------------------------------------------------------------
	 * -------------- CALLBACK, LISTENER, RECEIVER, RUNNABLE ------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Broadcast receiver for connectivity updates from connectivity manager.
	 */
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null && intent.getAction() != null) {

				if (intent.getAction().equals(
						ConnectivityManager.CONNECTIVITY_ACTION)) {
					boolean connectedNew = TmlNetwork
							.checkConnection(getBaseContext());
					/*
					 * Parent: if connectivity has become active sync and start
					 * periodic updates. if connectivity has been lost stop
					 * periodic sync
					 */
					if (TmlChild.isParentDevice()
							&& (connected != connectedNew)) {
						if (connectedNew) {
							syncRunnable.run();
						} else {
							periodicSyncHandler.removeCallbacks(syncRunnable);
						}
					}
					/*
					 * child: if pending sync and connection is available
					 * synchronize
					 */
					if (TmlChild.isChildDevice() && positionPendingSync
							&& connectedNew) {
						syncRunnable.run();
						positionPendingSync = false;
					}
					connected = connectedNew;
				}
			}
		}
	};

	/**
	 * Local broadcast receiver for GUI visibility
	 */
	private BroadcastReceiver localBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null && intent.getAction() != null) {
				if (intent.getAction().equals(
						TmlServicesManager.BROADCAST_ACTION_GUI_VISIBLE)) {
					syncPeriod = POSITION_SYNC_PERIOD_GUI;
					if (connected) {
						syncRunnable.run();
					}
				} else if (intent.getAction().equals(
						TmlServicesManager.BROADCAST_ACTION_GUI_HIDDEN)) {
					syncPeriod = POSITION_SYNC_PERIOD_BACKGROUND;
				}
			}

		}

	};

	/**
	 * Runnable used to periodically synchronize positions
	 */
	private Runnable syncRunnable = new Runnable() {

		/**
		 * Each time this runner is executed it queues a position
		 * synchronization request to Network service. It also delays a self
		 * execution after syncPeriod milliseconds.
		 * 
		 * This runnable runs only when initial loading of children has
		 * completed.
		 */
		@Override
		public void run() {
			/* Delete any pending run */
			periodicSyncHandler.removeCallbacks(syncRunnable);
			if (TmlChild.initialLoadingCompleted()) {
				/*
				 * Standard way of operation. Initial loading is completed. If
				 * this is a parent device another execution of this runnable is
				 * programmed after syncPeriod milliseconds
				 */
				startService(positionsSyncIntent);
				if (TmlChild.isParentDevice()) {
					periodicSyncHandler.postDelayed(syncRunnable, syncPeriod);
				}
			} else {
				/*
				 * Initial synchronization needs to wait
				 * INITIAL_SYNC_CHECK_INTERVAL milliseconds due to incomplete
				 * child loading
				 */
				periodicSyncHandler.postDelayed(syncRunnable,
						INITIAL_SYNC_CHECK_INTERVAL);
			}

		}

	};

	/**
	 * Listener as required by the TmlChild class
	 */
	private TmlChildrenListener childrenListener = new TmlChildrenListener() {

		@Override
		public void onAdd(TmlChild child) {
			childrenList.add(child);
		}

		@Override
		public void onRemove(TmlChild child) {
			childrenList.remove(child);
			/*
			 * Deletion of user on server is performed during position sync. If
			 * this device deleted a child, a forced position synchronization is
			 * useful to propagate the deletion.
			 */
			if (connected) {
				startService(positionsSyncIntent);
			}
		}

		@Override
		public void onUpdate(TmlChild child) {
			if (TmlChild.isChildDevice()) {
				if (connected) {
					startService(positionsSyncIntent);
				} else {
					positionPendingSync = true;
				}
			}
		}

	};

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- ATTRIBUTES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Indicates whether this service is running or not
	 */
	public static boolean running = false;

	/**
	 * Intents used to queue synchronization requests to Network service
	 */
	private Intent positionsSyncIntent;

	/**
	 * Handler used to run periodic synchronization
	 */
	private final Handler periodicSyncHandler = new Handler();

	/**
	 * Internet connection availability
	 */
	private boolean connected = false;

	/**
	 * Set when a new position has not been sent due to absent connectivity.
	 * When connectivity returns sync is performed.
	 */
	private boolean positionPendingSync = false;

	/**
	 * Synchronization period for parent device
	 */
	private int syncPeriod = POSITION_SYNC_PERIOD_BACKGROUND;

	/**
	 * List of children instances
	 */
	private ArrayList<TmlChild> childrenList;

	/*
	 * ------------------------------------------------------------------------
	 * ------------------- ACTIVITY LIFECYCLE CALLBACK ------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Called when service is started using startService() or bindService().
	 * Executed only when the service is created.
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		Log.i(TAG, "Creating");

		Thread.currentThread().setName("TmlSync");

		/*
		 * Set the running state
		 */
		TmlSync.running = true;

		/*
		 * Instantiate the childrenList
		 */
		childrenList = TmlChild.getChildrenList(this, childrenListener);

		/*
		 * Initialize positionsSyncIntent used to queue synchronization requests
		 * to Network service
		 */
		positionsSyncIntent = new Intent(this, TmlNetwork.class);
		positionsSyncIntent.setAction(TmlNetwork.INTENT_ACTION_SYNC_POSITIONS);

		/* Determine actual network connection status */
		connected = TmlNetwork.checkConnection(this);

		/*
		 * Register BradcastReceiver used to get ConnectivityManager updates
		 */
		IntentFilter ifilter = new IntentFilter();
		ifilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		registerReceiver(broadcastReceiver, ifilter);

		/*
		 * Register LocalBroadcastReceiver used to receive GUI visibility
		 * intents
		 */
		ifilter = new IntentFilter();
		ifilter.addAction(TmlServicesManager.BROADCAST_ACTION_GUI_VISIBLE);
		ifilter.addAction(TmlServicesManager.BROADCAST_ACTION_GUI_HIDDEN);
		LocalBroadcastManager.getInstance(this).registerReceiver(
				localBroadcastReceiver, ifilter);

		/*
		 * If a connection is available start a synchronization, otherwise it
		 * will be started when a connection becomes available
		 */
		if (connected) {
			syncRunnable.run();
		}

	}

	/**
	 * Called after onCreate() when the service is started with bindService().
	 * Executed each time bindService() is called In this application is not
	 * used.
	 */
	@Override
	public IBinder onBind(Intent intent) {
		Log.e(TAG, "This service should not be started using bindService()");
		return null;
	}

	/**
	 * Called after onCreate() when the service is started with startService().
	 * Executed each time startService() is called.
	 */
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		/*
		 * Returning START_STICKY tells the system that if this service is
		 * stopped it must be restarted as soon as the resources are available
		 */
		return START_STICKY;
	}

	/**
	 * Called when service is stopped using stopService() of when system
	 * resources are critically low.
	 */
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.i(TAG, "Destroying");
		if (TmlChild.isParentDevice() && connected) {
			periodicSyncHandler.removeCallbacks(syncRunnable);
		}
		/*
		 * Unregister broadcast receivers
		 */
		unregisterReceiver(broadcastReceiver);
		LocalBroadcastManager.getInstance(this).unregisterReceiver(
				localBroadcastReceiver);

		TmlSync.running = false;
	}

}
