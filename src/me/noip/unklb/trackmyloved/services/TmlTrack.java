package me.noip.unklb.trackmyloved.services;

import me.noip.unklb.trackmyloved.R;
import me.noip.unklb.trackmyloved.controllers.TmlChild;
import me.noip.unklb.trackmyloved.controllers.TmlChild.TmlChildrenListener;
import me.noip.unklb.trackmyloved.controllers.TmlNotification;
import me.noip.unklb.trackmyloved.controllers.TmlPosition;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Tracking background service
 * 
 * Periodically retrieves the position of the device. The position update
 * interval is dynamically tuned in function of the detected user activity and
 * battery level and charging state.
 * 
 * Only used on child devices. It's a private service.
 * 
 */
public class TmlTrack extends Service {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Tag used for logging purpose
	 */
	private static final String TAG = "TmlTrack";

	/**
	 * Ratio applied to an update interval when battery level is low
	 */
	private static final float LOW_BATTERY_RATIO = 2.0f;

	/**
	 * Ratio applied to an update interval when: battery is charging and battery
	 * level is high; battery is fully charge
	 */
	private static final float CHARGE_RATIO = 0.5f;

	/**
	 * Threshold for high battery level
	 */
	private static final float BATTERY_HIGH_LEVEL = 0.90f;

	/**
	 * Ratio between fastest update interval and update interval
	 */
	private static final float FASTEST_RATIO = 0.5f;

	/*
	 * Location intervals (in milliseconds) for different activity types
	 */
	private static final long IN_VEHICLE_UPDATE_INTERVAL = 2 * 60 * 1000;
	private static final long ON_BICYCLE_UPDATE_INTERVAL = 3 * 60 * 1000;
	private static final long ON_FOOT_UPDATE_INTERVAL = 3 * 60 * 1000;
	private static final long RUNNING_UPDATE_INTERVAL = 2 * 60 * 1000;
	private static final long STILL_UPDATE_INTERVAL = 5 * 60 * 1000;
	private static final long TILTING_UPDATE_INTERVAL = 4 * 60 * 1000;
	private static final long UNKNOWN_UPDATE_INTERVAL = 4 * 60 * 1000;
	private static final long WALKING_UPDATE_INTERVAL = 3 * 60 * 1000;

	/**
	 * Location interval (in milliseconds) when GUI is visible
	 */
	private static final long GUI_UPDATE_INTERVAL = 10 * 1000;

	/**
	 * Activity recognition interval (in milliseconds)
	 */
	private static final long ACTIVITY_RECOGNITION_INTERVAL = 1 * 60 * 1000;

	/**
	 * Location services check interval (in milliseconds)
	 */
	private static final long LOCATION_SERVICES_CHECK_INTERVAL = 10 * 60 * 1000;

	/**
	 * Max number of trials to connect to GoogleApiClient
	 */
	private static final int GOOGLE_API_MAX_CONNECTION_ATTEMPS = 3;

	/*
	 * ------------------------------------------------------------------------
	 * -------------- CALLBACK, LISTENER, RECEIVER, RUNNABLE ------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Callback for GoogleApiClient
	 */
	private class GoogleApiListener implements
			GoogleApiClient.ConnectionCallbacks,
			GoogleApiClient.OnConnectionFailedListener {

		static final String TAG = "GoogleApiListener";

		@Override
		public void onConnectionFailed(ConnectionResult arg0) {
			if (googleApiConnectionAttempts <= GOOGLE_API_MAX_CONNECTION_ATTEMPS) {
				Log.w(TAG, "ConnectionFailed. Retry "
						+ googleApiConnectionAttempts + " attempt...");
			} else {
				Log.e(TAG, "ConnectionFailed. Stop trying.");
			}

		}

		@Override
		public void onConnected(Bundle arg0) {
			googleApiConnectionAttempts = 0;

			LocationServices.FusedLocationApi.requestLocationUpdates(
					googleApiClient, locationRequest, pendingIntent);

			ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(
					googleApiClient, ACTIVITY_RECOGNITION_INTERVAL,
					pendingIntent);

		}

		@Override
		public void onConnectionSuspended(int arg0) {
			Log.w(TAG, "Connection suspended");
		}

	}

	/**
	 * Listener as required by the TmlChild class
	 */
	private TmlChildrenListener childrenListener = new TmlChildrenListener() {

		@Override
		public void onAdd(TmlChild child) {
			/*
			 * Nothing to do here. Should never be called
			 */
		}

		@Override
		public void onRemove(TmlChild child) {
			/*
			 * Nothing to do here. Should never be called
			 */
		}

		@Override
		public void onUpdate(TmlChild child) {
			/*
			 * Nothing to do here.
			 */
		}

	};

	/**
	 * Broadcast receiver used to get updates about battery status
	 */
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			if (intent.getAction().equals(Intent.ACTION_BATTERY_CHANGED)) {
				/*
				 * Determine battery percentage
				 */
				int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
				int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
				batteryPct = level / (float) scale;

				/*
				 * Store battery status
				 */
				batteryStatus = intent.getIntExtra(BatteryManager.EXTRA_STATUS,
						-1);

			} else if (intent.getAction().equals(Intent.ACTION_BATTERY_LOW)) {
				batteryLow = true;
			} else if (intent.getAction().equals(Intent.ACTION_BATTERY_OKAY)) {
				batteryLow = false;
			}
		}

	};

	/**
	 * Local broadcast receiver for GUI visibility
	 */
	private BroadcastReceiver localBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null && intent.getAction() != null) {
				if (intent.getAction().equals(
						TmlServicesManager.BROADCAST_ACTION_GUI_VISIBLE)) {
					isGuiVisible = true;
					setLocationParameters();
					checkLocationServices();

				} else if (intent.getAction().equals(
						TmlServicesManager.BROADCAST_ACTION_GUI_HIDDEN)) {
					isGuiVisible = false;
					setLocationParameters();
				}
			}

		}

	};

	/**
	 * Gps status listener
	 */
	private GpsStatus.Listener gpsStatusListener = new GpsStatus.Listener() {

		@Override
		public void onGpsStatusChanged(int event) {

			switch (event) {
			case GpsStatus.GPS_EVENT_STARTED:
				TmlNotification
						.cancelNotification(
								getApplicationContext(),
								null,
								TmlNotification.NOTIFICATION_ID_LOCATION_DISABLED);
				break;
			case GpsStatus.GPS_EVENT_STOPPED:
				checkLocationServices();
				break;
			}

		}

	};

	/**
	 * Runner used to periodically check location services. Useful to restore
	 * the notification if it is canceled.
	 */
	private Runnable checkLocationServicesRunnable = new Runnable() {
		public void run() {
			checkLocationServices();
			handler.postDelayed(checkLocationServicesRunnable,
					LOCATION_SERVICES_CHECK_INTERVAL);
		}
	};

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- ATTRIBUTES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Indicates whether this service is running or not
	 */
	public static boolean running = false;

	/**
	 * The child representing this device
	 */
	private TmlChild child;

	/**
	 * Pending Intent used to receive Location and ActivityRecognition updates
	 */
	private PendingIntent pendingIntent;

	/**
	 * LocationRequest instance
	 */
	private LocationRequest locationRequest;

	/**
	 * Last known battery charge percentage
	 */
	private float batteryPct;

	/**
	 * Last known battery status
	 */
	private int batteryStatus;

	/**
	 * Flag indicating whether the battery level is low
	 */
	private boolean batteryLow = false;

	/**
	 * Number of connections trials to ActivityRecognitionClient
	 */
	private int googleApiConnectionAttempts = 0;

	/**
	 * Activity detected by ActivityRecognitionClient
	 */
	private DetectedActivity activity;

	/**
	 * Handler instance
	 */
	private Handler handler = new Handler();

	/**
	 * Set when GUI is visible
	 */
	private boolean isGuiVisible;

	/**
	 * GoogleApiClient instance
	 */
	private GoogleApiClient googleApiClient;

	/**
	 * GoogleApiClient callback listener instance
	 */
	private GoogleApiListener googleApiListener = new GoogleApiListener();

	/**
	 * Location manager instance
	 */
	private LocationManager locationManager;

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- METHODS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Insert the last location into database
	 * 
	 * @param lastLocation
	 */
	private void insertNewPosition(Location lastLocation) {
		if (lastLocation != null) {

			String activityName = "";

			if (activity != null) {
				activityName = activityNameFromType(activity.getType());
			}
			TmlPosition newPosition = new TmlPosition(lastLocation,
					activityName, batteryPct);

			/*
			 * Insert the new position only if initial loading is completed
			 */
			if (child.isInitialLoadingCompleted()) {
				child.insertPosition(newPosition);
			}

			Log.d(TAG,
					"New position. Activity: "
							+ activityName
							+ "; Update interval: "
							+ Long.toString(locationRequest.getInterval() / 1000)
							+ "s");

		}
	}

	/**
	 * Rise Location service disabled notification
	 */
	private void checkLocationServices() {

		if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

			Log.i(TAG, "Location services disabled");

			TmlNotification.showNotification(getApplicationContext(), null,
					TmlNotification.NOTIFICATION_ID_LOCATION_DISABLED,
					R.drawable.ic_stat_notify_error,
					R.drawable.notif_large_icon,
					getString(R.string.notification_location_title),
					getString(R.string.notification_location_text), new Intent(
							Settings.ACTION_LOCATION_SOURCE_SETTINGS));

		}

	}

	/**
	 * Retrieve last position from an intent and store it in lastLocation.
	 * newPosition() is then called.
	 * 
	 * @param intent
	 * @return true if location retrieved, false otherwise
	 */
	private boolean getLocationFromIntent(Intent intent) {
		if (intent != null
				&& intent.hasExtra(LocationClient.KEY_LOCATION_CHANGED)) {
			insertNewPosition((Location) intent.getExtras().get(
					LocationClient.KEY_LOCATION_CHANGED));
			return true;
		}
		return false;
	}

	/**
	 * Retrieve activity type from intent and store in activity. If a different
	 * activity from the stored one is detected call setLocationParameters()
	 * 
	 * @param intent
	 * @return true if activity retrieved, false otherwise
	 */
	private boolean getActivityFromIntent(Intent intent) {
		if (ActivityRecognitionResult.hasResult(intent)) {
			ActivityRecognitionResult result = ActivityRecognitionResult
					.extractResult(intent);
			DetectedActivity mostProbableActivity = result
					.getMostProbableActivity();

			if (activity == null) {
				/* No previously detected activity */
				activity = mostProbableActivity;
				setLocationParameters();
			} else if (mostProbableActivity.getType() != activity.getType()) {
				/* New activity detected */
				activity = mostProbableActivity;
				setLocationParameters();
			}
			return true;
		}
		return false;
	}

	/**
	 * Based on activity type, battery level and charge state set the update
	 * interval and the battery-accuracy priority.
	 */
	private void setLocationParameters() {

		if (!googleApiClient.isConnected()) {
			if (!googleApiClient.isConnecting()) {
				Log.e(TAG, "Google API client not connected");
				googleApiClient.connect();
			}
			return;
		}

		/*
		 * Determine battery coefficient used to scale the update interval
		 */
		float batteryCoeff = 1.0f;
		if (batteryLow) {
			batteryCoeff = LOW_BATTERY_RATIO;
		} else if ((batteryStatus == BatteryManager.BATTERY_STATUS_CHARGING && batteryPct > BATTERY_HIGH_LEVEL)
				|| batteryStatus == BatteryManager.BATTERY_STATUS_FULL) {
			batteryCoeff = CHARGE_RATIO;
		}

		if (isGuiVisible) {
			locationRequest
					.setInterval((long) (GUI_UPDATE_INTERVAL * batteryCoeff));
			locationRequest.setFastestInterval(locationRequest.getInterval());
			locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		} else {

			/*
			 * Based on activity type and battery coefficient set update
			 * interval and priority
			 */
			if (activity != null) {
				int activityType = activity.getType();

				switch (activityType) {
				case DetectedActivity.IN_VEHICLE:
					locationRequest
							.setInterval((long) (IN_VEHICLE_UPDATE_INTERVAL * batteryCoeff));
					locationRequest
							.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
					break;
				case DetectedActivity.ON_BICYCLE:
					locationRequest
							.setInterval((long) (ON_BICYCLE_UPDATE_INTERVAL * batteryCoeff));
					locationRequest
							.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
					break;
				case DetectedActivity.ON_FOOT:
					locationRequest
							.setInterval((long) (ON_FOOT_UPDATE_INTERVAL * batteryCoeff));
					locationRequest
							.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
					break;
				case DetectedActivity.RUNNING:
					locationRequest
							.setInterval((long) (RUNNING_UPDATE_INTERVAL * batteryCoeff));
					locationRequest
							.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
					break;
				case DetectedActivity.STILL:
					locationRequest
							.setInterval((long) (STILL_UPDATE_INTERVAL * batteryCoeff));
					locationRequest
							.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
					break;
				case DetectedActivity.TILTING:
					locationRequest
							.setInterval((long) (TILTING_UPDATE_INTERVAL * batteryCoeff));
					locationRequest
							.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
					break;
				case DetectedActivity.UNKNOWN:
					locationRequest
							.setInterval((long) (UNKNOWN_UPDATE_INTERVAL * batteryCoeff));
					locationRequest
							.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
					break;
				case DetectedActivity.WALKING:
					locationRequest
							.setInterval((long) (WALKING_UPDATE_INTERVAL * batteryCoeff));
					locationRequest
							.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
					break;
				}
				locationRequest.setFastestInterval((long) (locationRequest
						.getInterval() * FASTEST_RATIO));

			}
		}

		/*
		 * Refresh the location update request
		 */
		LocationServices.FusedLocationApi.requestLocationUpdates(
				googleApiClient, locationRequest, pendingIntent);
	}

	/**
	 * Convert activity type code into readable string.
	 * 
	 * @param activityType
	 * @return a string describing the activity type
	 */
	private static String activityNameFromType(int activityType) {
		switch (activityType) {
		case DetectedActivity.IN_VEHICLE:
			return "in_vehicle";
		case DetectedActivity.ON_BICYCLE:
			return "on_bicycle";
		case DetectedActivity.ON_FOOT:
			return "on_foot";
		case DetectedActivity.RUNNING:
			return "running";
		case DetectedActivity.STILL:
			return "still";
		case DetectedActivity.TILTING:
			return "tilting";
		case DetectedActivity.UNKNOWN:
			return "unknown";
		case DetectedActivity.WALKING:
			return "walking";
		}
		return "unknown";
	}

	/*
	 * ------------------------------------------------------------------------
	 * ------------------- ACTIVITY LIFECYCLE CALLBACK ------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Called when service is started using startService() or bindService().
	 * Executed only when the process is created
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		TmlTrack.running = true;
		Log.i(TAG, "Creating");

		Thread.currentThread().setName("TmlTrack");

		/*
		 * Instantiate the child
		 */
		try {
			child = TmlChild.getChildrenList(this, childrenListener).get(0);
		} catch (NullPointerException e) {
			Log.e(TAG, "Unable to create the child");
			stopSelf();
		}

		/*
		 * GoogleApiClient instantiate
		 */
		googleApiClient = new GoogleApiClient.Builder(this, googleApiListener,
				googleApiListener).addApi(LocationServices.API)
				.addApi(ActivityRecognition.API).build();

		/*
		 * Create the pending intent used by GoogleApiClient
		 */
		Intent intent = new Intent(this, TmlTrack.class);
		pendingIntent = PendingIntent.getService(this, 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);

		/* Instantiate LocationRequest */
		locationRequest = LocationRequest.create();
		/*
		 * Setup locationRequest power-accuracy priority, update interval and
		 * fastest update interval
		 */
		locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		locationRequest.setInterval(GUI_UPDATE_INTERVAL);
		locationRequest.setFastestInterval(GUI_UPDATE_INTERVAL);

		/*
		 * Setup an intent filter and a broadcast receiver to get updates from
		 * sticky intents broadcasted by BatteryManager
		 */
		IntentFilter ifilter = new IntentFilter();
		ifilter.addAction(Intent.ACTION_BATTERY_CHANGED);
		ifilter.addAction(Intent.ACTION_BATTERY_LOW);
		ifilter.addAction(Intent.ACTION_BATTERY_OKAY);
		registerReceiver(broadcastReceiver, ifilter);

		/*
		 * LocalBroadcastReceiver for GUI visibility
		 */
		ifilter = new IntentFilter();
		ifilter.addAction(TmlServicesManager.BROADCAST_ACTION_GUI_VISIBLE);
		ifilter.addAction(TmlServicesManager.BROADCAST_ACTION_GUI_HIDDEN);
		LocalBroadcastManager.getInstance(this).registerReceiver(
				localBroadcastReceiver, ifilter);

		/*
		 * Check location services and set GPS status listener
		 */
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationManager.addGpsStatusListener(gpsStatusListener);
		checkLocationServicesRunnable.run();

	}

	/**
	 * Called after onCreate() when the service is started with bindService().
	 * Executed each time bindService() is called In this application is not
	 * used.
	 */
	@Override
	public IBinder onBind(Intent intent) {
		Log.e(TAG, "This service should not be started using bindService()");
		return null;
	}

	/**
	 * Called after onCreate() when the service is started with startService().
	 * Executed each time startService() is called. GoogleApiClient call this
	 * method through the pending intent
	 */
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent != null && intent.getExtras() == null) {
			/*
			 * Service started. Connect to googleApiClient.
			 */
			googleApiClient.connect();
		} else {
			/*
			 * Check if the intent contains useful informations about Location
			 * or ActivityRecognition
			 */
			getLocationFromIntent(intent);
			getActivityFromIntent(intent);
		}

		return START_STICKY;
	}

	/**
	 * Called when service is stopped using stopService() of when system
	 * resources are critically low.
	 */
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.i(TAG, "Destroying");

		/*
		 * Disconnect from GoogleApiClient and invalidate pending intent
		 */
		googleApiClient.disconnect();
		pendingIntent.cancel();

		/*
		 * Stop location services check runnable and listener
		 */
		locationManager.removeGpsStatusListener(gpsStatusListener);
		handler.removeCallbacks(checkLocationServicesRunnable);

		/*
		 * Unregister broadcast receiver and local broadcast receiver
		 */
		unregisterReceiver(broadcastReceiver);
		LocalBroadcastManager.getInstance(this).unregisterReceiver(
				localBroadcastReceiver);

		/*
		 * Reset the running flag to indicate that the service has stopped
		 */
		TmlTrack.running = false;
	}

}
