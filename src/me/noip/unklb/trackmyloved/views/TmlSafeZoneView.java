package me.noip.unklb.trackmyloved.views;

import java.util.ArrayList;

import me.noip.unklb.trackmyloved.R;
import me.noip.unklb.trackmyloved.controllers.TmlChild;
import me.noip.unklb.trackmyloved.controllers.TmlSafeZone;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

public class TmlSafeZoneView {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	// private static final String TAG = "TmlSafeZoneView";

	private static final float SAFEZONE_POLYLINE_WIDTH = 2.5f;
	private static final float SAFEZONE_POLYLINE_WIDTH_THIN = 1.5f;

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- ATTRIBUTES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * The safeZone this view represents
	 */
	private final TmlSafeZone safeZone;

	/**
	 * The map this SafeZone belongs
	 */
	private final GoogleMap map;

	/**
	 * List of markers for the corners. Used to draw the safeZone and to show
	 * it.
	 */
	private ArrayList<Marker> markerList = new ArrayList<Marker>();

	/**
	 * Polyline used to draw the safeZone. Not shown when safeZone is completed
	 */
	private Polyline polyline;

	/**
	 * Polyline used to close the drawing safeZone. Not shown when safeZone is
	 * completed
	 */
	private Polyline polylineThin;

	/**
	 * Polygon representing the area of the safeZone when completed.
	 */
	private Polygon polygon;

	/**
	 * Options for the marker
	 */
	private MarkerOptions markerOptions;

	/**
	 * Options for the polygon
	 */
	private PolygonOptions polygonOptions;

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- METHODS -------------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Constructor called by TmlSafeZone.createView
	 * 
	 * @param map
	 * @param safeZone
	 */
	public TmlSafeZoneView(TmlSafeZone safeZone, GoogleMap map) {
		this.safeZone = safeZone;
		this.map = map;
		initMapElements();
	}

	/**
	 * The map this safeZone is drawn
	 * 
	 * @return
	 */
	private GoogleMap getMap() {
		return map;
	}

	/**
	 * Initialize map elements.
	 * 
	 * If the safeZone contains points only the polygon and the marker are
	 * shown, otherwise markerOptions, polyline options, polylineThin options
	 * and polygon options are prepared
	 */
	private void initMapElements() {

		markerOptions = new MarkerOptions();
		markerOptions.icon(BitmapDescriptorFactory
				.fromResource(R.drawable.safezone_point_green));
		markerOptions.anchor(0.5f, 0.5f);

		polygonOptions = new PolygonOptions();
		polygonOptions.fillColor(TmlChild.getContext().getResources()
				.getColor(R.color.safezone_fill_color));
		polygonOptions.strokeColor(TmlChild.getContext().getResources()
				.getColor(R.color.safezone_polyline_color));
		polygonOptions.strokeWidth(SAFEZONE_POLYLINE_WIDTH);

		if (safeZone.getPointList() == null
				|| safeZone.getPointList().size() == 0) {
			/*
			 * This is a drawing safeZone. Prepare polylines
			 */
			PolylineOptions polylineOptions = new PolylineOptions();
			polylineOptions.width(SAFEZONE_POLYLINE_WIDTH);
			polylineOptions.color(TmlChild.getContext().getResources()
					.getColor(R.color.safezone_polyline_color));
			polyline = getMap().addPolyline(polylineOptions);

			polylineOptions.width(SAFEZONE_POLYLINE_WIDTH_THIN);
			polylineThin = getMap().addPolyline(polylineOptions);

		} else {
			/*
			 * Draw the entire safezone but keep it invisible
			 */
			polygonOptions.addAll(safeZone.getPointList());
			polygon = getMap().addPolygon(polygonOptions);
			polygon.setVisible(false);

			for (LatLng point : safeZone.getPointList()) {
				markerOptions.position(point);
				Marker newMarker = getMap().addMarker(markerOptions);
				newMarker.setVisible(false);
				markerList.add(newMarker);
			}

		}

	}

	/**
	 * List of markers representing the vertex of this safeZone
	 * 
	 * @return
	 */
	public ArrayList<Marker> getMarkerList() {
		return markerList;
	}

	/**
	 * Remove all the markers, the polygon and the polylines from the map
	 */
	public void remove() {

		if (markerList != null) {
			for (Marker markerTemp : markerList) {
				markerTemp.remove();
			}
		}
		if (polygon != null) {
			polygon.remove();
		}
		if (polyline != null) {
			polyline.remove();
		}
		if (polylineThin != null) {
			polylineThin.remove();
		}

	}

	/**
	 * Set safe zone visibility. Shows marker and polyline (not the thin one)
	 * 
	 * @param visibility
	 */
	public void setVisible(boolean visibility) {
		for (Marker markerTemp : markerList) {
			markerTemp.setVisible(visibility);
		}
		polygon.setVisible(visibility);
	}

	/**
	 * Returns the visibility of this safeZone
	 * 
	 * @return
	 */
	public boolean isVisible() {
		return polygon.isVisible();
	}

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- DRAWING METHODS -----------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Add a new point to the safeZone being drawn
	 */
	public void addPoint(LatLng point) {

		markerOptions.position(point);
		markerList.add(getMap().addMarker(markerOptions));

		regeneratePolylines();

	}

	/**
	 * Remove last inserted point from the safeZone being drawn
	 */
	public void removeLastPoint() {
		markerList.get(markerList.size() - 1).remove();
		markerList.remove(markerList.size() - 1);

		regeneratePolylines();
	}

	/**
	 * Regenerate the polyline of the safeZone being drawn
	 */
	private void regeneratePolylines() {
		if (safeZone.getPointList().size() > 2) {
			ArrayList<LatLng> thinLatLng = new ArrayList<LatLng>();
			thinLatLng.add(safeZone.getPointList().get(
					safeZone.getPointList().size() - 1));
			thinLatLng.add(safeZone.getPointList().get(0));
			polylineThin.setPoints(thinLatLng);
			polylineThin.setVisible(true);
		} else {
			polylineThin.setVisible(false);
		}

		polyline.setPoints(safeZone.getPointList());
	}

	/**
	 * Called when the building of this safeZone has finished
	 */
	public void drawingDone() {
		/*
		 * Delete polylines
		 */
		polyline.remove();
		polyline = null;

		polylineThin.remove();
		polylineThin = null;

		/*
		 * Create polygon
		 */
		polygonOptions.addAll(safeZone.getPointList());
		polygon = getMap().addPolygon(polygonOptions);
	}

}
