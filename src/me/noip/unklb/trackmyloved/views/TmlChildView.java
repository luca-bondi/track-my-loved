package me.noip.unklb.trackmyloved.views;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import me.noip.unklb.trackmyloved.R;
import me.noip.unklb.trackmyloved.controllers.TmlChild;
import me.noip.unklb.trackmyloved.controllers.TmlColor;
import me.noip.unklb.trackmyloved.controllers.TmlMarker;
import me.noip.unklb.trackmyloved.controllers.TmlPosition;
import me.noip.unklb.trackmyloved.controllers.TmlSafeZone;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

/**
 * Child map elements controller. All the methods that perform actions on the UI
 * must be called by the UI thread
 */
public class TmlChildView {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	// private static final String TAG = "TmlChildView";

	private static final int TRACK_WIDTH = 7;

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- ATTRIBUTES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * The color assigned to this child
	 */
	private final TmlColor color;

	/**
	 * Marker representing the last filtered position
	 */
	private TmlMarker lastPositionMarker;

	/**
	 * List of Marker representing the static positions on the map
	 */
	private ArrayList<TmlMarker> placeMarkerList = new ArrayList<TmlMarker>();

	/**
	 * Polyline representing the filtered path of the user in the last
	 * historyInterval hours
	 */
	private Polyline track;

	/**
	 * The child that instantiated this class
	 */
	private final TmlChild child;
	/**
	 * The map this view draws on
	 */
	private final GoogleMap map;

	/**
	 * Set indicates that at next updateMarker the marker should be created
	 */
	private boolean markerDelayedCreation = false;

	/**
	 * Set indicates that at next updateTrackMarkerList the trackMarkerList
	 * should be created
	 */
	private boolean placeMarkerListDelayedCreation = false;

	/**
	 * Set indicates that at next updateTrack the track should be created
	 */
	private boolean trackDelayedCreation = false;

	/**
	 * Set when markerShow is called but marker still not created
	 */
	private boolean markerShow = false;

	/**
	 * Set when trackMarkerListShow is called but trackMsrkerList still not
	 * created
	 */
	private boolean placeMarkerListShow = false;

	/**
	 * Set when trackShow is called but track still not created
	 */
	private boolean trackShow = false;

	/**
	 * Set to true when marker creation is done
	 */
	private boolean markerCreationCompleted = false;
	/**
	 * Set to true when track creation is done
	 */
	private boolean trackCreationCompleted = false;
	/**
	 * Set to true when trackMarkerList creation is done
	 */
	private boolean placeMarkerListCreationCompleted = false;

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- METHODS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Constructor called only by TmlChild
	 * 
	 * @param map
	 * @param child
	 *            child that instantiated this view
	 */
	public TmlChildView(GoogleMap map, TmlChild child) {
		this.map = map;
		this.child = child;
		color = TmlColor.getColor();

		/*
		 * Initialize all map elements to be fast when user wants to see
		 * something
		 */
		if (TmlChild.isParentDevice()) {
			for (TmlSafeZone safeZone : child.getSafeZoneList()) {
				safeZone.createView(map);
			}
		}

		createMarker();
		createPlaceMarkerList();
		createTrack();

	}

	/**
	 * The color of this child
	 * 
	 * @return
	 */
	public TmlColor getColor() {
		return color;
	}

	/**
	 * The map this view draws on
	 * 
	 * @return
	 */
	public GoogleMap getMap() {
		return map;
	}

	/**
	 * The child this view is associated
	 * 
	 * @return
	 */
	public TmlChild getChild() {
		return child;
	}

	/**
	 * Cleans from places the reference to the cluster marker
	 */
	public void cleanClusteringReference() {
		for (TmlMarker placeMarker : placeMarkerList) {
			if (placeMarker.getPlace() != null) {
				placeMarker.getPlace().setClusterMarker(null);
			}
		}

	}

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- MAP ELEMENTS METHODS ------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * True if all the map elements have been created (except safezone). When
	 * true the map can be centered
	 * 
	 * @return
	 */
	public boolean isInitialViewCreationCompleted() {
		return markerCreationCompleted & trackCreationCompleted
				& placeMarkerListCreationCompleted;
	}

	/**
	 * Creates a lastPositionMarker on the lastFilteredPosition. markerPosition
	 * is set to lastFilteredPosition.
	 * 
	 * Marker is created hidden if markerShow is unset
	 * 
	 * @return
	 */
	private TmlMarker createMarker() {

		if (lastPositionMarker != null) {
			deleteMarker();
		}

		if (!child.isInitialLoadingCompleted()
				|| child.getLastFilteredPosition() == null) {
			markerDelayedCreation = true;
			return null;
		}

		lastPositionMarker = TmlMarker.getLastPositionMarker(this);

		if (!markerShow) {
			hideMarker();
		}

		/*
		 * Notify the UI that the view creation is complete
		 */
		if (!markerCreationCompleted) {
			markerCreationCompleted = true;
			if (isInitialViewCreationCompleted()) {
				child.triggerUpdate();
			}
		}

		return lastPositionMarker;
	}

	/**
	 * Create place markers showing static positions
	 * 
	 * @return
	 */
	private ArrayList<TmlMarker> createPlaceMarkerList() {

		if (child.getPlaceList().size() == 0) {
			return null;
		}

		if (!child.isInitialLoadingCompleted()) {
			placeMarkerListDelayedCreation = true;
			return null;
		}

		deletePlaceMarkerList();

		placeMarkerList = TmlMarker.getPlaceMarkerList(this);

		if (!placeMarkerListShow) {
			hidePlaceMarkerList();
		}

		/*
		 * Notify the UI that the view creation is complete
		 */
		if (!placeMarkerListCreationCompleted) {
			placeMarkerListCreationCompleted = true;
			if (isInitialViewCreationCompleted()) {
				child.triggerUpdate();
			}
		}

		return placeMarkerList;

	}

	/**
	 * Create track
	 */
	private Polyline createTrack() {

		if (track != null) {
			deleteTrack();
		}

		if (!child.isInitialLoadingCompleted()) {
			trackDelayedCreation = true;
			return null;
		}

		ArrayList<TmlPosition> trackPositions = child.getFilteredPositionList();

		if (trackPositions.size() == 0) {
			trackDelayedCreation = true;
			return null;
		}

		ArrayList<LatLng> points = positionsToPlacePoints(trackPositions);
		points.add(child.getLastFilteredPosition().getCoordinates());

		track = map.addPolyline(new PolylineOptions()
				.addAll(points)
				.color(TmlChild.getContext().getResources()
						.getColor(R.color.track_color)).geodesic(true)
				.width(TRACK_WIDTH));

		if (!trackShow) {
			hideTrack();
		}

		/*
		 * Notify the UI that the view creation is complete
		 */
		if (!trackCreationCompleted) {
			trackCreationCompleted = true;
			if (isInitialViewCreationCompleted()) {
				child.triggerUpdate();
			}
		}

		return track;

	}

	/**
	 * Called when an update of the map view components is necessary (i.e. after
	 * receiving new position intent)
	 * 
	 */
	public void update() {
		if (child.isInitialLoadingCompleted()) {
			updateMarker();
			updateTrack();
			updateTrackMarkerList();
			if (TmlChild.isParentDevice()) {
				updateSafeZoneList();
			}
		}
	}

	/**
	 * Updates the existing lastPositionMarker
	 * 
	 * @return
	 */
	private TmlMarker updateMarker() {
		if (getLastPositionMarker() == null) {
			if (markerDelayedCreation) {
				return createMarker();
			}
			return null;
		}
		return createMarker();
	}

	/**
	 * Update existing track markers
	 * 
	 * @return
	 */
	private ArrayList<TmlMarker> updateTrackMarkerList() {
		if (placeMarkerList == null) {
			if (placeMarkerListDelayedCreation) {
				return createPlaceMarkerList();
			}
			return null;
		}
		return createPlaceMarkerList();
	}

	/**
	 * Update existing track
	 */
	private Polyline updateTrack() {
		if (track == null) {
			if (trackDelayedCreation) {
				return createTrack();
			}
			return null;
		}
		return createTrack();
	}

	/**
	 * Updates safe zone
	 */
	private void updateSafeZoneList() {
		ArrayList<TmlSafeZone> toBeDeletedList = new ArrayList<TmlSafeZone>();
		for (TmlSafeZone safeZone : child.getSafeZoneList()) {
			if (safeZone.isDeleted()) {
				if (safeZone.getView() != null) {
					safeZone.getView().remove();
				}
				toBeDeletedList.add(safeZone);
			} else if (safeZone.getView() == null && !safeZone.toBeRemoved()
					&& !safeZone.isDeleted()) {
				safeZone.createView(map);
			}
		}
		child.getSafeZoneList().removeAll(toBeDeletedList);
	}

	/**
	 * Delete the existing lastPositionMarker
	 */
	private void deleteMarker() {
		if (lastPositionMarker != null) {
			lastPositionMarker.remove();
			lastPositionMarker = null;
		}
	}

	/**
	 * Delete existing track markers
	 */
	private void deletePlaceMarkerList() {
		if (placeMarkerList != null) {
			for (TmlMarker markerTemp : placeMarkerList) {
				markerTemp.remove();
			}
			placeMarkerList = null;
		}
	}

	/**
	 * Delete the existing track
	 */
	private void deleteTrack() {
		if (track != null) {
			track.remove();
			track = null;
		}
	}

	/**
	 * Marker for the last filtered position.
	 * 
	 * @return TmlMarker, null if not yet created
	 */
	public TmlMarker getLastPositionMarker() {
		return lastPositionMarker;
	}

	/**
	 * Place markers
	 * 
	 * @return ArrayList<TmlMarker>, null if not yet created
	 */
	public ArrayList<TmlMarker> getPlaceMarkerList() {
		return placeMarkerList;
	}

	/**
	 * Polyline track for the last filtered position.
	 * 
	 * @return Polyline, null if not yet created
	 */
	public Polyline getTrack() {
		return track;
	}

	/**
	 * Show the existing lastPositionMarker
	 */
	public void showLastPositionMarker() {
		if (lastPositionMarker != null) {
			lastPositionMarker.setVisible(true);
		}
		markerShow = true;

	}

	/**
	 * Show the existing track markers
	 */
	public void showPlaceMarkerList() {
		if (placeMarkerList != null) {
			for (TmlMarker placeMarker : placeMarkerList) {
				placeMarker.setVisible(true);
			}
		}
		placeMarkerListShow = true;
	}

	/**
	 * Show the existing track
	 */
	public void showTrack() {
		if (track != null) {
			track.setVisible(true);
		}
		trackShow = true;

	}

	/**
	 * Show existing safezone polygons
	 */
	public void showSafeZoneViewList() {
		if (child.getSafeZoneList() != null) {
			for (TmlSafeZone safeZone : child.getSafeZoneList()) {
				if (safeZone.getView() != null) {
					safeZone.getView().setVisible(true);
				}
			}
		}
	}

	/**
	 * Hide all the maps elements related to the child
	 */
	public void hide() {
		hideMarker();
		hidePlaceMarkerList();
		hideTrack();

		if (TmlChild.isParentDevice()) {
			hideSafeZoneViewList();
		}
	}

	/**
	 * Hide the existing lastPositionMarker
	 */
	private void hideMarker() {
		if (lastPositionMarker != null) {
			lastPositionMarker.setVisible(false);
		}
		markerShow = false;
	}

	/**
	 * Hide all track markers
	 */
	public void hidePlaceMarkerList() {
		if (placeMarkerList != null) {
			for (TmlMarker markerTemp : placeMarkerList) {
				markerTemp.setVisible(false);
			}
		}
		placeMarkerListShow = false;
	}

	/**
	 * Hide the existing track
	 */
	public void hideTrack() {
		if (track != null) {
			track.setVisible(false);
		}
		trackShow = false;
	}

	/**
	 * Hide existing safeZone views
	 */
	private void hideSafeZoneViewList() {
		if (child.getSafeZoneList() != null) {
			for (TmlSafeZone safeZone : child.getSafeZoneList()) {
				if (safeZone.getView() != null) {
					safeZone.getView().setVisible(false);
				}
			}
		}
	}

	/**
	 * Reset the highlight for all the places
	 */
	public void cleanHighlighting() {
		for (TmlMarker placeMarker : placeMarkerList) {
			placeMarker.setHighlighted(false);
		}
	}

	/*
	 * ------------------------------------------------------------------------
	 * --------- INFO VIEW / NAVIGATION DRAWER METHODS ------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Safeness icon resource id
	 * 
	 * @return
	 */
	public int getSafenessResource() {
		switch (child.getSafeness()) {
		case UNSAFE:
			return R.drawable.safeness_red;
		case SAFE:
			return R.drawable.safeness_green;
		default:
			return R.drawable.safeness_gray;
		}
	}

	/**
	 * Freshness icon resource id
	 * 
	 * @return
	 */
	public int getFreshnessResource() {
		switch (child.getFreshness()) {
		case BAD:
			return R.drawable.time_yellow;
		case GOOD:
			return R.drawable.time_green;
		default:
			return R.drawable.time_red;
		}
	}

	/**
	 * Battery charge resource id
	 * 
	 * @return
	 */
	public int getBatteryChargeResource() {
		switch (child.getBatteryCharge()) {
		case LOW:
			return R.drawable.battery_yellow;
		case OK:
			return R.drawable.battery_green;
		default:
			return R.drawable.battery_red;
		}

	}

	/**
	 * String describing the level of safeness
	 * 
	 * @return
	 */
	public String getSafenessString() {
		switch (child.getSafeness()) {
		case UNSAFE:
			return TmlChild.getContext().getString(
					R.string.map_infoView_safeness_unsafe);
		case SAFE:
			return TmlChild.getContext().getString(
					R.string.map_infoView_safeness_safe);
		default:
			return TmlChild.getContext().getString(
					R.string.map_infoView_safeness_unknown);
		}
	}

	/**
	 * The string used in infoView to show place information
	 * 
	 * @return
	 */
	public String getInfoTimeText(int selectedPositionIdx) {
		TmlPosition position = child.getFilteredPositionList().get(
				selectedPositionIdx);
		long firstTime = position.getFirstTimestamp();
		long lastTime = position.getLastTimestamp();

		if (isOnToday(firstTime) && isOnToday(lastTime)) {
			int resId;
			if (TmlChild.isParentDevice()) {
				resId = R.string.map_infoView_today_time_parent;
			} else {
				resId = R.string.map_infoView_today_time_child;
			}
			return TmlChild.getContext().getString(resId, getTime(firstTime),
					getTime(lastTime));
		}

		if (isOnYesterday(firstTime) && isOnYesterday(lastTime)) {
			int resId;
			if (TmlChild.isParentDevice()) {
				resId = R.string.map_infoView_yesterday_time_parent;
			} else {
				resId = R.string.map_infoView_yesterday_time_child;
			}
			return TmlChild.getContext().getString(resId, getTime(firstTime),
					getTime(lastTime));
		}

		if (areOnSameDate(firstTime, lastTime)) {
			return TmlChild.getContext().getString(
					R.string.map_infoView_sameDate_time, getDate(firstTime),
					getTime(firstTime), getTime(lastTime));
		}

		int resId;
		if (TmlChild.isParentDevice()) {
			resId = R.string.map_infoView_other_time_parent;
		} else {
			resId = R.string.map_infoView_other_time_child;
		}
		return TmlChild.getContext().getString(resId, getDateTime(firstTime),
				getDateTime(lastTime));
	}

	/**
	 * Formats timestamp as date according to locale settings
	 * 
	 * @return
	 */
	public String getDate() {
		if (child.getLastFilteredPosition() == null) {
			return "";
		}
		return getDate(child.getLastFilteredPosition().getTimestamp());
	}

	/**
	 * Formats timestamp as time according to locale settings
	 * 
	 * @return
	 */
	public String getTime() {
		if (child.getLastFilteredPosition() == null) {
			return "";
		}
		return getTime(child.getLastFilteredPosition().getTimestamp());
	}

	/**
	 * Battery charge percentage (ex. 86%)
	 * 
	 * @return
	 */
	public String getBatteryChargeString() {

		if (child.getLastFilteredPosition() == null) {
			return "";
		}

		return Integer.toString((int) (child.getLastFilteredPosition()
				.getBattery() * 100)) + "%";
	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- STATIC METHODS ---------------------------
	 * ------------------------------------------------------------------------
	 */
	/**
	 * Check if two timestamps are on the same day of the year. The year is not
	 * checked since this application conserves data only for 7 days
	 * 
	 * @param ts1
	 * @param ts2
	 * @return
	 */
	private static boolean areOnSameDate(long ts1, long ts2) {
		Calendar cal1 = new GregorianCalendar();
		Calendar cal2 = new GregorianCalendar();
		cal1.setTimeInMillis(ts1);
		cal2.setTimeInMillis(ts2);
		return cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
	}

	/**
	 * Determine if the given timestamp is between yesterday midnight to today
	 * midnight
	 * 
	 * @param ts
	 * @return
	 */
	private static boolean isOnYesterday(long ts) {
		if (ts >= getMidnight(-1) && ts < getMidnight(0)) {
			return true;
		}
		return false;
	}

	/**
	 * Determine if the given timestamp is after today midnight
	 * 
	 * @param ts
	 * @return
	 */
	private static boolean isOnToday(long ts) {
		if (ts >= getMidnight(0)) {
			return true;
		}
		return false;
	}

	/**
	 * Get timestamp of midnight. Offset 0 means last (today) midnight
	 * 
	 * @param offset
	 *            (<=0)
	 */
	private static long getMidnight(int offset) {
		Calendar date = new GregorianCalendar();

		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);

		date.add(Calendar.DAY_OF_MONTH, offset);

		return date.getTimeInMillis();
	}

	/**
	 * Formats timestamp as date and time according to locale settings
	 * 
	 * @return
	 */
	private static String getDateTime(long timestamp) {
		SimpleDateFormat sdf = (SimpleDateFormat) DateFormat
				.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
		sdf.applyPattern(sdf.toPattern().replaceAll("[^\\p{Alpha}]*y+", ""));
		return sdf.format(new Date(timestamp));
	}

	/**
	 * Formats timestamp as date according to locale settings
	 * 
	 * @return
	 */
	private static String getDate(long timestamp) {
		SimpleDateFormat sdf = (SimpleDateFormat) DateFormat
				.getDateInstance(DateFormat.SHORT);
		sdf.applyPattern(sdf.toPattern().replaceAll(
				"[^\\p{Alpha}]*y+[^\\p{Alpha}]*", ""));
		return sdf.format(new Date(timestamp));
	}

	/**
	 * Formats timestamp as time according to locale settings
	 * 
	 * @return
	 */
	public static String getTime(long timestamp) {
		return DateFormat.getTimeInstance(DateFormat.SHORT).format(
				new Date(timestamp));
	}

	/**
	 * Converts ArrayList of TmlPosition to ArrayList of LatLng take from place
	 * position
	 * 
	 * @param positionList
	 * @return
	 */
	private static ArrayList<LatLng> positionsToPlacePoints(
			ArrayList<TmlPosition> positionList) {
		ArrayList<LatLng> points = new ArrayList<LatLng>();
		for (int idx = 0; idx < positionList.size(); idx++) {
			points.add(positionList.get(idx).getPlace().getPosition()
					.getCoordinates());
		}
		return points;
	}
}
