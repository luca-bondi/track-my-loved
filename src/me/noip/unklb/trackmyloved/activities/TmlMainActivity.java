package me.noip.unklb.trackmyloved.activities;

import me.noip.unklb.trackmyloved.R;
import me.noip.unklb.trackmyloved.activities.TmlDialogFragment.MessageDialogListener;
import me.noip.unklb.trackmyloved.controllers.TmlDatabase;
import me.noip.unklb.trackmyloved.services.TmlServicesManager;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

/**
 * MainActivity is used only as routing activity.
 * 
 * It checks for Google Play Services. If not available prompts for
 * installation. No actions are allowed until Google Play Services are
 * installed.
 * 
 * If setup is not complete SetupActivity is started, otherwise MapActivity is
 * started
 */
public class TmlMainActivity extends ActionBarActivity implements
		MessageDialogListener {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	// private static final String TAG = "MainActivity";

	/**
	 * Id for Google Play Services error dialog
	 */
	private static final int GOOGLE_PLAY_DIALOG_ID = 1001;

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- ATTRIBUTES -------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Google Play Services missing dialog
	 */
	private Dialog googlePlayServicesDialog;

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- UI LISTENERS -------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Called when Google Play Services missing dialog is canceled
	 */
	private DialogInterface.OnCancelListener googlePlayServicesCancelListener = new DialogInterface.OnCancelListener() {

		@Override
		public void onCancel(DialogInterface dialog) {
			/*
			 * If GooglePlayServices missing dialog is canceled show another
			 * dialog that explains that the services are necessary.
			 */
			new TmlDialogFragment().setMessage(
					R.string.dialog_google_play_services_necessary).show(
					getSupportFragmentManager(), "TmlMainActivity");
		}

	};

	/**
	 * Called when a message dialog positive button is clicked
	 */
	@Override
	public void messageDialogPositive() {
		finish();
	}

	/**
	 * Called when a message dialog negative button is clicked
	 */
	@Override
	public void messageDialogNegative() {
		/*
		 * Show again Google Play Services missing dialog
		 */
		if (GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(getBaseContext()) != ConnectionResult.SUCCESS) {
			googlePlayServicesDialog.show();
		}
	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- LIFECYCLE METHODS ------------------------
	 * ------------------------------------------------------------------------
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		TmlDatabase db = TmlDatabase.getInstance(this);
		Intent intent = getIntent();
		if (intent != null && intent.getAction() != null) {
			if (intent.getAction().equals(
					TmlServicesManager.INTENT_ACTION_CHILD_DEVICE_RESET)) {
				if (db.isChildDevice()) {
					Intent sendIntent = new Intent(this, TmlMapActivity.class);
					sendIntent
							.setAction(TmlServicesManager.INTENT_ACTION_CHILD_DEVICE_RESET);
					startActivity(sendIntent);
					/* Stop Services */
					TmlServicesManager.resetDevice(this, false, false);
					finish();
				}
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) != ConnectionResult.SUCCESS) {
			/*
			 * If google play services are not installed prompt for installation
			 * and don't proceed until are installed
			 */
			googlePlayServicesDialog = GooglePlayServicesUtil.getErrorDialog(
					GooglePlayServicesUtil.isGooglePlayServicesAvailable(this),
					this, GOOGLE_PLAY_DIALOG_ID,
					googlePlayServicesCancelListener);

			googlePlayServicesDialog.show();
		} else {
			/*
			 * If the device has not been configured start TmlSetupActivity,
			 * otherwise start TmlMapActivity
			 */
			Intent intent;
			TmlDatabase db = TmlDatabase.getInstance(this);
			if (!db.isRoleDefined()) {
				intent = new Intent(this, TmlSetupActivity.class);
			} else {
				TmlServicesManager.startServices(this);
				intent = new Intent(this, TmlMapActivity.class);
			}
			startActivity(intent);
			finish();
		}
	}

}
