package me.noip.unklb.trackmyloved.activities;

import java.util.List;

import me.noip.unklb.trackmyloved.R;
import me.noip.unklb.trackmyloved.controllers.TmlChild;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * NavigationDrawer Array Adapter controller
 */
public class TmlNavigationDrawerAdapter extends ArrayAdapter<TmlChild> {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */
	/**
	 * Resource id navigation drawer item
	 */
	private static final int childLayoutId = R.layout.navigation_drawer_item;

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- CLASSES ----------------------------------
	 * ------------------------------------------------------------------------
	 */
	/**
	 * Drawer holder for all the view inside a navigation drawer item
	 */
	private static class DrawerItemHolder {
		TextView name;
		ImageView safeness;
		ImageView freshness;
		ImageView batteryCharge;
		ImageView square;
		TextView all;
	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- ATTRIBUTES -------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * LayoutInflater from the calling activity
	 */
	private LayoutInflater layoutInflater;

	/**
	 * Children list with null as last element
	 */
	private List<TmlChild> childrenList;

	/**
	 * Selected child
	 */
	private TmlChild selectedChild;

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- METHODS ---------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Constructor
	 * 
	 * @param activity
	 * @param childrenList
	 */
	public TmlNavigationDrawerAdapter(TmlMapActivity activity,
			List<TmlChild> childrenList) {
		super(activity, childLayoutId, childrenList);
		this.layoutInflater = activity.getLayoutInflater();
		this.childrenList = childrenList;
		/*
		 * Null element added to indicate the "Show all children" item
		 */
		this.childrenList.add(null);
	}

	/**
	 * Called to get the view for a specific item
	 * 
	 * @param positionThe
	 *            position of the item within the adapter's data set of the item
	 *            whose view we want.
	 * @param convertView
	 *            The old view to reuse, if possible. Can be null.
	 * @param parent
	 *            The parent that this view will eventually be attached to
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		DrawerItemHolder drawerHolder;

		if (convertView == null) {

			/*
			 * Instantiate and populate the drawerHolder with the view items
			 */
			drawerHolder = new DrawerItemHolder();

			convertView = layoutInflater.inflate(childLayoutId, parent, false);
			drawerHolder.name = (TextView) convertView
					.findViewById(R.id.navigation_drawer_item_name);
			drawerHolder.square = (ImageView) convertView
					.findViewById(R.id.navigation_drawer_item_color);
			drawerHolder.safeness = (ImageView) convertView
					.findViewById(R.id.navigation_drawer_item_safeness);
			drawerHolder.freshness = (ImageView) convertView
					.findViewById(R.id.navigation_drawer_item_freshness);
			drawerHolder.batteryCharge = (ImageView) convertView
					.findViewById(R.id.navigation_drawer_item_batterycharge);
			drawerHolder.all = (TextView) convertView
					.findViewById(R.id.navigation_drawer_item_all);

			convertView.setTag(drawerHolder);

		} else {
			/*
			 * Retrieve the drawer holder from the view
			 */
			drawerHolder = (DrawerItemHolder) convertView.getTag();
		}

		/*
		 * Retrieve the child
		 */
		TmlChild child = childrenList.get(position);

		if (child == null) {
			/*
			 * Show all children element
			 */
			drawerHolder.square.setVisibility(View.GONE);
			drawerHolder.safeness.setVisibility(View.GONE);
			drawerHolder.freshness.setVisibility(View.GONE);
			drawerHolder.batteryCharge.setVisibility(View.GONE);
			drawerHolder.name.setVisibility(View.GONE);
			drawerHolder.all.setVisibility(View.VISIBLE);
			if (selectedChild == null) {
				drawerHolder.all.setTypeface(null, Typeface.BOLD);
			} else {
				drawerHolder.all.setTypeface(null, Typeface.NORMAL);
			}

		} else {
			/*
			 * Child element
			 */
			drawerHolder.square.setVisibility(View.VISIBLE);
			drawerHolder.safeness.setVisibility(View.VISIBLE);
			drawerHolder.freshness.setVisibility(View.VISIBLE);
			drawerHolder.batteryCharge.setVisibility(View.VISIBLE);
			drawerHolder.name.setVisibility(View.VISIBLE);
			drawerHolder.all.setVisibility(View.GONE);

			if (child.getView() != null) {
				drawerHolder.square.setImageResource(child.getView().getColor()
						.getSquareResource());
				drawerHolder.safeness.setImageResource(child.getView()
						.getSafenessResource());
				drawerHolder.freshness.setImageResource(child.getView()
						.getFreshnessResource());
				drawerHolder.batteryCharge.setImageResource(child.getView()
						.getBatteryChargeResource());
			}
			drawerHolder.name.setText(child.getName());

			if (child.equals(selectedChild)) {
				drawerHolder.name.setTypeface(null, Typeface.BOLD);
			} else {
				drawerHolder.name.setTypeface(null, Typeface.NORMAL);
			}
		}

		return convertView;
	}

	/**
	 * Set the selected child and notify the dataset change to bold the text
	 * 
	 * @param selectedChild
	 *            null if all children visible
	 */
	public void setSelectedChild(TmlChild selectedChild) {
		this.selectedChild = selectedChild;
		notifyDataSetChanged();
	}

	/**
	 * Add a child to the navigation drawer
	 * 
	 * @param newChild
	 */
	public void addChild(TmlChild newChild) {
		this.insert(newChild, childrenList.size() - 1);
		notifyDataSetChanged();
	}

	/**
	 * Remove the child from the navigation drawer
	 * 
	 * @param deletedChild
	 */
	public void deleteChild(TmlChild deletedChild) {
		this.remove(deletedChild);
		notifyDataSetChanged();
	}

}