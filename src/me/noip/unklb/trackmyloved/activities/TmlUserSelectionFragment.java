package me.noip.unklb.trackmyloved.activities;

import java.util.ArrayList;

import me.noip.unklb.trackmyloved.R;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

/**
 * Controller for the User Selection Fragment
 */
public class TmlUserSelectionFragment extends Fragment {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- INTERFACES ---------------------------------
	 * ------------------------------------------------------------------------
	 */
	/**
	 * Callback interface to be implemented by the activity that hosts this
	 * fragment
	 */
	public interface UserSelectionFragmentCallbacks {
		/**
		 * Called when new user button is pressed
		 */
		void userSelectionButtonNewUserCallback();

		/**
		 * Called when cancel button is pressed
		 */
		void userSelectionButtonCancelCallback();

		/**
		 * Called when confirm button is pressed
		 * 
		 * @param user
		 */
		void userSelectionButtonConfirmCallback(String user);

	}

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- UI LISTENERS -------------------------------
	 * ------------------------------------------------------------------------
	 */
	/**
	 * User buttons listener
	 */
	private OnClickListener userSelectListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Button buttonView = (Button) v;

			unlockButton();

			if (selectedUserButton != null) {
				selectedUserButton
						.setBackgroundResource(R.drawable.selector_button_user_list);
			}
			selectedUserButton = buttonView;
			for (String userTemp : userList) {
				if (buttonView.getText().equals(userTemp)) {
					user = userTemp;
					buttonView
							.setBackgroundResource(R.drawable.selector_button_user_list_selected);
					return;
				}
			}
		}

	};

	/**
	 * Cancel button listener.
	 */
	private OnClickListener cancelButtonClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			hostActivity.userSelectionButtonCancelCallback();
		}

	};

	/**
	 * Confirm button listener.
	 */
	private OnClickListener confirmButtonClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (user != null) {
				hostActivity.userSelectionButtonConfirmCallback(user);
			}
		}

	};

	/**
	 * New user button listener.
	 */
	private OnClickListener newUserButtonClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			hostActivity.userSelectionButtonNewUserCallback();
		}

	};

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- ATTRIBUTES -------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Activity that hosts this fragment and implements the
	 * UserSelectionFragmentCallbacks
	 */
	private UserSelectionFragmentCallbacks hostActivity;

	/**
	 * User list
	 */
	private ArrayList<String> userList;

	/**
	 * Selected user
	 */
	private String user;

	/*
	 * Buttons
	 */
	private Button selectedUserButton;
	private Button okButton;
	private Button cancelButton;
	private Button newUserButton;

	/**
	 * Layout in which user buttons will be added
	 */
	private LinearLayout userSelectionLayout;

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- LIFECYCLE METHODS ------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Called when fragment attached to the activity. Try to cast the activity
	 * to the fragment callback. If cast failed throw exception
	 * 
	 * @param activity
	 *            host activity
	 * @throws ClassCastException
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			hostActivity = (UserSelectionFragmentCallbacks) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement UserSelectionFragmentCallbacks");
		}
	}

	/**
	 * Called when the activity requests the fragment to create its view.
	 * 
	 * @param inflater
	 * @param container
	 *            host activity container where the fragment should inflate
	 *            itself
	 * @param savedInstanceState
	 * @return the inflated view
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.setup_user_selection_fragment,
				container, false);
	}

	/**
	 * Called when the activity has been rendered. Not it's possible to use
	 * getActivity().findViewById() to get reference to the UI views
	 * 
	 * @param savedInstanceState
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		userSelectionLayout = (LinearLayout) getActivity().findViewById(
				R.id.user_selection_layout);

		okButton = (Button) getActivity().findViewById(
				R.id.user_selection_button_ok);
		okButton.setOnClickListener(confirmButtonClickListener);

		cancelButton = (Button) getActivity().findViewById(
				R.id.user_selection_button_cancel);
		cancelButton.setOnClickListener(cancelButtonClickListener);

		newUserButton = (Button) getActivity().findViewById(
				R.id.user_selection_button_new_child);
		newUserButton.setOnClickListener(newUserButtonClickListener);

		if (userList != null) {
			/*
			 * Generate user buttons
			 */
			LayoutParams layoutParams = new LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			layoutParams.gravity = Gravity.CENTER;
			for (String user : userList) {
				Button newButton = new Button(getActivity());
				newButton
						.setBackgroundResource(R.drawable.selector_button_user_list);
				newButton.setText(user);
				newButton.setOnClickListener(userSelectListener);
				userSelectionLayout.addView(newButton, layoutParams);
			}
		}
		cleanSelection();

	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- GENERIC METHODS --------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Set the users loaded for this account.
	 * 
	 * @param users
	 */
	public void setUsers(ArrayList<String> users) {
		this.userList = users;
	}

	/**
	 * Clean the selected user and lock OK button
	 */
	public void cleanSelection() {
		user = null;
		if (selectedUserButton != null) {
			selectedUserButton
					.setBackgroundResource(R.drawable.selector_button_user_list);
			selectedUserButton = null;
		}
		lockButton();
	}

	/**
	 * Disable OK button and change background
	 */
	private void lockButton() {
		/*
		 * Change button drawable and lock
		 */
		okButton.setEnabled(false);

		okButton.setTextColor(getResources().getColor(
				R.color.setup_activity_button_disabled_textColor));

		okButton.setBackgroundResource(R.drawable.selector_button_disabled);
	}

	/**
	 * Enable OK button and change background
	 */
	private void unlockButton() {
		/*
		 * Change button drawable and unlock
		 */
		okButton.setEnabled(true);

		okButton.setTextColor(getResources().getColor(
				R.color.setup_activity_button_textColor));

		okButton.setBackgroundResource(R.drawable.selector_button_green);
	}

}
