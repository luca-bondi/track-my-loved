package me.noip.unklb.trackmyloved.activities;

import me.noip.unklb.trackmyloved.R;
import me.noip.unklb.trackmyloved.controllers.TmlAccount;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

/**
 * Controller for the Account Login Fragment
 */
public class TmlAccountLoginFragment extends Fragment {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	// private static final String TAG = "TmlAccountLoginFragment";

	/**
	 * Bundle key used to save email
	 */
	private static final String BUNDLE_KEY_EMAIL = "email";

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- INTERFACES ---------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Callback interface to be implemented by the activity that hosts this
	 * fragment
	 */
	public interface AccountLoginFragmentCallbacks {
		/**
		 * Called when credentials are valid and Login button is clicked
		 * 
		 * @param email
		 * @param password
		 */
		public void accountLoginButtonLoginCallback(String email,
				String password);

		/**
		 * Called when Signup button is clicked
		 * 
		 * @param email
		 */
		public void accountLoginButtonSignupCallback(String email);
	}

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- UI LISTENERS -------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Password EditText action listener to let Enter and Done actions perform
	 * Login
	 */
	private OnEditorActionListener passwordEditorActionListener = new OnEditorActionListener() {
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
					|| (actionId == EditorInfo.IME_ACTION_DONE)) {
				hostActivity.accountLoginButtonLoginCallback(getEmail(),
						getPassword());
			}
			return false;
		}
	};

	/**
	 * Login button click listener. Credentials validity is checked. If
	 * credentials are valid the
	 * hostActivity.accountLoginButtonLoginCallback(email,password) is called
	 */
	private OnClickListener loginButtonClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (!TmlAccount.checkEmail(getEmail())) {
				new TmlDialogFragment().setMessage(
						R.string.setup_dialog_mail_not_valid).show(
						getFragmentManager(), "TmlAccountLoginFragment");
			} else if (getPassword().length() == 0) {
				new TmlDialogFragment().setMessage(
						R.string.setup_dialog_password_not_valid).show(
						getFragmentManager(), "TmlAccountLoginFragment");
			} else {
				hostActivity.accountLoginButtonLoginCallback(getEmail(),
						getPassword());
			}
		}

	};

	/**
	 * Signup button listener.
	 */
	private OnClickListener signupButtonClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			hostActivity.accountLoginButtonSignupCallback(getEmail());
		}

	};

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- ATTRIBUTES -------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Activity that hosts this fragment and implements the
	 * AccountLoginFragmentCallbacks
	 */
	private AccountLoginFragmentCallbacks hostActivity;

	/*
	 * Email and password EditText views
	 */
	private EditText emailEditText;
	private EditText passwordEditText;

	/*
	 * Login and Signup buttons
	 */
	private Button loginButton;
	private Button signupButton;

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- LIFECYCLE METHODS ------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Called when fragment attached to the activity. Try to cast the activity
	 * to the fragment callback. If cast failed throw exception
	 * 
	 * @param activity
	 *            host activity
	 * @throws ClassCastException
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			hostActivity = (AccountLoginFragmentCallbacks) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement AccountLoginFragmentCallbacks");
		}
	}

	/**
	 * Called when the activity requests the fragment to create its view.
	 * 
	 * @param inflater
	 * @param container
	 *            host activity container where the fragment should inflate
	 *            itself
	 * @param savedInstanceState
	 * @return the inflated view
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater
				.inflate(R.layout.setup_login_fragment, container, false);
	}

	/**
	 * Called when the activity has been rendered. Not it's possible to use
	 * getActivity().findViewById() to get reference to the UI views
	 * 
	 * @param savedInstanceState
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		emailEditText = (EditText) getActivity().findViewById(
				R.id.account_login_email_editText);

		/*
		 * If restoring the activity restore the email text
		 */
		if (savedInstanceState != null) {
			if (savedInstanceState.get(BUNDLE_KEY_EMAIL) != null) {
				emailEditText.setText(savedInstanceState
						.getString(BUNDLE_KEY_EMAIL));
			}
		}

		emailEditText.requestFocus();

		passwordEditText = (EditText) getActivity().findViewById(
				R.id.account_login_password_editText);
		passwordEditText
				.setOnEditorActionListener(passwordEditorActionListener);

		loginButton = (Button) getActivity().findViewById(
				R.id.account_login_login_button);
		signupButton = (Button) getActivity().findViewById(
				R.id.account_login_signup_button);
		/*
		 * Set click listeners for buttons. The android.onClick only works with
		 * host activity methods
		 */
		loginButton.setOnClickListener(loginButtonClickListener);
		signupButton.setOnClickListener(signupButtonClickListener);

	}

	/**
	 * Called when the activity is stopped to allow saving data
	 */
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(BUNDLE_KEY_EMAIL, getEmail());
	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- GENERIC METHODS --------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Set the editText email
	 * 
	 * @param email
	 */
	public void setEmail(String email) {
		emailEditText.setText(email);
	}

	/**
	 * Email inserted into the EditText. Start and end spaces are trimmed.
	 * 
	 * @return
	 */
	private String getEmail() {
		return emailEditText.getText().toString().trim();
	}

	/**
	 * Password inserted into the EditText
	 * 
	 * @return
	 */
	private String getPassword() {
		return passwordEditText.getText().toString();
	}

	/**
	 * Lock editTexts. Used when performing network operations.
	 * 
	 * @return
	 */
	public void lockCredentials() {
		emailEditText.setEnabled(false);
		passwordEditText.setEnabled(false);
	}

	/**
	 * Unlock editTexts
	 * 
	 * @return
	 */
	public void unlockCredentials() {
		if (emailEditText != null) {
			emailEditText.setEnabled(true);
		}
		if (passwordEditText != null) {
			passwordEditText.setEnabled(true);
		}
	}

}
