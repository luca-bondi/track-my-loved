package me.noip.unklb.trackmyloved.activities;

import me.noip.unklb.trackmyloved.R;
import me.noip.unklb.trackmyloved.controllers.TmlAccount;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

/**
 * Controller for the Account Signup Fragment
 */
public class TmlAccountSignupFragment extends Fragment {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	// private static final String TAG = "TmlAccountSignupFragment";

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- INTERFACES ---------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Callback interface to be implemented by the activity that hosts this
	 * fragment
	 */
	public interface AccountSignupFragmentCallbacks {
		/**
		 * Called when cancel button is clicked.
		 * 
		 * @param email
		 */
		public void accountSignupButtonCancelCallback(String email);

		/**
		 * Called when credentials are valid and Signup button is clicked
		 * 
		 * @param email
		 * @param password
		 */
		public void accountSignupButtonSignupCallback(String email,
				String password);
	}

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- UI LISTENERS -------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * PasswordRepeat EditText action listener to let Enter and Done actions
	 * perform Login
	 */
	private OnEditorActionListener passwordRepeatEditorActionListener = new OnEditorActionListener() {
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
					|| (actionId == EditorInfo.IME_ACTION_DONE)) {
				hostActivity.accountSignupButtonSignupCallback(getEmail(),
						getPassword());
			}
			return false;
		}
	};

	/**
	 * Signup button click listener. Credentials validity is checked. If
	 * credentials are valid the
	 * hostActivity.accountSignupButtonSignupCallback(email,password) is called
	 */
	private OnClickListener signupButtonClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (!TmlAccount.checkEmail(getEmail())) {
				new TmlDialogFragment().setMessage(
						R.string.setup_dialog_mail_not_valid).show(
						getFragmentManager(), "TmlAccountLoginFragment");
			} else if (getPassword().length() == 0) {
				new TmlDialogFragment().setMessage(
						R.string.setup_dialog_password_not_valid).show(
						getFragmentManager(), "TmlAccountLoginFragment");
			} else if (!getPassword().equals(getPasswordRepeat())) {
				new TmlDialogFragment().setMessage(
						R.string.setup_dialog_passwordMismatch_text).show(
						getFragmentManager(), "TmlAccountLoginFragment");
			} else {
				hostActivity.accountSignupButtonSignupCallback(getEmail(),
						getPassword());
			}
		}

	};

	/**
	 * Cancel button listener
	 */
	private OnClickListener cancelButtonClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			hostActivity.accountSignupButtonCancelCallback(getEmail());
		}

	};

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- ATTRIBUTES -------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Activity that hosts this fragment and implements the
	 * AccountSignupFragmentCallbacks
	 */
	private AccountSignupFragmentCallbacks hostActivity;

	/**
	 * Initial email value. Set with setEmail, its value can be written into
	 * emailEditText only during or after onActivityCreated
	 */
	private String emailInit = "";

	/*
	 * Email and passwords EditText views
	 */
	private EditText emailEditText;
	private EditText passwordEditText;
	private EditText passwordRepeatEditText;

	/*
	 * Cancel and Signup buttons
	 */
	private Button signupButton;
	private Button cancelButton;

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- LIFECYCLE METHODS ------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Called when fragment attached to the activity. Try to cast the activity
	 * to the fragment callback. If cast failed throw exception
	 * 
	 * @param activity
	 *            host activity
	 * @throws ClassCastException
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			hostActivity = (AccountSignupFragmentCallbacks) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement AccountSignupFragmentCallbacks");
		}
	}

	/**
	 * Called when the activity requests the fragment to create its view.
	 * 
	 * @param inflater
	 * @param container
	 *            host activity container where the fragment should inflate
	 *            itself
	 * @param savedInstanceState
	 * @return the inflated view
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.setup_signup_fragment, container,
				false);
	}

	/**
	 * Called when the activity has been rendered. Not it's possible to use
	 * getActivity().findViewById() to get reference to the UI views
	 * 
	 * @param savedInstanceState
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		emailEditText = (EditText) getActivity().findViewById(
				R.id.account_signup_email_editText);
		emailEditText.setText(emailInit);
		emailEditText.requestFocus();

		passwordEditText = (EditText) getActivity().findViewById(
				R.id.account_signup_password_editText);
		passwordRepeatEditText = (EditText) getActivity().findViewById(
				R.id.account_signup_password_repeat_editText);
		passwordRepeatEditText
				.setOnEditorActionListener(passwordRepeatEditorActionListener);

		signupButton = (Button) getActivity().findViewById(
				R.id.account_signup_signup_button);
		cancelButton = (Button) getActivity().findViewById(
				R.id.account_signup_cancel_button);
		cancelButton.setOnClickListener(cancelButtonClickListener);
		signupButton.setOnClickListener(signupButtonClickListener);

	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- GENERIC METHODS --------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Set the email editText
	 * 
	 * @param email
	 */
	public void setEmail(String email) {
		this.emailInit = email;
	}

	/**
	 * Email inserted into the EditText. Start and end spaces are trimmed.
	 * 
	 * @return
	 */
	public String getEmail() {
		return emailEditText.getText().toString().trim();
	}

	/**
	 * Password inserted into the EditText
	 * 
	 * @return
	 */
	public String getPassword() {
		return passwordEditText.getText().toString();
	}

	/**
	 * Password repeat inserted into the EditText
	 * 
	 * @return
	 */
	public String getPasswordRepeat() {
		return passwordRepeatEditText.getText().toString();
	}

	/**
	 * Lock editTexts. Used when performing network operations
	 * 
	 * @return
	 */
	public void lockCredentials() {
		emailEditText.setEnabled(false);
		passwordEditText.setEnabled(false);
		passwordRepeatEditText.setEnabled(false);
	}

	/**
	 * Unlock editTexts
	 * 
	 * @return
	 */
	public void unlockCredentials() {
		emailEditText.setEnabled(true);
		passwordEditText.setEnabled(true);
		passwordRepeatEditText.setEnabled(false);
	}

}
