package me.noip.unklb.trackmyloved.activities;

import java.util.ArrayList;

import me.noip.unklb.trackmyloved.R;
import me.noip.unklb.trackmyloved.activities.TmlAccountLoginFragment.AccountLoginFragmentCallbacks;
import me.noip.unklb.trackmyloved.activities.TmlAccountSignupFragment.AccountSignupFragmentCallbacks;
import me.noip.unklb.trackmyloved.activities.TmlRoleSelectionFragment.RoleSelectionFragmentCallbacks;
import me.noip.unklb.trackmyloved.activities.TmlUserCreateFragment.UserCreateFragmentCallbacks;
import me.noip.unklb.trackmyloved.activities.TmlUserSelectionFragment.UserSelectionFragmentCallbacks;
import me.noip.unklb.trackmyloved.controllers.TmlAccount;
import me.noip.unklb.trackmyloved.controllers.TmlDatabase;
import me.noip.unklb.trackmyloved.controllers.TmlUserGuide;
import me.noip.unklb.trackmyloved.controllers.TmlUserGuide.DescriptionType;
import me.noip.unklb.trackmyloved.services.TmlNetwork;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;

/**
 * Controller for the Setup Activity
 */
public class TmlSetupActivity extends ActionBarActivity implements
		AccountLoginFragmentCallbacks, AccountSignupFragmentCallbacks,
		UserSelectionFragmentCallbacks, RoleSelectionFragmentCallbacks,
		UserCreateFragmentCallbacks {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	// private static final String TAG = "SetupActivity";

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- CLASSES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Local BroadcastReceiver used to receive network communication results
	 */
	private BroadcastReceiver localBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {

				if (intent.getAction().equals(
						TmlNetwork.INTENT_ACTION_ACCOUNT_LOGIN)) {

					String errorResult = intent
							.getStringExtra(TmlNetwork.INTENT_EXTRA_KEY_ERROR);

					if (errorResult == null) {

						ArrayList<String> result = intent
								.getStringArrayListExtra(TmlNetwork.INTENT_EXTRA_KEY_RESULT);

						if (result == null) {
							messageDialog(R.string.setup_dialog_accountUnknown_text);
						} else {
							account = intent
									.getParcelableExtra(TmlNetwork.INTENT_EXTRA_KEY_ACCOUNT);
							users = result;

							initRoleSelectionFragment();
						}

					} else {
						serverError(errorResult);
					}

					accountLoginFragment.unlockCredentials();

				} else if (intent.getAction().equals(
						TmlNetwork.INTENT_ACTION_ACCOUNT_SIGNUP)) {

					String errorResult = intent
							.getStringExtra(TmlNetwork.INTENT_EXTRA_KEY_ERROR);

					if (errorResult == null) {

						String result = intent
								.getStringExtra(TmlNetwork.INTENT_EXTRA_KEY_RESULT);

						if (result == null) {
							messageDialog(R.string.setup_dialog_unknownError_text);
						} else if (result
								.equals(TmlNetwork.XML_ACCOUNT_REGISTRATION_ERROR_TEXT)) {
							messageDialog(R.string.setup_dialog_accountSignup_text);
						} else if (result
								.equals(TmlNetwork.XML_MAIL_ALREADY_REGISTERED_TEXT)) {
							messageDialog(R.string.setup_dialog_mailRegistered_text);
						} else if (result
								.equals(TmlNetwork.XML_ACCOUNT_REGISTRATION_SUCCESSFUL_TEXT)) {
							account = intent
									.getParcelableExtra(TmlNetwork.INTENT_EXTRA_KEY_ACCOUNT);
							users = null;
							initRoleSelectionFragment();
						} else {
							messageDialog(R.string.setup_dialog_unknownError_text);
						}

					} else {
						serverError(errorResult);
					}

					accountSignupFragment.unlockCredentials();

				} else if (intent.getAction().equals(
						TmlNetwork.INTENT_ACTION_USER_CREATE)) {

					String errorResult = intent
							.getStringExtra(TmlNetwork.INTENT_EXTRA_KEY_ERROR);

					if (errorResult == null) {
						String result = intent
								.getStringExtra(TmlNetwork.INTENT_EXTRA_KEY_RESULT);

						if (result == null) {
							messageDialog(R.string.setup_dialog_unknownError_text);
						} else if (result
								.equals(TmlNetwork.XML_USER_CREATION_ERROR_TEXT)) {
							messageDialog(R.string.setup_dialog_userCreationError_text);
						} else if (result
								.equals(TmlNetwork.XML_USER_EXISTS_TEXT)) {
							messageDialog(R.string.setup_dialog_userExistingError_text);
						} else if (result
								.equals(TmlNetwork.XML_USER_CREATION_SUCCESSFUL_TEXT)) {
							user = intent
									.getStringExtra(TmlNetwork.INTENT_EXTRA_KEY_USER);
							saveSettings();
						} else {
							messageDialog(R.string.setup_dialog_unknownError_text);
						}
					} else {
						serverError(errorResult);
					}

				}

				progressBar.setVisibility(View.INVISIBLE);
				LocalBroadcastManager.getInstance(getBaseContext())
						.unregisterReceiver(localBroadcastReceiver);
			}

		}

		/**
		 * Rise message dialog when server error occurred
		 * 
		 * @param errorResult
		 */
		private void serverError(String errorResult) {
			if (!TmlNetwork.checkConnection(thisActivity)) {
				messageDialog(R.string.setup_dialog_connectionUnavailable_text);
			} else {
				if (errorResult
						.equals(TmlNetwork.INTENT_EXTRA_ERROR_SERVER_ERROR)) {
					messageDialog(R.string.setup_dialog_serverError_text);
				} else if (errorResult
						.equals(TmlNetwork.INTENT_EXTRA_ERROR_SERVER_UNREACHABLE)) {
					messageDialog(R.string.setup_dialog_serverUnreachable_text);
				}
			}
		}
	};

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- ATTRIBUTES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Account used to login
	 */
	private TmlAccount account;

	/**
	 * Role chosen for this device
	 */
	private char role;

	/**
	 * Users associated to the account. If empty or null no users in this
	 * account
	 */
	private ArrayList<String> users;

	/**
	 * User selected or created
	 */
	private String user;

	/**
	 * Fragment manager instance
	 */
	private FragmentManager fragmentManager = getSupportFragmentManager();

	/**
	 * Database controller instance
	 */
	private TmlDatabase db = TmlDatabase.getInstance(this);

	/**
	 * Set when first fragment loaded. Only fragments after the first push the
	 * previous fragment in the fragment stack
	 */
	private boolean firstFragment;

	/**
	 * Indeterminate progress bar used to indicate that a network communication
	 * is in progress
	 */
	private ProgressBar progressBar;

	/**
	 * Instance of this activity
	 */
	private Activity thisActivity;

	/*
	 * Fragments instances
	 */
	private TmlAccountLoginFragment accountLoginFragment = new TmlAccountLoginFragment();
	private TmlAccountSignupFragment accountSignupFragment = new TmlAccountSignupFragment();
	private TmlRoleSelectionFragment roleSelectionFragment = new TmlRoleSelectionFragment();
	private TmlUserSelectionFragment userSelectionFragment = new TmlUserSelectionFragment();
	private TmlUserCreateFragment userCreateFragment = new TmlUserCreateFragment();

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------- FRAGMENTS CALLBACKS ----------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Called by TmlAccountLoginFragment when Login credentials are valid and
	 * Login button is clicked
	 * 
	 * @param email
	 * @param password
	 */
	@Override
	public void accountLoginButtonLoginCallback(String email, String password) {

		accountLoginFragment.lockCredentials();

		progressBar.setVisibility(View.VISIBLE);

		IntentFilter ifilter = new IntentFilter();
		ifilter.addAction(TmlNetwork.INTENT_ACTION_ACCOUNT_LOGIN);
		LocalBroadcastManager.getInstance(this).registerReceiver(
				localBroadcastReceiver, ifilter);

		Intent intent = new Intent(this, TmlNetwork.class);
		intent.setAction(TmlNetwork.INTENT_ACTION_ACCOUNT_LOGIN);
		intent.putExtra(TmlNetwork.INTENT_EXTRA_KEY_ACCOUNT, new TmlAccount(
				email, password));

		startService(intent);
	}

	/**
	 * Called by TmlAccountLoginFragment when Signup button is clicked
	 * 
	 * @param email
	 */
	@Override
	public void accountLoginButtonSignupCallback(String email) {
		initAccountSignupFragment(email);
	}

	/**
	 * Called by TmlAccountLoginFragment when credentials are valid and Signup
	 * button is clicked
	 * 
	 * @param email
	 * @param password
	 */
	@Override
	public void accountSignupButtonSignupCallback(String email, String password) {
		accountSignupFragment.lockCredentials();

		progressBar.setVisibility(View.VISIBLE);

		IntentFilter ifilter = new IntentFilter();
		ifilter.addAction(TmlNetwork.INTENT_ACTION_ACCOUNT_SIGNUP);
		LocalBroadcastManager.getInstance(this).registerReceiver(
				localBroadcastReceiver, ifilter);

		Intent intent = new Intent(this, TmlNetwork.class);
		intent.setAction(TmlNetwork.INTENT_ACTION_ACCOUNT_SIGNUP);
		intent.putExtra(TmlNetwork.INTENT_EXTRA_KEY_ACCOUNT, new TmlAccount(
				email, password));

		startService(intent);
	}

	/**
	 * Called by TmlAccountLoginFragment when cancel button is clicked.
	 * 
	 * @param email
	 */
	@Override
	public void accountSignupButtonCancelCallback(String email) {
		fragmentManager.popBackStack();
		accountLoginFragment.setEmail(email);
	}

	/**
	 * Called by TmlRoleSelectionFragment when cancel button is pressed
	 */
	@Override
	public void roleSelectionButtonCancelCallback() {
		roleSelectionFragment.cleanSelection();
		role = 0;
		if (fragmentManager.getBackStackEntryCount() > 0) {
			fragmentManager.popBackStack();
		} else {
			finish();
		}
	}

	/**
	 * Called by TmlRoleSelectionFragment when a valid role has been selected
	 * and confirm button is pressed.
	 * 
	 * @param role
	 */
	@Override
	public void roleSelectionButtonConfirmCallback(char role) {
		this.role = role;
		if (role == 's') {
			if (users == null || users.size() == 0) {
				initUserCreateSelectionFragment();
			} else {
				initUserSelectionFragment();
			}
		} else if (role == 'p') {
			saveSettings();
		}
	}

	/**
	 * Called by TmlUserSelectionFragment when new user button is pressed
	 */
	@Override
	public void userSelectionButtonNewUserCallback() {
		initUserCreateSelectionFragment();
	}

	/**
	 * Called by TmlUserSelectionFragment when a valid username has been typed
	 * and confirm button is pressed
	 * 
	 * @param user
	 */
	@Override
	public void userSelectionButtonConfirmCallback(String user) {
		this.user = user;
		saveSettings();
	}

	/**
	 * Called by TmlUserSelectionFragment when cancel button is pressed
	 */
	@Override
	public void userSelectionButtonCancelCallback() {
		userSelectionFragment.cleanSelection();
		this.user = null;
		fragmentManager.popBackStack();
	}

	/**
	 * Called by TmlUserCreateFragment when a user has been selected and confirm
	 * button is pressed
	 * 
	 * @param user
	 */
	@Override
	public void userCreateButtonConfirmCallback(String user) {

		progressBar.setVisibility(View.VISIBLE);

		IntentFilter ifilter = new IntentFilter();
		ifilter.addAction(TmlNetwork.INTENT_ACTION_USER_CREATE);
		LocalBroadcastManager.getInstance(this).registerReceiver(
				localBroadcastReceiver, ifilter);

		Intent intent = new Intent(this, TmlNetwork.class);
		intent.setAction(TmlNetwork.INTENT_ACTION_USER_CREATE);
		intent.putExtra(TmlNetwork.INTENT_EXTRA_KEY_ACCOUNT, account);
		intent.putExtra(TmlNetwork.INTENT_EXTRA_KEY_USER, user);

		startService(intent);

	}

	/**
	 * Called by TmlUserCreateFragment when cancel button is pressed
	 */
	@Override
	public void userCreateButtonCancelCallback() {
		hideSoftKeyboard(this, getCurrentFocus());
		userCreateFragment.clean();
		this.user = null;
		fragmentManager.popBackStack();
	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- LIFECYCLE METHODS ------------------------
	 * ------------------------------------------------------------------------
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		thisActivity = this;

		/*
		 * In phones lock the orientation for this activity in portrait
		 */
		if (!getResources().getBoolean(R.bool.isTablet)) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}

		/*
		 * Set activity content (empty linear layout) that will be filled with
		 * fragments
		 */
		setContentView(R.layout.setup_activity);

		/*
		 * Instantiate the progress bar
		 */
		progressBar = (ProgressBar) findViewById(R.id.setup_activity_progressBar);

		/*
		 * Remove action Bar
		 */
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.hide();
		}

		/*
		 * Retrieve the account from the database. Null if no account
		 * registered.
		 */
		TmlAccount accountTemp = db.getAccount();

		if (accountTemp != null) {
			/*
			 * Email and password are already set in the database. Use these
			 * credentials
			 */

			/*
			 * Show the progress bar
			 */
			progressBar.setVisibility(View.VISIBLE);

			/*
			 * Setup the LocalBroadcastManager to receive
			 * INTENT_ACTION_ACCOUNT_LOGIN
			 */
			IntentFilter ifilter = new IntentFilter();
			ifilter.addAction(TmlNetwork.INTENT_ACTION_ACCOUNT_LOGIN);
			LocalBroadcastManager.getInstance(this).registerReceiver(
					localBroadcastReceiver, ifilter);

			/*
			 * Send the request of login to the server with the credentials
			 * stored in the database
			 */
			Intent intent = new Intent(this, TmlNetwork.class);
			intent.setAction(TmlNetwork.INTENT_ACTION_ACCOUNT_LOGIN);
			intent.putExtra(TmlNetwork.INTENT_EXTRA_KEY_ACCOUNT, accountTemp);
			startService(intent);

		} else {
			/*
			 * First setup. Show user guide.
			 */
			TmlUserGuide userGuide = new TmlUserGuide(
					this,
					(LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE));
			userGuide.showDescription(DescriptionType.SETUP);

			initAccountLoginFragment();
		}

	}

	/*
	 * ------------------------------------------------------------------------
	 * ------------------------GENERIC METHODS --------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Initialize the TmlAccountLoginFragment
	 */
	private void initAccountLoginFragment() {
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.add(R.id.layoutSetupActivity, accountLoginFragment);
		fragmentTransaction.commit();

		firstFragment = true;
	}

	/**
	 * Initialize the TmlAccountSignupFragment
	 */
	private void initAccountSignupFragment(String email) {
		accountSignupFragment.setEmail(email);

		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.replace(R.id.layoutSetupActivity,
				accountSignupFragment);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
	}

	/**
	 * Initialize the TmlRoleSelectionFragment
	 */
	private void initRoleSelectionFragment() {
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.replace(R.id.layoutSetupActivity,
				roleSelectionFragment);
		if (firstFragment) {
			fragmentTransaction.addToBackStack(null);
		}
		fragmentTransaction.commit();
		hideSoftKeyboard(this, getCurrentFocus());

		firstFragment = true;
	}

	/**
	 * Initialize the TmlUserSelectionFragment
	 */
	private void initUserSelectionFragment() {
		userSelectionFragment.setUsers(users);

		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.replace(R.id.layoutSetupActivity,
				userSelectionFragment);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
		hideSoftKeyboard(this, getCurrentFocus());
	}

	/**
	 * Initialize the TmlUserCreateFragment
	 */
	private void initUserCreateSelectionFragment() {
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.replace(R.id.layoutSetupActivity,
				userCreateFragment);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
	}

	/**
	 * Show a dialog with the given string id as text
	 * 
	 * @param stringId
	 */
	private void messageDialog(int stringId) {
		new TmlDialogFragment().setMessage(getResources().getString(stringId))
				.show(fragmentManager, "TmlSetupActivity");
	}

	/**
	 * Save current settings and start the TmlMapActivity.
	 * 
	 * role, user and account must be set.
	 */
	private void saveSettings() {
		progressBar.setVisibility(View.VISIBLE);
		db.setRole(role);
		if (role == 's') {
			db.setUser(user);
		}
		db.setEmail(account.getEmail());
		db.setPwdSHA1(account.getPwdSHA1());
		if (db.isParentDevice() && users != null) {
			for (String userTemp : users) {
				db.insertChild(userTemp);
			}
		}
		Intent intent = new Intent(this, TmlMainActivity.class);
		startActivity(intent);
		finish();
	}

	/**
	 * Capture and consume the menu key event to avoid error due to ActionBar
	 * missing
	 */
	@Override
	public boolean onKeyDown(int keycode, KeyEvent e) {
		switch (keycode) {
		case KeyEvent.KEYCODE_MENU:
			return true;
		}
		return super.onKeyDown(keycode, e);
	}

	/**
	 * Hide the keyboard
	 */
	private static void hideSoftKeyboard(Activity activity, View focusedView) {
		InputMethodManager inputMethodManager = (InputMethodManager) activity
				.getSystemService(Activity.INPUT_METHOD_SERVICE);
		if (inputMethodManager != null && focusedView != null) {
			inputMethodManager.hideSoftInputFromWindow(
					focusedView.getWindowToken(), 0);
		}
	}

	/**
	 * Show the keyboard
	 */
	public static void showSoftKeyboard(Activity activity, View focusedView) {
		InputMethodManager inputMethodManager = (InputMethodManager) activity
				.getSystemService(Activity.INPUT_METHOD_SERVICE);
		if (inputMethodManager != null && focusedView != null) {
			inputMethodManager.showSoftInputFromInputMethod(
					focusedView.getApplicationWindowToken(), 0);
		}
	}

}
