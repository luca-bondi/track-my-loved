package me.noip.unklb.trackmyloved.activities;

import java.util.ArrayList;
import java.util.Locale;

import me.noip.unklb.trackmyloved.R;
import me.noip.unklb.trackmyloved.activities.TmlDialogFragment.MessageDialogListener;
import me.noip.unklb.trackmyloved.controllers.TmlChild;
import me.noip.unklb.trackmyloved.controllers.TmlChild.TmlChildrenListener;
import me.noip.unklb.trackmyloved.controllers.TmlMarker;
import me.noip.unklb.trackmyloved.controllers.TmlMarker.MarkerType;
import me.noip.unklb.trackmyloved.controllers.TmlNotification;
import me.noip.unklb.trackmyloved.controllers.TmlPlace;
import me.noip.unklb.trackmyloved.controllers.TmlPosition;
import me.noip.unklb.trackmyloved.controllers.TmlSafeZone;
import me.noip.unklb.trackmyloved.controllers.TmlUserGuide;
import me.noip.unklb.trackmyloved.controllers.TmlUserGuide.DescriptionType;
import me.noip.unklb.trackmyloved.controllers.TmlUserGuide.ToastType;
import me.noip.unklb.trackmyloved.controllers.TmlUserGuide.UserGuideCallbacks;
import me.noip.unklb.trackmyloved.services.TmlServicesManager;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;

/**
 * MapActivity controller
 */
public class TmlMapActivity extends ActionBarActivity implements
		MessageDialogListener, UserGuideCallbacks {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	private static final String TAG = "MapActivity";

	/**
	 * Intent extra key for the child name. Used by notifications.
	 */
	public static final String INTENT_EXTRA_CHILD_NAME = "INTENT_EXTRA_CHILD_NAME";

	/**
	 * Info view show animation time [ms]
	 */
	private static final long INFO_WINDOW_SHOW_DURATION = 500;

	/**
	 * Info view hide animation time [ms]
	 */
	private static final long INFO_WINDOW_HIDE_DURATION = 250;

	/**
	 * Id for startActivityForResult when calling settingsActivity
	 */
	private static final int REQUEST_SETTINGS = 100;

	/**
	 * Animation time for camera update [ms]
	 */
	private static final int CAMERA_DEFAULT_ANIMATION_TIME = 700;

	/**
	 * Camera default zoom level when zooming on a single marker
	 */
	private static final int CAMERA_DEFAULT_ZOOM = 17;

	/**
	 * Half diagonal of the minimum zoom level square [m]
	 */
	private static final double MIN_DIAG_DISTANCE = 70;

	/**
	 * Maximum marker dimension in pixel
	 */
	private static final int MAX_MARKER_DIMENSION = 44;

	/**
	 * Max value for initial sync progress bar
	 */
	private static final int INIT_SYNC_PROGRESS_BAR_MAX = 100;

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------- CALLBACKS AND LISTENERS ------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Called when a key event is dispatched. It is the only way to prevent that
	 * the back button causes the end of actionMode
	 */
	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		if (actionMode != null) {
			if (event.getKeyCode() == KeyEvent.KEYCODE_BACK
					&& event.getAction() == KeyEvent.ACTION_UP) {
				if (state.getState() == StateType.SAFE_ZONE_ADD) {
					if (safeZoneCandidate.getPointList().size() > 0) {
						safeZoneDrawingRemoveLastPoint();
					} else {
						state.exitSafeZoneAdd();
					}
					/*
					 * Consume the back event
					 */
					return true;
				}
			}
		}
		return super.dispatchKeyEvent(event);
	}

	/**
	 * Called when back button pressed
	 */
	@Override
	public void onBackPressed() {
		if (shadowType != ShadowType.NONE) {
			/*
			 * If a shadow is active hide the app
			 */
			super.onBackPressed();
			return;
		}

		/*
		 * If navigation drawer is opened close it
		 */
		if (navigationDrawerLayout != null
				&& navigationDrawerLayout
						.isDrawerOpen(navigationDrawerListView)) {
			navigationDrawerLayout.closeDrawers();
			return;
		}

		switch (state.getState()) {
		case CHILD_SELECTED:
			state.exitChildSelected();
		case SAFE_ZONE_ADD:
			/*
			 * Managed by dispatchKeyEvent(...)
			 */
			break;
		case SAFE_ZONE_CLEAR:
			/*
			 * Confirm dialog is canceled. Managed by messageDialogNegative(...)
			 */
			break;
		case SAFE_ZONE_REMOVE:
			state.exitSafeZoneRemove();
			break;
		case SINGLE_CHILD:
			if (TmlChild.isParentDevice()) {
				state.exitSingleChild();
				cameraShowLastPositions();
			} else {
				super.onBackPressed();
			}
			break;
		default:
			super.onBackPressed();
			break;
		}

	}

	/**
	 * Called when MessageDialog confirm button is pressed
	 */
	@Override
	public void messageDialogPositive() {
		switch (state.getState()) {
		case SAFE_ZONE_CLEAR:
			childSelected.removeAllSafeZones();
			state.exitSafeZoneClear();
			notificationManager.riseNotifications();
			break;
		case SAFE_ZONE_REMOVE:
			safeZoneSelected.remove();
			state.exitSafeZoneRemove();
			notificationManager.riseNotifications();
			break;
		default:
			break;
		}
	}

	/**
	 * Called when MessageDialog cancel button is pressed or back button is
	 * pressed
	 */
	@Override
	public void messageDialogNegative() {
		switch (state.getState()) {
		case SAFE_ZONE_CLEAR:
			state.exitSafeZoneClear();
			break;
		case SAFE_ZONE_REMOVE:
			state.exitSafeZoneRemove();
			break;
		default:
			break;
		}
	}

	/**
	 * Called when options button pressed or overflow button pressed. Causes the
	 * navigation drawer to be closed.
	 */
	@Override
	public boolean onMenuOpened(int featureId, Menu menu) {
		if (navigationDrawerLayout != null
				&& navigationDrawerLayout
						.isDrawerOpen(navigationDrawerListView)) {
			navigationDrawerLayout.closeDrawers();
		}
		return true;
	}

	/**
	 * To avoid activity restart when orientation changes
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		/* Pass any configuration change to the drawer toggle */
		if (actionBarDrawerToggle != null) {
			actionBarDrawerToggle.onConfigurationChanged(newConfig);
		}
	}

	/**
	 * Setup the action bar menu. Called when ActionBar created or after
	 * supportInvalidateOptionsMenu() called
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (shadowType != ShadowType.NONE) {
			menu.clear();
		} else {
			switch (state.getState()) {
			case ALL_CHILDREN:
				menu.clear();
				getMenuInflater().inflate(R.menu.actionbar, menu);
				actionBar.setTitle(R.string.app_name);
				break;
			case CHILD_SELECTED:
				break;
			case SAFE_ZONE_ADD:
				/*
				 * ActionMode is active, actionBar not visible
				 */
				break;
			case SAFE_ZONE_CLEAR:
				menu.clear();
				break;
			case SAFE_ZONE_REMOVE:
				menu.clear();
				break;
			case SINGLE_CHILD:
				if (TmlChild.isParentDevice()) {
					menu.clear();
					getMenuInflater().inflate(R.menu.actionbar_single_child,
							menu);

					MenuItem safeZoneRemoveItem = (MenuItem) menu
							.findItem(R.id.action_remove_safezone);
					MenuItem safeZoneClearItem = (MenuItem) menu
							.findItem(R.id.action_remove_all_safezones);

					if (childSelected.getSafeZoneList() == null
							|| childSelected.getValidSafeZoneList().size() == 0) {
						safeZoneClearItem.setVisible(false);
						safeZoneRemoveItem.setVisible(false);
					}
					actionBar.setTitle(childSelected.getName());
				} else {
					menu.clear();
					getMenuInflater().inflate(R.menu.actionbar, menu);
				}
				break;
			default:
				break;
			}
		}
		return true;
	}

	/**
	 * Action bar buttons callback
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {

		/*
		 * Tapping on AcitonBar title and logo opens left drawer
		 */
		if (actionBarDrawerToggle != null
				&& actionBarDrawerToggle.onOptionsItemSelected(menuItem)) {
			return true;
		}

		switch (menuItem.getItemId()) {

		case R.id.action_settings:
			startActivityForResult(new Intent(this, TmlSettingsActivity.class),
					REQUEST_SETTINGS);
			return true;
		case R.id.action_new_safezone:
			state.initSafeZoneAdd();
			return true;
		case R.id.action_remove_safezone:
			state.initSafeZoneRemove();
			return true;
		case R.id.action_remove_all_safezones:
			state.initSafeZoneClear();
			cameraShowAllSafeZones();
			return true;
		default:
			return super.onOptionsItemSelected(menuItem);
		}
	}

	/**
	 * Cancel listener for user guide Dialog. Called when UserGuide dialog is
	 * closed
	 */
	@Override
	public void onUserGuideDialogClosed(DialogInterface dialog) {
		/*
		 * Remove the entry from the register
		 */
		if (dialog == dialogNoChild) {
			dialogNoChild = null;
		}
		dialogRegister.remove(dialog);

		userGuideToastPositionMarkerTap();

		userGuideToastPositionMarkerTapAgain();

		userGuideToastSafeZoneAdd();

	}

	/**
	 * Called when UserGuide noChild Reset Device button is pressed
	 */
	@Override
	public void onUserGuideResetCallback(View v) {
		TmlServicesManager.resetDevice(this, false, true);
		this.finish();
	}

	/**
	 * Called when GoogleMap loading is completed. Loading shadow is closed and
	 * initial zoom is performed
	 */
	private OnMapLoadedCallback mapLoadedCallback = new OnMapLoadedCallback() {

		@Override
		public void onMapLoaded() {
			mapLoadingCompleted = true;

			if (shadowType == ShadowType.MAP_LOADING) {
				shadowLayoutHide();
				if (TmlChild.isChildDevice()
						&& childSelected.getLastFilteredPosition() == null) {
					shadowFirstPosition();
				}
				userGuideToastPositionMarkerTap();
			}

			/*
			 * Determine actionBar height and set Map padding
			 */
			actionBarHeight = actionBar.getHeight();
			if (TmlChild.isChildDevice()) {
				map.setPadding(0, actionBarHeight, 0, infoView.fullHeight);
			} else {
				map.setPadding(0, actionBarHeight, 0, 0);
			}

			/*
			 * First camera animation
			 */
			refreshMapElements();
			cameraInitialAnimation();
		}

	};

	/**
	 * Marker click listener used to setup and display infoView and to set
	 * selectedChild
	 */
	private OnMarkerClickListener markerClickListener = new OnMarkerClickListener() {

		/**
		 * Manage click marker.
		 * 
		 * @return true if no automatic camera animation is desired
		 */
		@Override
		public synchronized boolean onMarkerClick(Marker clickedMarker) {

			/*
			 * addSafeZoneMode check for polygon closed to trigger
			 * doneAddSafeZone()
			 */
			if (state.getState() == StateType.SAFE_ZONE_ADD
					&& safeZoneCandidate.getPointList().size() > 0
					&& clickedMarker.equals(safeZoneCandidate.getView()
							.getMarkerList().get(0))) {
				safeZoneDrawingDone();
				return true;
			}

			/*
			 * Determine the TmlMarker associate with the selected marker. Last
			 * position markers, place markers and clusters are considered. If
			 * not found exit
			 */
			TmlMarker selectedMarker = null;
			for (TmlMarker tempMarker : markerList) {
				if (tempMarker.getMarker().equals(clickedMarker)) {
					selectedMarker = tempMarker;
					break;
				}
			}
			if (selectedMarker == null) {
				// Log.w(TAG, "Selected marker not in markerList");
				/*
				 * No valid marker clicked. Ignore it.
				 */
				return true;
			}

			/*
			 * If the selected marker is a cluster marker zoom in and exit
			 */
			if (selectedMarker.getType() == MarkerType.CLUSTER_PLACE
					|| selectedMarker.getType() == MarkerType.CLUSTER_POSITION) {
				cameraShowSelectedMarker(selectedMarker, false);
				return true;
			}

			switch (state.getState()) {
			case ALL_CHILDREN:
			case CHILD_SELECTED:
				/*
				 * Determine the selected child
				 */
				TmlChild newChildSelected = null;
				for (TmlChild tempChild : childrenList) {
					if (tempChild.getView().getLastPositionMarker()
							.equals(selectedMarker)) {
						newChildSelected = tempChild;
						break;
					}
				}
				if (newChildSelected == null) {
					/*
					 * No child bind to the selected marker. Error.
					 */
					Log.e(TAG, "Unknown marker selected");
					return false;
				}
				if (state.getState() == StateType.ALL_CHILDREN) {
					/*
					 * New child selected. Show info view and save camera
					 * position
					 */
					childSelected = newChildSelected;
					state.initChildSelected();
					cameraSave();
					cameraShowSelectedMarker(selectedMarker, true);
				} else {
					/* CHILD_SELECTED */
					if (newChildSelected == childSelected) {
						/*
						 * Same child selected again. initialize singleChildMode
						 */
						state.childSelectedToSingleChild();

						cameraShowLastPosition();
					} else {
						/*
						 * New child selected. Update CHILD_SELECTED
						 */
						childSelected = newChildSelected;
						state.refreshChildSelected();
						cameraShowSelectedMarker(selectedMarker, true);
					}
				}

				break;
			case SINGLE_CHILD:
				if (selectedMarker.getType() == MarkerType.POSITION) {
					/*
					 * Last position marker selected
					 */
					navigator.last();
					return true;
				} else {
					/*
					 * Place marker selected
					 */
					navigator.set(selectedMarker);
				}
				cameraShowSelectedMarker(selectedMarker, true);
				break;
			default:
				break;
			}
			return true;
		}

	};

	/**
	 * Map click and long click listener
	 */
	private class MapClickListener implements OnMapClickListener,
			OnMapLongClickListener {

		/**
		 * Map click listener.
		 */
		@Override
		public void onMapClick(LatLng clickPoint) {
			switch (state.getState()) {
			case ALL_CHILDREN:
				break;
			case CHILD_SELECTED:
				childSelected = null;
				state.exitChildSelected();
				cameraRestore();
				break;
			case SAFE_ZONE_ADD:
				safeZoneDrawingAddPoint(clickPoint);
				break;
			case SAFE_ZONE_CLEAR:
				break;
			case SAFE_ZONE_REMOVE:
				for (TmlSafeZone safeZoneTemp : childSelected.getSafeZoneList()) {
					if (safeZoneTemp.contains(clickPoint)) {
						safeZoneSelected = safeZoneTemp;
						safeZoneRemoveSelectedConfirmDialog();
						break;
					}
				}
				break;
			case SINGLE_CHILD:
				break;
			default:
				break;
			}
		}

		/**
		 * Map long click listener.
		 * 
		 * Enter addSafeZoneMode or delete selected SafeZone
		 */
		@Override
		public void onMapLongClick(LatLng clickPoint) {
			if (state.getState() == StateType.SINGLE_CHILD) {
				/*
				 * Check if long click inside a safeZone
				 */
				boolean safeZoneRemove = false;
				for (TmlSafeZone safeZoneTemp : childSelected.getSafeZoneList()) {
					if (safeZoneTemp.isValid()
							&& safeZoneTemp.contains(clickPoint)) {
						safeZoneSelected = safeZoneTemp;
						state.initSafeZoneRemove();
						safeZoneRemoveSelectedConfirmDialog();
						safeZoneRemove = true;
						break;
					}
				}
				if (!safeZoneRemove) {
					state.initSafeZoneAdd();
				}
			}
		}
	}

	/**
	 * Listener for Navigation Drawer click event
	 */
	private OnItemClickListener navigationDrawerOnItemClickListener = new OnItemClickListener() {
		/**
		 * Navigation Drawer Click.
		 * 
		 * @param parent
		 * @param view
		 * @param position
		 *            index of the clicked element in the drawer
		 * @param id
		 */
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			if (position == childrenList.size()) {
				/*
				 * Show all children button pressed
				 */
				switch (state.getState()) {
				case ALL_CHILDREN:
					break;
				case CHILD_SELECTED:
					state.exitChildSelected();
					break;
				case SINGLE_CHILD:
					state.exitSingleChild();
					break;
				default:
					break;
				}
				cameraShowLastPositions();
			} else {
				/*
				 * Set the selectedChild, initialize the singleChildMode, move
				 * camera to selectedChild last position
				 */
				childSelected = childrenList.get(position);
				switch (state.getState()) {
				case ALL_CHILDREN:
					state.allChildrenToSingleChild();
					break;
				case CHILD_SELECTED:
					state.childSelectedToSingleChild();
					break;
				case SINGLE_CHILD:
					state.refreshSingleChild();
					break;
				default:
					break;
				}
				cameraShowLastPosition();
			}
			/*
			 * After any selection close the drawer
			 */
			navigationDrawerLayout.closeDrawers();
		}
	};

	/**
	 * Local Broadcast Receiver used to retrieve children notification updates.
	 * This is necessary because only the UI thread can call
	 * child.getView().update on the updated children
	 */
	private BroadcastReceiver localBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null
					&& intent.getAction() != null
					&& intent
							.getAction()
							.equals(TmlServicesManager.BROADCAST_ACTION_CHILDREN_UPDATE)) {
				shadowInitialSyncCheck();
				shadowFirstPositionCheck();
				initialViewCreationCheck();
				childrenUpdate();
			}
		}
	};

	/**
	 * Info view click listener. Enter in singleChildMode
	 */
	private OnClickListener infoViewClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (state.getState()) {
			case CHILD_SELECTED:
				state.childSelectedToSingleChild();
				cameraShowLastPosition();
				break;
			case SINGLE_CHILD:
				cameraShowSelectedMarker(navigator.getSelectedMarker(), true);
				break;
			default:
				break;
			}
		}
	};

	/**
	 * Called when camera animation ends
	 */
	private OnCameraChangeListener mapOnCameraChangeListener = new OnCameraChangeListener() {
		@Override
		public void onCameraChange(CameraPosition position) {
			/*
			 * Determine if the zoom level has changed since last camera
			 * animation
			 */
			float zoomLevel = map.getCameraPosition().zoom;
			if (zoomLevel > oldZoomLevel) {
				/*
				 * Zooming in. Delete old clusters and calculate new.
				 */
				restoreMarkerList();
				performClustering();
			} else if (zoomLevel < oldZoomLevel) {
				performClustering();
			}
			oldZoomLevel = zoomLevel;

			/*
			 * Set the centerCameraButton background resource if this animation
			 * has not been called with the centerCameraButton
			 */
			if (centerCameraAnimation) {
				centerCameraAnimation = false;
			} else {
				if (TmlChild.isParentDevice()
						&& (state.getState() == StateType.ALL_CHILDREN || state
								.getState() == StateType.CHILD_SELECTED)) {
					centerCameraButton
							.setImageResource(R.drawable.center_all_highlight);
				} else {
					centerCameraButton
							.setImageResource(R.drawable.center_child_highlight);
				}
				centerCameraButton.setEnabled(true);
			}
		}
	};

	/**
	 * Children listener as required by TmlChild. This callback is executed on
	 * the calling thread, that cannot modify the UI. Tue UI update is performed
	 * when the local broadcast BROADCAST_ACTION_CHILDREN_UPDATE is received.
	 * 
	 * This listener is used only to keep track of what was modified by the
	 * calling thread
	 */
	private TmlChildrenListener childrenListener = new TmlChildrenListener() {

		@Override
		public void onAdd(TmlChild child) {
			addedChildrenList.add(child);
		}

		@Override
		public void onRemove(TmlChild child) {
			removedChildrenList.add(child);
		}

		@Override
		public void onUpdate(TmlChild child) {
			if (!updatedChildrenList.contains(child)) {
				updatedChildrenList.add(child);
			}
		}

	};

	/**
	 * Callback for prev/next buttons in infoView
	 */
	private OnClickListener infoArrowClickListener = new OnClickListener() {
		@Override
		public void onClick(View pressedButton) {

			if (pressedButton.equals(infoView.nextButton)) {
				/*
				 * Next button pressed.
				 */
				navigator.next();
				cameraShowSelectedMarker(navigator.getSelectedMarker(), true);
			} else if (pressedButton.equals(infoView.prevButton)) {
				/*
				 * Prev button pressed.
				 */
				navigator.prev();
				cameraShowSelectedMarker(navigator.getSelectedMarker(), true);
			}
		}

	};

	/**
	 * Listener for centerCameraButton
	 */
	private OnClickListener centerCameraButtonListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (TmlChild.isParentDevice()
					&& (state.getState() == StateType.ALL_CHILDREN || state
							.getState() == StateType.CHILD_SELECTED)) {
				cameraShowLastPositions();
			} else {
				cameraShowHistory();
			}
		}

	};

	/**
	 * Callback for ActionMode used when drawing SafeZones
	 */
	private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

		/**
		 * Called when the action mode is created; startSupportActionMode() was
		 * called
		 */
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuInflater inflater = mode.getMenuInflater();
			inflater.inflate(R.menu.actionmode_add_safezone, menu);
			return true;
		}

		/**
		 * Called each time the action mode is shown. Always called after
		 * onCreateActionMode, but may be called multiple times if the mode is
		 * invalidated.
		 */
		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		/**
		 * Called when the user selects a contextual menu item
		 */
		@Override
		public boolean onActionItemClicked(ActionMode am, MenuItem item) {
			switch (item.getItemId()) {
			case R.id.action_back:
				if (state.getState() == StateType.SAFE_ZONE_ADD) {
					safeZoneDrawingRemoveLastPoint();
				}
				return true;
			case R.id.action_cancel:
				if (state.getState() == StateType.SAFE_ZONE_ADD) {
					state.exitSafeZoneAdd();
				}
				return true;
			}
			return false;
		}

		/**
		 * Called when the user exits the action mode using the End button
		 */
		@Override
		public void onDestroyActionMode(ActionMode am) {
			actionMode = null;
			if (state.getState() == StateType.SAFE_ZONE_ADD) {
				safeZoneDrawingDone();
			}
		}

	};

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- ENUMS ---------------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Shadow layout type. In descending order of importance: first
	 * synchronization, loading, waiting first position
	 */
	private enum ShadowType {
		NONE, INIT_SYNC, MAP_LOADING, FIRST_POSITION
	};

	/**
	 * Activity status types
	 */
	private enum StateType {
		NONE, ALL_CHILDREN, CHILD_SELECTED, SINGLE_CHILD, SAFE_ZONE_ADD, SAFE_ZONE_REMOVE, SAFE_ZONE_CLEAR
	};

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- CLASSES -------------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Activity status related fields and methods
	 */
	private class State {

		private static final String TAG = "TmlMapActivity.State";

		/**
		 * Active state
		 */
		private StateType state = StateType.NONE;

		/**
		 * The actual activity state
		 * 
		 * @return
		 */
		public StateType getState() {
			return state;
		}

		/*
		 * ---------------------- TRANSITIONS METHODS -------------------------
		 * 
		 * Camera update is not performed. markerList is cleaned and rebuilt
		 * based on shown elements
		 */

		/**
		 * Initialize ALL_CHILDREN
		 * 
		 * Parent only.
		 * 
		 * Called only from onCreate
		 */
		public void initAllChildren() {
			if (state != StateType.NONE) {
				Log.e(TAG, "initAllChildren incorrect state");
			}
			state = StateType.ALL_CHILDREN;
			supportInvalidateOptionsMenu();

			refreshMapElements();

			childSelected = null;
			navigationDrawerAdapter.setSelectedChild(null);

		}

		/**
		 * Initialize SINGLE_CHILD
		 * 
		 * Child only. childSelected can't be null.
		 * 
		 * Called only from onCreate.
		 * 
		 */
		public void initSingleChild() {
			if (state != StateType.NONE) {
				Log.e(TAG, "initSingleChild incorrect state");
			}
			state = StateType.SINGLE_CHILD;
			supportInvalidateOptionsMenu();

			refreshMapElements();

			navigator.last();
			infoView.show();

		}

		/**
		 * Refresh SINGLE_CHILD due to childSelected changed
		 * 
		 * Parent only. childSelected can't be null.
		 * 
		 * Called on navigation drawer selection or intent from notification
		 * 
		 */
		public void refreshSingleChild() {
			if (state != StateType.SINGLE_CHILD) {
				Log.e(TAG, "refreshSingleChild incorrect state");
			}
			supportInvalidateOptionsMenu();

			refreshMapElements();
			navigationDrawerAdapter.setSelectedChild(childSelected);

			navigator.last();
		}

		/**
		 * Exit SINGLE_CHILD and goto ALL_CHILDREN
		 * 
		 * Parent only.
		 * 
		 * Called when "Show all children" pressed in navigation drawer, back
		 * button pressed, childSelected is deleted
		 */
		public void exitSingleChild() {
			if (state != StateType.SINGLE_CHILD) {
				Log.e(TAG, "exitSingleChild incorrect state");
			}
			state = StateType.ALL_CHILDREN;
			supportInvalidateOptionsMenu();

			refreshMapElements();

			infoView.hide();

			childSelected = null;
			navigationDrawerAdapter.setSelectedChild(null);

		}

		/**
		 * Exit ALL_CHILDREN and goto CHILD_SELECTED
		 * 
		 * Parent only. childSelected can't be null.
		 * 
		 * Called when child marker is selected.
		 * 
		 */
		public void initChildSelected() {
			if (state != StateType.ALL_CHILDREN) {
				Log.e(TAG, "initChildSelected incorrect state");
			}
			state = StateType.CHILD_SELECTED;

			navigator.last();

			infoView.show();

		}

		/**
		 * Refresh CHILD_SELECTED due to childSelected changed
		 * 
		 * Parent only. childSelected can't be null.
		 * 
		 * Called on marker tap on different child
		 * 
		 */
		public void refreshChildSelected() {
			if (state != StateType.CHILD_SELECTED) {
				Log.e(TAG, "refreshChildSelected incorrect state");
			}

			navigator.last();

		}

		/**
		 * Exit CHILD_SELECTED and goto ALL_CHILDREN
		 * 
		 * Parent only.
		 * 
		 * Called when "Show all children" pressed in navigation drawer, back
		 * button pressed or empty map point clicked
		 */
		public void exitChildSelected() {
			if (state != StateType.CHILD_SELECTED) {
				Log.e(TAG, "exitChildSelected incorrect state");
			}
			state = StateType.ALL_CHILDREN;

			infoView.hide();

			childSelected = null;
		}

		/**
		 * Exit CHILD_SELECTED and goto SINGLE_CHILD
		 * 
		 * Parent only. childSelected can't be null.
		 * 
		 * 
		 */
		public void childSelectedToSingleChild() {
			if (state != StateType.CHILD_SELECTED) {
				Log.e(TAG, "childSelectedToSingleChild incorrect state");
			}
			state = StateType.SINGLE_CHILD;
			supportInvalidateOptionsMenu();

			refreshMapElements();

			navigationDrawerAdapter.setSelectedChild(childSelected);

			navigator.last();

			singleChildModeGuide();
		}

		/**
		 * Exit ALL_CHILDREN and goto SINGLE_CHILD
		 * 
		 * Parent only. childSelected can't be null.
		 * 
		 * Called on navigation drawer selection or intent from notification
		 */
		public void allChildrenToSingleChild() {
			if (state != StateType.ALL_CHILDREN) {
				Log.e(TAG, "allChildrenToSingleChild incorrect state");
			}
			state = StateType.SINGLE_CHILD;
			supportInvalidateOptionsMenu();

			refreshMapElements();

			navigator.last();
			infoView.show();

			navigationDrawerAdapter.setSelectedChild(childSelected);

			singleChildModeGuide();

		}

		/**
		 * Exit SINGLE_CHILD and goto SAFE_ZONE_ADD
		 * 
		 * Parent only. childSelected can't be null.
		 */
		public void initSafeZoneAdd() {
			if (state != StateType.SINGLE_CHILD) {
				Log.e(TAG, "initSafeZoneAdd incorrect state");
			}
			state = StateType.SAFE_ZONE_ADD;

			/*
			 * Create the candidateSafeZone
			 */
			safeZoneCandidate = TmlSafeZone.getNewSafeZone(childSelected);
			safeZoneCandidate.createView(map);

			/*
			 * Activate ActionMode
			 */
			if (actionMode == null) {
				actionMode = startSupportActionMode(actionModeCallback);
			}

			refreshMapElements();

			/*
			 * Lock navigation drawer
			 */
			navigationDrawerLayout
					.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

			infoView.hide();

			/*
			 * Show the addSafeZone description dialog
			 */
			if (userGuide.autoShow(DescriptionType.ADD_SAFE_ZONE)) {
				userGuide.showDescription(DescriptionType.ADD_SAFE_ZONE);
			}

		}

		/**
		 * Exit SAFE_ZONE_ADD and goto SINGLE_CHILD
		 * 
		 * Parent only. childSelected can't be null.
		 */
		public void exitSafeZoneAdd() {
			if (state != StateType.SAFE_ZONE_ADD) {
				Log.e(TAG, "exitSafeZoneAdd incorrect state");
			}
			state = StateType.SINGLE_CHILD;

			supportInvalidateOptionsMenu();

			/*
			 * Remove the candidate safeZone
			 */
			if (safeZoneCandidate != null) {
				safeZoneCandidate.cancel();
				safeZoneCandidate = null;
			}

			/*
			 * Deactivate ActionMode if not yet deactivated
			 */
			if (actionMode != null) {
				actionMode.finish();
			}

			refreshMapElements();
			/*
			 * A new safezone could have modified the safeness. Refresh the
			 * infoView
			 */
			navigator.update();
			infoView.show();

			/*
			 * Unlock the navigation drawer
			 */
			navigationDrawerLayout
					.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
		}

		/**
		 * Exit SINGLE_CHILD and goto SAFE_ZONE_REMOVE
		 * 
		 * Parent only. childSelected can't be null.
		 */
		public void initSafeZoneRemove() {
			if (state != StateType.SINGLE_CHILD) {
				Log.e(TAG, "initSafeZoneRemove incorrect state");
			}
			state = StateType.SAFE_ZONE_REMOVE;
			supportInvalidateOptionsMenu();

			infoView.hide();

			refreshMapElements();

			/*
			 * Lock navigation drawer
			 */
			navigationDrawerLayout
					.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

			userGuideToastSafeZoneRemove();
		}

		/**
		 * Exit SAFE_ZONE_REMOVE and goto SINGLE_CHILD
		 * 
		 * Parent only. childSelected can't be null.
		 */
		public void exitSafeZoneRemove() {
			if (state != StateType.SAFE_ZONE_REMOVE) {
				Log.e(TAG, "exitSafeZoneRemove incorrect state");
			}
			state = StateType.SINGLE_CHILD;
			supportInvalidateOptionsMenu();

			/*
			 * Deleted SafeZones could have modified the safeness. Refresh the
			 * infoView
			 */
			navigator.update();
			infoView.show();

			refreshMapElements();

			/*
			 * Unlock the navigation drawer
			 */
			navigationDrawerLayout
					.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
		}

		/**
		 * Exit SINGLE_CHILD and goto SAFE_ZONE_CLEAR
		 * 
		 * Parent only. childSelected can't be null.
		 */
		public void initSafeZoneClear() {
			if (state != StateType.SINGLE_CHILD) {
				Log.e(TAG, "initSafeZoneClear incorrect state");
			}
			state = StateType.SAFE_ZONE_CLEAR;

			infoView.hide();

			refreshMapElements();

			/*
			 * Show the clear SafeZone confirm dialog
			 */
			new TmlDialogFragment()
					.setMessage(R.string.map_dialog_clearSafezones_text)
					.showNegativeButton(true)
					.show(getSupportFragmentManager(),
							this.getClass().getName());
		}

		/**
		 * Exit SAFE_ZONE_CLEAR and goto SINGLE_CHILD
		 * 
		 * Parent only. childSelected can't be null.
		 */
		public void exitSafeZoneClear() {
			if (state != StateType.SAFE_ZONE_CLEAR) {
				Log.e(TAG, "exitSafeZoneClear incorrect state");
			}
			state = StateType.SINGLE_CHILD;

			supportInvalidateOptionsMenu();

			/*
			 * Deleted SafeZones could have modified the safeness. Refresh the
			 * infoView
			 */
			navigator.update();
			infoView.show();

			refreshMapElements();
		}

		/**
		 * Show the history description dialog or the addSafeZone long press
		 * toast
		 */
		private void singleChildModeGuide() {
			if (TmlChild.isParentDevice()
					&& userGuide.autoShow(DescriptionType.HISTORY)
					&& dialogRegister.size() == 0) {
				/*
				 * Show track description dialog
				 */
				dialogRegister.add(userGuide
						.showDescription(DescriptionType.HISTORY));

			} else {
				userGuideToastSafeZoneAdd();
			}
		}

	}

	/**
	 * History navigator. Used to setupInfoView and manage highlighted markers.
	 * No camera animation is performed
	 */
	private class Navigator {

		/**
		 * Index of the selected position
		 */
		private int idx = -1;

		/**
		 * Currently selected marker
		 */
		private TmlMarker selectedMarker;

		/**
		 * Selected position idx
		 * 
		 * @return
		 */
		public int getIdx() {
			return idx;
		}

		/**
		 * Get the currently selected marker
		 */
		public TmlMarker getSelectedMarker() {
			return selectedMarker;
		}

		/**
		 * Return the selected position.
		 * 
		 * @return null if last or invalid position is selected
		 */
		public TmlPosition getSelectedPosition() {
			if (idx < 0
					|| idx >= childSelected.getFilteredPositionList().size()) {
				return null;
			}
			return childSelected.getFilteredPositionList().get(idx);
		}

		/**
		 * Select next position, setup infoView and highlight selectedMarker
		 */
		public void next() {
			if (idx == -1) {
				return;
			}
			idx++;
			if (idx == childSelected.getFilteredPositionList().size()) {
				idx = -1;
			}
			update();
		}

		/**
		 * Select previous position, setup infoView and highlight selectedMarker
		 */
		public void prev() {
			if (idx == 0) {
				return;
			}
			if (idx == -1) {
				idx = childSelected.getFilteredPositionList().size() - 1;
			} else {
				idx--;
			}
			update();
		}

		/**
		 * Select last position, setup infoView and remove highlight from the
		 * place markers
		 */
		public void last() {
			idx = -1;
			update();
		}

		/**
		 * Place marker has been selected. Determine idx and setup If a place
		 * contains more than a position the last in chronological order is
		 * selected
		 * 
		 * @param clickedMarker
		 */
		public void set(TmlMarker clickedMarker) {
			for (idx = childSelected.getFilteredPositionList().size() - 1; idx >= 0; idx--) {
				if (childSelected.getFilteredPositionList().get(idx).getPlace()
						.getMarker().equals(clickedMarker)) {
					break;
				}
			}
			update();
		}

		/**
		 * Set idx and setup infoView on the given position. If not found (e.g.
		 * too old) last position is selected
		 * 
		 * @param position
		 */
		public void set(TmlPosition position) {
			idx = childSelected.getFilteredPositionList().indexOf(position);
			update();
		}

		/**
		 * Selected position is the first one
		 * 
		 * @return
		 */
		public boolean isFirst() {
			return idx == 0;
		}

		/**
		 * Highlight the selected position marker and de-highlight the others.
		 * Also update the infoView
		 */
		public void update() {

			/*
			 * Disable highlighted marker if visible
			 */
			if (selectedMarker != null) {
				selectedMarker.setHighlighted(false);
				selectedMarker = null;
			}

			/*
			 * Determine the marker to be highlighted
			 */
			if (idx == -1) {
				selectedMarker = childSelected.getView()
						.getLastPositionMarker();
			} else {
				TmlPlace selectedPlace = childSelected
						.getFilteredPositionList().get(idx).getPlace();
				selectedMarker = selectedPlace.getMarker();
			}

			if (selectedMarker != null) {
				selectedMarker.setHighlighted(true);
			}

			infoView.setup();

		}

	}

	/**
	 * InfoView related fields and methods
	 */
	private class InfoView {

		/*
		 * Enclosing layout
		 */
		public LinearLayout layout;

		/*
		 * Name and color square
		 */
		public TextView nameText;
		public ImageView nameIcon;

		/*
		 * Last position views
		 */
		public LinearLayout positionLayout;
		public TextView safeText;
		public ImageView safeIcon;
		public TextView timeText;
		public ImageView timeIcon;
		public TextView batteryText;
		public ImageView batteryIcon;

		/*
		 * Place views
		 */
		public LinearLayout placeLayout;
		public TextView placeTime;

		/*
		 * Navigation buttons
		 */
		public Button prevButton;
		public Button nextButton;

		/**
		 * Height calculated at runtime
		 */
		public int fullHeight;

		/*
		 * -------------------------- ANIMATIONS ------------------------------
		 */

		/**
		 * Animation used to show infoView and modify map padding
		 */
		private Animation showAnimation = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime,
					Transformation t) {
				int infoWindowHeight = (int) (fullHeight * (interpolatedTime));
				layout.getLayoutParams().height = infoWindowHeight;
				layout.requestLayout();
				map.setPadding(0, actionBarHeight, 0, infoWindowHeight);
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		/**
		 * Animation used to hide infoView and modify map padding
		 */
		private Animation hideAnimation = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime,
					Transformation t) {
				int infoWindowHeight = (int) (fullHeight * (1f - interpolatedTime));
				if (interpolatedTime == 1) {
					layout.setVisibility(View.INVISIBLE);
				}
				layout.getLayoutParams().height = infoWindowHeight;
				layout.requestLayout();
				map.setPadding(0, actionBarHeight, 0, infoWindowHeight);
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		/*
		 * -------------------------- METHODS ---------------------------------
		 */

		/**
		 * Initialize infoView
		 */
		public void init() {

			showAnimation.setDuration(INFO_WINDOW_SHOW_DURATION);
			hideAnimation.setDuration(INFO_WINDOW_HIDE_DURATION);

			RelativeLayout mapFrame = (RelativeLayout) findViewById(R.id.map_frame);

			if (TmlChild.isParentDevice()) {
				layoutInflater.inflate(R.layout.info_view_parent, mapFrame);
			} else {
				layoutInflater.inflate(R.layout.info_view_child, mapFrame);
			}

			layout = (LinearLayout) findViewById(R.id.info_view_layout);

			layout.measure(
					MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
					MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));

			fullHeight = layout.getMeasuredHeight();

			layout.setVisibility(View.INVISIBLE);
			layout.getLayoutParams().height = 0;
			layout.requestLayout();

			positionLayout = (LinearLayout) findViewById(R.id.info_view_position_layout);
			placeLayout = (LinearLayout) findViewById(R.id.info_view_place_layout);

			placeLayout.setVisibility(View.GONE);

			nameText = (TextView) findViewById(R.id.child_info_name_text);
			nameIcon = (ImageView) findViewById(R.id.child_info_name_icon);

			if (TmlChild.isParentDevice()) {
				safeText = (TextView) findViewById(R.id.child_info_safeness_text);
				safeIcon = (ImageView) findViewById(R.id.child_info_safeness_icon);
			}

			timeText = ((TextView) findViewById(R.id.child_info_freshness_text));
			timeIcon = ((ImageView) findViewById(R.id.child_info_freshness_icon));
			batteryText = ((TextView) findViewById(R.id.child_info_batterycharge_text));
			batteryIcon = ((ImageView) findViewById(R.id.child_info_batterycharge_icon));

			layout.setOnClickListener(infoViewClickListener);

			placeTime = (TextView) findViewById(R.id.info_view_time);

			prevButton = (Button) findViewById(R.id.info_view_prev);
			nextButton = (Button) findViewById(R.id.info_view_next);

			prevButton.setOnClickListener(infoArrowClickListener);
			nextButton.setOnClickListener(infoArrowClickListener);

			prevButton.setVisibility(View.INVISIBLE);
			nextButton.setVisibility(View.GONE);
		}

		/**
		 * Setup info window according to the state and the selected marker.
		 * Arrows are shown/hidden as required
		 */
		public void setup() {

			if (childSelected == null || navigator.getSelectedMarker() == null) {
				return;
			}

			/*
			 * Determine the infoView configuration based on the activity state
			 */
			switch (state.getState()) {
			case ALL_CHILDREN:
			case CHILD_SELECTED:
				nameIcon.setVisibility(View.VISIBLE);
				nameText.setVisibility(View.VISIBLE);

				prevButton.setVisibility(View.INVISIBLE);
				nextButton.setVisibility(View.GONE);

				break;
			case SINGLE_CHILD:
				if (navigator.getSelectedMarker().getType() == MarkerType.POSITION) {
					nextButton.setVisibility(View.GONE);
					prevButton.setVisibility(View.VISIBLE);
				} else {
					nextButton.setVisibility(View.VISIBLE);
					if (navigator.isFirst()) {
						prevButton.setVisibility(View.INVISIBLE);
					} else {
						prevButton.setVisibility(View.VISIBLE);
					}
				}
				break;
			default:
				break;

			}

			/*
			 * Setup the info view according to the selected marker
			 */
			switch (navigator.getSelectedMarker().getType()) {
			case POSITION:

				positionLayout.setVisibility(View.VISIBLE);
				placeLayout.setVisibility(View.GONE);

				nameIcon.setImageResource(childSelected.getView().getColor()
						.getSquareResource());

				if (TmlChild.isParentDevice()) {
					nameText.setText(childSelected.getName().toUpperCase(
							Locale.getDefault()));
				} else {
					if (childSelected.getName().length() > 3) {
						nameText.setText(childSelected.getName()
								.subSequence(0, 3).toString()
								.toUpperCase(Locale.getDefault()));
					} else {
						nameText.setText(childSelected.getName().toUpperCase(
								Locale.getDefault()));
					}
				}

				if (TmlChild.isParentDevice()) {

					safeIcon.setImageResource(childSelected.getView()
							.getSafenessResource());
					safeText.setText(childSelected.getView()
							.getSafenessString());
				}

				if (childSelected.isLastPositionEarlierThan(24)) {
					timeText.setText(childSelected.getView().getTime());
				} else {
					timeText.setText(childSelected.getView().getDate());
				}
				timeIcon.setImageResource(childSelected.getView()
						.getFreshnessResource());

				batteryText.setText(childSelected.getView()
						.getBatteryChargeString());
				batteryIcon.setImageResource(childSelected.getView()
						.getBatteryChargeResource());

				break;
			case PLACE:
				positionLayout.setVisibility(View.GONE);
				placeLayout.setVisibility(View.VISIBLE);

				placeTime.setText(childSelected.getView().getInfoTimeText(
						navigator.getIdx()));

				break;
			default:
				break;
			}
		}

		/**
		 * Show info window at the bottom of the screen
		 */
		public void show() {
			if (layout.getLayoutParams().height == 0
					&& layout.getVisibility() == View.INVISIBLE) {
				/*
				 * If necessary show info view infoView description dialog
				 */
				if (TmlChild.isParentDevice()
						&& state.getState() == StateType.CHILD_SELECTED
						&& userGuide.autoShow(DescriptionType.INFO_VIEW)) {
					dialogRegister.add(userGuide
							.showDescription(DescriptionType.INFO_VIEW));
				}
				layout.setVisibility(View.VISIBLE);
				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
					map.setPadding(0, actionBarHeight, 0, fullHeight);
					layout.getLayoutParams().height = fullHeight;
					layout.requestLayout();
				} else {
					layout.startAnimation(showAnimation);
				}

			}
		}

		/**
		 * Hide info window at the bottom of the screen
		 */
		public void hide() {
			if (layout.getLayoutParams().height > 0
					&& layout.getVisibility() == View.VISIBLE) {
				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
					layout.setVisibility(View.INVISIBLE);
					map.setPadding(0, actionBarHeight, 0, 0);
					layout.getLayoutParams().height = 0;
					layout.requestLayout();
				} else {
					layout.startAnimation(hideAnimation);
				}
			}

		}
	}

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- ATTRIBUTES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Map instance on this activity
	 */
	private GoogleMap map;

	/**
	 * Set when map loading has completed
	 */
	private boolean mapLoadingCompleted;

	/**
	 * Activity state instance
	 */
	private State state = new State();

	/**
	 * History navigator instance
	 */
	private Navigator navigator = new Navigator();

	/**
	 * InfoView instance
	 */
	private InfoView infoView = new InfoView();

	/**
	 * CenterCamera Button
	 */
	private ImageButton centerCameraButton;

	/**
	 * Set when an automatic camera animation is started with the center button.
	 * reset at the end of the animation
	 */
	private boolean centerCameraAnimation;

	/**
	 * Instance of MapClickListener
	 */
	private MapClickListener mapClickListener = new MapClickListener();

	/**
	 * List of TmlChild. In a parent device this includes an instance for every
	 * child. In a child device this list has a single element
	 */
	private ArrayList<TmlChild> childrenList;

	/**
	 * Initial view creation completed
	 */
	private boolean initialViewCreationCompleted;

	/**
	 * List of markers shown on the map without clustering
	 */
	private final ArrayList<TmlMarker> markerListFull = new ArrayList<TmlMarker>();

	/**
	 * List of markers actually shown on the map.
	 * 
	 * In singleChildMode this contains the placeMarkers and the
	 * lastPositionMarker.
	 * 
	 * In multiChildrenMode this contains the lastPositionMarker for every
	 * child.
	 * 
	 * After clustering is performed the list contains only the visible markers
	 * and the generated cluster markers.
	 */
	private final ArrayList<TmlMarker> markerList = new ArrayList<TmlMarker>();

	/**
	 * List of children whose addition has been notified through
	 * TmlChildListener.onAdd().
	 */
	private final ArrayList<TmlChild> addedChildrenList = new ArrayList<TmlChild>();

	/**
	 * List of children whose update has been notified through
	 * TmlChildListener.onUpdate().
	 */
	private final ArrayList<TmlChild> updatedChildrenList = new ArrayList<TmlChild>();

	/**
	 * List of children whose removal has been notified through
	 * TmlChildListener.onRemove().
	 */
	private final ArrayList<TmlChild> removedChildrenList = new ArrayList<TmlChild>();

	/*
	 * Navigation drawer related
	 */
	private DrawerLayout navigationDrawerLayout;
	private ListView navigationDrawerListView;
	private ActionBarDrawerToggle actionBarDrawerToggle;
	private TmlNavigationDrawerAdapter navigationDrawerAdapter;

	/**
	 * ActionBar instance
	 */
	private ActionBar actionBar;

	/**
	 * ActionMode instance
	 */
	private ActionMode actionMode;

	/**
	 * ActionBar height in pixel. Determined at runtime and used to set map
	 * padding
	 */
	private int actionBarHeight;

	/**
	 * Selected child, in singleChildMode or not
	 */
	private TmlChild childSelected;

	/**
	 * SafeZone instance used when building a new SafeZone
	 */
	private TmlSafeZone safeZoneCandidate;

	/**
	 * SafeZoneView instance used when removing a SafeZone
	 */
	private TmlSafeZone safeZoneSelected;

	/**
	 * Camera position settings used to backup a specific camera position and
	 * restore it when needed
	 */
	private CameraPosition cameraPosition;

	/**
	 * Display metrics
	 */
	private DisplayMetrics displayMetrics;

	/**
	 * Notification manager instance. Used to call riseNotification() when
	 * safeZone modified
	 */
	private TmlNotification notificationManager;

	/**
	 * Zoom level of last camera animation
	 */
	private float oldZoomLevel;

	/**
	 * Layout inflater instance
	 */
	private LayoutInflater layoutInflater;

	/**
	 * TmlUserGuide instance
	 */
	private TmlUserGuide userGuide;

	/**
	 * User guide dialog register
	 */
	private final ArrayList<DialogInterface> dialogRegister = new ArrayList<DialogInterface>();

	/**
	 * User guide dialog noChild dialog instance. Kept to automatically cancel
	 * the dialog when a child is available
	 */
	private AlertDialog dialogNoChild;

	/**
	 * Shadow Layout instance
	 */
	private ViewGroup shadowLayout;

	/**
	 * Shadow layout shown
	 */
	private ShadowType shadowType = ShadowType.NONE;

	/**
	 * List of progress bar shown during initial synchronization
	 */
	private ArrayList<ProgressBar> initialSyncProgressBarList;

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- METHODS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Add, remove, update view elements when all the initialization procedures
	 * are done
	 */
	private void childrenUpdate() {

		if (initialViewCreationCompleted && mapLoadingCompleted) {

			int idx;
			/*
			 * Cycle through the added children. If initial loading has been
			 * completed create the view and delete from addedChildrenList
			 */
			idx = 0;
			while (idx < addedChildrenList.size()) {
				TmlChild addedChild = addedChildrenList.get(idx);
				addedChild.createView(map);
				if (state.getState() == StateType.ALL_CHILDREN
						|| state.getState() == StateType.CHILD_SELECTED) {
					addedChild.getView().showLastPositionMarker();
				}
				childrenList.add(addedChild);
				if (TmlChild.isParentDevice() && childrenList.size() == 1) {
					/*
					 * First children added. Unlock the interface
					 */
					for (DialogInterface dialog : dialogRegister) {
						if (dialog.equals(dialogNoChild)) {
							dialog.cancel();
						}
					}
					refreshMapElements();
					cameraInitialAnimation();
					userGuideToastPositionMarkerTap();
				}
				navigationDrawerAdapter.addChild(addedChild);
				addedChildrenList.remove(idx);
			}

			/*
			 * Cycle through the removed children. Remove their views and delete
			 * the reference in childrenList. If selectedChild is the removed
			 * one show all the children
			 */
			idx = 0;
			while (idx < removedChildrenList.size()) {
				TmlChild removedChild = removedChildrenList.get(idx);
				if (removedChild.isInitialLoadingCompleted()) {
					removedChild.deleteView();
					childrenList.remove(removedChild);
					if (TmlChild.isParentDevice() && childrenList.size() == 0) {
						/*
						 * Show no child screen. It can't be closed manually.
						 */
						dialogNoChild = userGuide
								.showDescription(DescriptionType.NO_CHILD);
						dialogRegister.add(dialogNoChild);
					}
					if (childSelected == removedChild) {
						childSelected = null;
						switch (state.getState()) {
						case CHILD_SELECTED:
							state.exitChildSelected();
							break;
						case SAFE_ZONE_ADD:
							state.exitSafeZoneAdd();
							state.exitSingleChild();
							break;
						case SAFE_ZONE_CLEAR:
							state.exitSafeZoneClear();
							state.exitSingleChild();
							break;
						case SAFE_ZONE_REMOVE:
							state.exitSafeZoneRemove();
							state.exitSingleChild();
							break;
						case SINGLE_CHILD:
							state.exitSingleChild();
							break;
						default:
							break;
						}
						cameraShowLastPositions();
					}
					navigationDrawerAdapter.remove(removedChild);
					removedChildrenList.remove(idx);
				} else {
					idx++;
				}
			}

			/*
			 * Cycle through the updated children.
			 */
			while (updatedChildrenList.size() > 0) {
				TmlChild updatedChild = updatedChildrenList.get(0);

				/*
				 * Store previously selected position
				 */
				TmlPosition selectedPosition = null;
				if (childSelected != null && childSelected.equals(updatedChild)) {
					selectedPosition = navigator.getSelectedPosition();
				}

				/*
				 * Update all the map components for the child
				 */
				if (updatedChild.getView() != null) {
					updatedChild.getView().update();
				}

				/*
				 * Determine the new selectedPositionIdx
				 */
				if (state.getState() == StateType.SINGLE_CHILD) {
					if (childSelected.equals(updatedChild)) {
						navigator.set(selectedPosition);
					}
				}

				updatedChildrenList.remove(updatedChild);
			}

			/*
			 * Refresh map elements. If necessary update infoView
			 */
			refreshMapElements();
			navigationDrawerUpdate();
			if (state.getState() == StateType.CHILD_SELECTED
					|| state.getState() == StateType.SINGLE_CHILD) {
				navigator.update();
			}
		}

	}

	/**
	 * Check if initialViewCreation is completed. If true and
	 * mapLoadingCompleted perform camera animation. If false update view
	 * elements.
	 */
	private void initialViewCreationCheck() {
		if (!initialViewCreationCompleted) {
			if (TmlChild.initialViewCreationCompleted()) {
				initialViewCreationCompleted = true;
				if (mapLoadingCompleted) {
					refreshMapElements();
					cameraInitialAnimation();
				}
			} else {
				for (TmlChild child : childrenList) {
					if (!child.getView().isInitialViewCreationCompleted()) {
						child.getView().update();
					}
				}
			}
		}
	}

	/*
	 * ------------------------------------------------------------------------
	 * ------------------- SHADOW LAYOUT METHODS ------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Lock the interface and show the shadowLayout
	 */
	private void shadowLayoutShow() {

		/*
		 * Show and configure the shadow layout
		 */
		shadowLayout.setVisibility(View.VISIBLE);

		/*
		 * Disable the actionBar
		 */
		actionBar.setDisplayHomeAsUpEnabled(false);
		supportInvalidateOptionsMenu();

		/*
		 * Lock the navigation drawer
		 */
		if (TmlChild.isParentDevice()) {
			actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
			navigationDrawerLayout
					.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
		}

		/*
		 * Disable infoViewClickListener
		 */
		infoView.layout.setClickable(false);
		infoView.prevButton.setClickable(false);
		infoView.nextButton.setClickable(false);

		/*
		 * Disable map navigation
		 */
		map.getUiSettings().setAllGesturesEnabled(false);
		centerCameraButton.setClickable(false);

	}

	/**
	 * Unlock the interface and hide the shadowLayout. shadowType set to NONE
	 */
	private void shadowLayoutHide() {

		shadowType = ShadowType.NONE;

		/*
		 * Hide the shadow layout
		 */
		shadowLayout.removeAllViews();
		shadowLayout.setVisibility(View.GONE);

		/*
		 * Enable the actionBar
		 */
		if (TmlChild.isParentDevice()) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
		supportInvalidateOptionsMenu();

		/*
		 * Unlock the navigation drawer
		 */
		if (TmlChild.isParentDevice()) {
			actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
			navigationDrawerLayout
					.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
		}

		/*
		 * Enable infoViewClickListener
		 */
		infoView.layout.setClickable(true);
		infoView.prevButton.setClickable(true);
		infoView.nextButton.setClickable(true);

		/*
		 * Enable map navigation
		 */
		map.getUiSettings().setZoomGesturesEnabled(true);
		map.getUiSettings().setScrollGesturesEnabled(true);
		centerCameraButton.setClickable(true);

	}

	/**
	 * Show initial sync shadow and setup initial sync progress bar
	 */
	private void shadowInitialSync() {
		shadowType = ShadowType.INIT_SYNC;

		shadowLayout.removeAllViews();
		layoutInflater.inflate(R.layout.shadow_initial_sync, shadowLayout);
		initialSyncProgressBarList = new ArrayList<ProgressBar>();

		LinearLayout progressLayout = (LinearLayout) findViewById(R.id.shadow_init_sync_progress_layout);

		LinearLayout.LayoutParams progressBarLayoutParams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		progressBarLayoutParams.bottomMargin = (int) (8 * displayMetrics.density);

		for (int idx = 0; idx < childrenList.size(); idx++) {
			ProgressBar progressBar = new ProgressBar(this, null,
					android.R.attr.progressBarStyleHorizontal);
			progressBar.setLayoutParams(progressBarLayoutParams);
			progressBar.setMax(INIT_SYNC_PROGRESS_BAR_MAX);

			progressLayout.addView(progressBar);
			initialSyncProgressBarList.add(progressBar);
		}

		shadowLayoutShow();

	}

	/**
	 * Show initial loading shadow
	 */
	private void shadowLoading() {
		shadowType = ShadowType.MAP_LOADING;
		shadowLayout.removeAllViews();
		layoutInflater.inflate(R.layout.shadow_loading, shadowLayout);

		shadowLayoutShow();
	}

	/**
	 * Show first position shadow
	 */
	private void shadowFirstPosition() {
		shadowType = ShadowType.FIRST_POSITION;
		shadowLayout.removeAllViews();
		layoutInflater.inflate(R.layout.shadow_first_position, shadowLayout);

		shadowLayoutShow();
	}

	/**
	 * Update and eventually close initial sync shadow
	 */
	private void shadowInitialSyncCheck() {

		if (shadowType == ShadowType.INIT_SYNC) {

			for (int childIdx = 0; childIdx < childrenList.size(); childIdx++) {
				TmlChild child = childrenList.get(childIdx);
				try {
					float percentage = ((float) (child.getLastLocalTimestamp() - child
							.getFirstLocalTimestamp()))
							/ (child.getLastRemoteTimestamp() - child
									.getFirstLocalTimestamp());
					initialSyncProgressBarList.get(childIdx).setProgress(
							(int) (percentage * INIT_SYNC_PROGRESS_BAR_MAX));
				} catch (IndexOutOfBoundsException e) {
					/*
					 * Added child. Renew the initial sync shadow
					 */
					shadowInitialSync();
				} catch (NullPointerException e) {
					/*
					 * Ignore
					 */
				}
			}

			/*
			 * If initial sync ended hide shadow layout
			 */
			if (!TmlChild.getDb().isInitialSyncRequired()) {
				shadowLayoutHide();
				refreshMapElements();
				cameraInitialAnimation();
				userGuideToastPositionMarkerTap();
				if (TmlChild.isChildDevice()
						&& childSelected.getLastFilteredPosition() == null) {
					shadowFirstPosition();
				}
			}
		}
	}

	/**
	 * Close first position shadow if first position is available
	 */
	private void shadowFirstPositionCheck() {
		if (TmlChild.isChildDevice() && shadowType == ShadowType.FIRST_POSITION
				&& childSelected.getLastFilteredPosition() != null) {

			shadowLayoutHide();
			refreshMapElements();
			cameraInitialAnimation();
		}
	}

	/*
	 * -------------------------------------------------------------------------
	 * ---------------------- NAVIGATION DRAWER METHODS ------------------------
	 * -------------------------------------------------------------------------
	 */

	/**
	 * Initialize navigation drawer used to access history and check children
	 * status
	 */
	private void navigationDrawerInit() {

		/*
		 * Retrieve navigation drawer from layout
		 */
		navigationDrawerListView = (ListView) findViewById(R.id.navigation_drawer);
		/*
		 * Set the adapter for the drawer and its listener
		 */
		navigationDrawerAdapter = new TmlNavigationDrawerAdapter(this,
				new ArrayList<TmlChild>(childrenList));
		navigationDrawerListView.setAdapter(navigationDrawerAdapter);
		navigationDrawerListView
				.setOnItemClickListener(navigationDrawerOnItemClickListener);

		/*
		 * Set a custom shadow that overlays the main content when the drawer
		 * opens
		 */
		navigationDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		navigationDrawerLayout.setDrawerShadow(
				R.drawable.navigation_drawer_shadow, GravityCompat.START);

		/*
		 * ActionBarDrawerToggle used to show three lines on the left of the
		 * action bar
		 */
		actionBarDrawerToggle = new ActionBarDrawerToggle(this,
				navigationDrawerLayout, R.drawable.ic_navigation_drawer,
				R.string.navigationDrawer_open_contentDescription,
				R.string.navigationDrawer_close_contentDescription);
		navigationDrawerLayout.setDrawerListener(actionBarDrawerToggle);

		navigationDrawerListView.setAdapter(navigationDrawerAdapter);

	}

	/**
	 * remove navigation drawer
	 */
	private void navigationDrawerRemove() {

		navigationDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		navigationDrawerLayout
				.removeView((ListView) findViewById(R.id.navigation_drawer));
		navigationDrawerLayout = null;
	}

	/**
	 * Notifies the navigation drawer adapter that data changes occurred
	 */
	private void navigationDrawerUpdate() {
		if (navigationDrawerAdapter != null) {
			navigationDrawerAdapter.notifyDataSetChanged();
		}

	}

	/*
	 * -------------------------------------------------------------------------
	 * --------------------------- SAFE ZONE METHODS ---------------------------
	 * -------------------------------------------------------------------------
	 */

	/**
	 * New point added to a candidate SafeZone
	 * 
	 * @param position
	 */
	private void safeZoneDrawingAddPoint(LatLng position) {
		safeZoneCandidate.addPoint(position);
	}

	/**
	 * Remove last point to the candidate SafeZone
	 */
	private void safeZoneDrawingRemoveLastPoint() {
		if (safeZoneCandidate == null) {
			return;
		}
		if (!safeZoneCandidate.removeLastPoint()) {
			state.exitSafeZoneAdd();
		}
	}

	/**
	 * SafeZone drawing completed
	 */
	private void safeZoneDrawingDone() {
		if (safeZoneCandidate != null
				&& safeZoneCandidate.getPointList().size() >= 3) {
			safeZoneCandidate.createFromUI();
			safeZoneCandidate = null;
		}

		state.exitSafeZoneAdd();
		notificationManager.riseNotifications();

		/*
		 * Show safe zone description dialog
		 */
		if (userGuide.autoShow(DescriptionType.SAFE_ZONE)) {
			dialogRegister.add(userGuide
					.showDescription(DescriptionType.SAFE_ZONE));
		}

	}

	/**
	 * Create a dialog to confirm the deletion of the selected safeZone
	 */
	private void safeZoneRemoveSelectedConfirmDialog() {

		new TmlDialogFragment()
				.setMessage(R.string.map_dialog_removeSafezone_text)
				.showNegativeButton(true)
				.show(getSupportFragmentManager(), this.getClass().getName());
	}

	/*
	 * -------------------------------------------------------------------------
	 * ------------------------------ CAMERA METHODS ---------------------------
	 * -------------------------------------------------------------------------
	 */

	/**
	 * Save the current camera settings
	 */
	private void cameraSave() {
		cameraPosition = map.getCameraPosition();
	}

	/**
	 * Restore the previously saved camera settings
	 */
	private void cameraRestore() {
		if (cameraPosition != null) {
			map.animateCamera(
					CameraUpdateFactory.newCameraPosition(cameraPosition),
					CAMERA_DEFAULT_ANIMATION_TIME, null);
		}
	}

	/**
	 * If all the conditions are satisfied perform initial camera animation
	 */
	private void cameraInitialAnimation() {
		if (mapLoadingCompleted && initialViewCreationCompleted) {
			if (TmlChild.isParentDevice()
					&& state.getState() == StateType.ALL_CHILDREN) {
				cameraShowLastPositions();
			} else {
				navigator.last();
				cameraShowLastPosition();
			}
		}
	}

	/**
	 * Center the camera on the selected marker. If the marker is a cluster
	 * marker it is opened, otherwise the default zoom on the marker is
	 * performed
	 * 
	 * @param selectedMarker
	 * @param keepZoomLevel
	 */
	private void cameraShowSelectedMarker(TmlMarker selectedMarker,
			boolean keepZoomLevel) {

		if (selectedMarker != null) {
			if (selectedMarker.getType() == MarkerType.CLUSTER_POSITION
					|| selectedMarker.getType() == MarkerType.CLUSTER_PLACE) {
				LatLngBounds.Builder latLngBoundsBuilder = new LatLngBounds.Builder();

				for (TmlPosition position : selectedMarker.getPositionList()) {
					latLngBoundsBuilder.include(position.getCoordinates());
				}

				cameraAnimate(latLngBoundsBuilder.build());
			} else {
				map.animateCamera(CameraUpdateFactory.newLatLng(selectedMarker
						.getPosition()));
			}
		}
	}

	/**
	 * Center the camera on all the safezones of the selected child
	 */
	private void cameraShowAllSafeZones() {
		if (childSelected == null) {
			return;
		}

		LatLngBounds.Builder latLngBoundsBuilder = new LatLngBounds.Builder();

		boolean somethingToShow = false;

		for (TmlSafeZone safeZone : childSelected.getSafeZoneList()) {
			if (!safeZone.isDeleted() && !safeZone.toBeRemoved()) {
				if (safeZone.getView().getMarkerList() != null
						&& safeZone.getView().getMarkerList().size() > 0
						&& safeZone.getView().getMarkerList().get(0)
								.isVisible()) {
					somethingToShow = true;
					for (Marker marker : safeZone.getView().getMarkerList()) {
						latLngBoundsBuilder.include(marker.getPosition());
					}

				}
			}
		}

		if (somethingToShow) {
			cameraAnimate(latLngBoundsBuilder.build());
		}

	}

	/**
	 * Center the camera on the history of the selectedChild
	 */
	private void cameraShowHistory() {

		if (childSelected == null || childSelected.getView() == null) {
			return;
		}

		LatLngBounds.Builder latLngBoundsBuilder = new LatLngBounds.Builder();

		boolean somethingToShow = false;
		if (childSelected.getView().getLastPositionMarker() != null
				&& childSelected.getView().getLastPositionMarker().isVisible()) {
			latLngBoundsBuilder.include(childSelected.getView()
					.getLastPositionMarker().getPosition());
			somethingToShow = true;
		}
		if (childSelected.getView().getPlaceMarkerList() != null
				&& childSelected.getView().getPlaceMarkerList().size() > 0
				&& childSelected.getView().getPlaceMarkerList().get(0)
						.isVisible()) {
			for (TmlMarker marker : childSelected.getView()
					.getPlaceMarkerList()) {
				latLngBoundsBuilder.include(marker.getPosition());
			}
			somethingToShow = true;
		}

		if (somethingToShow) {
			cameraAnimate(latLngBoundsBuilder.build());
		}

		centerCameraAnimation = true;
		centerCameraButton.setImageResource(R.drawable.center_child);
		centerCameraButton.setEnabled(false);
	}

	/**
	 * Center the camera on the last position marker of the selectedChild
	 */
	private void cameraShowLastPosition() {

		if (childSelected == null || childSelected.getView() == null
				|| childSelected.getView().getLastPositionMarker() == null
				|| !childSelected.getView().getLastPositionMarker().isVisible()) {
			return;
		}

		cameraAnimate(childSelected.getView().getLastPositionMarker()
				.getPosition(), false);
	}

	/**
	 * Center the camera on the last position markers of all children
	 */
	private void cameraShowLastPositions() {

		LatLngBounds.Builder latLngBoundsBuilder = new LatLngBounds.Builder();

		boolean somethingToShow = false;
		for (TmlChild child : childrenList) {
			if (child.getView() != null
					&& child.getView().getLastPositionMarker() != null) {
				latLngBoundsBuilder.include(child.getView()
						.getLastPositionMarker().getPosition());
				somethingToShow = true;
			}
		}
		if (somethingToShow) {
			cameraAnimate(latLngBoundsBuilder.build());
		}

		centerCameraAnimation = true;
		centerCameraButton.setImageResource(R.drawable.center_all);
		centerCameraButton.setEnabled(false);
	}

	/**
	 * Animate the camera with default animation time to the given latLngBounds.
	 * Padding is added to avoid infoView and ActionBar overlap
	 * 
	 * @param latLngBounds
	 */
	private void cameraAnimate(LatLngBounds latLngBounds) {

		/*
		 * Since map padding is set through map.setPadding it's only necessary
		 * to introduce the padding for the markers
		 */

		latLngBounds = TmlPosition.latLngMinDistance(latLngBounds,
				MIN_DIAG_DISTANCE);

		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(
				latLngBounds, MAX_MARKER_DIMENSION);
		map.animateCamera(cameraUpdate, CAMERA_DEFAULT_ANIMATION_TIME, null);
	}

	/**
	 * Animate the camera with default animation time to the given latLng. The
	 * CAMERA_DEFAULT_ZOOM is used as zoom level
	 * 
	 * @param latLng
	 * @param keepZoomLevel
	 */
	private void cameraAnimate(LatLng latLng, boolean keepZoomLevel) {

		CameraUpdate cameraUpdate;

		if (keepZoomLevel) {
			cameraUpdate = CameraUpdateFactory.newLatLng(latLng);
		} else {
			cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng,
					CAMERA_DEFAULT_ZOOM);
		}

		map.animateCamera(cameraUpdate, CAMERA_DEFAULT_ANIMATION_TIME, null);
	}

	/*
	 * ------------------------------------------------------------------------
	 * ------------------- MAP ELEMENTS METHODS -------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Delete all clusters from the map and empty markerList
	 */
	private void clearMarkerList() {
		while (markerList.size() > 0) {
			if (markerList.get(0).getType() != MarkerType.POSITION
					&& markerList.get(0).getType() != MarkerType.PLACE) {
				markerList.get(0).remove();
			}
			markerList.remove(0);
		}
	}

	/**
	 * Restore markerList from markerListFull
	 */
	private void restoreMarkerList() {
		clearMarkerList();
		markerList.addAll(markerListFull);
		for (TmlMarker marker : markerList) {
			marker.setVisible(true);
		}
	}

	/**
	 * Rebuild markerList and markerListFull based on the state. Clustering is
	 * also performed Called after each update, and when state is changed.
	 */
	private void refreshMapElements() {
		clearMarkerList();

		switch (state.getState()) {
		case ALL_CHILDREN:
		case CHILD_SELECTED:
			for (TmlChild child : childrenList) {
				child.getView().hide();
				child.getView().showLastPositionMarker();
				if (child.getView().getLastPositionMarker() != null) {
					markerList.add(child.getView().getLastPositionMarker());
				}
			}
			break;
		case SAFE_ZONE_ADD:
		case SAFE_ZONE_CLEAR:
		case SAFE_ZONE_REMOVE:
			for (TmlChild child : childrenList) {
				child.getView().hide();
				if (child == childSelected) {

					child.getView().showSafeZoneViewList();

					child.getView().showLastPositionMarker();
					if (child.getView().getPlaceMarkerList() != null) {
						markerList.add(child.getView().getLastPositionMarker());
					}
				}
			}
			break;
		case SINGLE_CHILD:
			for (TmlChild child : childrenList) {
				child.getView().hide();
				if (child == childSelected) {

					child.getView().showSafeZoneViewList();

					child.getView().showPlaceMarkerList();
					if (child.getView().getPlaceMarkerList() != null) {
						markerList.addAll(child.getView().getPlaceMarkerList());
					}

					child.getView().showTrack();

					child.getView().showLastPositionMarker();
					if (child.getView().getLastPositionMarker() != null) {
						markerList.add(child.getView().getLastPositionMarker());
					}
				}
			}
			break;
		default:
			break;
		}
		markerListFull.clear();
		markerListFull.addAll(markerList);

		performClustering();
	}

	/**
	 * Perform the clustering based on current state
	 */
	private void performClustering() {
		if (state.getState() == StateType.SINGLE_CHILD
				|| state.getState() == StateType.ALL_CHILDREN
				|| state.getState() == StateType.CHILD_SELECTED) {
			TmlMarker.clusterize(markerList, displayMetrics.density);
		}
	}

	/*
	 * ------------------------------------------------------------------------
	 * ---------------------- USER GUIDE METHODS ------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Show ToastPositionMarkerTap if all the conditions are satisfied
	 */
	private void userGuideToastPositionMarkerTap() {
		if (TmlChild.isParentDevice()
				&& state.getState() == StateType.ALL_CHILDREN
				&& userGuide.autoShow(ToastType.POSITION_MARKER_TAP)
				&& dialogRegister.size() == 0
				&& TmlChild.initialViewCreationCompleted()
				&& shadowType == ShadowType.NONE) {
			userGuide.showToast(ToastType.POSITION_MARKER_TAP);
		}
	}

	/**
	 * Show ToastPositionMarkerTapAgain if all the conditions are satisfied
	 */
	private void userGuideToastPositionMarkerTapAgain() {
		if (TmlChild.isParentDevice()
				&& state.getState() == StateType.CHILD_SELECTED
				&& userGuide.autoShow(ToastType.POSITION_MARKER_TAP_AGAIN)
				&& dialogRegister.size() == 0) {
			userGuide.showToast(ToastType.POSITION_MARKER_TAP_AGAIN);
		}
	}

	/**
	 * Show ToastSafeZoneAdd if all the conditions are satisfied
	 */
	private void userGuideToastSafeZoneAdd() {
		if (TmlChild.isParentDevice()
				&& state.getState() == StateType.SINGLE_CHILD
				&& userGuide.autoShow(ToastType.ADD_SAFEZONE_LONG_PRESS)
				&& dialogRegister.size() == 0) {
			userGuide.showToast(ToastType.ADD_SAFEZONE_LONG_PRESS,
					childSelected.getName());
		}
	}

	/**
	 * Show ToastSafeZoneRemove if all the conditions are satisfied
	 */
	private void userGuideToastSafeZoneRemove() {
		if (TmlChild.isParentDevice()
				&& state.getState() == StateType.SAFE_ZONE_REMOVE
				&& safeZoneSelected == null
				&& userGuide.autoShow(ToastType.REMOVE_SAFEZONE)) {
			userGuide.showToast(ToastType.REMOVE_SAFEZONE);
		}
	}

	/*
	 * ------------------------------------------------------------------------
	 * ------------------- ACTIVITY LIFECYCLE METHODS -------------------------
	 * ------------------------------------------------------------------------
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/*
		 * In phones lock the orientation for this activity in portrait
		 */
		if (!getResources().getBoolean(R.bool.isTablet)) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}

		/*
		 * Setup layout independent variables
		 */
		layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		userGuide = new TmlUserGuide(this, layoutInflater);

		displayMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

		childrenList = TmlChild.getChildrenList(this, childrenListener);

		/*
		 * Setup layout and views
		 */
		setContentView(R.layout.map_activity);

		shadowLayout = (RelativeLayout) findViewById(R.id.map_activity_shadow_layout);

		centerCameraButton = (ImageButton) findViewById(R.id.map_activity_center_button);
		centerCameraButton.setOnClickListener(centerCameraButtonListener);

		map = ((SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map_fragment)).getMap();

		map.setOnCameraChangeListener(mapOnCameraChangeListener);
		map.setOnMapLoadedCallback(mapLoadedCallback);

		map.getUiSettings().setRotateGesturesEnabled(false);
		map.getUiSettings().setTiltGesturesEnabled(false);

		/*
		 * Set listeners for marker and map
		 */
		if (TmlChild.isParentDevice()) {
			map.setOnMapLongClickListener(mapClickListener);
		}
		map.setOnMarkerClickListener(markerClickListener);
		map.setOnMapClickListener(mapClickListener);

		/*
		 * If device allows gestures to be used to zoom the map don't show
		 * manual zoom buttons
		 */
		if (map.getUiSettings().isZoomGesturesEnabled()) {
			map.getUiSettings().setZoomControlsEnabled(false);
		}

		/*
		 * Action Bar without back icon
		 */
		actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

		if (TmlChild.isParentDevice()) {
			actionBar.setHomeButtonEnabled(true);
			/*
			 * Needed to show actionBar drawer toggle
			 */
			actionBar.setDisplayHomeAsUpEnabled(true);
		} else {
			actionBar.setHomeButtonEnabled(false);
			actionBar.setDisplayHomeAsUpEnabled(false);
		}

		/*
		 * Initialize child view
		 */
		for (TmlChild child : childrenList) {
			if (child.getView() == null) {
				child.createView(map);
				if (child.getView() != null) {
					child.getView().showLastPositionMarker();
				}
			}
		}

		/*
		 * Initialize navigation drawer, info view, notification manager
		 */
		if (TmlChild.isParentDevice()) {
			navigationDrawerInit();
			notificationManager = TmlNotification.getInstance(this);
		} else {
			navigationDrawerRemove();
		}
		infoView.init();

		/*
		 * No child dialog
		 */
		if (TmlChild.isParentDevice() && childrenList.size() == 0) {
			dialogNoChild = userGuide.showDescription(DescriptionType.NO_CHILD);
			dialogRegister.add(dialogNoChild);
		}
		/*
		 * Welcome dialog
		 */
		if (userGuide.autoShow(DescriptionType.WELCOME)) {
			dialogRegister.add(userGuide
					.showDescription(DescriptionType.WELCOME));
		}

		/*
		 * Lock the interface up to complete loading
		 */
		if (TmlChild.getDb().isInitialSyncRequired() && childrenList.size() > 0) {
			shadowInitialSync();
		} else {
			shadowLoading();
		}

		/*
		 * Initial state
		 */
		if (TmlChild.isParentDevice()) {
			state.initAllChildren();

			/*
			 * Check for notification intent
			 */
			Intent receivedIntent = getIntent();
			if (receivedIntent != null
					&& receivedIntent.hasExtra(INTENT_EXTRA_CHILD_NAME)) {
				String username = receivedIntent
						.getStringExtra(INTENT_EXTRA_CHILD_NAME);
				for (TmlChild childView : childrenList) {
					if (childView.getName().equals(username)) {
						childSelected = childView;
						break;
					}
				}
				state.allChildrenToSingleChild();
			}
		} else {
			childSelected = childrenList.get(0);
			state.initSingleChild();
		}
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		if (actionBarDrawerToggle != null) {
			actionBarDrawerToggle.syncState();
		}
	}

	/**
	 * Called when startActivity(...) is called and the activity is already
	 * created
	 */
	@Override
	protected void onNewIntent(Intent intent) {

		if (intent != null) {
			if (intent.hasExtra(INTENT_EXTRA_CHILD_NAME)) {
				String username = intent
						.getStringExtra(INTENT_EXTRA_CHILD_NAME);
				for (TmlChild child : childrenList) {
					if (child.getName().equals(username)) {
						childSelected = child;
						break;
					}
				}
				switch (state.getState()) {
				case ALL_CHILDREN:
					state.allChildrenToSingleChild();
					break;
				case CHILD_SELECTED:
					state.childSelectedToSingleChild();
					break;
				case SAFE_ZONE_ADD:
					state.exitSafeZoneAdd();
					break;
				case SAFE_ZONE_CLEAR:
					state.exitSafeZoneClear();
					break;
				case SAFE_ZONE_REMOVE:
					state.exitSafeZoneRemove();
					break;
				case SINGLE_CHILD:
					state.refreshSingleChild();
					break;
				default:
					break;
				}
				cameraShowLastPosition();

			} else if (intent.getAction() != null) {
				if (intent.getAction().equals(
						TmlServicesManager.INTENT_ACTION_CHILD_DEVICE_RESET)) {
					/*
					 * child device reset from server due to deleted user
					 */
					finish();
				}
			}

		}
	}

	/**
	 * Registers the broadcast receiver for new positions and child
	 */
	@Override
	protected void onResume() {
		super.onResume();

		LocalBroadcastManager.getInstance(this).sendBroadcast(
				new Intent(TmlServicesManager.BROADCAST_ACTION_GUI_VISIBLE));
		/*
		 * Register LocalBroadcastReceiver
		 */
		IntentFilter ifilter = new IntentFilter();
		ifilter.addAction(TmlServicesManager.BROADCAST_ACTION_CHILDREN_UPDATE);
		LocalBroadcastManager.getInstance(this).registerReceiver(
				localBroadcastReceiver, ifilter);

		initialViewCreationCompleted = TmlChild.initialViewCreationCompleted();

		/*
		 * Operations necessary when coming back from settings activity and
		 * resuming from launcher, due to LocalBroadcastManager not active
		 */
		shadowInitialSyncCheck();
		shadowFirstPositionCheck();
		initialViewCreationCheck();
		childrenUpdate();

		if (state.getState() == StateType.SINGLE_CHILD) {
			if (getIntent().getAction() != null
					&& !getIntent().getAction().equals(
							TmlSettingsActivity.INTENT_RESULT_RESET_DEVICE)) {
				cameraInitialAnimation();
			}
		}
	}

	/**
	 * Settings activity returns here due to startActivityForResult(...)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == REQUEST_SETTINGS) {
			if (resultCode == RESULT_OK) {
				if (data.getAction() != null) {
					if (data.getAction().equals(
							TmlSettingsActivity.INTENT_RESULT_RESET_DEVICE)) {
						TmlServicesManager.resetDevice(this, false, true);
						this.finish();
					} else if (data.getAction().equals(
							TmlSettingsActivity.INTENT_RESULT_CHANGE_USER)) {

						TmlServicesManager.resetDevice(this, true, true);

						this.finish();
					} else if (data
							.getAction()
							.equals(TmlSettingsActivity.INTENT_RESULT_CHANGE_HISTORY_INTERVAL)) {

						updatedChildrenList.addAll(childrenList);
						childrenUpdate();

						if (state.getState() == StateType.SINGLE_CHILD) {
							navigator.last();
							cameraShowHistory();
						}

					} else if (data.getAction().equals(
							TmlSettingsActivity.INTENT_RESULT_RESET_USER_GUIDE)) {
						userGuide.resetCounters();
						finish();
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								startActivity(new Intent(getBaseContext(),
										TmlMapActivity.class));
							}
						}, 1000);

					}
				}
			}
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		/*
		 * Unregister Local Broadcast Receiver
		 */
		LocalBroadcastManager.getInstance(this).unregisterReceiver(
				localBroadcastReceiver);

		/*
		 * Notify other app components that the GUI is no more visible
		 */
		LocalBroadcastManager.getInstance(this).sendBroadcast(
				new Intent(TmlServicesManager.BROADCAST_ACTION_GUI_HIDDEN));

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		for (DialogInterface dialog : dialogRegister) {
			dialog.cancel();
		}

		for (TmlChild child : childrenList) {
			child.deleteView();
		}
	}
}
