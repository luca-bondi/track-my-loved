package me.noip.unklb.trackmyloved.activities;

import me.noip.unklb.trackmyloved.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;

/**
 * Class used to display a Dialog in a running Activity. See
 * TmlConfirmPreference to display a Dialog in a PreferenceActivity.
 * 
 * If callback needed implement MessageDialogListener interface
 * 
 * Usage:
 * 
 * <pre>
 * {@code
 * new TmlDialogFragment()
 *     <.setMessage(String|int)>
 *     <.setTitle(String|int)>
 *     <.showNegativeButton(bool)>
 *     <.showPositiveButton(bool)>
 *     .show();
 * }
 * </pre>
 * 
 */
public class TmlDialogFragment extends DialogFragment {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	// private final static String TAG = "MessageDialogFragment";

	/*
	 * ------------------------------------------------------------------------
	 * ---------------------------- INTERFACES --------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Interface that can be implemented in the hosting activity if want to
	 * receive confirmation and cancel callbacks.
	 */
	public interface MessageDialogListener {
		/**
		 * Called when confirm button is pressed
		 */
		public void messageDialogPositive();

		/**
		 * Called when back or negative buttons are pressed
		 */
		public void messageDialogNegative();
	}

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- ATTRIBUTES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Message string to display
	 */
	private String messageText;

	/**
	 * Message resource id
	 */
	private int messageResId;

	/**
	 * Title string to display
	 */
	private String titleText;

	/**
	 * Title resource id
	 */
	private int titleResId;

	/**
	 * Flag set or reset during onAttach indicating whether the calling activity
	 * implements or not the MessageDialogListener interface
	 */
	private boolean hostActivityImplementsListener;

	/**
	 * Listener provided by the calling activity that implements
	 * MessageDialogListener interface
	 */
	private MessageDialogListener hostActivity;

	/**
	 * Flag set if negative button has to be shown.
	 */
	private boolean showNegativeButton = false;;

	/**
	 * Flag set if positive button has to be shown.
	 */
	private boolean showPositiveButton = true;

	/**
	 * Shown dialog
	 */
	private AlertDialog dialog;

	/*
	 * ------------------------------------------------------------------------
	 * ---------------------------- METHODS -----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Set the message string to display
	 * 
	 * @param message
	 * @return
	 */
	public TmlDialogFragment setMessage(String message) {
		this.messageText = message;
		return this;
	}

	/**
	 * Set the message string to display
	 * 
	 * @param messageResId
	 * @return
	 */
	public TmlDialogFragment setMessage(int messageResId) {
		this.messageResId = messageResId;
		return this;
	}

	/**
	 * Set the title string to display
	 * 
	 * @param title
	 * @return
	 */
	public TmlDialogFragment setTitle(String title) {
		this.titleText = title;
		return this;
	}

	/**
	 * Set the title string to display
	 * 
	 * @param titleResId
	 * @return
	 */
	public TmlDialogFragment setTitle(int titleResId) {
		this.titleResId = titleResId;
		return this;
	}

	/**
	 * Set whether to show or not the cancel button. Default behavior is not.
	 */
	public TmlDialogFragment showNegativeButton(boolean showCancelButton) {
		this.showNegativeButton = showCancelButton;
		return this;
	}

	/**
	 * Set whether to show or not the buttons. Default behavior is yes.
	 */
	public TmlDialogFragment showPositiveButton(boolean showButtons) {
		this.showPositiveButton = showButtons;
		return this;
	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- LIFECYCLE METHODS ------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Called when fragment attached to the activity.
	 * 
	 * @param activity
	 *            host activity
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			hostActivity = (MessageDialogListener) activity;
			hostActivityImplementsListener = true;
		} catch (ClassCastException e) {
			hostActivityImplementsListener = false;
		}
	}

	/**
	 * Called when show() is called on this class to show the dialog
	 */
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		if (messageText != null) {
			builder.setMessage(messageText);
		} else if (messageResId != 0) {
			builder.setMessage(messageResId);
		}

		if (titleText != null) {
			builder.setTitle(titleText);
		} else if (titleResId != 0) {
			builder.setTitle(titleResId);
		}

		if (showPositiveButton) {
			builder.setPositiveButton(R.string.setup_button_ok_text,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							if (hostActivityImplementsListener) {
								hostActivity.messageDialogPositive();
							}
						}
					});
		}

		if (showNegativeButton) {
			builder.setNegativeButton(R.string.setup_button_cancel_text,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							if (hostActivityImplementsListener) {
								hostActivity.messageDialogNegative();
							}
						}
					});
		}

		/*
		 * Back button listener
		 */
		builder.setOnKeyListener(new Dialog.OnKeyListener() {
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode,
					KeyEvent event) {
				if (hostActivityImplementsListener
						&& keyCode == KeyEvent.KEYCODE_BACK) {
					dialog.cancel();
					hostActivity.messageDialogNegative();
				}
				return true;
			}
		});

		dialog = builder.create();

		return dialog;
	}
}
