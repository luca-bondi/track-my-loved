package me.noip.unklb.trackmyloved.activities;

import me.noip.unklb.trackmyloved.R;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Controller for the Role Selection Fragment
 */
public class TmlRoleSelectionFragment extends Fragment {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- INTERFACES ---------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Callback interface to be implemented by the activity that hosts this
	 * fragment
	 */
	public interface RoleSelectionFragmentCallbacks {

		/**
		 * Called when confirm button is pressed.
		 * 
		 * @param role
		 */
		public void roleSelectionButtonConfirmCallback(char role);

		/**
		 * Called when cancel button is pressed
		 */
		public void roleSelectionButtonCancelCallback();
	}

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- UI LISTENERS -------------------------------
	 * ------------------------------------------------------------------------
	 */
	/**
	 * Cancel button listener.
	 */
	private OnClickListener cancelButtonClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			hostActivity.roleSelectionButtonCancelCallback();
		}

	};

	/**
	 * Confirm button listener.
	 */
	private OnClickListener confirmButtonClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (role == 's' || role == 'p') {
				hostActivity.roleSelectionButtonConfirmCallback(role);
			}
		}

	};

	/**
	 * Parent or child button listener.
	 */
	private OnClickListener roleButtonClickListener = new OnClickListener() {

		@Override
		public void onClick(View pressedButton) {
			if (pressedButton.equals(parentButton)) {
				role = 'p';
				parentButton.setBackgroundResource(R.drawable.button_focused);
				sonButton.setBackgroundResource(R.drawable.button_simple);
				roleDescription
						.setText(getString(R.string.setup_role_parent_description));
			} else {
				role = 's';
				sonButton.setBackgroundResource(R.drawable.button_focused);
				parentButton.setBackgroundResource(R.drawable.button_simple);
				roleDescription
						.setText(getString(R.string.setup_role_child_description));
			}
			unlockButton();
		}
	};

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- ATTRIBUTES -------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Activity that hosts this fragment and implements the
	 * AccountLoginFragmentCallbacks
	 */
	private RoleSelectionFragmentCallbacks hostActivity;

	/**
	 * Selected role. 0 if not yet selected
	 */
	private char role;

	/**
	 * Role description TextView
	 */
	private TextView roleDescription;

	/*
	 * Buttons
	 */
	private Button parentButton;
	private Button sonButton;
	private Button cancelButton;
	private Button okButton;

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- LIFECYCLE METHODS ------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Called when fragment attached to the activity. Try to cast the activity
	 * to the fragment callback. If cast failed throw exception
	 * 
	 * @param activity
	 *            host activity
	 * @throws ClassCastException
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			hostActivity = (RoleSelectionFragmentCallbacks) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement RoleSelectionFragmentCallbacks");
		}
	}

	/**
	 * Called when the activity requests the fragment to create its view.
	 * 
	 * @param inflater
	 * @param container
	 *            host activity container where the fragment should inflate
	 *            itself
	 * @param savedInstanceState
	 * @return the inflated view
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.setup_role_selection_fragment,
				container, false);
	}

	/**
	 * Called when the activity has been rendered. Not it's possible to use
	 * getActivity().findViewById() to get reference to the UI views
	 * 
	 * @param savedInstanceState
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		parentButton = (Button) getActivity().findViewById(
				R.id.role_selection_button_parent);
		sonButton = (Button) getActivity().findViewById(
				R.id.role_selection_button_child);
		parentButton.setOnClickListener(roleButtonClickListener);
		sonButton.setOnClickListener(roleButtonClickListener);

		cancelButton = (Button) getActivity().findViewById(
				R.id.role_selection_button_cancel);
		cancelButton.setOnClickListener(cancelButtonClickListener);

		okButton = (Button) getActivity().findViewById(
				R.id.role_selection_button_ok);
		okButton.setOnClickListener(confirmButtonClickListener);

		roleDescription = (TextView) getActivity().findViewById(
				R.id.role_selection_description);

		cleanSelection();
	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- GENERIC METHODS --------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Clear selected role, reset role buttons background, disable OK button
	 */
	public void cleanSelection() {
		role = 0;
		sonButton.setBackgroundResource(R.drawable.button_simple);
		parentButton.setBackgroundResource(R.drawable.button_simple);
		roleDescription.setText("");
		lockButton();
	}

	/**
	 * Disable OK button and change background
	 */
	private void lockButton() {
		/*
		 * Change button drawable and lock
		 */
		okButton.setEnabled(false);

		okButton.setTextColor(getResources().getColor(
				R.color.setup_activity_button_disabled_textColor));

		okButton.setBackgroundResource(R.drawable.selector_button_disabled);
	}

	/**
	 * Enable OK button and change background
	 */
	private void unlockButton() {
		/*
		 * Change button drawable and unlock
		 */
		okButton.setEnabled(true);

		okButton.setTextColor(getResources().getColor(
				R.color.setup_activity_button_textColor));

		okButton.setBackgroundResource(R.drawable.selector_button_green);
	}

}
