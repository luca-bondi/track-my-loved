package me.noip.unklb.trackmyloved.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.Log;

/**
 * Subclass of DialogPreference necessary to show confirmation dialog in a
 * PreferenceActivity
 */
public class TmlConfirmPreference extends DialogPreference {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	private static final String TAG = "TmlConfirmPreference";

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- ATTRIBUTES -------------------------------
	 * ------------------------------------------------------------------------
	 */
	/**
	 * Host activity that implements DialogInterface.OnClickListener
	 */
	private DialogInterface.OnClickListener hostActivity;

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- CONSTRUCTORS -----------------------------
	 * ------------------------------------------------------------------------
	 */
	/**
	 * Default constructor for a DialogPreference
	 * 
	 * @param context
	 * @param attrs
	 */
	public TmlConfirmPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- LISTENERS --------------------------------
	 * ------------------------------------------------------------------------
	 */
	/**
	 * Called when a button is pressed on the dialog
	 * 
	 * @param dialog
	 * @param which
	 *            pressed button
	 */
	@Override
	public void onClick(DialogInterface dialog, int which) {
		if (hostActivity == null) {
			Log.e(TAG, "Use setListenerCallback() to define a listener");
		} else {
			hostActivity.onClick(dialog, which);
		}
	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- GENERIC METHODS --------------------------
	 * ------------------------------------------------------------------------
	 */
	/**
	 * Set the DialogInterface.OnClickListener to be called when a button is
	 * pressed
	 */
	public void setListenerCallback(
			DialogInterface.OnClickListener onClickListener) {
		this.hostActivity = onClickListener;
	}
}