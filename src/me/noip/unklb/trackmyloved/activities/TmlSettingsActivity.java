package me.noip.unklb.trackmyloved.activities;

import java.io.IOException;
import java.util.ArrayList;

import me.noip.unklb.trackmyloved.R;
import me.noip.unklb.trackmyloved.controllers.TmlChild;
import me.noip.unklb.trackmyloved.controllers.TmlChild.TmlChildrenListener;
import me.noip.unklb.trackmyloved.controllers.TmlHash;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Build;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Xml;
import android.widget.Toast;

/**
 * Controller for the Settings Activity
 */
public class TmlSettingsActivity extends PreferenceActivity implements
		OnSharedPreferenceChangeListener, OnPreferenceClickListener {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	// private static final String TAG = "SettingsActivity";

	public static final String INTENT_RESULT_RESET_DEVICE = "INTENT_RESULT_RESET_DEVICE";
	public static final String INTENT_RESULT_CHANGE_USER = "INTENT_RESULT_CHANGE_USER";
	public static final String INTENT_RESULT_DELETE_CHILD = "INTENT_RESULT_DELETE_CHILD";
	public static final String INTENT_RESULT_CHANGE_HISTORY_INTERVAL = "INTENT_RESULT_CHANGE_HISTORY_INTERVAL";
	public static final String INTENT_RESULT_RESET_USER_GUIDE = "INTENT_RESULT_RESET_USER_GUIDE";

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- CLASSES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Preference fragment necessary to implement Preferences for API > 11
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public class SettingsFragment extends PreferenceFragment {

		/**
		 * Initialize all the preference instances
		 */
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);

			if (TmlChild.isChildDevice()) {
				addPreferencesFromResource(R.xml.settings_child);
				changeDeviceUserEditTextPreference = (EditTextPreference) findPreference("pref_change_device_user");
				resetDeviceEditTextPreference = (EditTextPreference) findPreference("pref_reset_device");
			} else {
				addPreferencesFromResource(R.xml.settings_parent);
				changUserConfirmPreference = (TmlConfirmPreference) findPreference("pref_change_device_user");
				resetDeviceConfirmPreference = (TmlConfirmPreference) findPreference("pref_reset_device");
				userDeletePreferenceScreen = (PreferenceScreen) findPreference("pref_user_delete");
			}
			resetGuidePreference = (Preference) findPreference("pref_reset_guide");
			commonVersionSetup();
		}
	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- LISTENERS --------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Listener as required by the TmlChild class
	 */
	private TmlChildrenListener childrenListener = new TmlChildrenListener() {

		@Override
		public void onAdd(TmlChild child) {
			childrenList.add(child);
		}

		@Override
		public void onRemove(TmlChild child) {
			childrenList.remove(child);
		}

		@Override
		public void onUpdate(TmlChild child) {
		}

	};

	/**
	 * Called when a preference value is changed.
	 * 
	 * On parent device used only for history interval. On child device used for
	 * history interval, change user and reset device
	 */
	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPref,
			String key) {
		if (key.equals("pref_history_interval")) {
			/*
			 * History interval changed
			 */
			int newHistoryInterval = Integer.parseInt(sharedPref.getString(key,
					getString(R.integer.pref_history_default_value)));
			setResult(RESULT_OK, new Intent(
					INTENT_RESULT_CHANGE_HISTORY_INTERVAL));
			TmlChild.setHistoryInterval(newHistoryInterval);
		} else if (key.equals("pref_change_device_user")) {
			/*
			 * Change user password check
			 */
			if (TmlHash.SHA1(sharedPref.getString(key, "")).equals(
					TmlChild.getDb().getPwdSHA1())) {
				changeUser();
			} else {
				Toast.makeText(this,
						getString(R.string.pref_password_mismatch),
						Toast.LENGTH_SHORT).show();
			}
		} else if (key.equals("pref_reset_device")) {
			/*
			 * Reset device password check
			 */
			if (TmlHash.SHA1(sharedPref.getString(key, "")).equals(
					TmlChild.getDb().getPwdSHA1())) {
				resetDevice();
			} else {
				Toast.makeText(this,
						getString(R.string.pref_password_mismatch),
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	/**
	 * Called when a Preference is clicked.
	 * 
	 * On parent device user delete and reset guide are called here. On child
	 * device only reset guide is called
	 */
	@Override
	public boolean onPreferenceClick(Preference preference) {
		if (preference.getKey().startsWith("pref_user_delete_")) {
			for (TmlChild child : childrenList) {
				if (preference.getKey().substring("pref_user_delete_".length())
						.equals(child.getName())) {
					childToBeDeleted = child;
					break;
				}
			}
			return true;
		} else if (preference.getKey().equals("pref_reset_guide")) {
			setResult(RESULT_OK, new Intent(INTENT_RESULT_RESET_USER_GUIDE));
			finish();
		}
		return false;

	}

	/**
	 * Reset device confirm dialog listener
	 */
	private OnClickListener resetDeviceListener = new OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			if (which == DialogInterface.BUTTON_POSITIVE) {
				resetDevice();
			}
		}
	};

	/**
	 * Change user confirm dialog listener
	 */
	private OnClickListener changeUserListener = new OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			if (which == DialogInterface.BUTTON_POSITIVE) {
				changeUser();
			}

		}

	};

	/**
	 * Delete user confirm dialog listener
	 */
	private OnClickListener userDeleteListener = new OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			if (which == DialogInterface.BUTTON_POSITIVE) {
				deleteUser(childToBeDeleted);
			} else {
				childToBeDeleted = null;
			}

		}

	};

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- ATTRIBUTES ---------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Child selected to be deleted
	 */
	private TmlChild childToBeDeleted;

	/**
	 * Instance of shared preferences
	 */
	private SharedPreferences sharedPreferences;

	/**
	 * Children list
	 */
	private ArrayList<TmlChild> childrenList;

	/**
	 * Preference screen where delete user buttons are added
	 */
	private PreferenceScreen userDeletePreferenceScreen;

	/*
	 * Instance of preferences
	 */
	private EditTextPreference changeDeviceUserEditTextPreference;
	private EditTextPreference resetDeviceEditTextPreference;
	private TmlConfirmPreference changUserConfirmPreference;
	private TmlConfirmPreference resetDeviceConfirmPreference;
	private Preference resetGuidePreference;

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------- LIFECYCLE METHODS ------------------------------
	 * ------------------------------------------------------------------------
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/*
		 * Initialize children list
		 */
		if (TmlChild.isParentDevice()) {
			childrenList = TmlChild.getChildrenList(this, childrenListener);
		}

		/*
		 * Initialize the preference instances
		 */
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			initPreHoneycomb();
		} else {
			initHoneycomb();
		}

		/*
		 * Set ActionBar title and back button
		 */
		setTitle(getTitle() + " (" + TmlChild.getDb().getEmail() + ")");

	}

	@Override
	public void onResume() {
		super.onResume();

		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

		if (TmlChild.isChildDevice()) {
			/*
			 * Reset the account password in confirmation dialog
			 */
			changeDeviceUserEditTextPreference.getEditText().setInputType(
					InputType.TYPE_CLASS_TEXT
							| InputType.TYPE_TEXT_VARIATION_PASSWORD);
			resetDeviceEditTextPreference.getEditText().setInputType(
					InputType.TYPE_CLASS_TEXT
							| InputType.TYPE_TEXT_VARIATION_PASSWORD);
			changeDeviceUserEditTextPreference.setText("");
			resetDeviceEditTextPreference.setText("");
			sharedPreferences.edit().putString("pref_change_device_user", "")
					.commit();
			sharedPreferences.edit().putString("pref_reset_device", "")
					.commit();
		}

		sharedPreferences
				.edit()
				.putString("pref_history_interval",
						TmlChild.getHistoryInterval().toString()).commit();

		sharedPreferences.registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------- GENERIC METHODS -------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Initialization for API <= 11.
	 * 
	 * Initialize all the preference instances
	 */
	@SuppressWarnings("deprecation")
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	private void initPreHoneycomb() {
		if (TmlChild.isChildDevice()) {
			addPreferencesFromResource(R.xml.settings_child);
			changeDeviceUserEditTextPreference = (EditTextPreference) findPreference("pref_change_device_user");
			resetDeviceEditTextPreference = (EditTextPreference) findPreference("pref_reset_device");
		} else {
			addPreferencesFromResource(R.xml.settings_parent);
			changUserConfirmPreference = (TmlConfirmPreference) findPreference("pref_change_device_user");
			resetDeviceConfirmPreference = (TmlConfirmPreference) findPreference("pref_reset_device");
			userDeletePreferenceScreen = (PreferenceScreen) findPreference("pref_user_delete");
		}
		resetGuidePreference = (Preference) findPreference("pref_reset_guide");
		commonVersionSetup();
	}

	/**
	 * Attach the PreferenceFragment as required for API > 11
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void initHoneycomb() {
		getFragmentManager().beginTransaction()
				.replace(android.R.id.content, new SettingsFragment()).commit();
	}

	/**
	 * Initialization for all APIs
	 */
	private void commonVersionSetup() {

		if (TmlChild.isParentDevice()) {

			/*
			 * Initialize the attribute set for the user delete preference
			 */
			AttributeSet attributeSet = null;
			XmlPullParser parser = getResources().getXml(
					R.xml.settings_parent_user_delete);
			int state = 0;
			do {
				try {
					state = parser.next();
				} catch (XmlPullParserException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				if (state == XmlPullParser.START_TAG) {
					if (parser
							.getName()
							.equals("me.noip.unklb.trackmyloved.activities.TmlConfirmPreference")) {
						attributeSet = Xml.asAttributeSet(parser);
						break;
					}
				}
			} while (state != XmlPullParser.END_DOCUMENT);

			/*
			 * Instantiate the user delete preference for each child
			 */
			for (TmlChild child : childrenList) {
				TmlConfirmPreference userPreference = new TmlConfirmPreference(
						this, attributeSet);
				userPreference.setTitle(child.getName());
				userPreference.setKey("pref_user_delete_" + child.getName());
				userPreference.setDialogMessage(getString(
						R.string.pref_delete_user_dialog_message,
						child.getName()));
				userPreference.setListenerCallback(userDeleteListener);
				userPreference.setOnPreferenceClickListener(this);
				userDeletePreferenceScreen.addPreference(userPreference);
			}

			/*
			 * Setup listener for reset device and change user preferences
			 */
			resetDeviceConfirmPreference
					.setListenerCallback(resetDeviceListener);
			changUserConfirmPreference.setListenerCallback(changeUserListener);

			/*
			 * Setup listener for resetGuidePreference
			 */
			resetGuidePreference.setOnPreferenceClickListener(this);
		}

		/*
		 * ActionBar behavior
		 */
		if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
			actionBarSetupGingerbread();
		} else if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB_MR2) {
			actionBarSetupHoneycomb();
		} else {
			actionBarSetupIcecreamsandwich();
		}

	}

	/**
	 * ActionBar initialization for Build.VERSION.SDK_INT <=
	 * Build.VERSION_CODES.GINGERBREAD_MR1
	 */
	private void actionBarSetupGingerbread() {
	}

	/**
	 * ActionBar initialization for Build.VERSION.SDK_INT <=
	 * Build.VERSION_CODES.HONEYCOMB_MR2
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void actionBarSetupHoneycomb() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	/**
	 * ActionBar initialization for Build.VERSION.SDK_INT >
	 * Build.VERSION_CODES.HONEYCOMB_MR2
	 */
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	private void actionBarSetupIcecreamsandwich() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
	}

	/**
	 * User delete confirmed
	 * 
	 * @param child
	 */
	private void deleteUser(TmlChild child) {
		child.delete();
		setResult(RESULT_OK, new Intent(INTENT_RESULT_DELETE_CHILD));
		finish();
	}

	/**
	 * Reset device confirmed
	 */
	private void resetDevice() {
		setResult(RESULT_OK, new Intent(INTENT_RESULT_RESET_DEVICE));
		finish();
	}

	/**
	 * Change user confirmed
	 */
	private void changeUser() {
		setResult(RESULT_OK, new Intent(INTENT_RESULT_CHANGE_USER));
		finish();
	}

}
