package me.noip.unklb.trackmyloved.activities;

import me.noip.unklb.trackmyloved.R;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

/**
 * Controller for the User Create Fragment
 */
public class TmlUserCreateFragment extends Fragment {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- INTERFACES ---------------------------------
	 * ------------------------------------------------------------------------
	 */
	/**
	 * Callback interface to be implemented by the activity that hosts this
	 * fragment
	 */
	public interface UserCreateFragmentCallbacks {

		/**
		 * Called when cancel button is pressed
		 */
		void userCreateButtonCancelCallback();

		/**
		 * Called when confirm button is pressed
		 * 
		 * @param user
		 */
		void userCreateButtonConfirmCallback(String user);
	}

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- UI LISTENERS -------------------------------
	 * ------------------------------------------------------------------------
	 */
	/**
	 * User EditText action listener to let Enter and Done actions perform user
	 * creation
	 */
	private OnEditorActionListener userEditorActionListener = new OnEditorActionListener() {
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
					|| (actionId == EditorInfo.IME_ACTION_DONE)) {
				if (getNewUserName().length() > 0) {
					hostActivity
							.userCreateButtonConfirmCallback(getNewUserName());
				}
			}
			return false;
		}
	};

	/**
	 * TextWatcher used to unlock the confirm button when the new user name has
	 * length > 0
	 */
	private TextWatcher textWatcher = new TextWatcher() {

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {

		}

		@Override
		public void afterTextChanged(Editable s) {
			try {
				if (newUserEditText.getText().length() > 0) {
					unlockButton();
				} else {
					lockButton();
				}
			} catch (NullPointerException e) {
				lockButton();
			}
		}
	};

	/**
	 * Cancel button listener.
	 */
	private OnClickListener cancelButtonClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			hostActivity.userCreateButtonCancelCallback();
		}

	};

	/**
	 * Confirm button listener.
	 */
	private OnClickListener confirmButtonClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (getNewUserName().length() > 0) {
				hostActivity.userCreateButtonConfirmCallback(getNewUserName());
			}
		}

	};

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- ATTRIBUTES -------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Activity that hosts this fragment and implements the
	 * UserCreateFragmentCallbacks
	 */
	private UserCreateFragmentCallbacks hostActivity;

	/**
	 * New user EditText
	 */
	private EditText newUserEditText;

	/*
	 * Buttons
	 */
	private Button okButton;
	private Button cancelButton;

	/**
	 * Called when fragment attached to the activity. Try to cast the activity
	 * to the fragment callback. If cast failed throw exception
	 * 
	 * @param activity
	 *            host activity
	 * @throws ClassCastException
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			hostActivity = (UserCreateFragmentCallbacks) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement UserCreateFragmentCallbacks");
		}
	}

	/**
	 * Called when the activity requests the fragment to create its view.
	 * 
	 * @param inflater
	 * @param container
	 *            host activity container where the fragment should inflate
	 *            itself
	 * @param savedInstanceState
	 * @return the inflated view
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.setup_user_create_fragment, container,
				false);
	}

	/**
	 * Called when the activity has been rendered. Not it's possible to use
	 * getActivity().findViewById() to get reference to the UI views
	 * 
	 * @param savedInstanceState
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		newUserEditText = (EditText) getActivity().findViewById(
				R.id.new_user_edit_text);

		newUserEditText.setOnEditorActionListener(userEditorActionListener);
		newUserEditText.requestFocus();
		newUserEditText.addTextChangedListener(textWatcher);

		okButton = (Button) getActivity().findViewById(
				R.id.user_create_button_ok);
		okButton.setOnClickListener(confirmButtonClickListener);
		cancelButton = (Button) getActivity().findViewById(
				R.id.user_create_button_cancel);
		cancelButton.setOnClickListener(cancelButtonClickListener);

		clean();

		TmlSetupActivity.showSoftKeyboard(getActivity(), newUserEditText);
	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- GENERIC METHODS --------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Retrieve typed user name
	 * 
	 * @return
	 */
	private String getNewUserName() {
		return newUserEditText.getText().toString().trim();
	}

	/**
	 * Clean the new user name and lock the confirm button
	 */
	public void clean() {
		newUserEditText.setText("");
		lockButton();
	}

	/**
	 * Disable OK button and change background
	 */
	private void lockButton() {
		/*
		 * Change button drawable and lock
		 */
		okButton.setEnabled(false);

		okButton.setTextColor(getResources().getColor(
				R.color.setup_activity_button_disabled_textColor));

		okButton.setBackgroundResource(R.drawable.selector_button_disabled);
	}

	/**
	 * Enable OK button and change background
	 */
	private void unlockButton() {
		/*
		 * Change button drawable and unlock
		 */
		okButton.setEnabled(true);

		okButton.setTextColor(getResources().getColor(
				R.color.setup_activity_button_textColor));

		okButton.setBackgroundResource(R.drawable.selector_button_green);
	}

}
