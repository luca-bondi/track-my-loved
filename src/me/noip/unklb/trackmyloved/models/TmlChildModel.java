package me.noip.unklb.trackmyloved.models;

import java.util.ArrayList;

import me.noip.unklb.trackmyloved.controllers.TmlPlace;
import me.noip.unklb.trackmyloved.controllers.TmlPosition;
import me.noip.unklb.trackmyloved.controllers.TmlSafeZone;

/**
 * Model containing all the child data. It's a superclass for TmlChild
 * controller
 */
public class TmlChildModel {

	/**
	 * The name of this child. The name is unique within the same account.
	 */
	protected final String name;

	/**
	 * All the position of this child stored in the database.
	 */
	protected final ArrayList<TmlPosition> positionList = new ArrayList<TmlPosition>();

	/**
	 * All the filtered position of this child.
	 */
	protected final ArrayList<TmlPosition> filteredPositionList = new ArrayList<TmlPosition>();

	/**
	 * All the places of this child
	 */
	protected final ArrayList<TmlPlace> placeList = new ArrayList<TmlPlace>();

	/**
	 * The safe zones for this child
	 */
	protected final ArrayList<TmlSafeZone> safeZoneList = new ArrayList<TmlSafeZone>();

	/**
	 * Indicates that this child has been deleted.
	 */
	protected boolean deleted;

	/**
	 * Default constructor for the child.
	 * 
	 * @param name
	 */
	protected TmlChildModel(String name) {
		this.name = name;
	}

	/**
	 * Name of the child
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * This child is deleted
	 * 
	 * @return
	 */
	public boolean getDeleted() {
		return deleted;
	}

}
