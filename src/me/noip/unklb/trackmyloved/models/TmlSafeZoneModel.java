package me.noip.unklb.trackmyloved.models;

import java.util.ArrayList;

import com.google.android.gms.maps.model.LatLng;

/**
 * Model containing all the safeZone data. It's a superclass for TmlSafeZone
 * controller
 */
public class TmlSafeZoneModel {

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- ATTRIBUTES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Serial number of this safeZone in the device database
	 */
	protected final int serial;

	/**
	 * List of vertex for this safeZone. The order is important.
	 */
	protected ArrayList<LatLng> pointList = new ArrayList<LatLng>();

	/**
	 * Set when this safeZone has been created on this device and the server
	 * hasn't been notified about its existence
	 */
	protected boolean toBeCreated = false;

	/**
	 * Set when this safeZone has been removed on this device and the server
	 * hasn't been notified about its deletion
	 */
	protected boolean toBeRemoved = false;

	/**
	 * Set when safeZone deleted from Network service. Indicates that the
	 * safeZoneView must be deleted (from UI thread) then the safeZone can be
	 * deleted from the safeZoneList
	 */
	protected boolean deleted = false;

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- METHODS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Constructor used when drawing a new safe zone
	 * 
	 * @param serial
	 */
	protected TmlSafeZoneModel(int serial) {
		this.serial = serial;
	}

	/**
	 * Constructor used when loading safeZone from the database of when a new
	 * safeZone is added through synchronization
	 * 
	 * @param serial
	 * @param pointList
	 * @param created
	 * @param removed
	 */
	protected TmlSafeZoneModel(int serial, ArrayList<LatLng> pointList,
			boolean created, boolean removed) {
		this.serial = serial;
		this.pointList = new ArrayList<LatLng>(pointList);
		this.toBeCreated = created;
		this.toBeRemoved = removed;
	}

	/**
	 * This safeZone has to be deleted from UI thread
	 * 
	 * @return
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * This safeZone has been created on this device and the server has not been
	 * notified
	 * 
	 * @return
	 */
	public boolean toBeCreated() {
		return toBeCreated;
	}

	/**
	 * This safeZone has been removed on this device and the server has not been
	 * notified
	 * 
	 * @return
	 */
	public boolean toBeRemoved() {
		return toBeRemoved;
	}

	/**
	 * This SafeZone is valid (not deleted nor to be removed)
	 * 
	 * @return
	 */
	public boolean isValid() {
		return !deleted & !toBeRemoved;
	}

}
