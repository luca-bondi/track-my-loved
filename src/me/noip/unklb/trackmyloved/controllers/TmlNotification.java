package me.noip.unklb.trackmyloved.controllers;

import java.util.ArrayList;

import me.noip.unklb.trackmyloved.R;
import me.noip.unklb.trackmyloved.activities.TmlMainActivity;
import me.noip.unklb.trackmyloved.activities.TmlMapActivity;
import me.noip.unklb.trackmyloved.controllers.TmlChild.BatteryLevel;
import me.noip.unklb.trackmyloved.controllers.TmlChild.FreshnessLevel;
import me.noip.unklb.trackmyloved.controllers.TmlChild.SafenessLevel;
import me.noip.unklb.trackmyloved.controllers.TmlChild.TmlChildrenListener;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

/**
 * Notification controller
 * 
 * Max one notification per child is shown.
 */
public class TmlNotification extends Notification {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/*
	 * Notification ids
	 */
	public static final int NOTIFICATION_ID_GPS_MISSING = 0;
	public static final int NOTIFICATION_ID_INITIAL_SYNC = 1;
	public static final int NOTIFICATION_ID_LOCATION_DISABLED = 2;
	public static final int NOTIFICATION_ID_CHILD = 10;

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CLASSES ------------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Class used to manage the notification for each child
	 */
	private class NotificationRegisterEntry {

		private final TmlChild child;

		public boolean safenessShown = false;
		public boolean freshnessShown = false;
		public boolean batteryShown = false;

		public int freshnessHours;

		public Float batteryLevel;

		public NotificationRegisterEntry(TmlChild child) {
			this.child = child;
		}

		public TmlChild getChild() {
			return child;
		}

	}

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- CALLBACKS -----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Callback as required by TmlChild controller
	 */
	private TmlChildrenListener childrenListener = new TmlChildrenListener() {

		@Override
		public void onAdd(TmlChild child) {
			notificationRegister.add(new NotificationRegisterEntry(child));
		}

		@Override
		public void onRemove(TmlChild child) {
			for (NotificationRegisterEntry entry : notificationRegister) {
				if (entry.getChild().equals(child)) {
					cancelNotification(context, child.getName(),
							NOTIFICATION_ID_CHILD);
					notificationRegister.remove(entry);
					break;
				}
			}

		}

		@Override
		public void onUpdate(TmlChild child) {
		}

	};

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- ATTRIBUTES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Calling context
	 */
	private static Context context;

	/**
	 * Notification register
	 */
	private static ArrayList<NotificationRegisterEntry> notificationRegister = new ArrayList<NotificationRegisterEntry>();

	/**
	 * Instance of this class
	 */
	private static TmlNotification instance;

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- METHODS -------------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Return the instance of this singleton class
	 * 
	 * @param context
	 * @return
	 */
	public static TmlNotification getInstance(Context context) {
		if (TmlNotification.instance == null) {
			TmlNotification.instance = new TmlNotification(context);
			TmlNotification.context = context.getApplicationContext();
		}
		return TmlNotification.instance;
	}

	/**
	 * Constructor. Instantiate the children in the notificationRegister
	 * 
	 * @param context
	 */
	private TmlNotification(Context context) {
		for (TmlChild child : TmlChild.getChildrenList(context,
				childrenListener)) {
			notificationRegister.add(new NotificationRegisterEntry(child));
		}
	}

	/**
	 * Called by TmlNetwork after each synchronization. Called by TmlMapActivity
	 * when SafeZones edited
	 * 
	 * Rise or cancel notifications as needed. Only one notification per child
	 * is shown. In descending priority: time, safeness, battery
	 */
	public void riseNotifications() {

		for (NotificationRegisterEntry registryEntry : notificationRegister) {
			TmlChild child = registryEntry.getChild();

			/*
			 * Prepare intent used when tapping on the notification
			 */
			Intent intent = new Intent(context, TmlMapActivity.class);
			intent.putExtra(TmlMapActivity.INTENT_EXTRA_CHILD_NAME,
					child.getName());

			if (child.getFreshness() == FreshnessLevel.WORSE) {
				/*
				 * Throw or update time notification
				 */
				if (!registryEntry.freshnessShown
						|| (registryEntry.freshnessShown && registryEntry.freshnessHours != child
								.hoursSinceLastPosition())) {
					registryEntry.batteryShown = false;
					registryEntry.safenessShown = false;
					registryEntry.freshnessShown = true;
					String title = context.getString(
							R.string.notification_freshness_title,
							child.getName());
					String text;
					registryEntry.freshnessHours = child
							.hoursSinceLastPosition();
					if (registryEntry.freshnessHours <= 24) {
						text = context.getString(
								R.string.notification_freshness_text_hours,
								registryEntry.freshnessHours);
					} else {
						text = context
								.getString(
										R.string.notification_freshness_text_days,
										(int) (Math
												.ceil(registryEntry.freshnessHours / 24.0)));
					}
					TmlNotification.showNotification(context, child.getName(),
							TmlNotification.NOTIFICATION_ID_CHILD,
							R.drawable.ic_stat_notify_time,
							R.drawable.notif_large_icon, title, text, intent);
				}
			} else if (child.getSafeness() == SafenessLevel.UNSAFE) {
				/*
				 * Throw safezone notification
				 */
				if (!registryEntry.safenessShown) {
					registryEntry.batteryShown = false;
					registryEntry.safenessShown = true;
					String title = context.getString(
							R.string.notification_safeness_title,
							child.getName());
					String text = "";
					TmlNotification.showNotification(context, child.getName(),
							TmlNotification.NOTIFICATION_ID_CHILD,
							R.drawable.ic_stat_notify_unsafe,
							R.drawable.notif_large_icon, title, text, intent);
				}
			} else if (child.getBatteryCharge() == BatteryLevel.LOWEST) {
				/*
				 * Throw battery notification
				 */
				if (!registryEntry.batteryShown) {
					registryEntry.batteryShown = true;
					registryEntry.batteryLevel = child.getLastPosition()
							.getBattery();
					String title = context.getString(
							R.string.notification_batterycharge_title,
							child.getName());
					String text = context.getString(
							R.string.notification_batterycharge_text,
							(int) (registryEntry.batteryLevel * 100));
					TmlNotification.showNotification(context, child.getName(),
							TmlNotification.NOTIFICATION_ID_CHILD,
							R.drawable.ic_stat_notify_battery,
							R.drawable.notif_large_icon, title, text, intent);
				}
			} else {
				/*
				 * Remove all notifications
				 */
				TmlNotification.cancelNotification(context, child.getName(),
						TmlNotification.NOTIFICATION_ID_CHILD);
				registryEntry.batteryShown = false;
				registryEntry.safenessShown = false;
				registryEntry.freshnessShown = false;
			}
			if (child.getBatteryCharge() != BatteryLevel.LOWEST) {
				registryEntry.batteryShown = false;
			}
			if (child.getSafeness() != SafenessLevel.UNSAFE) {
				registryEntry.safenessShown = false;
			}
			if (child.getFreshness() != FreshnessLevel.WORSE) {
				registryEntry.freshnessShown = false;
			}

		}

	}

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- STATIC METHODS ------------------------------
	 * ------------------------------------------------------------------------
	 */
	/**
	 * Show notification
	 * 
	 * @param context
	 * @param tag
	 *            can be null
	 * @param notificationId
	 * @param smallIconResId
	 * @param largeIconResId
	 * @param title
	 * @param text
	 * @return the shown notification
	 */
	public static Notification showNotification(Context context, String tag,
			int notificationId, int smallIconResId, int largeIconResId,
			String title, String text) {

		NotificationManager mNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);

		Notification notification = buildNotification(context, smallIconResId,
				largeIconResId, title, text);

		mNotificationManager.notify(tag, notificationId, notification);

		return notification;
	}

	/**
	 * Show notification
	 * 
	 * @param context
	 * @param tag
	 *            can be null
	 * @param notificationId
	 * @param smallIconResId
	 * @param largeIconResId
	 * @param title
	 * @param text
	 * @param intent
	 * @return the shown notification
	 */
	public static Notification showNotification(Context context, String tag,
			int notificationId, int smallIconResId, int largeIconResId,
			String title, String text, Intent intent) {

		NotificationManager mNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);

		Notification notification = buildNotification(context, smallIconResId,
				largeIconResId, title, text, intent);
		mNotificationManager.notify(tag, notificationId, notification);

		return notification;

	}

	/**
	 * Build notification
	 * 
	 * @param context
	 * @param smallIconResId
	 * @param bigIconResId
	 * @param title
	 * @param text
	 * @return the built notification
	 */
	public static Notification buildNotification(Context context,
			int smallIconResId, int bigIconResId, String title, String text) {

		return buildNotification(context, smallIconResId, bigIconResId, title,
				text, new Intent(context, TmlMainActivity.class));
	}

	/**
	 * Build notification
	 * 
	 * @param context
	 * @param smallIconResId
	 * @param bigIconResId
	 * @param title
	 * @param text
	 * @param intent
	 * @return the built notification
	 */
	public static Notification buildNotification(Context context,
			int smallIconResId, int bigIconResId, String title, String text,
			Intent intent) {

		PendingIntent contentIntent = PendingIntent.getActivity(context,
				intent.hashCode(), intent, 0);

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				context)
				.setSmallIcon(smallIconResId)
				.setLargeIcon(
						BitmapFactory.decodeResource(context.getResources(),
								bigIconResId)).setContentTitle(title)
				.setContentText(text);
		mBuilder.setContentIntent(contentIntent);
		mBuilder.setAutoCancel(true);

		return mBuilder.build();
	}

	/**
	 * Cancel notification
	 * 
	 * @param context
	 * @param tag
	 *            child name
	 * @param notificationId
	 */
	public static void cancelNotification(Context context, String tag,
			int notificationId) {
		NotificationManager mNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancel(tag, notificationId);
	}

	/**
	 * Cancel all notifications
	 * 
	 * @param context
	 */
	public static void cancelAll(Context context) {
		NotificationManager mNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancelAll();
	}

	/**
	 * Called at device reset. Cancel all the notifications and reset
	 * notification register and notification controller instance
	 */
	public static void resetDevice() {
		if (context != null) {
			cancelAll(context);
		}
		notificationRegister = new ArrayList<NotificationRegisterEntry>();
		instance = null;
	}

}
