package me.noip.unklb.trackmyloved.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import me.noip.unklb.trackmyloved.activities.TmlMainActivity;
import me.noip.unklb.trackmyloved.models.TmlChildModel;
import me.noip.unklb.trackmyloved.services.TmlServicesManager;
import me.noip.unklb.trackmyloved.views.TmlChildView;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;

/**
 * Children controller
 */
public final class TmlChild extends TmlChildModel {

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- CONSTANTS -----------------------------------
	 * ------------------------------------------------------------------------
	 */
	private static final String TAG = "TmlChild";

	/**
	 * During initial sync not every inserted position is notified, but a
	 * notification is rise only every INITIAL_SYNC_POSITIONS_UPDATE_COUNTER
	 * positions
	 */
	private static final int INITIAL_SYNC_POSITIONS_UPDATE_COUNTER = 100;

	/**
	 * Threshold between GOOD and BAD freshness
	 */
	private static final float FRESHNESS_GOOD_BAD_HOURS = 0.5f;
	/**
	 * Threshold between BAD and WORSE freshness
	 */
	public static final float FRESHNESS_BAD_WORSE_HOURS = 2;
	/**
	 * Threshold between OK and LOW battery level
	 */
	private static final float BATTERY_OK_LOW_LEVEL = 0.5f;
	/**
	 * Threshold between LOW and LOWEST battery level
	 */
	public static final float BATTERY_LOW_LOWEST_LEVEL = 0.15f;

	/**
	 * Threshold on accuracy to declare a position accurate [m]
	 */
	private static final int ACCURACY_TH = 40;

	/**
	 * Minimum time spent in a location to declare that it's a filtered position
	 */
	private static final long TRACK_MIN_TIMESTAMP_DIFFERENCE = 5 * 60 * 1000;

	/**
	 * Ratio between the distance between two location and the sum of accuracies
	 * to determine that are the same location. The lower more filtered
	 * positions are created
	 */
	private static final float ACCURACY_RATIO = 1.5f;

	/**
	 * Timestamp difference to declare that two positions are near in time
	 */
	private static final long NEAR_TIMESTAMP_DIFFERENCE = 240 * 1000;

	/**
	 * Ratio between the distance between two location and the sum of accuracies
	 * to determine that are the same location. The lower more filtered
	 * positions are created. Used when the two positions are near in time
	 */
	private static final float ACCURACY_RATIO_NEAR_TIME = 3f;

	/**
	 * Timestamp difference to declare that two positions are very near in time
	 */
	private static final long VERY_NEAR_TIMESTAMP_DIFFERENCE = 120 * 1000;

	/**
	 * Ratio between the distance between two location and the sum of accuracies
	 * to determine that are the same location. The lower more filtered
	 * positions are created. Used when the two positions are very near in time
	 */
	private static final float ACCURACY_RATIO_VERY_NEAR_TIME = 4f;

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- ENUMS ---------------------------------------
	 * ------------------------------------------------------------------------
	 */
	public static enum SafenessLevel {
		UNKNOWN, UNSAFE, SAFE
	};

	public static enum FreshnessLevel {
		WORSE, BAD, GOOD
	};

	public static enum BatteryLevel {
		LOWEST, LOW, OK
	};

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- INTERFACES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Interface containing all the callback prototypes necessary to handle the
	 * children list operations: add, remove, update
	 * 
	 */
	public interface TmlChildrenListener {

		/**
		 * Called by TmlChild when child has been added to the application
		 * 
		 * @param child
		 */
		public void onAdd(TmlChild child);

		/**
		 * Called by TmlChild when child has been removed from the application
		 * 
		 * @param child
		 */
		public void onRemove(TmlChild child);

		/**
		 * Called by TmlChild when child has been updated
		 * 
		 * @param child
		 */
		public void onUpdate(TmlChild child);

	}

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- STATIC ATTRIBUTES ---------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Instance of the DatabaseController
	 */
	private static TmlDatabase db;

	/**
	 * Instance of the application appContext
	 */
	private static Context appContext;

	/**
	 * This is a child device
	 */
	private static boolean isChildDevice;

	/**
	 * List of all the listeners to which provide updates about the children
	 * list
	 */
	private final static ArrayList<TmlChildrenListener> listenerList = new ArrayList<TmlChildrenListener>();

	/**
	 * List of instances of this class, one for each user.
	 */
	private final static ArrayList<TmlChild> instanceList = new ArrayList<TmlChild>();

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- ATTRIBUTES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Last filtered position. Used in filterPositionList
	 */
	private TmlPosition lastFilteredPosition;

	/**
	 * List of positions that build the last filtered position
	 */
	private ArrayList<TmlPosition> lastFilteredPositionList = new ArrayList<TmlPosition>();

	/**
	 * Positions that cannot be inserted due to initial loading in progress.
	 * Store here the positions that will be inserted when initial loading
	 * finishes
	 */
	private ArrayList<TmlPosition> positionsToBeInserted;

	/**
	 * Instance of TmlChildView that contains all the view attributes and
	 * methods to manage the visualization of this child on the map
	 */
	private TmlChildView view;

	/**
	 * Last timestamp available on server for this child. Used when performing
	 * synchronization operations
	 */
	private Long lastRemoteTimestamp;

	/**
	 * Initial loading of data from database has been completed. While this is
	 * reset the tracking and the sync service must not try to insert new
	 * positions
	 */
	private boolean initialLoadingCompleted = false;

	/**
	 * Number of positions inserted since last update during initial sync phase
	 */
	private int initialSyncPositionsCounter;

	/*
	 * ------------------------------------------------------------------------
	 * --------------------- STATIC METHODS -----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * The list of children. Every class that requires the list needs to provide
	 * a valid context and a listener implementing the callback functions
	 * necessary to notify the presence of a new child, the removal of a child,
	 * the update of a child
	 * 
	 * @param context
	 * @param listener
	 * @return ArrayList<TmlChild>, null if no users registered
	 */
	public synchronized static final ArrayList<TmlChild> getChildrenList(
			Context context, TmlChildrenListener listener) {

		if (context == null || listener == null) {
			Log.e(TAG, "Incorrect call to getChildrenList");
			return null;
		}

		if (db == null && appContext == null) {
			Log.i(TAG, "Initializing children");
			/*
			 * Class initialization.
			 * 
			 * Static attributes are initialized.
			 */
			appContext = context.getApplicationContext();
			db = TmlDatabase.getInstance(appContext);

			if (!db.isRoleDefined()) {
				Log.e(TAG, "Role not defined");
				return null;
			}
			isChildDevice = db.isChildDevice();
			/*
			 * Users from database are initialized
			 */
			for (String childName : db.getChildren()) {
				instanceList.add(new TmlChild(childName));
			}
		}

		/*
		 * Add listener to the lists if necessary
		 */
		if (!listenerList.contains(listener)) {
			listenerList.add(listener);
		}

		/*
		 * Return a copy of instanceList
		 */
		return new ArrayList<TmlChild>(instanceList);

	}

	/**
	 * Notify the UI thread that an update has occurred. Even if the UI thread
	 * has a TmlChildListener the UI update can be done only from the UI thread
	 */
	public static void notifyUiThreadUpdate() {

		LocalBroadcastManager
				.getInstance(appContext)
				.sendBroadcast(
						new Intent(
								TmlServicesManager.BROADCAST_ACTION_CHILDREN_UPDATE));

	}

	/**
	 * Determine if all the children have been completely loaded
	 * 
	 * @return true if initial loading completed, false otherwise
	 */
	public synchronized static boolean initialLoadingCompleted() {
		boolean retVal = true;
		for (TmlChild child : instanceList) {
			retVal &= child.isInitialLoadingCompleted();
		}
		return retVal;
	}

	/**
	 * Determine if all the children have been completely loaded and all the map
	 * elements have been created
	 * 
	 * @return
	 */
	public static boolean initialViewCreationCompleted() {
		boolean retVal = true;
		for (TmlChild child : instanceList) {
			if (child.getView() == null) {
				retVal &= false;
			} else {
				retVal &= child.getView().isInitialViewCreationCompleted();
			}
		}
		return retVal;
	}

	/**
	 * Add a new child to the database
	 * 
	 * Child is added to the database and to the instanceList. All the listeners
	 * are notified that a new child is available.
	 * 
	 * @param childName
	 *            of the new child
	 * @return new child, null if errors
	 */
	public static synchronized TmlChild add(String childName) {

		/*
		 * Check that a child with the same name does not exist
		 */
		for (TmlChild child : instanceList) {
			if (child.getName().equals(childName)) {
				Log.e(TAG, "A child instance already exists for " + childName);
				return null;
			}
		}

		db.insertChild(childName);

		/*
		 * Instantiate the new child and add it to the instance list
		 */
		TmlChild newChild = new TmlChild(childName);
		instanceList.add(newChild);

		/*
		 * Notify add to the listeners
		 */
		for (TmlChildrenListener listener : listenerList) {
			listener.onAdd(newChild);
		}

		notifyUiThreadUpdate();

		return newChild;
	}

	/**
	 * Application context
	 * 
	 * @return
	 */
	public static Context getContext() {
		return appContext;
	}

	/**
	 * Database controller instance
	 * 
	 * @return
	 */
	public static TmlDatabase getDb() {
		return db;
	}

	/**
	 * This is a child device
	 * 
	 * @return
	 */
	public static boolean isChildDevice() {
		return isChildDevice;
	}

	/**
	 * This is a parent device
	 * 
	 * @return
	 */
	public static boolean isParentDevice() {
		return !isChildDevice;
	}

	/**
	 * History interval in hours
	 * 
	 * @return
	 */
	public static Integer getHistoryInterval() {
		return db.getHistoryInterval();
	}

	/**
	 * Set history interval in hours.
	 * 
	 * @param hours
	 */
	public static void setHistoryInterval(int hours) {
		db.setHistoryInterval(hours);
	}

	/**
	 * Called when device is reset
	 */
	public static void resetDevice() {
		db = null;
		appContext = null;
		listenerList.clear();
		instanceList.clear();
	}

	/*
	 * ------------------------------------------------------------------------
	 * --------------------- GENERIC METHODS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Instantiate a new child and load all the data from the database. Children
	 * are instantiated only from getChildrenList(...) or add(...)
	 */
	private TmlChild(String username) {
		super(username);

		Log.d(TAG, "Building " + username);

		/*
		 * Load all positions from the database and populate the filtered
		 * positions list
		 */
		new AsyncTask<TmlChild, Void, ArrayList<TmlPosition>>() {

			private TmlChild instance;

			@Override
			protected synchronized ArrayList<TmlPosition> doInBackground(
					TmlChild... params) {
				Thread.currentThread().setName(getName() + " DB loading");
				instance = params[0];
				return db.getPositions(instance.getName());
			}

			@Override
			protected synchronized void onPostExecute(
					ArrayList<TmlPosition> newPositionList) {
				positionList.addAll(newPositionList);
				if (positionList.size() > 0) {
					lastFilteredPosition = positionList
							.get(positionList.size() - 1);
					lastFilteredPositionList.add(lastFilteredPosition);
					filterPositionList(positionList);
				}
				/*
				 * Set the initial loading completed. Up to here no operations
				 * involving positionList and filteredPositionList can be
				 * executed
				 */
				initialLoadingCompleted = true;

				if (positionsToBeInserted != null
						&& positionsToBeInserted.size() > 0) {
					/*
					 * Insert positions read from the database and trigger
					 * update
					 */
					insertPositionList(positionsToBeInserted);
				} else {
					triggerUpdate();
				}

			}

		}.execute(this);

		if (!isChildDevice) {
			/*
			 * Load the safe zones from the database
			 */
			safeZoneList.addAll(TmlSafeZone.getSafeZones(this));
		}

	}

	/**
	 * Delete this child from the application.
	 * 
	 * Child is removed from the database and from the instanceList. All the
	 * listeners are notified that this child has been removed.
	 */
	public synchronized void delete() {

		if (isChildDevice) {
			/*
			 * Start application silent reset
			 */
			Intent intent = new Intent(appContext, TmlMainActivity.class);
			intent.setAction(TmlServicesManager.INTENT_ACTION_CHILD_DEVICE_RESET);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			appContext.startActivity(intent);
		} else {

			/*
			 * Notify removal to the listeners
			 */
			for (TmlChildrenListener listener : listenerList) {
				listener.onRemove(this);
			}
			/*
			 * Notify the update to the UI thread
			 */
			notifyUiThreadUpdate();

			instanceList.remove(this);
			db.deleteChild(name);

			/*
			 * The deleted flag is set to indicate other threads having a
			 * reference to the deleted child that it is deleted. Necessary for
			 * TmlNetwork no notify the server that the user has been deleted.
			 */
			deleted = true;
		}

	}

	/**
	 * Call listener.onUpdate and notifyUiThreadUpdate for this child
	 */
	public void triggerUpdate() {

		for (TmlChildrenListener listener : listenerList) {
			listener.onUpdate(this);
		}
		notifyUiThreadUpdate();
	}

	/*
	 * ------------------------------------------------------------------------
	 * --------------------- CHILD INFO METHODS -------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Safeness level of the child based on safe zones
	 * 
	 * @return SafenessLevel
	 */
	public synchronized SafenessLevel getSafeness() {
		if (getLastPosition() == null || getSafeZoneList() == null
				|| getSafeZoneList().size() == 0) {
			return SafenessLevel.UNKNOWN;
		}

		SafenessLevel bestSafeness = SafenessLevel.UNSAFE;

		ArrayList<TmlSafeZone> safeZoneListTemp = new ArrayList<TmlSafeZone>(
				safeZoneList);

		boolean validSafeZonesFound = false;
		for (TmlSafeZone safeZone : safeZoneListTemp) {
			if (!safeZone.isDeleted() && !safeZone.toBeRemoved()) {
				validSafeZonesFound = true;
				if (safeZone.contains(getLastFilteredPosition()
						.getCoordinates())) {
					bestSafeness = SafenessLevel.SAFE;
					break;
				} else {
					for (double angle = 0; angle < 2 * Math.PI; angle += Math.PI / 4) {
						if (safeZone.contains(TmlPosition.moveLatLng(
								getLastFilteredPosition().getCoordinates(),
								Math.sin(angle)
										* getLastFilteredPosition()
												.getAccuracy(), Math.cos(angle)
										* getLastFilteredPosition()
												.getAccuracy()))) {
							bestSafeness = SafenessLevel.SAFE;
							break;
						}
					}
				}
			}
		}

		if (!validSafeZonesFound) {
			return SafenessLevel.UNKNOWN;
		}

		return bestSafeness;
	}

	/**
	 * Freshness of last retrieved position
	 * 
	 * @return FRESHNESS_WORSE | FRESHNESS_BAD | FRESHNESS_GOOD
	 */
	public synchronized FreshnessLevel getFreshness() {

		if (getLastFilteredPosition() == null) {
			return FreshnessLevel.GOOD;
		}

		long lastTimestamp = getLastFilteredPosition().getTimestamp();
		long currentTimestamp = System.currentTimeMillis();

		if (lastTimestamp > currentTimestamp - FRESHNESS_GOOD_BAD_HOURS * 3600
				* 1000) {
			return FreshnessLevel.GOOD;
		}
		if (lastTimestamp > currentTimestamp - FRESHNESS_BAD_WORSE_HOURS * 3600
				* 1000) {
			return FreshnessLevel.BAD;
		}

		return FreshnessLevel.WORSE;
	}

	/**
	 * Battery level of last position
	 * 
	 * @return BATTERY_LOWEST | BATTERY_LOW | BATTERY_OK
	 */
	public synchronized BatteryLevel getBatteryCharge() {

		if (getLastPosition() == null) {
			return BatteryLevel.OK;
		}

		float lastBatteryLevel = getLastPosition().getBattery();

		if (lastBatteryLevel > BATTERY_OK_LOW_LEVEL) {
			return BatteryLevel.OK;
		}
		if (lastBatteryLevel > BATTERY_LOW_LOWEST_LEVEL) {
			return BatteryLevel.LOW;
		}

		return BatteryLevel.LOWEST;
	}

	/**
	 * Hours since last position received
	 * 
	 * @return
	 */
	public int hoursSinceLastPosition() {
		return Math
				.round((float) (System.currentTimeMillis() - getLastLocalTimestamp())
						/ (3600 * 1000));
	}

	/**
	 * Determines whether the last retrieved position has been taken after today
	 * 00:00
	 * 
	 * @return
	 */
	public boolean isLastPositionOnToday() {

		if (getLastFilteredPosition() == null) {
			return false;
		}

		Calendar lastMidnight = new GregorianCalendar();
		lastMidnight.set(Calendar.HOUR_OF_DAY, 0);
		lastMidnight.set(Calendar.MINUTE, 0);
		lastMidnight.set(Calendar.SECOND, 0);
		lastMidnight.set(Calendar.MILLISECOND, 0);

		return getLastFilteredPosition().getTimestamp() > lastMidnight
				.getTimeInMillis();
	}

	/**
	 * Determines whether the last retrieved position comes form the last hours
	 * 
	 * @return
	 */
	public boolean isLastPositionEarlierThan(int hours) {

		if (getLastFilteredPosition() == null) {
			return false;
		}

		return getLastFilteredPosition().getTimestamp() > System
				.currentTimeMillis() - (long) hours * 3600 * 1000;
	}

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- VIEW METHODS --------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Create the view for this child
	 * 
	 * @param map
	 */
	public void createView(GoogleMap map) {
		this.view = new TmlChildView(map, this);
	}

	/**
	 * Get the TmlChildView relative to this child
	 * 
	 * @return
	 */
	public TmlChildView getView() {
		return view;
	}

	/**
	 * Delete all visible objects, scale color counter and invalidate view
	 * pointer
	 */
	public void deleteView() {
		if (view != null) {
			TmlColor.remove(view.getColor());
			view.hide();
		}
		view = null;
	}

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- POSITIONS METHODS ---------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Initial loading completed
	 * 
	 * @return true if loading completed, false otherwise
	 */
	public boolean isInitialLoadingCompleted() {
		return initialLoadingCompleted;
	}

	/**
	 * Get the last timestamp known by the remote server
	 */
	public Long getLastRemoteTimestamp() {
		return lastRemoteTimestamp;
	}

	/**
	 * Set the last timestamp known by the remote server
	 * 
	 * @param lastRemoteTimestamp
	 */
	public void setLastRemoteTimestamp(Long lastRemoteTimestamp) {
		this.lastRemoteTimestamp = lastRemoteTimestamp;
	}

	/**
	 * Get the first timestamp known in this device for the child
	 */
	public Long getFirstLocalTimestamp() {
		if (positionList == null || positionList.size() == 0) {
			return 0l;
		}
		return positionList.get(0).getFirstTimestamp();
	}

	/**
	 * Get the last timestamp known in this device for the child
	 */
	public synchronized Long getLastLocalTimestamp() {
		TmlPosition lastPosition = getLastPosition();
		if (lastPosition == null)
			return 0l;
		return lastPosition.getTimestamp();
	}

	/**
	 * Last position available in the database, null if no positions.
	 * 
	 * @return
	 */
	public synchronized TmlPosition getLastPosition() {

		if (positionList.size() == 0) {
			return null;
		}
		return positionList.get(positionList.size() - 1);
	}

	/**
	 * Last filtered position available.
	 * 
	 * @return
	 */
	public synchronized TmlPosition getLastFilteredPosition() {
		if (filteredPositionList.size() == 0) {
			return null;
		}
		return filteredPositionList.get(filteredPositionList.size() - 1);
	}

	/**
	 * Last historyInterval hours of filtered positions
	 * 
	 * @return ArrayList<TmlPosition>, empty if no positions
	 */
	public synchronized ArrayList<TmlPosition> getFilteredPositionList() {
		if (filteredPositionList.size() == 0) {
			return filteredPositionList;
		}

		long oldestTimestamp = getLastLocalTimestamp() - getHistoryInterval()
				* 3600 * 1000;

		ArrayList<TmlPosition> filteredPositionHistoryList = new ArrayList<TmlPosition>();

		int idx = filteredPositionList.size() - 1;
		while (idx >= 0
				&& filteredPositionList.get(idx).getTimestamp() > oldestTimestamp) {
			filteredPositionHistoryList.add(0, filteredPositionList.get(idx));
			idx--;
		}

		return filteredPositionHistoryList;
	}

	/**
	 * Get the list of places for this child in the last historyInterval hours
	 * 
	 * @return
	 */
	public synchronized ArrayList<TmlPlace> getPlaceList() {
		if (getFilteredPositionList().size() == 0) {
			return new ArrayList<TmlPlace>();
		}

		ArrayList<TmlPosition> positionList = getFilteredPositionList();

		ArrayList<TmlPlace> placeHistoryList = new ArrayList<TmlPlace>();

		for (TmlPosition position : positionList) {
			if (!placeHistoryList.contains(position.getPlace())) {
				placeHistoryList.add(position.getPlace());
			}
		}

		return placeHistoryList;
	}

	/**
	 * Insert a new position in this instance and in the database. Filtering is
	 * also performed. If view is available the necessary update flag is set.
	 * 
	 * Only used by child device (TmlTrack)
	 * 
	 * @param position
	 */
	public synchronized void insertPosition(TmlPosition position) {

		if (deleted) {
			Log.e(TAG, getName() + " deleted");
			return;
		}
		if (!initialLoadingCompleted) {
			if (positionsToBeInserted == null) {
				positionsToBeInserted = new ArrayList<TmlPosition>();
			}
			positionsToBeInserted.add(position);
			return;
		}
		positionList.add(position);
		if (isChildDevice) {
			db.insertNewPosition(position);
		} else {
			db.insertNewPosition(getName(), position);
		}

		filterNewPosition();

		if (!db.isInitialSyncRequired() && isInitialLoadingCompleted()) {
			/*
			 * Notify update to the listeners
			 */
			for (TmlChildrenListener listener : listenerList) {
				listener.onUpdate(this);
			}
			/*
			 * Notify the update to the UI thread
			 */
			notifyUiThreadUpdate();

		} else {
			if (++initialSyncPositionsCounter == INITIAL_SYNC_POSITIONS_UPDATE_COUNTER) {
				initialSyncPositionsCounter = 0;
				for (TmlChildrenListener listener : listenerList) {
					listener.onUpdate(this);
				}
				notifyUiThreadUpdate();
			}
		}
	}

	/**
	 * Used when multiple positions are retrieved through the sync service
	 * 
	 * @param newPositionList
	 */
	public synchronized void insertPositionList(
			ArrayList<TmlPosition> newPositionList) {

		if (deleted) {
			Log.e(TAG, getName() + " deleted");
			return;
		}
		if (!initialLoadingCompleted) {
			if (positionsToBeInserted == null) {
				positionsToBeInserted = new ArrayList<TmlPosition>(
						newPositionList);
			} else {
				positionsToBeInserted.addAll(newPositionList);
			}
			return;
		}
		positionList.addAll(newPositionList);
		if (isChildDevice) {
			db.insertNewPositionList(newPositionList);
		} else {
			db.insertNewPositionList(getName(), newPositionList);
		}

		if (lastFilteredPosition == null) {
			ArrayList<TmlPosition> newPositionListTemp = new ArrayList<TmlPosition>(
					1);
			newPositionListTemp.add(getLastPosition());
			lastFilteredPosition = getLastPosition();
			lastFilteredPositionList = newPositionListTemp;
		}
		filterPositionList(newPositionList);

		triggerUpdate();

	}

	/**
	 * Convenience method to call position filter
	 * 
	 * @return
	 */
	private synchronized boolean filterNewPosition() {
		ArrayList<TmlPosition> newPositionList = new ArrayList<TmlPosition>(1);
		newPositionList.add(getLastPosition());
		if (lastFilteredPosition == null) {
			lastFilteredPosition = getLastPosition();
			lastFilteredPositionList = newPositionList;
		}
		return filterPositionList(newPositionList);
	}

	/**
	 * Effective position filter. Take a list of new positions and filter them.
	 * lastFilteredPositionList must be ArrayList<TmlPosition>.
	 * lastFilteredPosition must be TmlPosition. filteredPositionList must be
	 * ArrayList<TmlPosition>
	 * 
	 * This method directly updates filteredPositionList
	 * 
	 * @param newPositionList
	 * @return true if new positions added or last position modified, false
	 *         otherwise
	 */
	private synchronized boolean filterPositionList(
			ArrayList<TmlPosition> newPositionList) {
		long filteredPositionLastTimestamp = 0;

		/*
		 * Remove last position from filteredPositionList and retrieve the
		 * current last timestamp
		 */
		if (filteredPositionList.size() > 0) {
			filteredPositionLastTimestamp = filteredPositionList.get(
					filteredPositionList.size() - 1).getTimestamp();
			filteredPositionList.remove(filteredPositionList.size() - 1);
		}

		for (TmlPosition lastPosition : newPositionList) {
			if (lastPosition.getAccuracy() < ACCURACY_TH) {
				/* Accurate position */
				if (isSameLocation(lastFilteredPosition, lastPosition)) {
					/* merge location */
					lastFilteredPositionList.add(lastPosition);
					lastFilteredPosition = TmlPosition
							.mergePositions(lastFilteredPositionList);
				} else {
					/* new location */
					if (lastFilteredPosition.getTimestamp()
							- lastFilteredPosition.getFirstTimestamp() > TRACK_MIN_TIMESTAMP_DIFFERENCE) {
						positionToPlace(lastFilteredPosition);
						filteredPositionList.add(lastFilteredPosition);
					}

					lastFilteredPositionList = new ArrayList<TmlPosition>();
					lastFilteredPositionList.add(lastPosition);
					lastFilteredPosition = lastPosition;
				}
			} else if (isSameLocation(lastFilteredPosition, lastPosition)) {
				/* merge location */
				lastFilteredPositionList.add(lastPosition);
				lastFilteredPosition = TmlPosition
						.mergePositions(lastFilteredPositionList);
			}
		}
		positionToPlace(lastFilteredPosition);
		filteredPositionList.add(lastFilteredPosition);

		return lastFilteredPosition.getTimestamp() > filteredPositionLastTimestamp;
	}

	/**
	 * The place relative to this position. Searched in placeList. If not found
	 * a new place is created. position is linked to the place
	 * 
	 * @param position
	 * @return null if not found
	 */
	private TmlPlace positionToPlace(TmlPosition position) {
		if (position.getPlace() != null) {
			position.getPlace().remove(position);
			if (position.getPlace().getPositionList().size() == 0) {
				placeList.remove(position.getPlace());
			} else {
			}
		}
		TmlPlace place = null;
		for (int idx = 0; idx < placeList.size(); idx++) {
			TmlPlace placeTemp = placeList.get(idx);
			if (placeTemp.nearTo(position)) {
				place = placeTemp;
				break;
			}
		}

		if (place == null) {
			place = new TmlPlace(position);
			placeList.add(place);
		} else {
			place.add(position);
		}
		/* Link position to the place */
		position.setPlace(place);

		return place;
	}

	/**
	 * Determines whether two positions can represent the same location
	 * 
	 * @param pos1
	 * @param pos2
	 * @return
	 */
	private boolean isSameLocation(TmlPosition pos1, TmlPosition pos2) {
		float distance = pos1.distanceTo(pos2);
		float accSum = pos1.getAccuracy() + pos2.getAccuracy();
		long timeDiff = Math.abs(pos2.getFirstTimestamp()
				- pos1.getLastTimestamp());
		if (distance < accSum * ACCURACY_RATIO) {
			return true;
		}
		if (timeDiff < NEAR_TIMESTAMP_DIFFERENCE
				&& distance < accSum * ACCURACY_RATIO_NEAR_TIME) {
			return true;
		}
		if (timeDiff < VERY_NEAR_TIMESTAMP_DIFFERENCE
				&& distance < accSum * ACCURACY_RATIO_VERY_NEAR_TIME) {
			return true;
		}
		/*
		 * Log.v(TAG, "isSameLocation - pos1LastTime: " +
		 * TmlChildView.getTime(pos1.getLastTimestamp()) + " pos2FirstTime: " +
		 * TmlChildView.getTime(pos2.getFirstTimestamp()) + " distance: " +
		 * Float.toString(distance) + " accSum: " + Float.toString(accSum));
		 */
		return false;
	}

	/**
	 * Check if server is in sync with this child local data
	 * 
	 * @return
	 */
	public boolean serverInSync() {
		if (getLastRemoteTimestamp() == null || getLastLocalTimestamp() == null) {
			return false;
		}
		return getLastLocalTimestamp().longValue() == getLastRemoteTimestamp()
				.longValue();
	}

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- SAFEZONE METHODS ----------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * The SafeZone list
	 * 
	 * @return
	 */
	public ArrayList<TmlSafeZone> getSafeZoneList() {
		return safeZoneList;
	}

	/**
	 * List of valid safeZones (not deleted or to be removed)
	 * 
	 * @return
	 */
	public ArrayList<TmlSafeZone> getValidSafeZoneList() {
		ArrayList<TmlSafeZone> validSafeZoneList = new ArrayList<TmlSafeZone>();

		for (TmlSafeZone safeZone : safeZoneList) {
			if (!(safeZone.isDeleted() || safeZone.toBeRemoved())) {
				validSafeZoneList.add(safeZone);
			}
		}

		return validSafeZoneList;
	}

	/**
	 * Remove all the safeZones for this child
	 */
	public void removeAllSafeZones() {
		for (TmlSafeZone safeZone : safeZoneList) {
			safeZone.remove();
		}
	}
}
