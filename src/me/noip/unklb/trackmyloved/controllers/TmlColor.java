package me.noip.unklb.trackmyloved.controllers;

import me.noip.unklb.trackmyloved.R;

/**
 * Color controller
 */
public class TmlColor {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Marker resource library
	 */
	private static final int[] markerResourceLibrary = {
			R.drawable.marker_position_blue, R.drawable.marker_position_red,
			R.drawable.marker_position_yellow,
			R.drawable.marker_position_violet, R.drawable.marker_position_green };

	/**
	 * Square resource library
	 */
	private static final int[] squareResourceLibrary = {
			R.drawable.square_blue, R.drawable.square_red,
			R.drawable.square_yellow, R.drawable.square_violet,
			R.drawable.square_green };

	/**
	 * Color register
	 */
	private static int[] colorCounter = { 0, 0, 0, 0, 0 };

	/**
	 * Minimum color counter in color register
	 */
	private static int colorCounterMin = 0;

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- ATTRIBUTES ---------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Color index between 0 and colorCounter.length
	 */
	private int colorIndex;

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- METHODS ------------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Constructor
	 * 
	 * @param colorIndex
	 */
	private TmlColor(int colorIndex) {
		this.colorIndex = colorIndex;
	}

	/**
	 * The marker background resource for this color
	 * 
	 * @return
	 */
	public int getMarkerBackgroundResource() {
		return markerResourceLibrary[colorIndex];
	}

	/**
	 * The square resource
	 * 
	 * @return
	 */
	public int getSquareResource() {
		return squareResourceLibrary[colorIndex];
	}

	/*
	 * ------------------------------------------------------------------------
	 * ------------------------- STATIC METHODS -------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Return an unused color. If all colors are used restart the color cycle
	 * 
	 * @return
	 */
	public static TmlColor getColor() {
		for (int colorIdx = 0; colorIdx < colorCounter.length; colorIdx++) {
			if (colorCounter[colorIdx] == colorCounterMin) {
				colorCounter[colorIdx]++;
				return new TmlColor(colorIdx);
			}
		}
		/*
		 * If this line reached all the colors have been used. Start again
		 * distributing the colors
		 */
		colorCounter[0]++;
		colorCounterMin++;
		return new TmlColor(0);
	}

	/**
	 * Decreases the counter of the given color
	 * 
	 * @param color
	 */
	public static void remove(TmlColor color) {
		int min = colorCounterMin;
		colorCounter[color.colorIndex]--;
		for (int idx = 0; idx < colorCounter.length; idx++) {
			min = colorCounter[idx] < min ? colorCounter[idx] : min;
		}
		colorCounterMin = min;
	}

	/**
	 * Resets the counters
	 */
	public static void resetDevice() {
		colorCounterMin = 0;
		for (int colorIdx = 0; colorIdx < colorCounter.length; colorIdx++) {
			colorCounter[colorIdx] = 0;
		}
	}
}
