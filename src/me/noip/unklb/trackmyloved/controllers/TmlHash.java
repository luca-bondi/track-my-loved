package me.noip.unklb.trackmyloved.controllers;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.util.Log;

/**
 * Class used to generate the SHA1 of a string.
 */
public class TmlHash {

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- CONSTANTS --------------------------------
	 * ------------------------------------------------------------------------
	 */

	private final static String TAG = "TmlHash";

	/*
	 * ------------------------------------------------------------------------
	 * ------------------------- STATIC METHODS -------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * SHA1 of a string
	 * 
	 * @param text
	 *            input string
	 * @return SHA1(text), null if errors
	 */
	public static String SHA1(String text) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-1");
			md.update(text.getBytes("iso-8859-1"), 0, text.length());
			byte[] sha1hash = md.digest();
			return convertToHex(sha1hash);
		} catch (NoSuchAlgorithmException e) {
			Log.e(TAG, "NoSuchAlgorithmException");
		} catch (UnsupportedEncodingException e) {
			Log.e(TAG, "UnsupportedEncodingException");
		}
		return null;
	}

	/**
	 * Converts a byte array to a hex string array
	 * 
	 * @param data
	 * @return
	 */
	private static String convertToHex(byte[] data) {
		StringBuilder buf = new StringBuilder();
		for (byte b : data) {
			int halfbyte = (b >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte)
						: (char) ('a' + (halfbyte - 10)));
				halfbyte = b & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

}
