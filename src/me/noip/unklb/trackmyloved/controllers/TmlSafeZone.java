package me.noip.unklb.trackmyloved.controllers;

import java.util.ArrayList;

import me.noip.unklb.trackmyloved.models.TmlSafeZoneModel;
import me.noip.unklb.trackmyloved.views.TmlSafeZoneView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

/**
 * SafeZone controller
 * 
 */
public final class TmlSafeZone extends TmlSafeZoneModel {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	// private static final String TAG = "TmlSafeZone";

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- ATTRIBUTES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * The child that instantiated this safeZone
	 */
	private final TmlChild child;

	/**
	 * Instance of the view associated with this safeZone
	 */
	private TmlSafeZoneView view;

	/**
	 * Set when drawing the safeZone on the map
	 */
	private boolean drawing;

	/**
	 * Constructor used when drawing a new safe zone
	 * 
	 * @param serial
	 */
	public TmlSafeZone(TmlChild child, int serial) {
		super(serial);
		this.child = child;
	}

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- METHODS -------------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Constructor used when loading safeZone from the database or when a new
	 * safeZone is added through synchronization
	 * 
	 * @param serial
	 * @param pointList
	 */
	public TmlSafeZone(TmlChild child, int serial, ArrayList<LatLng> pointList,
			boolean created, boolean removed) {
		super(serial, pointList, created, removed);
		this.child = child;
	}

	/**
	 * The safe zones for the given child. Not automatically added to
	 * safeZoneList
	 * 
	 * @param child
	 * @return
	 */
	public static ArrayList<TmlSafeZone> getSafeZones(TmlChild child) {
		return TmlChild.getDb().getSafeZones(child);
	}

	/**
	 * The child this safeZone belongs to
	 * 
	 * @return
	 */
	public TmlChild getChild() {
		return child;
	}

	/**
	 * The device serial number of this safeZone
	 * 
	 * @return
	 */
	public int getSerial() {
		return serial;
	}

	/**
	 * The list of points describing this safeZone
	 * 
	 * @return
	 */
	public ArrayList<LatLng> getPointList() {
		return this.pointList;
	}

	/**
	 * Create the safeZoneView associated with this safeZone
	 * 
	 * @param map
	 */
	public void createView(GoogleMap map) {
		if (!toBeRemoved) {
			this.view = new TmlSafeZoneView(this, map);
		}
	}

	/**
	 * The SafeZoneView associated to this safeZone
	 * 
	 * @return
	 */
	public TmlSafeZoneView getView() {
		return view;
	}

	/**
	 * Determine if the safeZone contains the point. Known not to work for
	 * polygons crossing the Greenwich antimeridian
	 * 
	 * @see <a
	 *      href="http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html">PNPOLY</a>
	 * 
	 * @param point
	 * @return
	 */
	public boolean contains(LatLng point) {

		int numPoints = pointList.size();
		int i, j;
		boolean contained = false;

		for (i = 0, j = numPoints - 1; i < numPoints; j = i++) {
			if (((pointList.get(i).longitude > point.longitude) != (pointList
					.get(j).longitude > point.longitude))
					&& (point.latitude < (pointList.get(j).latitude - pointList
							.get(i).latitude)
							* (point.longitude - pointList.get(i).longitude)
							/ (pointList.get(j).longitude - pointList.get(i).longitude)
							+ pointList.get(i).latitude))
				contained = !contained;
		}
		return contained;
	}

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- DRAWING METHODS -----------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Get a new safeZone for the child. The serial number is the maximum serial
	 * number of a safeZone for this child + 1.
	 * 
	 * Used to get a new candidate safeZone when drawing one
	 * 
	 * @param child
	 * @return
	 */
	public static TmlSafeZone getNewSafeZone(TmlChild child) {
		/*
		 * Determine the max serial number used for the safe zones of this child
		 */
		int maxSerial = -1;

		for (TmlSafeZone safeZoneTemp : child.getSafeZoneList()) {
			maxSerial = safeZoneTemp.getSerial() > maxSerial ? safeZoneTemp
					.getSerial() : safeZoneTemp.getSerial();
		}

		TmlSafeZone newSafeZone = new TmlSafeZone(child, maxSerial + 1);
		newSafeZone.setBuilding();
		return newSafeZone;
	}

	/**
	 * This is a candidate safeZone being draw.
	 */
	private void setBuilding() {
		drawing = true;
	}

	/**
	 * Add a point to the point list
	 * 
	 * @param point
	 */
	public void addPoint(LatLng point) {
		pointList.add(point);
		if (drawing && view != null) {
			view.addPoint(point);
		}
	}

	/**
	 * Remove last point of the safeZone
	 * 
	 * @return true if removed, false if no more points
	 */
	public boolean removeLastPoint() {
		if (pointList.size() == 0) {
			return false;
		}
		pointList.remove(pointList.size() - 1);
		if (drawing) {
			view.removeLastPoint();
		}
		return true;
	}

	/**
	 * Create this safeZone on the device and set the toBeCreated flag to let
	 * the safeZone be created on the server
	 */
	public void createFromUI() {
		drawing = false;
		toBeCreated = true;
		view.drawingDone();
		TmlChild.getDb().setSafeZoneToBeCreated(this);
		TmlChild.getDb().insertSafeZone(this);
		child.getSafeZoneList().add(this);
	}

	/**
	 * Stop the drawing of a new safeZone. Remember to invalidate the pointer.
	 */
	public void cancel() {
		if (view != null) {
			view.remove();
			view = null;
		}
	}

	/**
	 * This safeZone has to be removed locally and on the server. Don't remove
	 * the safeZone from the safeZoneList on the child. Do it with delete() only
	 * when the server has been notified.
	 */
	public void remove() {
		if (view != null) {
			view.remove();
			view = null;
		}
		if (toBeCreated) {
			resetToBeCreated();
		}
		toBeRemoved = true;
		TmlChild.getDb().setSafeZoneToBeRemoved(this);
	}

	/**
	 * Delete this safeZone from the database.
	 */
	public void delete() {

		deleted = true;

		TmlChild.getDb().deleteSafeZone(this);

		if (view != null) {
			/* Notify the UI to update the Map Elements */
			child.triggerUpdate();
		} else {
			child.getSafeZoneList().remove(this);
		}

	}

	/*
	 * -------------------------------------------------------------------------
	 * ------------------------- SERVER SYNC METHODS ---------------------------
	 * -------------------------------------------------------------------------
	 */

	/**
	 * Set that this safeZone has been created on this device and the server
	 * knows about its existence.
	 */
	public void resetToBeCreated() {
		TmlChild.getDb().resetSafeZoneToBeCreated(this);
		toBeCreated = false;
	}

	/**
	 * Called when a new safeZone received from remote has been completely
	 * defined
	 */
	public void insertFromRemote() {

		drawing = false;
		TmlChild.getDb().insertSafeZone(this);
		child.getSafeZoneList().add(this);
		child.triggerUpdate();
	}
}
