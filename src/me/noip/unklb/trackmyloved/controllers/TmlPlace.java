package me.noip.unklb.trackmyloved.controllers;

import java.util.ArrayList;

/**
 * Place controller
 */
public class TmlPlace {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Ratio between the distance between a place and a position and the sum of
	 * the accuracies of the two. The bigger the easier is for a position to
	 * enter in a place
	 */
	private static final float NEAR_ACCURACY_FACTOR = 2.5f;

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- ATTRIBUTES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * The position of this place
	 */
	private TmlPosition position;

	/**
	 * List of positions inside this place
	 */
	private ArrayList<TmlPosition> positionList = new ArrayList<TmlPosition>();

	/**
	 * The marker associated to this place
	 */
	private TmlMarker marker;

	/**
	 * The cluster marker containing this place.
	 */
	private TmlMarker clusterMarker;

	/**
	 * Set if this place is highlighted
	 */
	private boolean highlighted;

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- METHODS -------------------------------------
	 * ------------------------------------------------------------------------
	 */
	/**
	 * Constructor used to create a new place
	 * 
	 * @param position
	 */
	public TmlPlace(TmlPosition position) {
		this.position = new TmlPosition(position);
		this.position.setPlace(this);
		this.positionList.add(this.position);
	}

	/**
	 * Get the position of this place
	 * 
	 * @return
	 */
	public TmlPosition getPosition() {
		return position;
	}

	/**
	 * Get the position list
	 * 
	 * @return
	 */
	public ArrayList<TmlPosition> getPositionList() {
		return positionList;
	}

	/**
	 * Get the position list for last historyInterval hours
	 * 
	 * @return
	 */
	public ArrayList<TmlPosition> getPositionList(long lastTimestamp,
			int historyInterval) {

		long oldestTimestamp = lastTimestamp - historyInterval * 3600 * 1000;

		ArrayList<TmlPosition> historyPositionList = new ArrayList<TmlPosition>();

		int idx = positionList.size() - 1;
		while (idx >= 0
				&& positionList.get(idx).getTimestamp() > oldestTimestamp) {
			historyPositionList.add(0, positionList.get(idx));
			idx--;
		}
		return historyPositionList;
	}

	/**
	 * Add a new position to this place
	 * 
	 * @param newPosition
	 */
	public void add(TmlPosition newPosition) {
		TmlPosition newPositionCopy = new TmlPosition(newPosition);
		newPositionCopy.setPlace(this);
		this.positionList.add(newPositionCopy);
		this.position = TmlPosition.mergePositions(positionList);
	}

	/**
	 * Remove a position from this place
	 * 
	 * @param removedPosition
	 */
	public void remove(TmlPosition removedPosition) {
		positionList.remove(removedPosition);
		if (positionList.size() > 0) {
			this.position = TmlPosition.mergePositions(positionList);
		}
	}

	/**
	 * Check if a candidate position could belong to this place
	 * 
	 * @param candidatePosition
	 * @return
	 */
	public boolean nearTo(TmlPosition candidatePosition) {
		float distance = candidatePosition.distanceTo(position);
		float accSum = position.getAccuracy() + candidatePosition.getAccuracy();
		return distance < NEAR_ACCURACY_FACTOR * accSum;
	}

	/**
	 * Set the marker representing this place
	 * 
	 * @param marker
	 */
	public void setMarker(TmlMarker marker) {
		this.marker = marker;
	}

	/**
	 * Retrieve the marker representing this place
	 * 
	 * @return
	 */
	public TmlMarker getMarker() {
		return marker;
	}

	/**
	 * The cluster marker visible on the map containing this place
	 * 
	 * @return
	 */
	public TmlMarker getClusterMarker() {
		return clusterMarker;
	}

	/**
	 * Set the cluster marker visible on the map that contains this place
	 * 
	 * @param clusterMarker
	 */
	public void setClusterMarker(TmlMarker clusterMarker) {
		this.clusterMarker = clusterMarker;
	}

	/**
	 * Set the highlighted state for this place
	 * 
	 * @param highlight
	 */
	public void setHighlighted(boolean highlight) {
		this.highlighted = highlight;
	}

	/**
	 * Get the highlighted state for this place
	 * 
	 * @return
	 */
	public boolean getHighlighted() {
		return this.highlighted;
	}

}
