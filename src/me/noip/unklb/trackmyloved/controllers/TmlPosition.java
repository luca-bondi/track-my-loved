package me.noip.unklb.trackmyloved.controllers;

import java.util.ArrayList;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

/**
 * Position controller
 */
public class TmlPosition {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	// private static final String TAG = "TmlPosition";

	/**
	 * Earth radius [m]
	 */
	private static final double EARTHRADIUS = 6731000;

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- ATTRIBUTES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Coordinates of this position. If multiple positions are merged this is
	 * the mean of the coordinates weighted on the inverse of the accuracy
	 */
	private final LatLng coordinates;
	/**
	 * First timestamp for this position
	 */
	private final Long firstTimestamp;
	/**
	 * Last timestamp for this position. Set when multiple positions are merged.
	 * If null this position is referred to a single timestamp.
	 */
	private final Long lastTimestamp;
	/**
	 * Battery level at lastTimestamp if defined, otherwise at firstTimestamp.
	 * Between 0 and 1.
	 */
	private final Float battery;
	/**
	 * Accuracy of this position in meters. If multiple positions are merged
	 * this is the harmonic mean of the accuracies
	 */
	private final Float accuracy;
	/**
	 * Activity detected when this position was recorded. As from
	 * ActivityRecognitionClient. If multiple positions are merged this is the
	 * activity of the oldest position.
	 */
	private final String activity;

	/**
	 * Place this position is associated to
	 */
	private TmlPlace place;

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- METHODS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Constructor
	 * 
	 * @param coordinates
	 * @param firstTimestamp
	 * @param accuracy
	 * @param battery
	 * @param activity
	 */
	public TmlPosition(LatLng coordinates, Long timestamp, Float accuracy,
			String activity, Float battery) {
		this.coordinates = coordinates;
		this.firstTimestamp = timestamp;
		this.lastTimestamp = timestamp;
		this.accuracy = accuracy;
		this.activity = activity;
		this.battery = battery;
	}

	/**
	 * Constructor
	 * 
	 * @param coordinates
	 * @param firstTimestamp
	 * @param lastTimestamp
	 * @param accuracy
	 * @param battery
	 * @param activity
	 */
	public TmlPosition(LatLng coordinates, Long firstTimestamp,
			Long lastTimestamp, Float accuracy, String activity, Float battery) {
		this.coordinates = coordinates;
		this.firstTimestamp = firstTimestamp;
		this.lastTimestamp = lastTimestamp;
		this.accuracy = accuracy;
		this.activity = activity;
		this.battery = battery;
	}

	/**
	 * Constructor
	 * 
	 * @param location
	 * @param activity
	 * @param battery
	 */
	public TmlPosition(Location location, String activity, Float battery) {
		this.coordinates = new LatLng(location.getLatitude(),
				location.getLongitude());
		this.firstTimestamp = location.getTime();
		this.lastTimestamp = this.firstTimestamp;
		this.accuracy = location.getAccuracy();
		this.activity = activity;
		this.battery = battery;
	}

	/**
	 * Copy of the given position
	 * 
	 * @param position
	 */
	public TmlPosition(TmlPosition position) {
		this.coordinates = position.coordinates;
		this.firstTimestamp = position.firstTimestamp;
		this.lastTimestamp = position.lastTimestamp;
		this.accuracy = position.getAccuracy();
		this.activity = position.activity;
		this.battery = position.battery;
	}

	/**
	 * Get the coordinates
	 * 
	 * @return
	 */
	public LatLng getCoordinates() {
		return coordinates;
	}

	/**
	 * Get lastTimestamp.
	 * 
	 * @return
	 */
	public long getTimestamp() {
		return lastTimestamp;
	}

	/**
	 * Get lastTimestamp
	 * 
	 * @return
	 */
	public long getLastTimestamp() {
		return lastTimestamp;
	}

	/**
	 * Get firstTimestamp
	 * 
	 * @return
	 */
	public long getFirstTimestamp() {
		return firstTimestamp;
	}

	/**
	 * Get battery level
	 * 
	 * @return
	 */
	public Float getBattery() {
		return battery;
	}

	/**
	 * Get accuracy distance
	 * 
	 * @return
	 */
	public Float getAccuracy() {
		return accuracy;
	}

	/**
	 * Get the position activity
	 * 
	 * @return
	 */
	public String getActivity() {
		return activity;
	}

	/**
	 * Get the place this positions belongs to
	 * 
	 * @return
	 */
	public TmlPlace getPlace() {
		return place;
	}

	/**
	 * Set the place this position belongs to
	 * 
	 * @param place
	 */
	public void setPlace(TmlPlace place) {
		this.place = place;
	}

	/**
	 * Distance in meters between this position and another one
	 * 
	 * @param otherPosition
	 * @return
	 */
	public float distanceTo(TmlPosition otherPosition) {
		Location thisLocation = new Location("tml");
		thisLocation.setLatitude(getCoordinates().latitude);
		thisLocation.setLongitude(getCoordinates().longitude);

		Location otherLocation = new Location("tml");
		otherLocation.setLatitude(otherPosition.getCoordinates().latitude);
		otherLocation.setLongitude(otherPosition.getCoordinates().longitude);

		return thisLocation.distanceTo(otherLocation);
	}

	/**
	 * Check if this TmlPosition is equal to the given one
	 */
	@Override
	public boolean equals(Object other) {
		if (other == null)
			return false;
		if (other == this)
			return true;

		if (!(other instanceof TmlPosition))
			return false;

		TmlPosition otherMyClass;
		try {
			otherMyClass = (TmlPosition) other;
		} catch (ClassCastException e) {
			return false;
		}

		if (accuracy == null) {
			if (otherMyClass.accuracy != null)
				return false;
		} else if (otherMyClass.accuracy == null
				|| !otherMyClass.accuracy.equals(accuracy))
			return false;

		if (activity == null) {
			if (otherMyClass.activity != null)
				return false;
		} else if (otherMyClass.activity == null
				|| !otherMyClass.activity.equals(activity))
			return false;

		if (battery == null) {
			if (otherMyClass.battery != null)
				return false;
		} else if (otherMyClass.battery == null
				|| !otherMyClass.battery.equals(battery))
			return false;

		if (coordinates == null) {
			if (otherMyClass.coordinates != null)
				return false;
		} else if (otherMyClass.coordinates == null
				|| !otherMyClass.coordinates.equals(coordinates))
			return false;

		if (firstTimestamp != otherMyClass.firstTimestamp) {
			return false;
		}

		if (lastTimestamp != otherMyClass.lastTimestamp) {
			return false;
		}

		return true;
	}

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- STATIC METHODS ------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Merge an array of TmlPosition in a new single TmlPosition. positionList
	 * must be timestamp ascending ordered.
	 * 
	 * Lat and Lng are the weighted sum over the inverse of the accuracy of Lat
	 * and Lng for each position
	 * 
	 * accuracy is the harmonic mean of the accuracies
	 * 
	 * battery is the battery level of the last position
	 * 
	 * activity is the activity of the first position
	 * 
	 * @param positionList
	 * @return
	 */
	public static TmlPosition mergePositions(ArrayList<TmlPosition> positionList) {

		if (positionList.size() == 1) {
			return positionList.get(0);
		}

		double newLat = 0;
		double newLng = 0;
		float invAccSum = 0;

		for (TmlPosition position : positionList) {
			newLat += position.getCoordinates().latitude
					/ position.getAccuracy();
			newLng += position.getCoordinates().longitude
					/ position.getAccuracy();
			invAccSum += 1f / position.getAccuracy();
		}
		newLat /= invAccSum;
		newLng /= invAccSum;

		float newAcc = positionList.size() / invAccSum;

		return new TmlPosition(new LatLng(newLat, newLng), positionList.get(0)
				.getFirstTimestamp(), positionList.get(positionList.size() - 1)
				.getLastTimestamp(), newAcc, positionList.get(0).getActivity(),
				positionList.get(positionList.size() - 1).getBattery());
	}

	/**
	 * Create a new LatLng which lies toNorth meters north and toEast meters
	 * east of centerLatLng
	 */
	public static LatLng moveLatLng(LatLng centerLatLng, double toNorth,
			double toEast) {
		double lonDiff = meterToLongitude(toEast, centerLatLng.latitude);
		double latDiff = meterToLatitude(toNorth);
		return new LatLng(centerLatLng.latitude + latDiff,
				centerLatLng.longitude + lonDiff);
	}

	/**
	 * Modify given LatLngBounds such that the diagonal is at least
	 * 2xMIN_DIAG_DISTANCE
	 * 
	 * @param in
	 * @return
	 */
	public static LatLngBounds latLngMinDistance(LatLngBounds in,
			double diagonal) {

		LatLng center = in.getCenter();

		LatLng norhtEastMin = moveLatLng(center, diagonal, diagonal);
		LatLng southWestMin = moveLatLng(center, -diagonal, -diagonal);

		return new LatLngBounds.Builder().include(norhtEastMin)
				.include(southWestMin).include(in.northeast)
				.include(in.southwest).build();
	}

	/**
	 * Convert meters to longitude degrees
	 * 
	 * @param meterToEast
	 * @param latitude
	 * @return
	 */
	private static double meterToLongitude(double meterToEast, double latitude) {
		double latArc = Math.toRadians(latitude);
		double radius = Math.cos(latArc) * EARTHRADIUS;
		double rad = meterToEast / radius;
		return Math.toDegrees(rad);
	}

	/**
	 * Convert meters to latitude degrees
	 * 
	 * @param meterToNorth
	 * @return
	 */
	private static double meterToLatitude(double meterToNorth) {
		double rad = meterToNorth / EARTHRADIUS;
		return Math.toDegrees(rad);
	}

}
