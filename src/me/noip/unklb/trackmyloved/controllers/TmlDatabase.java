package me.noip.unklb.trackmyloved.controllers;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

/**
 * Database controller.
 */
public class TmlDatabase extends SQLiteOpenHelper {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	private static final String TAG = "TmlDatabase";

	/*
	 * Database data
	 */
	private static final String DB_NAME = "TrackMyLoved";
	private static final int DB_VER = 1;

	/*
	 * Tables names
	 */
	private static final String TABLE_POSITIONS = "positions";
	private static final String TABLE_POINTS = "points";
	private static final String TABLE_SAFE_ZONES = "safeZones";
	private static final String TABLE_USERS = "users";
	private static final String TABLE_LOCAL_SETTINGS = "localSettings";

	/*
	 * Columns names
	 */
	// Columns: Table positions
	private static final String TABLE_POSITIONS_COLUMN_NAME_USER = "nameUser";
	private static final String TABLE_POSITIONS_COLUMN_TIMESTAMP = "timestamp";
	private static final String TABLE_POSITIONS_COLUMN_LAT = "lat";
	private static final String TABLE_POSITIONS_COLUMN_LNG = "lng";
	private static final String TABLE_POSITIONS_COLUMN_ACCURACY = "accuracy";
	private static final String TABLE_POSITIONS_COLUMN_BATTERY = "battery";
	private static final String TABLE_POSITIONS_COLUMN_ACTIVITY = "activity";

	// Columns: Table points
	private static final String TABLE_POINTS_COLUMN_NAME_USER = "nameUser";
	private static final String TABLE_POINTS_COLUMN_SERIAL_SAFE_ZONE = "serialSafeZone";
	private static final String TABLE_POINTS_COLUMN_SERIAL = "serial";
	private static final String TABLE_POINTS_COLUMN_LAT = "lat";
	private static final String TABLE_POINTS_COLUMN_LNG = "lng";

	// Columns: Table safeZones
	private static final String TABLE_SAFE_ZONES_COLUMN_NAME_USER = "nameUser";
	private static final String TABLE_SAFE_ZONES_COLUMN_SERIAL = "serial";
	private static final String TABLE_SAFE_ZONES_COLUMN_CREATED = "created";
	private static final String TABLE_SAFE_ZONES_COLUMN_REMOVED = "removed";

	// Columns: Table users
	private static final String TABLE_USERS_COLUMN_NAME = "name";

	// Columns: Table localSettings
	private static final String TABLE_LOCAL_SETTINGS_COLUMN_KEY = "key";
	private static final String TABLE_LOCAL_SETTINGS_COLUMN_VALUE = "value";

	/*
	 * Default values for various parameters
	 */
	private static final Integer DEFAULT_HISTORY_INTERVAL = 72; // 3gg
	private static final Integer MAX_HISTORY_INTERVAL = 168; // 1 week

	/*
	 * Create table statements
	 */
	// Create table positions - Child
	private static final String CREATE_CHILD_TABLE_POSITIONS = ""
			+ "CREATE TABLE IF NOT EXISTS " + TABLE_POSITIONS + "("
			+ TABLE_POSITIONS_COLUMN_TIMESTAMP
			+ " INTEGER PRIMARY KEY DEFAULT CURRENT_TIMESTAMP,"
			+ TABLE_POSITIONS_COLUMN_LAT + " REAL NOT NULL,"
			+ TABLE_POSITIONS_COLUMN_LNG + " REAL NOT NULL,"
			+ TABLE_POSITIONS_COLUMN_ACCURACY + " REAL NOT NULL,"
			+ TABLE_POSITIONS_COLUMN_BATTERY + " REAL NOT NULL,"
			+ TABLE_POSITIONS_COLUMN_ACTIVITY + " TEXT" + ")";

	// Create table positions - Parent
	private static final String CREATE_PARENT_TABLE_POSITIONS = ""
			+ "CREATE TABLE IF NOT EXISTS "
			+ TABLE_POSITIONS
			+ "("
			+ TABLE_POSITIONS_COLUMN_NAME_USER
			+ " TEXT NOT NULL,"
			+ TABLE_POSITIONS_COLUMN_TIMESTAMP
			+ " INTEGER NOT NULL,"
			+ TABLE_POSITIONS_COLUMN_LAT
			+ " REAL NOT NULL,"
			+ TABLE_POSITIONS_COLUMN_LNG
			+ " REAL NOT NULL,"
			+ TABLE_POSITIONS_COLUMN_ACCURACY
			+ " REAL NOT NULL,"
			+ TABLE_POSITIONS_COLUMN_BATTERY
			+ " REAL NOT NULL,"
			+ TABLE_POSITIONS_COLUMN_ACTIVITY
			+ " TEXT,"
			+ "PRIMARY KEY ("
			+ TABLE_POSITIONS_COLUMN_NAME_USER
			+ ","
			+ TABLE_POSITIONS_COLUMN_TIMESTAMP
			+ "),"
			+ "FOREIGN KEY ("
			+ TABLE_POSITIONS_COLUMN_NAME_USER
			+ ") REFERENCES "
			+ TABLE_USERS
			+ "(" + TABLE_USERS_COLUMN_NAME + ")" + ")";

	// Create table users - Parent
	private static final String CREATE_PARENT_TABLE_USERS = ""
			+ "CREATE TABLE IF NOT EXISTS " + TABLE_USERS + "("
			+ TABLE_USERS_COLUMN_NAME + " TEXT PRIMARY KEY" + ")";

	// Create table localSettings
	private static final String CREATE_TABLE_LOCAL_SETTINGS = ""
			+ "CREATE TABLE IF NOT EXISTS " + TABLE_LOCAL_SETTINGS + "("
			+ TABLE_LOCAL_SETTINGS_COLUMN_KEY + " TEXT PRIMARY KEY,"
			+ TABLE_LOCAL_SETTINGS_COLUMN_VALUE + " TEXT DEFAULT NULL" + ")";

	// Create table safeZones - Parent
	private static final String CREATE_PARENT_TABLE_SAFE_ZONES = ""
			+ "CREATE TABLE IF NOT EXISTS "
			+ TABLE_SAFE_ZONES
			+ "("
			+ TABLE_SAFE_ZONES_COLUMN_NAME_USER
			+ " TEXT NOT NULL,"
			+ TABLE_SAFE_ZONES_COLUMN_SERIAL
			+ " INTEGER NOT NULL,"
			+ TABLE_SAFE_ZONES_COLUMN_CREATED
			+ " INTEGER NOT NULL DEFAULT 0,"
			+ TABLE_SAFE_ZONES_COLUMN_REMOVED
			+ " INTEGER NOT NULL DEFAULT 0,"
			+ "PRIMARY KEY ("
			+ TABLE_SAFE_ZONES_COLUMN_NAME_USER
			+ ","
			+ TABLE_SAFE_ZONES_COLUMN_SERIAL
			+ "),"
			+ "FOREIGN KEY ("
			+ TABLE_SAFE_ZONES_COLUMN_NAME_USER
			+ ") REFERENCES "
			+ TABLE_USERS
			+ "(" + TABLE_USERS_COLUMN_NAME + ")" + ")";

	// Create table points - Parent
	private static final String CREATE_PARENT_TABLE_POINTS = ""
			+ "CREATE TABLE IF NOT EXISTS "
			+ TABLE_POINTS
			+ "("
			+ TABLE_POINTS_COLUMN_NAME_USER
			+ " TEXT NOT NULL,"
			+ TABLE_POINTS_COLUMN_SERIAL_SAFE_ZONE
			+ " INTEGER NOT NULL,"
			+ TABLE_POINTS_COLUMN_SERIAL
			+ " INTEGER NOT NULL,"
			+ TABLE_POINTS_COLUMN_LAT
			+ " REAL NOT NULL,"
			+ TABLE_POINTS_COLUMN_LNG
			+ " REAL NOT NULL,"
			+ "PRIMARY KEY ("
			+ TABLE_POINTS_COLUMN_NAME_USER
			+ ","
			+ TABLE_POINTS_COLUMN_SERIAL_SAFE_ZONE
			+ ","
			+ TABLE_POINTS_COLUMN_SERIAL
			+ "),"
			+ "FOREIGN KEY ("
			+ TABLE_POINTS_COLUMN_NAME_USER
			+ ") REFERENCES "
			+ TABLE_USERS
			+ "("
			+ TABLE_USERS_COLUMN_NAME
			+ "),"
			+ "FOREIGN KEY ("
			+ TABLE_POINTS_COLUMN_SERIAL_SAFE_ZONE
			+ ") REFERENCES "
			+ TABLE_SAFE_ZONES
			+ "("
			+ TABLE_SAFE_ZONES_COLUMN_SERIAL
			+ ")"
			+ ")";

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- ATTRIBUTES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Database connection instance
	 */
	private static SQLiteDatabase db;

	/**
	 * Role set for this device. <"p"|"s"|0>
	 */
	private char role;

	/**
	 * History interval [hours] set for this device.
	 */
	private Integer historyInterval;

	/**
	 * User of this device. Valid only for child device.
	 */
	private String user;

	/**
	 * Mail of the account
	 */
	private String mail;

	/**
	 * Password SHA1 of the account
	 */
	private String pwdSha1;

	/**
	 * Initial server synchronization required. Set during setup, reset when
	 * initial synchronization finished
	 */
	private Boolean syncRequired;

	/**
	 * Instance of this singleton class
	 */
	private static TmlDatabase instance;

	/**
	 * List of children registered in the database. Valid only for parent
	 * device.
	 */
	private ArrayList<String> childrenList = new ArrayList<String>();

	/*
	 * ------------------------------------------------------------------------
	 * ------------------ DATABASE AND TABLES METHODS -------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Default constructor. Old position cleaning is performed.
	 * 
	 * @param context
	 */
	private TmlDatabase(Context context) {
		super(context, DB_NAME, null, DB_VER);
		cleanOldPositions();
	}

	/**
	 * Get an instance of TmlDatabase
	 * 
	 * @param context
	 * @return
	 */
	public synchronized static TmlDatabase getInstance(Context context) {
		if (instance == null) {
			Log.i(TAG, "Creating instance");
			instance = new TmlDatabase(context.getApplicationContext());
		}
		return instance;
	}

	/**
	 * Drop all database tables except for TABLE_LOCAL_SETTINGS which is
	 * flushed. Tables need to be recreated with createTables().
	 */
	public synchronized void clearDatabase() {
		ArrayList<String> queries = new ArrayList<String>();
		queries.add("DROP TABLE IF EXISTS " + TABLE_POINTS);
		queries.add("DROP TABLE IF EXISTS " + TABLE_SAFE_ZONES);
		queries.add("DROP TABLE IF EXISTS " + TABLE_POSITIONS);
		queries.add("DROP TABLE IF EXISTS " + TABLE_USERS);
		queries.add("DELETE FROM " + TABLE_LOCAL_SETTINGS);
		dbExecSQL(queries);

		user = null;
		mail = null;
		pwdSha1 = null;

		syncRequired = null;

		role = 0;
		historyInterval = null;

		childrenList = new ArrayList<String>();

		db = null;

		Log.i(TAG, "Database cleaned");
	}

	/**
	 * Called when no database exists for the application. The database is first
	 * automatically created and then this method is called. Creates the table
	 * localSettings to allow the storage of the role. Other tables are created
	 * calling method createTables().
	 * 
	 * @param db
	 */
	@Override
	public synchronized void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_LOCAL_SETTINGS);
		Log.i("onCreate", "Database created");
	}

	/**
	 * Called when database upgrade is necessary
	 * 
	 * @param db
	 * @param oldVersion
	 * @param newVersion
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

	/**
	 * Close the database connection opened with getWritableDatabase() or
	 * getReadableDatabase()
	 */
	@Override
	public synchronized void close() {
		if (db != null) {
			if (db.isOpen()) {
				db.close();
			}
		}
		super.close();
	}

	/**
	 * Executes queries that don't produce results. Used to create / flush /
	 * delete tables
	 * 
	 * @param queries
	 */
	private synchronized void dbExecSQL(ArrayList<String> queries) {
		db = getWritableDatabase();
		try {
			for (String query : queries) {
				db.execSQL(query);
			}
		} catch (SQLiteException e) {
			Log.e(TAG, "dbExecSQL");
		} finally {
			db.close();
		}
	}

	/**
	 * Insert command withOnConflict
	 * 
	 * @param table
	 * @param nullColumnHack
	 * @param values
	 */
	private synchronized Long dbInsert(String table, String nullColumnHack,
			ContentValues values, int conflictAlgorithm) {
		db = getWritableDatabase();
		try {
			return db.insertWithOnConflict(table, nullColumnHack, values,
					conflictAlgorithm);
		} catch (SQLiteException e) {
			Log.e(TAG, "dbInsert");
			return null;
		} finally {
			db.close();
		}

	}

	/**
	 * Update command
	 * 
	 * @param table
	 * @param nullColumnHack
	 * @param values
	 * @param conflictAlgorithm
	 */
	private synchronized void dbUpdate(String table, ContentValues values,
			String whereClause, String[] whereArgs, int conflictAlgorithm) {
		db = getWritableDatabase();
		try {
			db.updateWithOnConflict(table, values, whereClause, whereArgs,
					conflictAlgorithm);
		} catch (SQLiteException e) {
			Log.e(TAG, "dbUpdate");
		} finally {
			db.close();
		}

	}

	/**
	 * Delete tuple in table.
	 * 
	 * @param table
	 * @param whereClause
	 * @param whereArgs
	 * @return
	 */
	private synchronized int dbDelete(String table, String whereClause,
			String[] whereArgs) {
		if (whereClause == null) {
			whereClause = "1";
		}
		db = getWritableDatabase();
		int affectedRows = db.delete(table, whereClause, whereArgs);
		db.close();
		return affectedRows;
	}

	/*
	 * Table management methods ----------
	 */

	/**
	 * Creates the tables (if not existing) for the child phone.
	 * 
	 */
	private void createTablesChild() {
		ArrayList<String> queries = new ArrayList<String>();
		queries.add(CREATE_CHILD_TABLE_POSITIONS);
		dbExecSQL(queries);
	}

	/**
	 * Creates the tables (if not existing) for the parent phone.
	 * 
	 */
	private void createTablesParent() {
		ArrayList<String> queries = new ArrayList<String>();
		queries.add(CREATE_PARENT_TABLE_USERS);
		queries.add(CREATE_PARENT_TABLE_POSITIONS);
		queries.add(CREATE_PARENT_TABLE_SAFE_ZONES);
		queries.add(CREATE_PARENT_TABLE_POINTS);
		dbExecSQL(queries);
	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------- SETTINGS METHODS -------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * History interval in hours. Database is accessed only the first time this
	 * function is called.
	 * 
	 * @return
	 */
	public Integer getHistoryInterval() {

		if (historyInterval == null) {
			historyInterval = getLocalSettingInteger("historyInterval");
			if (historyInterval == null) {
				historyInterval = DEFAULT_HISTORY_INTERVAL;
			}
		}
		return historyInterval;
	}

	/**
	 * History interval shown in map activity
	 * 
	 * @param historyInterval
	 */
	public void setHistoryInterval(Integer historyInterval) {
		this.historyInterval = historyInterval;
		if (historyInterval != null) {
			setLocalSetting("historyInterval", historyInterval);
		}
	}

	/*
	 * General purpose methods ----------------
	 */

	/**
	 * Store a local setting
	 * 
	 * @param key
	 * @param value
	 */
	public void setLocalSetting(String key, String value) {
		if (key != null && key.length() > 0) {
			ContentValues values = new ContentValues();
			values.put(TABLE_LOCAL_SETTINGS_COLUMN_KEY, key);
			values.put(TABLE_LOCAL_SETTINGS_COLUMN_VALUE, value);
			dbInsert(TABLE_LOCAL_SETTINGS, TABLE_LOCAL_SETTINGS_COLUMN_VALUE,
					values, SQLiteDatabase.CONFLICT_REPLACE);
		} else {
			Log.e(TAG, "setLocalSetting: key length must be > 0");
		}
	}

	/**
	 * Store a local setting
	 * 
	 * @param key
	 * @param value
	 */
	public void setLocalSetting(String key, int value) {
		if (key != null && key.length() > 0) {
			ContentValues values = new ContentValues();
			values.put(TABLE_LOCAL_SETTINGS_COLUMN_KEY, key);
			values.put(TABLE_LOCAL_SETTINGS_COLUMN_VALUE, value);
			dbInsert(TABLE_LOCAL_SETTINGS, TABLE_LOCAL_SETTINGS_COLUMN_VALUE,
					values, SQLiteDatabase.CONFLICT_REPLACE);
		} else {
			Log.e(TAG, "setLocalSetting: key length must be > 0");
		}
	}

	/**
	 * Store a local setting
	 * 
	 * @param key
	 * @param value
	 */
	public void setLocalSetting(String key, boolean value) {
		if (key != null && key.length() > 0) {
			ContentValues values = new ContentValues();
			values.put(TABLE_LOCAL_SETTINGS_COLUMN_KEY, key);
			values.put(TABLE_LOCAL_SETTINGS_COLUMN_VALUE, value);
			dbInsert(TABLE_LOCAL_SETTINGS, TABLE_LOCAL_SETTINGS_COLUMN_VALUE,
					values, SQLiteDatabase.CONFLICT_REPLACE);
		} else {
			Log.e(TAG, "setLocalSetting: key length must be > 0");
		}
	}

	/**
	 * Get a local setting string
	 * 
	 * @param key
	 * @return
	 */
	public synchronized String getLocalSettingString(String key) {
		String retVal = null;
		if (key != null && key.length() > 0) {
			db = getReadableDatabase();
			try {
				Cursor cursor = db.query(TABLE_LOCAL_SETTINGS,
						new String[] { TABLE_LOCAL_SETTINGS_COLUMN_VALUE },
						TABLE_LOCAL_SETTINGS_COLUMN_KEY + " like ? ",
						new String[] { key }, null, null, null);
				if (cursor != null && !cursor.isClosed()
						&& cursor.moveToFirst()) {
					retVal = cursor.getString(0);
					cursor.close();
				}
			} catch (SQLiteException e) {
				Log.e(TAG, "getLocalSetting");
				return null;
			} finally {
				db.close();
			}
		} else {
			Log.e(TAG, "getLocalSetting: key length must be > 0");
		}
		return retVal;
	}

	/**
	 * Get a local setting integer
	 * 
	 * @param key
	 * @return
	 */
	public synchronized Integer getLocalSettingInteger(String key) {
		Integer retVal = null;
		if (key != null && key.length() > 0) {
			db = getReadableDatabase();
			try {
				Cursor cursor = db.query(TABLE_LOCAL_SETTINGS,
						new String[] { TABLE_LOCAL_SETTINGS_COLUMN_VALUE },
						TABLE_LOCAL_SETTINGS_COLUMN_KEY + " like ? ",
						new String[] { key }, null, null, null);
				if (cursor != null && !cursor.isClosed()
						&& cursor.moveToFirst()) {
					retVal = cursor.getInt(0);
					cursor.close();
				}
			} catch (SQLiteException e) {
				Log.e(TAG, "getLocalSetting");
				return null;
			} finally {
				db.close();
			}
		} else {
			Log.e(TAG, "getLocalSetting: key length must be > 0");
		}
		return retVal;
	}

	/**
	 * Get a local setting char
	 * 
	 * @param key
	 * @return 0 if not found
	 */
	public synchronized char getLocalSettingChar(String key) {
		char retVal = 0;
		if (key != null && key.length() > 0) {
			db = getReadableDatabase();
			try {
				Cursor cursor = db.query(TABLE_LOCAL_SETTINGS,
						new String[] { TABLE_LOCAL_SETTINGS_COLUMN_VALUE },
						TABLE_LOCAL_SETTINGS_COLUMN_KEY + " like ? ",
						new String[] { key }, null, null, null);
				if (cursor != null && !cursor.isClosed()
						&& cursor.moveToFirst()) {
					retVal = (char) cursor.getInt(0);
					cursor.close();
				}
			} catch (SQLiteException e) {
				Log.e(TAG, "getLocalSetting");
				return 0;
			} finally {
				db.close();
			}
		} else {
			Log.e(TAG, "getLocalSetting: key length must be > 0");
		}
		return retVal;
	}

	/**
	 * Get a local setting boolean
	 * 
	 * @param key
	 * @return
	 */
	public synchronized Boolean getLocalSettingBoolean(String key) {
		Boolean retVal = null;
		if (key != null && key.length() > 0) {
			db = getReadableDatabase();
			try {
				Cursor cursor = db.query(TABLE_LOCAL_SETTINGS,
						new String[] { TABLE_LOCAL_SETTINGS_COLUMN_VALUE },
						TABLE_LOCAL_SETTINGS_COLUMN_KEY + " like ? ",
						new String[] { key }, null, null, null);
				if (cursor != null && !cursor.isClosed()
						&& cursor.moveToFirst()) {
					retVal = cursor.getInt(0) > 0;
					cursor.close();
				}
			} catch (SQLiteException e) {
				Log.e(TAG, "getLocalSetting");
				return null;
			} finally {
				db.close();
			}
		} else {
			Log.e(TAG, "getLocalSetting: key length must be > 0");
		}
		return retVal;
	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------- INITIAL SYNC METHODS ---------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Initial sync with server necessary returns true, false otherwise.
	 * 
	 * @return
	 */
	public Boolean isInitialSyncRequired() {
		if (syncRequired == null) {
			syncRequired = getLocalSettingBoolean("syncRequired");
			if (syncRequired == null) {
				setLocalSetting("syncRequired", true);
				syncRequired = true;
			}
		}
		return syncRequired;
	}

	/**
	 * Initial sync no more necessary
	 */
	public void syncDone() {
		syncRequired = false;
		setLocalSetting("syncRequired", false);
	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------- ACCOUNT METHODS --------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Account credentials
	 * 
	 * @return null if credentials not defined
	 */
	public TmlAccount getAccount() {
		if (getEmail() != null && getPwdSHA1() != null) {
			return new TmlAccount(getEmail()).setPwdSha1(getPwdSHA1());
		} else {
			return null;
		}
	}

	/**
	 * Get the email of the device account
	 * 
	 * @return
	 */
	public String getEmail() {
		if (mail == null) {
			mail = getLocalSettingString("mail");
		}
		return mail;
	}

	/**
	 * Set the email of this device account
	 * 
	 * @param mail
	 */
	public void setEmail(String mail) {
		this.mail = mail;
		setLocalSetting("mail", mail);
	}

	/**
	 * Get the password SHA1 of the device account
	 * 
	 * @return
	 */
	public String getPwdSHA1() {
		if (pwdSha1 == null) {
			pwdSha1 = getLocalSettingString("pwdSha1");
		}
		return pwdSha1;
	}

	/**
	 * Set the password SHA1 of the device account
	 * 
	 * @param pwdSha1
	 */
	public void setPwdSHA1(String pwdSha1) {
		this.pwdSha1 = pwdSha1;
		setLocalSetting("pwdSha1", pwdSha1);
	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------- ROLE METHODS -----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Set the role of the phone and create the correct tables. clearDatabase()
	 * must be called before this method
	 * 
	 * @param paramRole
	 *            'p'|'s'
	 * @return true if insertion correct or role already set to the provided
	 *         value, false otherwise
	 */
	public boolean setRole(char paramRole) {

		if (!(paramRole == 's' || paramRole == 'p')) {
			throw new IllegalArgumentException("paramRole must be 's' or 'p'");
		}

		role = paramRole;

		setLocalSetting("role", role);

		if (isChildDevice()) {
			createTablesChild();
		} else if (isParentDevice()) {
			createTablesParent();
		} else {
			Log.e(TAG, "setRole");
			return false;
		}

		return true;
	}

	/**
	 * The role of the phone. If not yet loaded from the database it is loaded
	 * here
	 * 
	 * @return 'p'(parent)|'s'(son)|0(role not defined)
	 */
	private char getRole() {
		if (role == 0) {
			try {
				role = getLocalSettingChar("role");
			} catch (NullPointerException e) {
				role = 0;
			}
			if (role != 's' && role != 'p') {
				role = 0;
			}
		}
		return role;

	}

	/**
	 * Check if this is a parent device
	 * 
	 * @return
	 */
	public boolean isParentDevice() {
		return getRole() == 'p';
	}

	/**
	 * Check if this is a child device
	 * 
	 * @return
	 */
	public boolean isChildDevice() {
		return getRole() == 's';
	}

	/**
	 * Check if role is correctly defined in the database
	 * 
	 * @return boolean true if role is correctly defined ('p'|'s') false
	 *         otherwise
	 */
	public boolean isRoleDefined() {
		return getRole() == 'p' || getRole() == 's';
	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------- USER/CHILD METHODS -----------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Retrieve the user registered for this device, null if not set. Valid only
	 * for child device
	 * 
	 * @return the user
	 */
	private String getUser() {

		if (isChildDevice()) {
			if (user == null) {
				user = getLocalSettingString("user");
			}
		} else {
			Log.e(TAG, "getUser callable only from child device");
		}
		return user;
	}

	/**
	 * Sets the user using the device. Valid only for child device
	 * 
	 * @param user
	 */
	public void setUser(String user) {
		if (isChildDevice()) {
			this.user = user;
			setLocalSetting("user", user);
		} else {
			Log.e(TAG, "setUser callable only from child device");
		}
	}

	/**
	 * Insert a child in this the device. Valid only for parent device
	 * 
	 * @param childName
	 */
	public void insertChild(String childName) {
		if (isParentDevice()) {
			ContentValues values = new ContentValues();
			childrenList.add(childName);
			values = new ContentValues();
			values.put(TABLE_USERS_COLUMN_NAME, childName);
			dbInsert(TABLE_USERS, null, values, SQLiteDatabase.CONFLICT_IGNORE);
		} else {
			Log.e(TAG, "insertChild callable only from parent device");
		}

	}

	/**
	 * Delete a child from the database. If parent device the child and all its
	 * data are deleted. If child device the database is cleared
	 * 
	 * @param childName
	 * 
	 */
	public void deleteChild(String childName) {

		if (isParentDevice()) {

			/*
			 * Deleting from users list
			 */
			childrenList.remove(childName);

			/*
			 * Deleting safe zones
			 */
			dbDelete(TABLE_POINTS, TABLE_POINTS_COLUMN_NAME_USER + " like ?",
					new String[] { childName });

			dbDelete(TABLE_SAFE_ZONES, TABLE_SAFE_ZONES_COLUMN_NAME_USER
					+ " like ?", new String[] { childName });

			/*
			 * Deleting positions
			 */
			dbDelete(TABLE_POSITIONS, TABLE_POSITIONS_COLUMN_NAME_USER
					+ " like ?", new String[] { childName });

			/*
			 * Deleting user
			 */
			dbDelete(TABLE_USERS, TABLE_USERS_COLUMN_NAME + " like ?",
					new String[] { childName });

		} else {
			clearDatabase();
		}

	}

	/**
	 * The user names from the database. If called by parent device returns a
	 * single element array containing the name of the user. If called by a
	 * parent device returns an array of strings
	 * 
	 * @return ArrayList of Strings
	 */
	public synchronized ArrayList<String> getChildren() {

		if (childrenList.size() == 0) {

			if (isParentDevice()) {

				db = getReadableDatabase();

				try {
					Cursor cursor = db.query(TABLE_USERS,
							new String[] { TABLE_USERS_COLUMN_NAME }, null,
							null, null, null, null);
					if (cursor != null && !cursor.isClosed()) {
						if (cursor.moveToFirst()) {
							do {
								childrenList.add(cursor.getString(0));
							} while (cursor.moveToNext());
						}
						cursor.close();
					}

				} catch (SQLiteException e) {
					Log.e(TAG, "getUsers");
					return null;
				} finally {
					db.close();
				}
			} else {
				childrenList.add(getUser());
			}

		}

		return childrenList;
	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------- POSITIONS METHODS ------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Child only: insert a new position in the database
	 * 
	 * @param position
	 * @return
	 */
	public long insertNewPosition(TmlPosition position) {
		return insertNewPosition(null, position);
	}

	/**
	 * Parent only: insert a new position list in the database
	 * 
	 * @param positionList
	 * @return
	 */
	public long insertNewPositionList(ArrayList<TmlPosition> positionList) {
		return insertNewPositionList(null, positionList);
	}

	/**
	 * Insert a new position in the database for the specified user
	 * 
	 * @param user
	 * @param position
	 * @return
	 */
	public long insertNewPosition(String user, TmlPosition position) {

		long result = 0;

		ContentValues values = new ContentValues();

		if (user != null) {
			values.put(TABLE_POSITIONS_COLUMN_NAME_USER, user);
		}
		values.put(TABLE_POSITIONS_COLUMN_TIMESTAMP, position.getTimestamp());
		values.put(TABLE_POSITIONS_COLUMN_LAT,
				position.getCoordinates().latitude);
		values.put(TABLE_POSITIONS_COLUMN_LNG,
				position.getCoordinates().longitude);
		values.put(TABLE_POSITIONS_COLUMN_ACCURACY, position.getAccuracy());
		values.put(TABLE_POSITIONS_COLUMN_BATTERY, position.getBattery());
		values.put(TABLE_POSITIONS_COLUMN_ACTIVITY, position.getActivity());

		result = dbInsert(TABLE_POSITIONS, null, values,
				SQLiteDatabase.CONFLICT_IGNORE);

		return result;

	}

	/**
	 * Insert a new position list in the database for the specified user
	 * 
	 * @param user
	 * @param positionList
	 * @return
	 */
	public long insertNewPositionList(String user,
			ArrayList<TmlPosition> positionList) {

		long result = 0;

		for (TmlPosition newPosition : positionList) {
			result += insertNewPosition(user, newPosition);
		}

		return result;
	}

	/**
	 * Retrieve all the positions for the given user
	 * 
	 * @param username
	 */
	public ArrayList<TmlPosition> getPositions(String username) {
		return getLastPositions(username, 0l);
	}

	/**
	 * Retrieves last position from database for the given user
	 * 
	 * @param username
	 * @return
	 */
	private synchronized TmlPosition getLastPosition(String username) {

		if (isChildDevice()) {
			return getLastPosition();
		}
		TmlPosition position = null;

		db = getReadableDatabase();

		try {

			Cursor cursor;

			cursor = db.query(TABLE_POSITIONS, new String[] {
					TABLE_POSITIONS_COLUMN_TIMESTAMP,
					TABLE_POSITIONS_COLUMN_LAT, TABLE_POSITIONS_COLUMN_LNG,
					TABLE_POSITIONS_COLUMN_ACCURACY,
					TABLE_POSITIONS_COLUMN_BATTERY,
					TABLE_POSITIONS_COLUMN_ACTIVITY },
					TABLE_POSITIONS_COLUMN_NAME_USER + " like ? ",
					new String[] { username }, null, null,
					TABLE_POSITIONS_COLUMN_TIMESTAMP + " desc", "1");

			if (cursor != null && !cursor.isClosed()) {
				if (cursor.moveToFirst()) {
					position = new TmlPosition(new LatLng(cursor.getDouble(1),
							cursor.getDouble(2)), Long.valueOf(cursor
							.getLong(0)), Float.valueOf(cursor.getFloat(3)),
							cursor.getString(5), Float.valueOf(cursor
									.getFloat(4)));
				}
				cursor.close();
			}

		} catch (SQLiteException e) {
			// e.printStackTrace();
			Log.e(TAG, "getLastPosition");

			return null;
		} finally {
			db.close();
		}

		return position;
	}

	/**
	 * Child only: last known position
	 * 
	 * @return
	 */
	private synchronized TmlPosition getLastPosition() {

		TmlPosition position = null;

		db = getReadableDatabase();

		try {

			Cursor cursor;

			cursor = db.query(TABLE_POSITIONS, new String[] {
					TABLE_POSITIONS_COLUMN_TIMESTAMP,
					TABLE_POSITIONS_COLUMN_LAT, TABLE_POSITIONS_COLUMN_LNG,
					TABLE_POSITIONS_COLUMN_ACCURACY,
					TABLE_POSITIONS_COLUMN_BATTERY,
					TABLE_POSITIONS_COLUMN_ACTIVITY }, null, null, null, null,
					TABLE_POSITIONS_COLUMN_TIMESTAMP + " desc", "1");

			if (cursor != null && !cursor.isClosed()) {
				if (cursor.moveToFirst()) {
					position = new TmlPosition(new LatLng(cursor.getDouble(1),
							cursor.getDouble(2)), Long.valueOf(cursor
							.getLong(0)), Float.valueOf(cursor.getFloat(3)),
							cursor.getString(5), Float.valueOf(cursor
									.getFloat(4)));
				}
				cursor.close();
			}
		} catch (SQLiteException e) {
			Log.e(TAG, "getLastPosition");

			return null;
		} finally {
			db.close();
		}

		return position;
	}

	/**
	 * Child only. Positions of the user since last known timestamp
	 * 
	 * @param lastKnownTimestamp
	 * @return ArrayList<TmlPosition>, null if errors
	 */
	public ArrayList<TmlPosition> getLastPositions(long lastKnownTimestamp) {
		if (isParentDevice()) {
			Log.e(TAG, "Only child device can call this method");
			return null;
		}
		return getLastPositions(getUser(), lastKnownTimestamp);
	}

	/**
	 * Last historyInterval hours of positions for nameUser
	 * 
	 * @param nameUser
	 * @param historyInterval
	 *            (h)
	 * @return
	 */
	public ArrayList<TmlPosition> getLastPositions(String nameUser,
			Integer historyInterval) {

		return getLastPositions(nameUser, getLastPosition(nameUser)
				.getTimestamp() - historyInterval * 3600000);
	}

	/**
	 * Positions of the given user after the given timestamp
	 * 
	 * @param nameUser
	 * @param timestamp
	 * @return ArrayList<TmlPosition>, null if errors
	 */
	private synchronized ArrayList<TmlPosition> getLastPositions(
			String nameUser, Long timestamp) {
		ArrayList<TmlPosition> positionList = new ArrayList<TmlPosition>();

		db = getReadableDatabase();

		try {

			Cursor cursor;

			if (isParentDevice()) {
				cursor = db
						.query(TABLE_POSITIONS, new String[] {
								TABLE_POSITIONS_COLUMN_TIMESTAMP,
								TABLE_POSITIONS_COLUMN_LAT,
								TABLE_POSITIONS_COLUMN_LNG,
								TABLE_POSITIONS_COLUMN_ACCURACY,
								TABLE_POSITIONS_COLUMN_BATTERY,
								TABLE_POSITIONS_COLUMN_ACTIVITY },
								TABLE_POSITIONS_COLUMN_NAME_USER
										+ " like ? AND "
										+ TABLE_POSITIONS_COLUMN_TIMESTAMP
										+ " >= ?", new String[] { nameUser,
										Long.toString(timestamp + 1) }, null,
								null, TABLE_POSITIONS_COLUMN_TIMESTAMP);
			} else {
				cursor = db.query(TABLE_POSITIONS, new String[] {
						TABLE_POSITIONS_COLUMN_TIMESTAMP,
						TABLE_POSITIONS_COLUMN_LAT, TABLE_POSITIONS_COLUMN_LNG,
						TABLE_POSITIONS_COLUMN_ACCURACY,
						TABLE_POSITIONS_COLUMN_BATTERY,
						TABLE_POSITIONS_COLUMN_ACTIVITY },
						TABLE_POSITIONS_COLUMN_TIMESTAMP + " >= ?",
						new String[] { Long.toString(timestamp + 1) }, null,
						null, TABLE_POSITIONS_COLUMN_TIMESTAMP);
			}
			if (cursor != null && !cursor.isClosed()) {
				if (cursor.moveToFirst()) {
					do {
						TmlPosition position = new TmlPosition(new LatLng(
								cursor.getDouble(1), cursor.getDouble(2)), // coordinates
								Long.valueOf(cursor.getLong(0)), // timestamp
								Float.valueOf(cursor.getFloat(3)), // accuracy
								cursor.getString(5), // activity
								Float.valueOf(cursor.getFloat(4)) // battery
						);
						positionList.add(position);
					} while (cursor.moveToNext());
				}
				cursor.close();
			}
		} catch (SQLiteException e) {
			Log.e(TAG, "getLastPositions");

			return null;
		} finally {
			db.close();
		}

		return positionList;

	}

	/**
	 * Clean positions older than MAX_HISTORY_INTERVAL
	 */
	private void cleanOldPositions() {
		if (isRoleDefined()) {
			if (isChildDevice()) {
				TmlPosition lastPosition = getLastPosition();
				if (lastPosition == null) {
					return;
				}
				long lastTimestamp = lastPosition.getTimestamp();
				Long olderTimestampToKeep = lastTimestamp
						- MAX_HISTORY_INTERVAL * 3600 * 1000;
				dbDelete(TABLE_POSITIONS, TABLE_POSITIONS_COLUMN_TIMESTAMP
						+ " < ? ",
						new String[] { olderTimestampToKeep.toString() });
			} else {
				for (String user : getChildren()) {
					TmlPosition lastPosition = getLastPosition(user);
					if (lastPosition != null) {
						long lastTimestamp = lastPosition.getTimestamp();
						Long olderTimestampToKeep = lastTimestamp
								- MAX_HISTORY_INTERVAL * 3600 * 1000;
						dbDelete(TABLE_POSITIONS,
								TABLE_POSITIONS_COLUMN_NAME_USER
										+ " like ? AND "
										+ TABLE_POSITIONS_COLUMN_TIMESTAMP
										+ " < ? ", new String[] { user,
										olderTimestampToKeep.toString() });
					}

				}
			}
		}
	}

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------- SAFE ZONES METHODS -----------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Insert a safeZone in the database for the specified user. If a safeZone
	 * with the same serial number already exists it is overwritten
	 * 
	 * @param safeZone
	 */
	public void insertSafeZone(TmlSafeZone safeZone) {

		String username = safeZone.getChild().getName();

		if (username == null || safeZone == null) {
			Log.e(TAG, "username or safeZone is null");
			return;
		}

		ContentValues values = new ContentValues();

		/*
		 * Remove safezone with same serial if exists
		 */
		deleteSafeZone(safeZone);

		/*
		 * Insert safeZone serial number
		 */
		values.put(TABLE_SAFE_ZONES_COLUMN_NAME_USER, username);
		values.put(TABLE_SAFE_ZONES_COLUMN_SERIAL, safeZone.getSerial());
		dbInsert(TABLE_SAFE_ZONES, null, values,
				SQLiteDatabase.CONFLICT_REPLACE);

		/*
		 * Insert safeZone points
		 */

		for (int idx = 0; idx < safeZone.getPointList().size(); idx++) {
			LatLng point = safeZone.getPointList().get(idx);
			values.put(TABLE_POINTS_COLUMN_NAME_USER, username);
			values.put(TABLE_POINTS_COLUMN_SERIAL_SAFE_ZONE,
					safeZone.getSerial());
			values.put(TABLE_POINTS_COLUMN_SERIAL, idx);
			values.put(TABLE_POINTS_COLUMN_LAT, point.latitude);
			values.put(TABLE_POINTS_COLUMN_LNG, point.longitude);

			dbInsert(TABLE_POINTS, null, values,
					SQLiteDatabase.CONFLICT_REPLACE);
		}

	}

	/**
	 * Safe zones associated to a given child
	 * 
	 * @param child
	 * @return ArrayList of TmlSafeZone
	 */
	public synchronized ArrayList<TmlSafeZone> getSafeZones(TmlChild child) {

		if (!isParentDevice()) {
			return null;
		}

		ArrayList<TmlSafeZone> safeZones = new ArrayList<TmlSafeZone>();

		db = getReadableDatabase();

		try {

			// retrieve the serials
			Cursor cursorSafeZone = db.query(TABLE_SAFE_ZONES, new String[] {
					TABLE_SAFE_ZONES_COLUMN_SERIAL,
					TABLE_SAFE_ZONES_COLUMN_CREATED,
					TABLE_SAFE_ZONES_COLUMN_REMOVED },
					TABLE_SAFE_ZONES_COLUMN_NAME_USER + " LIKE ? ",
					new String[] { child.getName() }, null, null, null, null);

			// cycle trough the safe zones
			if (cursorSafeZone != null) {
				if (cursorSafeZone.moveToFirst()) {
					do {
						int serial = cursorSafeZone.getInt(0);
						boolean created = cursorSafeZone.getInt(1) > 0;
						boolean removed = cursorSafeZone.getInt(2) > 0;
						Cursor cursorPoint = db.query(
								TABLE_POINTS,
								new String[] { TABLE_POINTS_COLUMN_LAT,
										TABLE_POINTS_COLUMN_LNG },
								TABLE_POINTS_COLUMN_NAME_USER + " LIKE ? AND "
										+ TABLE_POINTS_COLUMN_SERIAL_SAFE_ZONE
										+ " = ? ",
								new String[] { child.getName(),
										Integer.toString(serial) }, null, null,
								TABLE_POINTS_COLUMN_SERIAL, null);

						ArrayList<LatLng> pointList = new ArrayList<LatLng>();
						// cycle trough the points of this safe zone
						if (cursorPoint != null) {
							if (cursorPoint.moveToFirst()) {
								do {
									pointList.add(new LatLng(cursorPoint
											.getDouble(0), cursorPoint
											.getDouble(1)));
								} while (cursorPoint.moveToNext());
							}
							cursorPoint.close();
						}

						safeZones.add(new TmlSafeZone(child, serial, pointList,
								created, removed));
					} while (cursorSafeZone.moveToNext());
				}

				cursorSafeZone.close();
			}
		} catch (SQLiteException e) {
			Log.e(TAG, "getSafeZones");

			return null;
		} finally {
			db.close();
		}

		/*
		 * SafeZones consistency check
		 */
		int idx = 0;
		while (idx < safeZones.size()) {
			if (safeZones.get(idx).getPointList().size() < 3) {
				deleteSafeZone(safeZones.get(idx));
				safeZones.remove(idx);
				Log.e(TAG, "Removing inconsistent SafeZone");
			} else {
				idx++;
			}
		}

		/*
		 * SafeZones cleaner
		 */
		if (safeZones.size() == 0) {
			cleanSafeZones(child.getName());
		}

		return safeZones;
	}

	/**
	 * Delete the specified safeZone in the database for the specified user.
	 * 
	 * @param safeZone
	 */
	public void deleteSafeZone(TmlSafeZone safeZone) {

		String username = safeZone.getChild().getName();

		if (username == null || safeZone == null) {
			return;
		}

		/*
		 * Delete points
		 */
		dbDelete(TABLE_POINTS, TABLE_POINTS_COLUMN_NAME_USER + " like ? AND "
				+ TABLE_POINTS_COLUMN_SERIAL_SAFE_ZONE + " == ?", new String[] {
				username, Integer.toString(safeZone.getSerial()) });

		/*
		 * Delete serial
		 */
		dbDelete(
				TABLE_SAFE_ZONES,
				TABLE_SAFE_ZONES_COLUMN_NAME_USER + " like ? AND "
						+ TABLE_SAFE_ZONES_COLUMN_SERIAL + " == ?",
				new String[] { username, Integer.toString(safeZone.getSerial()) });

	}

	/**
	 * Clean all points and safezones for the user
	 * 
	 * @param username
	 */
	public void cleanSafeZones(String username) {

		dbDelete(TABLE_POINTS, TABLE_POINTS_COLUMN_NAME_USER + " like ? ",
				new String[] { username });
		dbDelete(TABLE_SAFE_ZONES, TABLE_SAFE_ZONES_COLUMN_NAME_USER
				+ " like ? ", new String[] { username });

	}

	/**
	 * Set the toBeCreated flag for the given safeZone
	 * 
	 * @param tmlSafeZone
	 */
	public void setSafeZoneToBeCreated(TmlSafeZone safeZone) {
		setValueSafeZone(safeZone, TABLE_SAFE_ZONES_COLUMN_CREATED, true);
	}

	/**
	 * Reset the toBeCreated flag for the given safeZone
	 * 
	 * @param tmlSafeZone
	 */
	public void resetSafeZoneToBeCreated(TmlSafeZone safeZone) {
		setValueSafeZone(safeZone, TABLE_SAFE_ZONES_COLUMN_CREATED, false);
	}

	/**
	 * Set the toBeRemoved flag for the given safeZone
	 * 
	 * @param tmlSafeZone
	 */
	public void setSafeZoneToBeRemoved(TmlSafeZone safeZone) {
		setValueSafeZone(safeZone, TABLE_SAFE_ZONES_COLUMN_REMOVED, true);
	}

	/**
	 * Set the provided tag for the safeZone at value
	 * 
	 * @param safeZone
	 * @param tag
	 * @param value
	 */
	private void setValueSafeZone(TmlSafeZone safeZone, String tag,
			boolean value) {
		ContentValues content = new ContentValues();
		content.put(tag, value);
		dbUpdate(
				TABLE_SAFE_ZONES,
				content,
				TABLE_SAFE_ZONES_COLUMN_NAME_USER + " like ? AND "
						+ TABLE_SAFE_ZONES_COLUMN_SERIAL + " = ? ",
				new String[] { safeZone.getChild().getName(),
						Integer.toString(safeZone.getSerial()) },
				SQLiteDatabase.CONFLICT_REPLACE);
	}

}
