package me.noip.unklb.trackmyloved.controllers;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Patterns;

/**
 * Class used to handle account credentials in a single object
 * 
 * The Parcelable interface is implemented to pass this class as extra in
 * intents
 * 
 */
public class TmlAccount implements Parcelable {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */
	// private static final String TAG = "TmlAccount";

	/*
	 * ------------------------------------------------------------------------
	 * -------------------------- ATTRIBUTES ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * email address
	 */
	private String email;

	/**
	 * Clear text password
	 */
	private String pwd;

	/**
	 * SHA1 of the password
	 */
	private String pwdSHA1;

	/*
	 * ------------------------------------------------------------------------
	 * ----------------------------- METHODS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Default constructor
	 * 
	 * @param email
	 *            email address that must be checked by the user with the static
	 *            method TmlAccount.checkEmail(email)
	 * @param passw
	 *            clear text password
	 */
	public TmlAccount(String email, String passw) {
		this.email = email;
		this.pwd = passw;
		this.pwdSHA1 = TmlHash.SHA1(passw);
	}

	/**
	 * Special constructor
	 * 
	 * @param email
	 *            email address that must be checked by the user with the static
	 *            method TmlAccount.checkEmail(email)
	 */
	public TmlAccount(String email) {
		this.email = email;
	}

	/**
	 * Get the email address
	 * 
	 * @return
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Get the SHA1 of the password
	 * 
	 * @return
	 */
	public String getPwdSHA1() {
		return pwdSHA1;
	}

	/**
	 * Set the SHA password
	 * 
	 */
	public TmlAccount setPwdSha1(String pwdSha1) {
		this.pwdSHA1 = pwdSha1;
		return this;
	}

	/**
	 * Email pattern check
	 * 
	 * @param email
	 * @return
	 */
	public static boolean checkEmail(String email) {
		return Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

	/*
	 * ------------------------------------------------------------------------
	 * ------------------------- PARCELABLE INTERFACE -------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Parcelable.Creator interface needed to parcelize the class
	 */
	public static final Parcelable.Creator<TmlAccount> CREATOR = new Parcelable.Creator<TmlAccount>() {
		public TmlAccount createFromParcel(Parcel in) {
			return new TmlAccount(in);
		}

		public TmlAccount[] newArray(int size) {
			return new TmlAccount[size];
		}
	};

	/**
	 * Constructor used to build this class from a parcel representation
	 * 
	 * @param in
	 */
	private TmlAccount(Parcel in) {
		email = in.readString();
		pwd = in.readString();
		pwdSHA1 = in.readString();
	}

	/**
	 * Method used to build parcel representation of this class
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(email);
		dest.writeString(pwd);
		dest.writeString(pwdSHA1);
	}

	/**
	 * Not used
	 */
	@Override
	public int describeContents() {
		return 0;
	}

}