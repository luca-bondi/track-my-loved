package me.noip.unklb.trackmyloved.controllers;

import me.noip.unklb.trackmyloved.R;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Controller for user guide dialog and toast
 */
public class TmlUserGuide {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	// private static final String TAG = "TmlUserGuide";

	private static final int TOAST_OFFSET = 80;

	/**
	 * Database keys used to identify the description types
	 */
	private static final String[] descriptionKey = { "description_welcome",
			"description_no_child", "description_info_view",
			"description_history", "description_safe_zone",
			"description_add_safe_zone", "description_setup" };

	/**
	 * Database keys used to identify the toast types
	 */
	private static final String[] toastKey = { "toast_position_marker_tap",
			"toast_position_marker_tap_again", "toast_add_safezone_long_press",
			"toast_remove_safezone", "toast_add_safezone_vertex" };

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- ENUM TYPES ---------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Description type. Order must match the descriptionKey order and
	 * descriptionMax
	 */
	public enum DescriptionType {
		WELCOME, NO_CHILD, INFO_VIEW, HISTORY, SAFE_ZONE, ADD_SAFE_ZONE, SETUP
	};

	/**
	 * Toast type. Order must match the toastKey order and toastMax
	 */
	public enum ToastType {
		POSITION_MARKER_TAP, POSITION_MARKER_TAP_AGAIN, ADD_SAFEZONE_LONG_PRESS, REMOVE_SAFEZONE, ADD_SAFEZONE_VERTEX
	};

	/**
	 * The maximum number of times a description should be automatically shown.
	 * Some screen are shown every time a condition occurs. 0 means show always
	 */
	private static final int descriptionMax[] = { 1, 0, 1, 1, 1, 1, 0 };

	/**
	 * The maximum number of times a toast should be automatically shown. Some
	 * toast are shown every time a condition occurs. 0 means show always
	 */
	private static final int toastMax[] = { 1, 1, 1, 3, 1 };

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- INTERFACES ---------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Callback to be implemented by the hosting activity to get feedback about
	 * closed dialog and reset device button. Necessary if the activity wants to
	 * show the nochild description
	 */
	public interface UserGuideCallbacks {
		/**
		 * Called when dialog closed
		 * 
		 * @param dialog
		 */
		public void onUserGuideDialogClosed(DialogInterface dialog);

		/**
		 * Called when reset button pressed in no child dialog.
		 * 
		 * @param view
		 */
		public void onUserGuideResetCallback(View view);

	}

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- ATTRIBUTES ---------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Counter for the number of times a shadow screen has been automatically
	 * shown
	 */
	private int descriptionCount[] = { 0, 0, 0, 0, 0, 0, 0 };

	/**
	 * Counter for the number of times a toast has been automatically shown
	 */
	private int toastCount[] = { 0, 0, 0, 0, 0 };

	/**
	 * Instance of database controller
	 */
	private TmlDatabase db;

	/**
	 * Layout inflater for toast view inflation
	 */
	private LayoutInflater layoutInflater;

	/**
	 * Calling activity
	 */
	private ActionBarActivity activity;

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- METHODS ------------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Constructor
	 * 
	 * @param activity
	 * @param layoutInflater
	 * @param shadowLayout
	 */
	public TmlUserGuide(ActionBarActivity activity,
			LayoutInflater layoutInflater) {
		db = TmlDatabase.getInstance(activity);

		for (int idx = 0; idx < descriptionCount.length; idx++) {
			Integer temp = db.getLocalSettingInteger(descriptionKey[idx]);
			descriptionCount[idx] = temp == null ? 0 : temp;
		}

		for (int idx = 0; idx < toastCount.length; idx++) {
			Integer temp = db.getLocalSettingInteger(toastKey[idx]);
			toastCount[idx] = temp == null ? 0 : temp;
		}

		this.layoutInflater = layoutInflater;
		this.activity = activity;

	}

	/**
	 * Determine if a userGuide element should be automatically shown
	 * 
	 * @param type
	 * @return
	 */
	public boolean autoShow(DescriptionType type) {
		return descriptionMax[type.ordinal()] == 0
				| descriptionCount[type.ordinal()] < descriptionMax[type
						.ordinal()];
	}

	/**
	 * Determine if a userGuide element should be automatically shown
	 * 
	 * @param type
	 * @return
	 */
	public boolean autoShow(ToastType type) {
		return toastMax[type.ordinal()] == 0
				| toastCount[type.ordinal()] < toastMax[type.ordinal()];
	}

	/**
	 * Show the given toast type. toastCount is updated.
	 * 
	 * @param type
	 */
	public void showToast(ToastType type) {
		showToast(type, null);
	}

	/**
	 * Show the given toast type. toastCount is updated.
	 * 
	 * @param type
	 * @param value
	 */
	public void showToast(ToastType type, String value) {

		View layout = layoutInflater.inflate(R.layout.toast_layout,
				(ViewGroup) activity.findViewById(R.id.toast_layout_root));

		TextView text = (TextView) layout.findViewById(R.id.toast_text);

		Toast toast = new Toast(activity.getApplicationContext());
		toast.setGravity(Gravity.CENTER, 0, TOAST_OFFSET);
		toast.setDuration(Toast.LENGTH_LONG);

		switch (type) {
		case ADD_SAFEZONE_LONG_PRESS:
			text.setText(activity.getString(R.string.map_toast_addSafezone,
					value));
			break;
		case POSITION_MARKER_TAP:
			text.setText(activity.getString(R.string.map_toast_marker_tap));
			break;
		case POSITION_MARKER_TAP_AGAIN:
			text.setText(activity
					.getString(R.string.map_toast_marker_tap_again));
			break;
		case REMOVE_SAFEZONE:
			text.setText(activity.getString(R.string.map_toast_removeSafezone));
			break;
		case ADD_SAFEZONE_VERTEX:
			text.setText(activity
					.getString(R.string.map_toast_addSafezoneVertex));
			break;
		default:
			break;

		}

		toast.setView(layout);
		toast.show();

		/*
		 * Update toastCount
		 */
		toastCount[type.ordinal()]++;
		db.setLocalSetting(toastKey[type.ordinal()], toastCount[type.ordinal()]);

	}

	/**
	 * Show a description Dialog
	 * 
	 * @param type
	 */
	public AlertDialog showDescription(DescriptionType type) {

		LayoutInflater inflater = activity.getLayoutInflater();

		int layoutId;
		switch (type) {
		case HISTORY:
			layoutId = R.layout.user_guide_history;
			break;
		case INFO_VIEW:
			layoutId = R.layout.user_guide_info_view;
			break;
		case NO_CHILD:
			layoutId = R.layout.user_guide_no_child;
			break;
		case SAFE_ZONE:
			layoutId = R.layout.user_guide_safe_zone;
			break;
		case ADD_SAFE_ZONE:
			layoutId = R.layout.user_guide_add_safe_zone;
			break;
		case WELCOME:
			if (TmlChild.isParentDevice()) {
				layoutId = R.layout.user_guide_welcome;
			} else {
				layoutId = R.layout.user_guide_welcome_child;
			}
			break;
		case SETUP:
			layoutId = R.layout.user_guide_setup;
			break;
		default:
			return null;

		}

		/*
		 * Show the dialog
		 */
		View view = inflater.inflate(layoutId, null, false);

		AlertDialog.Builder builder = new AlertDialog.Builder(activity);

		/*
		 * Update shadowCount
		 */
		descriptionCount[type.ordinal()]++;
		db.setLocalSetting(descriptionKey[type.ordinal()],
				descriptionCount[type.ordinal()]);

		builder.setView(view);
		builder.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				try {
					UserGuideCallbacks hostActivity = (UserGuideCallbacks) activity;
					hostActivity.onUserGuideDialogClosed(dialog);
				} catch (ClassCastException e) {
				}
			}
		});
		if (type == DescriptionType.NO_CHILD) {
			builder.setCancelable(false);
		} else {
			builder.setNeutralButton(R.string.user_guide_neutral_button,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							try {
								UserGuideCallbacks hostActivity = (UserGuideCallbacks) activity;
								hostActivity.onUserGuideDialogClosed(dialog);
							} catch (ClassCastException e) {
							}
						}
					});
		}

		AlertDialog dialog = builder.show();

		return dialog;
	}

	/**
	 * Reset the counters for toast and description
	 */
	public void resetCounters() {
		for (int idx = 0; idx < descriptionCount.length; idx++) {
			db.setLocalSetting(descriptionKey[idx], 0);
			descriptionCount[idx] = 0;
		}

		for (int idx = 0; idx < toastCount.length; idx++) {
			db.setLocalSetting(toastKey[idx], 0);
			toastCount[idx] = 0;
		}
	}

}
