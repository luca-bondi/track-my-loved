package me.noip.unklb.trackmyloved.controllers;

import java.util.ArrayList;

import me.noip.unklb.trackmyloved.R;
import me.noip.unklb.trackmyloved.views.TmlChildView;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Marker controller
 */
public class TmlMarker {

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- CONSTANTS ----------------------------------
	 * ------------------------------------------------------------------------
	 */

	private static final String TAG = "TmlMarker";

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- ENUMS --------------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Marker types
	 */
	public enum MarkerType {
		POSITION, PLACE, CLUSTER_POSITION, CLUSTER_PLACE
	}

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- ATTRIBUTES ---------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Map instance this marker belongs to
	 */
	private static GoogleMap map;

	/**
	 * GoogleMap marker representing this TmlMarker
	 */
	private Marker marker;

	/**
	 * Type of the marker
	 */
	private MarkerType markerType;

	/**
	 * Positions that belong to the marker
	 */
	private ArrayList<TmlPosition> positionList = new ArrayList<TmlPosition>();

	/**
	 * Places that belong to the marker
	 */
	private ArrayList<TmlPlace> placeList = new ArrayList<TmlPlace>();

	/**
	 * Place represented by this marker. Null if this is not a PLACE marker
	 */
	private TmlPlace place;

	/**
	 * Removed marker
	 */
	private boolean removed;

	/**
	 * TmlChildView this marker belongs to
	 */
	private TmlChildView childView;

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- METHODS ------------------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Single position constructor
	 * 
	 * @param childView
	 * @param markerOptions
	 * @param markerType
	 * @param position
	 */
	private TmlMarker(TmlChildView childView, MarkerOptions markerOptions,
			MarkerType markerType, TmlPosition position) {

		this.positionList.add(position);
		this.childView = childView;
		TmlMarker.map = childView.getMap();
		this.markerType = markerType;
		this.marker = map.addMarker(markerOptions);
		drawIcon();

	}

	/**
	 * Cluster constructor
	 * 
	 * @param childView
	 * @param markerOptions
	 * @param markerType
	 * @param positionList
	 */
	private TmlMarker(TmlChildView childView, MarkerOptions markerOptions,
			MarkerType markerType, ArrayList<TmlPosition> positionList,
			ArrayList<TmlPlace> placeList) {

		this.childView = childView;

		TmlMarker.map = childView.getMap();

		this.positionList.addAll(positionList);
		this.markerType = markerType;
		this.marker = map.addMarker(markerOptions);

		this.placeList.addAll(placeList);

		drawIcon();
	}

	/**
	 * Place constructor
	 * 
	 * @param childView
	 * @param markerOptions
	 * @param markerType
	 * @param positionList
	 */
	private TmlMarker(TmlChildView childView, MarkerOptions markerOptions,
			MarkerType markerType, TmlPlace place) {

		this.positionList.add(place.getPosition());
		this.childView = childView;
		TmlMarker.map = childView.getMap();
		this.place = place;
		this.markerType = markerType;
		this.marker = map.addMarker(markerOptions);
		drawIcon();
	}

	/**
	 * List of position inside this cluster marker. Used to perform zoom in
	 * MapActivity
	 * 
	 * @return
	 */
	public ArrayList<TmlPosition> getPositionList() {
		return positionList;
	}

	/**
	 * The list of places contained in this place cluster marker
	 * 
	 * @return
	 */
	private ArrayList<TmlPlace> getPlaceList() {
		return placeList;
	}

	/**
	 * The place represented by this marker.
	 * 
	 * @return null if this is a cluster or a position marker
	 */
	public TmlPlace getPlace() {
		return place;
	}

	/**
	 * Based on current highlight state and number of elements (if cluster) set
	 * the icon
	 */
	private void drawIcon() {

		if (removed) {
			return;
		}

		switch (markerType) {
		case CLUSTER_PLACE:
			if (getHighlighted()) {
				marker.setIcon(resourcesToBitmapDescriptor(
						TmlChild.getContext(),
						R.drawable.marker_place_highlight,
						getClusterSizeResource(MarkerType.CLUSTER_PLACE,
								this.positionList.size(), true)));
			} else {
				marker.setIcon(resourcesToBitmapDescriptor(
						TmlChild.getContext(),
						R.drawable.marker_place,
						getClusterSizeResource(MarkerType.CLUSTER_PLACE,
								this.positionList.size(), false)));
			}
			break;
		case CLUSTER_POSITION:
			marker.setIcon(resourcesToBitmapDescriptor(
					TmlChild.getContext(),
					R.drawable.marker_position_multiple,
					getClusterSizeResource(MarkerType.CLUSTER_POSITION,
							positionList.size(), false)));
			break;
		case PLACE:
			if (getHighlighted()) {
				marker.setIcon(BitmapDescriptorFactory
						.fromResource(R.drawable.marker_place_highlight));
			} else {
				marker.setIcon(BitmapDescriptorFactory
						.fromResource(R.drawable.marker_place));
			}
			break;
		case POSITION:
			marker.setIcon(resourcesToBitmapDescriptor(TmlChild.getContext(),
					childView.getColor().getMarkerBackgroundResource(),
					getNameFirstLetterBitmap(childView.getChild().getName())));
			break;
		default:
			break;

		}
	}

	/**
	 * Get highlight status of the marker
	 * 
	 * @return
	 */
	private boolean getHighlighted() {
		if (place != null) {
			return place.getHighlighted();
		}
		if (placeList != null) {
			for (TmlPlace place : placeList) {
				if (place.getHighlighted()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Set this marker as belonging to the given cluster. If this is already a
	 * cluster marker all the places thath belongto this cluster are assigned to
	 * the new one
	 * 
	 * @param newCluster
	 */
	private void setClusterMarker(TmlMarker newCluster) {
		if (place != null) {
			place.setClusterMarker(newCluster);
		}
		if (placeList != null) {
			for (TmlPlace placeTemp : placeList) {
				placeTemp.setClusterMarker(newCluster);
			}
		}
	}

	/**
	 * Set the visibility of the marker
	 * 
	 * @param visibility
	 */
	public void setVisible(boolean visibility) {
		this.marker.setVisible(visibility);
	}

	/**
	 * Get the marker visibility on the map
	 * 
	 * @return
	 */
	public boolean isVisible() {
		return this.marker.isVisible();
	}

	/**
	 * Remove the marker from the map
	 */
	public void remove() {
		removed = true;
		this.marker.remove();
	}

	/**
	 * Get the marker position
	 * 
	 * @return
	 */
	public LatLng getPosition() {
		return this.marker.getPosition();
	}

	/**
	 * Return the Marker
	 * 
	 * @return
	 */
	public Marker getMarker() {
		return this.marker;
	}

	/**
	 * Set the highlighting of the current marker. If not a place marker no
	 * action is performed
	 * 
	 * @param highlight
	 */
	public void setHighlighted(boolean highlight) {

		if (place != null) {
			place.setHighlighted(highlight);
			drawIcon();
			if (place.getClusterMarker() != null) {
				place.getClusterMarker().drawIcon();
			}
		}

	}

	/**
	 * Marker type
	 * 
	 * @return
	 */
	public MarkerType getType() {
		return markerType;
	}

	/*
	 * ------------------------------------------------------------------------
	 * --------------------------- STATIC METHODS -----------------------------
	 * ------------------------------------------------------------------------
	 */

	/**
	 * Last position marker for the given child on the lastFilteredPosition
	 * 
	 * @param childView
	 * @return
	 */
	public static TmlMarker getLastPositionMarker(TmlChildView childView) {

		TmlPosition markerPosition = childView.getChild()
				.getLastFilteredPosition();

		MarkerOptions markerOptions = new MarkerOptions()
				.position(markerPosition.getCoordinates());

		return new TmlMarker(childView, markerOptions, MarkerType.POSITION,
				markerPosition);

	}

	/**
	 * Place markers for the given child
	 * 
	 * @param childView
	 * @return
	 */
	public static ArrayList<TmlMarker> getPlaceMarkerList(TmlChildView childView) {

		ArrayList<TmlMarker> markerList = new ArrayList<TmlMarker>();

		MarkerOptions markerOptions = new MarkerOptions().anchor(0.5f, 0.5f);

		ArrayList<TmlPlace> placeList = childView.getChild()
				.getPlaceList();

		/*
		 * Create place points
		 */
		for (int idx = 0; idx < placeList.size(); idx++) {

			markerOptions.position(placeList.get(idx).getPosition()
					.getCoordinates());
			TmlMarker newMarker = new TmlMarker(childView, markerOptions,
					MarkerType.PLACE, placeList.get(idx));
			placeList.get(idx).setMarker(newMarker);
			markerList.add(newMarker);
		}

		return markerList;

	}

	/**
	 * Overlay two resources into a single bitmap descriptor that can be used as
	 * icon in a marker
	 * 
	 * @param context
	 * @param backgroundResource
	 *            background resource
	 * @param frontResource
	 *            front resource
	 * @return
	 */
	private static BitmapDescriptor resourcesToBitmapDescriptor(
			Context context, int backgroundResource, int frontResource) {

		Resources res = context.getResources();

		Bitmap b1 = BitmapFactory.decodeResource(res, backgroundResource);
		Bitmap b2 = BitmapFactory.decodeResource(res, frontResource);

		int width = Math.max(b1.getWidth(), b2.getWidth());
		int height = Math.max(b1.getHeight(), b2.getHeight());

		Bitmap resultBitmap = Bitmap.createBitmap(width, height,
				Bitmap.Config.ARGB_8888);

		Canvas comboImage = new Canvas(resultBitmap);

		comboImage.drawBitmap(b1, 0f, 0f, null);
		comboImage.drawBitmap(b2, 0f, 0f, null);

		return BitmapDescriptorFactory.fromBitmap(resultBitmap);
	}

	/**
	 * The resource of lastPositionMarker letter (first letter of child name)
	 * 
	 * @param name
	 * @return
	 */
	private static int getNameFirstLetterBitmap(String name) {

		/*
		 * Retrieve the first letter. If none found set firstLetter to ' '
		 */
		int firstLetterIdx = 0;
		char firstLetter = name.charAt(0);
		while (firstLetterIdx < name.length()
				&& !((firstLetter >= 'a' && firstLetter <= 'z') || (firstLetter >= 'A' && firstLetter <= 'Z'))) {
			firstLetterIdx++;
			if (firstLetterIdx == name.length()) {
				firstLetter = ' ';
			} else {
				firstLetter = name.charAt(firstLetterIdx);
			}
		}

		/* Convert to lowercase */
		firstLetter = Character.toLowerCase(firstLetter);

		/*
		 * Return the bitmap corresponding to the first letter of the name
		 */
		switch (firstLetter) {
		case 'a':
			return R.drawable.font_position_a;
		case 'b':
			return R.drawable.font_position_b;
		case 'c':
			return R.drawable.font_position_c;
		case 'd':
			return R.drawable.font_position_d;
		case 'e':
			return R.drawable.font_position_e;
		case 'f':
			return R.drawable.font_position_f;
		case 'g':
			return R.drawable.font_position_g;
		case 'h':
			return R.drawable.font_position_h;
		case 'i':
			return R.drawable.font_position_i;
		case 'j':
			return R.drawable.font_position_j;
		case 'k':
			return R.drawable.font_position_k;
		case 'l':
			return R.drawable.font_position_l;
		case 'm':
			return R.drawable.font_position_m;
		case 'n':
			return R.drawable.font_position_n;
		case 'o':
			return R.drawable.font_position_o;
		case 'p':
			return R.drawable.font_position_p;
		case 'q':
			return R.drawable.font_position_q;
		case 'r':
			return R.drawable.font_position_r;
		case 's':
			return R.drawable.font_position_s;
		case 't':
			return R.drawable.font_position_t;
		case 'u':
			return R.drawable.font_position_u;
		case 'v':
			return R.drawable.font_position_v;
		case 'w':
			return R.drawable.font_position_w;
		case 'x':
			return R.drawable.font_position_x;
		case 'y':
			return R.drawable.font_position_y;
		case 'z':
			return R.drawable.font_position_z;
		default:
			return R.drawable.font_position_null;
		}

	}

	/**
	 * Given the cluster size returns the resource icon with the number of
	 * elements
	 * 
	 * @param markerType
	 * @param clusterSize
	 * @return
	 */
	private static int getClusterSizeResource(MarkerType markerType,
			int clusterSize, boolean highlighted) {

		if (markerType == MarkerType.CLUSTER_POSITION) {
			switch (clusterSize) {
			case 0:
				Log.e(TAG, "Cluster size cannot be 0");
				return 0;
			case 1:
				Log.e(TAG, "A cluster of size 1 doesn't make sense");
				return 0;
			case 2:
				return R.drawable.font_position_2;
			case 3:
				return R.drawable.font_position_3;
			case 4:
				return R.drawable.font_position_4;
			case 5:
				return R.drawable.font_position_5;
			default:
				return R.drawable.font_position_more;
			}
		} else if (markerType == MarkerType.CLUSTER_PLACE) {
			switch (clusterSize) {
			case 0:
				Log.e(TAG, "Cluster size cannot be 0");
				return 0;
			case 1:
				Log.e(TAG, "A cluster of size 1 doesn't make sense");
				return 0;
			case 2:
				if (highlighted) {
					return R.drawable.font_place_highlight_2;
				}
				return R.drawable.font_place_2;
			case 3:
				if (highlighted) {
					return R.drawable.font_place_highlight_3;
				}
				return R.drawable.font_place_3;
			case 4:
				if (highlighted) {
					return R.drawable.font_place_highlight_4;
				}
				return R.drawable.font_place_4;
			case 5:
				if (highlighted) {
					return R.drawable.font_place_highlight_5;
				}
				return R.drawable.font_place_5;
			default:
				if (highlighted) {
					return R.drawable.font_place_highlight_more;
				}
				return R.drawable.font_place_more;
			}
		}
		return clusterSize;
	}

	/**
	 * Clustering method. Given a shownMarkerList returns the clustered
	 * shownMarkerList
	 * 
	 * @param shownMarkerList
	 * @param displayDensity
	 * @return
	 */
	public static ArrayList<TmlMarker> clusterize(
			ArrayList<TmlMarker> shownMarkerList, float displayDensity) {

		boolean overlap = true;

		while (overlap) {
			overlap = false;

			int idx1 = 0;
			while (idx1 < shownMarkerList.size() - 1) {
				TmlMarker m1 = shownMarkerList.get(idx1);
				int idx2 = idx1 + 1;
				boolean m1overlap = false;
				while (idx2 < shownMarkerList.size()) {
					TmlMarker m2 = shownMarkerList.get(idx2);
					if (areMarkersOverlapping(m1, m2, displayDensity)) {
						TmlMarker newCluster = createCluster(m1, m2,
								displayDensity);
						shownMarkerList.add(newCluster);
						m1.setVisible(false);
						m1.setClusterMarker(newCluster);

						m2.setVisible(false);
						m2.setClusterMarker(newCluster);

						newCluster.setHighlighted(m1.getHighlighted()
								| m2.getHighlighted());

						shownMarkerList.remove(m1);
						shownMarkerList.remove(m2);
						overlap = true;
						m1overlap = true;
						break;
					} else {
						idx2++;
					}
				}
				if (!m1overlap) {
					idx1++;
				}
			}
		}

		return shownMarkerList;
	}

	/**
	 * Create a cluster from the given markers
	 * 
	 * @param m1
	 * @param m2
	 * @param displayDensity
	 * @return
	 */
	private static TmlMarker createCluster(TmlMarker m1, TmlMarker m2,
			float displayDensity) {

		MarkerType markerType = MarkerType.CLUSTER_PLACE;
		MarkerOptions markerOptions = new MarkerOptions();

		markerOptions.position(middleMarkerPosition(m1, m2));

		if ((m1.markerType == MarkerType.POSITION || m1.markerType == MarkerType.CLUSTER_POSITION)
				&& (m2.markerType == MarkerType.POSITION || m2.markerType == MarkerType.CLUSTER_POSITION)) {
			markerType = MarkerType.CLUSTER_POSITION;
		}

		switch (markerType) {
		case CLUSTER_PLACE:
			markerOptions.anchor(0.5f, 0.5f);
			break;
		case CLUSTER_POSITION:
			markerOptions.anchor(0.5f, 1f);
			break;
		case PLACE:
			break;
		case POSITION:
			break;
		default:
			break;

		}

		ArrayList<TmlPosition> positionList = new ArrayList<TmlPosition>();
		positionList.addAll(m1.getPositionList());
		positionList.addAll(m2.getPositionList());

		ArrayList<TmlPlace> placeList = new ArrayList<TmlPlace>();
		if (m1.getPlace() != null) {
			placeList.add(m1.getPlace());
		}
		if (m1.getPlaceList() != null) {
			placeList.addAll(m1.getPlaceList());
		}
		if (m2.getPlace() != null) {
			placeList.add(m2.getPlace());
		}
		if (m2.getPlaceList() != null) {
			placeList.addAll(m2.getPlaceList());
		}

		return new TmlMarker(m1.childView, markerOptions, markerType,
				positionList, placeList);
	}

	/**
	 * Middle position between two markers
	 * 
	 * @param m1
	 * @param m2
	 * @return
	 */
	private static LatLng middleMarkerPosition(TmlMarker m1, TmlMarker m2) {
		double lat = (m1.getPosition().latitude + m2.getPosition().latitude) / 2;
		double lng = (m1.getPosition().longitude + m2.getPosition().longitude) / 2;
		return new LatLng(lat, lng);
	}

	/**
	 * Determine if two markers are overlapping. Position markers overlap only
	 * with position markers (or cluster of position marker). place parker
	 * overlap only with place markers (or cluster of place marker)
	 * 
	 * @param m1
	 * @param m2
	 * @param displayDensity
	 *            in px/dp
	 * @return
	 */
	private static boolean areMarkersOverlapping(TmlMarker m1, TmlMarker m2,
			float displayDensity) {

		if ((m1.getType() == MarkerType.POSITION || m1.getType() == MarkerType.CLUSTER_POSITION)
				&& (m2.getType() == MarkerType.PLACE || m2.getType() == MarkerType.CLUSTER_PLACE)) {
			return false;
		}
		if ((m2.getType() == MarkerType.POSITION || m2.getType() == MarkerType.CLUSTER_POSITION)
				&& (m1.getType() == MarkerType.PLACE || m1.getType() == MarkerType.CLUSTER_PLACE)) {
			return false;
		}

		Rect m1Rect = getMarkerRect(m1, displayDensity);
		Rect m2Rect = getMarkerRect(m2, displayDensity);

		return Rect.intersects(m1Rect, m2Rect);
	}

	/**
	 * Returns the Rect containing the given marker localized in the correct
	 * point of the map
	 * 
	 * @param marker
	 * @param displayDensity
	 *            in px/dp
	 * @return
	 */
	private static Rect getMarkerRect(TmlMarker marker, float displayDensity) {

		/*
		 * Rect describing the markers area. (0,0) is the anchor point.
		 * Dimensions are in px
		 */
		final Rect lastPositionRect = new Rect((int) (-13 * displayDensity),
				(int) (-41 * displayDensity), (int) (13 * displayDensity),
				(int) (0 * displayDensity));

		final Rect placeRect = new Rect((int) (-17 * displayDensity),
				(int) (-17 * displayDensity), (int) (17 * displayDensity),
				(int) (17 * displayDensity));

		final Rect placeHighlightedRect = new Rect(
				(int) (-19 * displayDensity), (int) (-19 * displayDensity),
				(int) (19 * displayDensity), (int) (19 * displayDensity));

		Rect markerRect;

		switch (marker.markerType) {
		case POSITION:
			markerRect = lastPositionRect;
			break;
		case CLUSTER_POSITION:
			markerRect = lastPositionRect;
			break;
		case PLACE:
			if (marker.getHighlighted()) {
				markerRect = placeHighlightedRect;
			} else {
				markerRect = placeRect;
			}
			break;
		case CLUSTER_PLACE:
			if (marker.getHighlighted()) {
				markerRect = placeHighlightedRect;
			} else {
				markerRect = placeRect;
			}
			break;
		default:
			markerRect = placeRect;
		}

		/*
		 * Determine lastPositionMarker position in pixel (x is left to right, y
		 * is top to bottom)
		 */
		Projection projection = map.getProjection();
		Point position = projection.toScreenLocation(marker.getPosition());

		/*
		 * Offset the base lastPositionMarker rectangle
		 */
		Rect outRect = new Rect(markerRect);
		outRect.offsetTo(position.x, position.y);

		return outRect;

	}

}
